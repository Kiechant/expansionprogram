cmake_minimum_required(VERSION 3.15)
project(ExpansionProgram VERSION 1.0.0)

# Required packages.

find_package(Catch2 REQUIRED)
find_package(nlohmann_json REQUIRED)

# Compiler and linker flags for debug and release editions.

set(CXX_FLAGS_COMMON -std=c++20 -Wall -Wextra)
set(CXX_FLAGS_DEBUG ${CXX_FLAGS_COMMON} -O0 -g --debug
    -fprofile-instr-generate -fcoverage-mapping
    -fsanitize=address,undefined)
set(CXX_FLAGS_RELEASE ${CXX_FLAGS_COMMON} -O3)

# Base flags exported as LDFLAGS in the calling file.
set(LD_FLAGS_DEBUG -fprofile-instr-generate -fcoverage-mapping
    -fsanitize=address,undefined)

# Adds library for base debug program ignoring main.cpp,
# to link against the debug and test executables.

file(GLOB_RECURSE SRCS "src/*.cpp")
list(REMOVE_ITEM SRCS "${CMAKE_CURRENT_SOURCE_DIR}/src/main.cpp")
file(GLOB_RECURSE HDRS "src/*.hpp" "src/*.tpp")
add_library(${PROJECT_NAME}DebugLib STATIC ${SRCS})

# Links debug executable to library.

add_executable(${PROJECT_NAME}Debug "src/main.cpp")
target_link_libraries(${PROJECT_NAME}Debug ${PROJECT_NAME}DebugLib)

# Collates test source files and links test executable to library.

file(GLOB_RECURSE TEST_SRCS "tests/*.cpp")
add_executable(${PROJECT_NAME}Test ${TEST_SRCS})
target_link_libraries(${PROJECT_NAME}Test ${PROJECT_NAME}DebugLib)

# Creates release project executable.

add_executable(${PROJECT_NAME}Release ${SRCS} "src/main.cpp")

# Compiler and linker options.

target_compile_options(${PROJECT_NAME}DebugLib PUBLIC ${CXX_FLAGS_DEBUG})
target_link_options(${PROJECT_NAME}DebugLib PUBLIC ${LD_FLAGS_DEBUG})
target_compile_options(${PROJECT_NAME}Release PRIVATE ${CXX_FLAGS_RELEASE})

# Function lists the subdirectories including and under parent.

function(list_subdirs subdir_list parent)
    file(GLOB_RECURSE children LIST_DIRECTORIES true "${parent}/*")
    set(_subdir_list "${CMAKE_SOURCE_DIR}/${parent}")
    foreach(child ${children})
        if(IS_DIRECTORY "${child}")
            list(APPEND _subdir_list "${child}")
        endif()
    endforeach()
    set("${subdir_list}" "${_subdir_list}" PARENT_SCOPE)
endfunction()

# Includes subdirectories under 'src'.

list_subdirs(SUBDIRS "src")
target_include_directories(${PROJECT_NAME}DebugLib PUBLIC ${SUBDIRS})
target_include_directories(${PROJECT_NAME}Release PRIVATE ${SUBDIRS})

# Includes subdirectories under 'tests' for tests only.

list_subdirs(TEST_SUBDIRS "tests")
target_include_directories(${PROJECT_NAME}Test PRIVATE ${TEST_SUBDIRS})

# Links debug library with JSON parsing library.

target_link_libraries(${PROJECT_NAME}DebugLib PRIVATE nlohmann_json::nlohmann_json)

# Links test executable with Catch2.

target_link_libraries(${PROJECT_NAME}Test Catch2::Catch2)

# Creates custom targets for running and building executables.

add_custom_target(run-release
    COMMAND ./${PROJECT_NAME}Release
    DEPENDS ${PROJECT_NAME}Release)

add_custom_target(run-debug
    COMMAND ./${PROJECT_NAME}Debug
    DEPENDS ${PROJECT_NAME}Debug)

add_custom_target(run-tests
    COMMAND ./${PROJECT_NAME}Test
    DEPENDS ${PROJECT_NAME}Test)

# Creates custom targets for generating coverage information.

function(add_cov_targets target_prefix exe_suffix)
    set(EXE_NAME "${PROJECT_NAME}${exe_suffix}")

    # Generates coverage data from a single execution.
    add_custom_target(${target_prefix}-cov-preprocess
        COMMAND LLVM_PROFILE_FILE=${EXE_NAME}.profraw ./${EXE_NAME}
        COMMAND llvm-profdata merge -sparse ${EXE_NAME}.profraw -o ${EXE_NAME}.profdata
        DEPENDS ${EXE_NAME})

    # Displays the coverage data for each individual file in the program.
    add_custom_target(${target_prefix}-cov-show
        COMMAND llvm-cov show ${EXE_NAME} -instr-profile=${EXE_NAME}.profdata
        DEPENDS ${target_prefix}-cov-preprocess)

    # Summarises the code coverage of the whole program.
    add_custom_target(${target_prefix}-cov-report
        COMMAND llvm-cov report ${EXE_NAME} -instr-profile=${EXE_NAME}.profdata
        DEPENDS ${target_prefix}-cov-preprocess)

    # Generates a html structure with whole-program and per-file coverage data. 
    add_custom_target(${target_prefix}-cov
        COMMAND python3 ../remove_coverage_directory.py ${EXE_NAME}Coverage
        COMMAND llvm-cov show ${EXE_NAME} -instr-profile=${EXE_NAME}.profdata
            -show-line-counts-or-regions -output-dir=${EXE_NAME}Coverage -format=html
        COMMAND python3 ../remove_coverage_prefix.py ${EXE_NAME}Coverage
        DEPENDS ${target_prefix}-cov-preprocess)
endfunction()

add_cov_targets("tests" "Test")
add_cov_targets("debug" "Debug")
