import argparse
from datetime import datetime
import re
import os

parser = argparse.ArgumentParser()
parser.add_argument("contentType", choices=["file", "class"])
parser.add_argument("componentType", choices=["cpp", "hpp", "tpp"])
parser.add_argument("destDir")
parser.add_argument("destFileName")
args = parser.parse_args()

fileNameOfContentType = {
	"file": "BasicFile",
	"class": "Class"
}
skeletonFileName = fileNameOfContentType[args.contentType]

fileExtOfComponentType = {
	"cpp": ".cpp",
	"hpp": ".hpp",
	"tpp": ".tpp"
}
skeletonFileExt = fileExtOfComponentType[args.componentType]

skeletonFilePath = os.path.join("./skeletons", skeletonFileName + skeletonFileExt)
assert (os.path.exists(skeletonFilePath) and os.path.isfile(skeletonFilePath)), "Skeleton file does not exist: " + str(skeletonFilePath)

destFilePath = os.path.join(args.destDir, args.destFileName + skeletonFileExt)
assert (os.path.exists(args.destDir) and os.path.isdir(args.destDir)), "Destination directory does not exist: " + str(args.destDir)
assert (not os.path.exists(destFilePath)), "Destination file already exists: " + str(destFilePath)

dateStr = datetime.today().strftime("%-d/%-m/%Y")

def camelCaseToUpperCase(word):
	matcher = re.compile("(?<=[a-z0-9])([A-Z])")
	lower = 0
	newWordParts = []
	for match in matcher.finditer(word):
		newWordParts.append(word[lower : match.start()].upper())
		lower = match.start()
	newWordParts.append(word[lower :].upper())
	return "_".join(newWordParts)

destFileSymbol = camelCaseToUpperCase(args.destFileName)

skeletonFileContents = ""
with open(skeletonFilePath, "r") as skeletonFile:
	skeletonFileContents = skeletonFile.read()

def replaceSkeletonSymbol(match):
	if match.group() == "DATE":
		return dateStr
	elif match.group() == "FILE_NAME":
		return args.destFileName
	elif match.group() == "FILE_SYMBOL":
		return destFileSymbol

matcher = re.compile("DATE|FILE_NAME|FILE_SYMBOL")
destFileContents = matcher.sub(replaceSkeletonSymbol, skeletonFileContents)

with open(destFilePath, "w") as destFile:
	destFile.write(destFileContents)
