#include "../../src/Expansion/UserFunction.tpp"

using namespace std;

extern "C" EpValue func(EpArgumentList args)
{
	int value0 = args.getNext<EpValueCategory::Int>();
	float value1 = args.getNext<EpValueCategory::Float>();
	string value2 = args.getNext<EpValueCategory::String>();
	string ret = to_string(value0) + ", " + to_string(value1) + ", " + value2;
	return EpValue::fromValueWithCategory<EpValueCategory::String>(ret);
}