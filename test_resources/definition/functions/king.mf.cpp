#include "../../src/Expansion/UserFunction.tpp"

using namespace std;

extern "C" EpValue announce_law(EpArgumentList args)
{
	string personality = args.getNext<EpValueCategory::String>();
	unsigned wisdom = args.getNext<EpValueCategory::Unsigned>();
	unsigned boredom = args.getNext<EpValueCategory::Unsigned>();

	if (personality == "insane")
	{
		if (wisdom == 10 /* out of 100 */ && boredom == 3 /* out of 3 */)
		{
			return makeEpValue<EpValueCategory::String>("Kill every second child!");
		}
	}

	return makeEpValue<EpValueCategory::String>("I don't know.");
}
