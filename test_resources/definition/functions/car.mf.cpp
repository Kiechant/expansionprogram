#include "../../src/Expansion/UserFunction.tpp"

extern "C" EpValue calc_speed(EpArgumentList args)
{
	double weight = args.getNext<EpValueCategory::Float>();
	double horsepower = args.getNext<EpValueCategory::Float>();
	double friction = args.getNext<EpValueCategory::Float>();
	double speed = horsepower / (weight * friction);
	return makeEpValue<EpValueCategory::Float>(speed);
}

extern "C"
{
EpValue sum_component_weights(EpArgumentList args)
{
	double body_wt = args.getNext<EpValueCategory::Float>();
	double engine_wt = args.getNext<EpValueCategory::Float>();
	double wheel_wt = args.getNext<EpValueCategory::Float>();
	double sum = body_wt + engine_wt + wheel_wt;
	return makeEpValue<EpValueCategory::Float>(sum);
}
}
