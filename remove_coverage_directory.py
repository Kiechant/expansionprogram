import argparse
import shutil
import os

parser = argparse.ArgumentParser()
parser.add_argument("coverage_dir_name")
args = parser.parse_args()

coverage_dir = os.path.join(os.getcwd(), args.coverage_dir_name)

if os.path.isdir(coverage_dir):
	shutil.rmtree(coverage_dir)
