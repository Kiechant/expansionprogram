#include <catch2/catch.hpp>

#include <exception>

using namespace std;

string getCurrentExceptionMessage(int level)
{
	string msg = "";
	try
	{
		throw;
	}
	catch (exception const& e)
	{
		msg = string(level * 2, ' ') + e.what();
	}
	return msg;
}

string getCurrentNestedExceptionMessage(int level)
{
	string msg = "";

	try
	{
		throw;
	}
	catch(...)
	{
		msg += getCurrentExceptionMessage(level) + "\n";
	}

	try
	{
		throw;
	}
	catch (const std::nested_exception& nested)
	{
		try
		{
			nested.rethrow_nested();
		}
		catch(const std::exception& e)
		{
			msg += getCurrentNestedExceptionMessage(level + 1) + "\n";
		}
	}
	catch (...)
	{
		// No further nested exception. Ignores.
	}
	return msg;
}

CATCH_TRANSLATE_EXCEPTION(nested_exception const&)
{
	return getCurrentNestedExceptionMessage(0);
}
