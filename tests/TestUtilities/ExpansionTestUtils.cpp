//
//  ExpansionTestUtils.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 30/7/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "ExpansionTestUtils.hpp"

#include "Cache.tpp"
#include "CacheMap.tpp"
#include "Expander.hpp"
#include "ExpansionContext.hpp"
#include "ExpansionContextItem.hpp"
#include "ExpansionDefinition.hpp"
#include "ExpanderOptions.hpp"
#include "MemberCombination.hpp"
#include "TemplateTree.hpp"
#include "Tree.tpp"

using namespace std;
namespace fs = filesystem;


/** Staitc Member Instantiation **/

unique_ptr<ExpanderOptions> ExpansionTestUtils::cachedOptions;
unique_ptr<ExpansionDefinition> ExpansionTestUtils::cachedDefinition;
unordered_map<string, unique_ptr<TemplateTree>> ExpansionTestUtils::cachedTemplateNameTree;
unordered_map<string, unique_ptr<TemplateTree>> ExpansionTestUtils::cachedTemplateContentsTree;
unordered_map<string, unique_ptr<ExpansionContext>> ExpansionTestUtils::cachedContext;
unordered_map<string, unique_ptr<MemberCombination>> ExpansionTestUtils::cachedMemberCombo;
unique_ptr<Expander> ExpansionTestUtils::cachedExpander;

const fs::path ExpansionTestUtils::basicName = "car@my_<car:0@name>_description.template";
const fs::path ExpansionTestUtils::multiGroupSpecName = "car(wheels[0].trim,engine),car_engine,person@a_<car@id>_with_components.template";
const fs::path ExpansionTestUtils::multiNestedSpecName = "driver(garage[0].car(colour,wheels[0].trim,engine))@a_<driver>_with_many_garages.template";
const fs::path ExpansionTestUtils::multiPropertyName = "anything@<a.b.c>_<x@a>_<5@a.b[0].c.d[2]>_<y:2@a[3]>.template";
const fs::path ExpansionTestUtils::validMultiGroupName = "car(wheels[0].trim,engine),car_engine,person@a_<car@id>_with_<person@name>_<car@wheels[0].trim>_<car_engine@name>_<car@engine>.template";


/** Directories **/

fs::path ExpansionTestUtils::getTestResDir()
{
	return "../test_resources";
}

fs::path ExpansionTestUtils::getDefnDir()
{
	return getTestResDir() / "definition";
}

fs::path ExpansionTestUtils::getDefnDummyDir()
{
	return getTestResDir() / "definition_dummy";
}

fs::path ExpansionTestUtils::getDefnTestDir()
{
	return getTestResDir() / "definition_tests";
}

fs::path ExpansionTestUtils::getTmplDir()
{
	return getTestResDir() / "templates";
}

fs::path ExpansionTestUtils::getActualExpnDir()
{
	return getTestResDir() / "actual_expansions";
}

fs::path ExpansionTestUtils::getExpectedExpnDir()
{
	return getTestResDir() / "expected_expansions";
}


/** Template Names **/

fs::path ExpansionTestUtils::getBasicName()
{
	return basicName;
}

fs::path ExpansionTestUtils::getMultiGroupSpecName()
{
	return multiGroupSpecName;
}

fs::path ExpansionTestUtils::getMultiNestedSpecName()
{
	return multiNestedSpecName;
}

fs::path ExpansionTestUtils::getMultiPropertyName()
{
	return multiPropertyName;
}

fs::path ExpansionTestUtils::getValidMultiGroupName()
{
	return validMultiGroupName;
}



/** Data Structure Shortcuts **/

CacheMap<shared_ptr<ExpanderOptions const>, fs::path> const& ExpansionTestUtils::defnTestOptions()
{
	static auto _defnTestOptions = CacheMap<shared_ptr<ExpanderOptions const>, fs::path>(
		[](fs::path const& testSubpath)
		{
			auto testFullPath = getDefnTestDir() / testSubpath;
			auto opts = make_shared<ExpanderOptions>();
			opts->addPath(ExpanderOptions::PathCategory::Groups, fs::absolute(testFullPath / "groups"));
			opts->addPath(ExpanderOptions::PathCategory::Members, fs::absolute(testFullPath / "members"));
			opts->addPath(ExpanderOptions::PathCategory::Functions, fs::absolute(testFullPath / "functions"));
			return opts;
		});
	return _defnTestOptions;
}

CacheMap<shared_ptr<ExpansionDefinition const>, fs::path> const& ExpansionTestUtils::defnTestDefinition()
{
	static auto _defnTestDefinition = CacheMap<shared_ptr<ExpansionDefinition const>, fs::path>(
		[](fs::path const& testSubpath)
		{
			return make_shared<ExpansionDefinition>(getDefnDummyDir(), defnTestOptions().get(testSubpath).get());
		});
	return _defnTestDefinition;
}

CacheMap<shared_ptr<ExpansionContext const>, pair<fs::path, string>> const& ExpansionTestUtils::defnTestContext()
{
	static auto _defnTestContext = CacheMap<shared_ptr<ExpansionContext const>, pair<fs::path, string>>(
		[](pair<fs::path, string> const& item)
		{
			auto testSubpath = get<0>(item);
			auto templateName = get<1>(item);

			auto templateNameTree = TemplateTree::parseTemplateName(templateName);
			auto groupSpec = TemplateTree::getGroupSpecifier(templateNameTree->tree->getRoot());
			auto context = shared_ptr<ExpansionContext>(ExpansionContext::readContext(groupSpec, defnTestDefinition().get(testSubpath).get()));
			delete templateNameTree;

			return context;
		});
	return _defnTestContext;
}

shared_ptr<ExpanderOptions> ExpansionTestUtils::getOptions()
{
	return make_shared<ExpanderOptions>(*getCachedOptions());
}

shared_ptr<ExpansionDefinition> ExpansionTestUtils::getDefinition()
{
	return make_shared<ExpansionDefinition>(*getCachedDefinition());
}

shared_ptr<TemplateTree> ExpansionTestUtils::getTemplateNameTree(string const& fileName)
{
	return make_shared<TemplateTree>(*getCachedTemplateNameTree(fileName));
}

shared_ptr<TemplateTree> ExpansionTestUtils::getTemplateContentsTree(string const& fileName)
{
	return make_shared<TemplateTree>(*getCachedTemplateContentsTree(fileName));
}

shared_ptr<ExpansionContext> ExpansionTestUtils::getContext(string const& fileName)
{
	return make_shared<ExpansionContext>(*getCachedContext(fileName));
}

shared_ptr<MemberCombination> ExpansionTestUtils::getMemberCombo(string const& fileName)
{
	return make_shared<MemberCombination>(*getCachedMemberCombo(fileName));
}

shared_ptr<Expander> ExpansionTestUtils::getExpander()
{
	return make_shared<Expander>(*getCachedExpander());
}


/** Caching **/

ExpanderOptions const* ExpansionTestUtils::getCachedOptions()
{
	if (!cachedOptions)
	{
		cachedOptions = make_unique<ExpanderOptions>();
		cachedOptions->mode = ExpanderOptions::Mode::CleanThenExpand;
	}
	return cachedOptions.get();
}

ExpansionDefinition const* ExpansionTestUtils::getCachedDefinition()
{
	if (!cachedDefinition)
	{
		cachedDefinition = make_unique<ExpansionDefinition>(getDefnDir(), getCachedOptions());
	}
	return cachedDefinition.get();
}

TemplateTree const* ExpansionTestUtils::getCachedTemplateNameTree(string const& fileName)
{
	if (cachedTemplateNameTree.count(fileName) == 0)
	{
		auto tree = TemplateTree::parseTemplateName(fileName);
		cachedTemplateNameTree.emplace(fileName, unique_ptr<TemplateTree>(tree));
	}
	return cachedTemplateNameTree.at(fileName).get();
}

TemplateTree const* ExpansionTestUtils::getCachedTemplateContentsTree(string const& fileName)
{
	if (cachedTemplateContentsTree.count(fileName) == 0)
	{
		auto tree = TemplateTree::parseTemplateContentsAtPath(getTmplDir() / fileName);
		cachedTemplateContentsTree.emplace(fileName, unique_ptr<TemplateTree>(tree));
	}
	return cachedTemplateContentsTree.at(fileName).get();
}

ExpansionContext const* ExpansionTestUtils::getCachedContext(string const& fileName)
{
	if (cachedContext.count(fileName) == 0)
	{
		auto templateNameTree = getTemplateNameTree(fileName);
		auto groupSpec = TemplateTree::getGroupSpecifier(templateNameTree->tree->getRoot());
		auto context = ExpansionContext::readContext(groupSpec, getCachedDefinition());
		cachedContext.emplace(fileName, unique_ptr<ExpansionContext>(context));
	}
	return cachedContext.at(fileName).get();
}

MemberCombination const* ExpansionTestUtils::getCachedMemberCombo(string const& fileName)
{
	if (cachedMemberCombo.count(fileName) == 0)
	{
		auto context = new ExpansionContext(*getCachedContext(fileName));
		auto memberCombo = MemberCombination::initFromContext(context);
		cachedMemberCombo.emplace(fileName, unique_ptr<MemberCombination>(memberCombo));
	}
	return cachedMemberCombo.at(fileName).get();
}

Expander const* ExpansionTestUtils::getCachedExpander()
{
	if (!cachedExpander)
	{
		cachedExpander = make_unique<Expander>(getCachedDefinition(), getTmplDir(), getActualExpnDir());
	}
	return cachedExpander.get();
}
