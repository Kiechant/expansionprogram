//
//  ExpansionTestUtils.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 30/7/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef EXPANSION_TEST_UTILS_HPP
#define EXPANSION_TEST_UTILS_HPP

template<typename Item> class Cache;
template<typename Item, typename Key> class CacheMap;
class Expander;
class ExpansionContext;
class ExpansionDefinition;
class ExpanderOptions;
class MemberCombination;
class TemplateTree;

#include <filesystem>
#include <memory>
#include <unordered_map>

class ExpansionTestUtils
{
public:
	/** Directories **/

	static std::filesystem::path getTestResDir();
	static std::filesystem::path getDefnDir();
	static std::filesystem::path getDefnDummyDir();
	static std::filesystem::path getDefnTestDir();
	static std::filesystem::path getTmplDir();
	static std::filesystem::path getActualExpnDir();
	static std::filesystem::path getExpectedExpnDir();


	/** Template Names **/

	static std::filesystem::path getBasicName();
	static std::filesystem::path getMultiGroupSpecName();
	static std::filesystem::path getMultiNestedSpecName();
	static std::filesystem::path getMultiPropertyName();
	static std::filesystem::path getValidMultiGroupName();

	
	/** Data Structure Shortcuts **/

	static CacheMap<std::shared_ptr<ExpanderOptions const>, std::filesystem::path> const& defnTestOptions();
	static CacheMap<std::shared_ptr<ExpansionDefinition const>, std::filesystem::path> const& defnTestDefinition();
	static CacheMap<std::shared_ptr<ExpansionContext const>, std::pair<std::filesystem::path, std::string>> const& defnTestContext();

	static std::shared_ptr<ExpanderOptions> getOptions();
	static std::shared_ptr<ExpansionDefinition> getDefinition();
	static std::shared_ptr<TemplateTree> getTemplateNameTree(std::string const& fileName);
	static std::shared_ptr<TemplateTree> getTemplateContentsTree(std::string const& fileName);
	static std::shared_ptr<ExpansionContext> getContext(std::string const& fileName);
	static std::shared_ptr<MemberCombination> getMemberCombo(std::string const& fileName);
	static std::shared_ptr<Expander> getExpander();

private:
	static std::unique_ptr<ExpanderOptions> cachedOptions;
	static std::unique_ptr<ExpansionDefinition> cachedDefinition;
	static std::unordered_map<std::string, std::unique_ptr<TemplateTree>> cachedTemplateNameTree;
	static std::unordered_map<std::string, std::unique_ptr<TemplateTree>> cachedTemplateContentsTree;
	static std::unordered_map<std::string, std::unique_ptr<ExpansionContext>> cachedContext;
	static std::unordered_map<std::string, std::unique_ptr<MemberCombination>> cachedMemberCombo;
	static std::unique_ptr<Expander> cachedExpander;

	static const std::filesystem::path basicName;
	static const std::filesystem::path multiGroupSpecName;
	static const std::filesystem::path multiNestedSpecName;
	static const std::filesystem::path multiPropertyName;
	static const std::filesystem::path validMultiGroupName;


	/** Caching **/

	static ExpanderOptions const* getCachedOptions();
	static ExpansionDefinition const* getCachedDefinition();
	static TemplateTree const* getCachedTemplateNameTree(std::string const& fileName);
	static TemplateTree const* getCachedTemplateContentsTree(std::string const& fileName);
	static ExpansionContext const* getCachedContext(std::string const& fileName);
	static MemberCombination const* getCachedMemberCombo(std::string const& fileName);
	static Expander const* getCachedExpander();
};

#endif /* EXPANSION_TEST_UTILS_HPP */
