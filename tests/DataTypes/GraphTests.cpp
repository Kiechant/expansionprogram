#include <catch2/catch.hpp>

#include "Graph.tpp"

#include "StdComparison.hpp"

using namespace std;

pair<Graph<int>*, vector<GraphNode<int>*>> graphWithManyVerticesAndEdges()
{
	Graph<int>* graph = new Graph<int>();

	auto nodes = vector<GraphNode<int>*>();

	// Adds a buffer so nodes can be indexed from 1.
	nodes.push_back(nullptr);

	// Adds vertices 1 to 9.
	nodes.push_back(graph->addVertex(1));
	nodes.push_back(graph->addVertex(2));
	nodes.push_back(graph->addVertex(3));
	nodes.push_back(graph->addVertex(4));
	nodes.push_back(graph->addVertex(5));
	nodes.push_back(graph->addVertex(6));
	nodes.push_back(graph->addVertex(7));
	nodes.push_back(graph->addVertex(8));
	nodes.push_back(graph->addVertex(9));

	// Adds edges between vertices.
	graph->addEdge(nodes[1], nodes[2]);
	graph->addEdge(nodes[1], nodes[3]);
	graph->addEdge(nodes[2], nodes[3]);
	graph->addEdge(nodes[1], nodes[8]);
	graph->addEdge(nodes[3], nodes[4]);
	graph->addEdge(nodes[3], nodes[5]);
	graph->addEdge(nodes[5], nodes[6]);
	graph->addEdge(nodes[6], nodes[4]);
	graph->addEdge(nodes[7], nodes[1]);
	graph->addEdge(nodes[9], nodes[6]);

	// Graph with vertices {1, 2, 3, 4, 5, 6, 7, 8, 9}.
	// and edges {(1, 2), (1, 3), (1, 8), (2, 3), (3, 4), (3, 5), (5, 6), (6, 4), (7, 1), (9, 6)}.

	return make_pair(graph, nodes);
}

SCENARIO("Graph - Adding and removing vertices and edges in same graph")
{
	Graph<int> graph = Graph<int>();
	
	GIVEN("No vertices")   
	{
		THEN("Vertex count is zero, and vertex 0 is inaccessible")
		{
			CHECK(graph.getVertexCount() == 0);
			CHECK_THROWS(graph.getVertex(0));
			CHECK_THROWS(graph.getMutableVertex(0));
		}
	}

	GraphNode<int>* node1;
	REQUIRE_NOTHROW(node1 = graph.addVertex(1));

	GIVEN("Single vertex and no edge")
	{
		THEN("Vertex count is 1, and only vertex 0 is accessible")
		{
			REQUIRE(graph.getVertexCount() == 1);
			CHECK(graph.getVertex(0)->getValue() == 1);
			CHECK_THROWS(graph.getVertex(1));
		}

		THEN("The vertex's graph is consistent")
		{
			CHECK(node1->getGraph() == &graph);
		}
		
		THEN("The vertex has no incoming or outgoing edges")
		{
			CHECK(node1->getDegree() == 0);
			CHECK(node1->getIndegree() == 0);
			CHECK(node1->getOutdegree() == 0);

			// Throws when getting non-existent incoming or outgoing node for no edges.
			CHECK_THROWS(node1->getIncomingNode(0));
			CHECK_THROWS(node1->getOutgoingNode(0));
		}
	}

	GraphNode<int>* node2 = graph.addVertex(2);
	GraphNode<int>* node3 = graph.addVertex(3);

	// TODO: Test edge adding AND REMOVING validity.
	REQUIRE_NOTHROW(graph.addEdge(node1, node2));
	graph.addEdge(node1, node3);
	graph.addEdge(node2, node3);

	// Graph with vertices {1, 2, 3} and edges {(1, 2), (1, 3), (2, 3)}.

	GIVEN("Multiple vertices and edges")
	{
		THEN("The vertices can be accessed")
		{
			REQUIRE(graph.getVertexCount() == 3);
			CHECK(graph.getVertex(1)->getValue() == 2);
			CHECK(graph.getVertex(2)->getValue() == 3);
			CHECK_THROWS(graph.getVertex(3));
		}

		WHEN("Removing a vertex")
		{
			REQUIRE_NOTHROW(graph.removeVertex(node2));

			THEN("Vertex is inaccessible and deleted")
			{
				CHECK(graph.getVertexCount() == 2);
				CHECK(graph.getVertex(0)->getValue() != 2);
				CHECK(graph.getVertex(1)->getValue() != 2);
				CHECK_THROWS(graph.getVertex(2));
			}

			THEN("Corresponding edges are removed from graph")
			{
				CHECK(node1->getOutdegree() == 1);
				CHECK(node1->getOutgoingNode(0)->getValue() != 2);
				CHECK(node3->getIndegree() == 1);
				CHECK(node3->getIncomingNode(0)->getValue() != 2);
			}

			THEN("Corresponding edges are removed from the removed vertex")
			{
				CHECK(node2->getIndegree() == 0);
				CHECK(node2->getOutdegree() == 0);
			}
		}

		THEN("The edges are updated and accessible for each vertex")
		{
			// Outgoing edges in vertex 3 to vertex 7 and 9.
			REQUIRE(node1->getOutdegree() == 2);
			CHECK(node1->getOutgoingNode(0)->getValue() == 2);
			CHECK(node1->getOutgoingNode(1)->getValue() == 3);

			// Edges in vertex 7: Incoming from 3, outgoing to 9.
			REQUIRE(node2->getIndegree() == 1);
			CHECK(node2->getIncomingNode(0)->getValue() == 1);
			REQUIRE(node2->getOutdegree() == 1);
			CHECK(node2->getOutgoingNode(0)->getValue() == 3);

			// Incoming edges in vertex 9 from vertex 3 and 7.
			REQUIRE(node3->getIndegree() == 2);
			CHECK(node3->getIncomingNode(0)->getValue() == 1);
			CHECK(node3->getIncomingNode(1)->getValue() == 2);

			// Consistency of degree and indexing.
			CHECK(node1->getDegree() == 2);
			CHECK(node1->getIndegree() == 0);
			CHECK(node2->getDegree() == 2);
			CHECK(node3->getOutdegree() == 0);

			// Throws when getting non-existent incoming or outgoing nodes for more than one edge.
			CHECK_THROWS(node1->getOutgoingNode(2));
			CHECK_THROWS(node3->getIncomingNode(2));
		}

		THEN("Can remove an edge")
		{
			CHECK_NOTHROW(graph.removeEdge(node1, node3));
			CHECK(graph.getVertex(0)->getOutdegree() == 1);
			CHECK(graph.getVertex(0)->getOutgoingNode(0)->getValue() == 2);
		}

		WHEN("Adding a back edge to an existing edge")
		{
			CHECK_NOTHROW(graph.addEdge(node3, node1));

			THEN("Forward edge remains")
			{
				CHECK(graph.getVertex(0)->getOutgoingNode(1)->getValue() == 3);
			}

			THEN("Back edge is accessible")
			{
				CHECK(graph.getVertex(0)->getIncomingNode(0)->getValue() == 3);
				CHECK(graph.getVertex(2)->getOutgoingNode(0)->getValue() == 1);
			}

			AND_WHEN("Removing the back edge")
			{
				CHECK_NOTHROW(graph.removeEdge(node3, node1));

				THEN("Forward edge remains")
				{
					CHECK(graph.getVertex(0)->getOutgoingNode(1)->getValue() == 3);
				}

				THEN("Back edge is inaccessible")
				{
					CHECK_THROWS(graph.getVertex(0)->getIncomingNode(0));
					CHECK_THROWS(graph.getVertex(2)->getOutgoingNode(0));
				}
			}
		}

		THEN("Cannot re-add existing edge")
		{
			CHECK_THROWS(graph.addEdge(node1, node3));
		}

		THEN("Cannot remove non-existent edge")
		{
			CHECK_THROWS(graph.removeEdge(node3, node1));
		}
	}
}

SCENARIO("Graph - Adding vertices and edges between different graphs")
{
	Graph<int> graph1 = Graph<int>();
	Graph<int> graph2 = Graph<int>();

	auto node_1_1 = graph1.addVertex(11);
	graph1.addVertex(12);
	auto node_1_3 = graph1.addVertex(13);

	auto node_2_1 = graph2.addVertex(21);
	auto node_2_2 = graph2.addVertex(22);

	GIVEN("Two graphs with multiple vertices")
	{
		THEN("Can add edges between vertices of the same graph")
		{
			CHECK_NOTHROW(graph1.addEdge(node_1_1, node_1_3));
			CHECK_NOTHROW(graph2.addEdge(node_2_1, node_2_2));
		}

		THEN("Cannot add edges between vertices of another graph")
		{
			CHECK_THROWS(graph1.addEdge(node_1_1, node_2_1));
			CHECK_THROWS(graph1.addEdge(node_2_1, node_1_1));
			CHECK_THROWS(graph1.addEdge(node_2_1, node_2_2));
		}
	}
}

SCENARIO("Graph - Accessing and modifying the value of a vertex")
{
	Graph<int> graph = Graph<int>();
	GraphNode<int>* vertex = graph.addVertex(1);

	GIVEN("Single vertex")
	{
		THEN("The correct value can be accessed")
		{
			CHECK(vertex->getValue() == 1);
		}

		THEN("The value can be set")
		{
			vertex->setValue(5);
			CHECK(vertex->getValue() == 5);
		}

		THEN("The vertex can be reaccessed as mutable")
		{
			REQUIRE_NOTHROW(vertex = graph.getMutableVertex(0));
			vertex->setValue(5);
			CHECK(vertex->getValue() == 5);

			// Cannot access later vertices.
			CHECK_THROWS(graph.getMutableVertex(1));
		}

		THEN("The value can be modified with a lambda")
		{
			vertex->modifyValue([](int* value){ *value = *value * 5; });
			CHECK(vertex->getValue() == 5);
		}
	}
}

SCENARIO("Graph - Sources and sinks are updated after vertex and edge addition and removal")
{
	Graph<int> graph;

	GIVEN("No vertices")
	{
		THEN("Has no sources")
		{
			CHECK(graph.getSourceCount() == 0);
			CHECK_THROWS(graph.getSource(0));
		}

		THEN("Has no sinks")
		{
			CHECK(graph.getSinkCount() == 0);
			CHECK_THROWS(graph.getSink(0));
		}
	}

	graph.addVertex(1);

	GIVEN("Single vertex")
	{
		THEN("Has one source")
		{
			REQUIRE(graph.getSourceCount() == 1);
			CHECK(graph.getSource(0)->getValue() == 1);
			CHECK_THROWS(graph.getSource(1));
		}

		THEN("Has one sink")
		{
			REQUIRE(graph.getSinkCount() == 1);
			CHECK(graph.getSink(0)->getValue() == 1);
			CHECK_THROWS(graph.getSink(1));
		}
	}

	GIVEN("Many vertices and edges")
	{
		Graph<int>* graph;
		vector<GraphNode<int>*> nodes;
		tie(graph, nodes) = graphWithManyVerticesAndEdges();

		THEN("Has correct sources")
		{
			// Two sources: vertex 7 and 9.
			REQUIRE(graph->getSourceCount() == 2);
			CHECK(graph->getSource(0)->getValue() == 7);
			CHECK(graph->getSource(1)->getValue() == 9);
			CHECK_THROWS(graph->getSource(2));
		}

		THEN("Has correct sinks")
		{
			// Two sinks: vertex 4 and 8.
			REQUIRE(graph->getSinkCount() == 2);
			CHECK(graph->getSink(0)->getValue() == 4);
			CHECK(graph->getSink(1)->getValue() == 8);
			CHECK_THROWS(graph->getSink(2));
		}

		WHEN("Removing some edges")
		{
			graph->removeEdge(nodes[1], nodes[3]);
			graph->removeEdge(nodes[2], nodes[3]);
			graph->removeEdge(nodes[5], nodes[6]);

			THEN("Adds new sources")
			{
				// Added source: vertex 3.
				REQUIRE(graph->getSourceCount() == 3);
				CHECK(graph->getSource(2)->getValue() == 3);
			}

			THEN("Adds new sinks")
			{
				// Added sinks: vertex 2 and 5.
				REQUIRE(graph->getSinkCount() == 4);
				CHECK(graph->getSink(2)->getValue() == 2);
				CHECK(graph->getSink(3)->getValue() == 5);
			}
		}

		WHEN("Removing a vertex")
		{
			graph->removeVertex(nodes[3]);

			THEN("Adds new sources")
			{
				// Added source: vertex 5.
				REQUIRE(graph->getSourceCount() == 3);
				CHECK(graph->getSource(2)->getValue() == 5);
			}

			THEN("Adds new sinks")
			{
				// Added sink: vertex 2.
				REQUIRE(graph->getSinkCount() == 3);
				CHECK(graph->getSink(2)->getValue() == 2);
			}
		}
	}
}

SCENARIO("Graph - Detecting whether a cycle exists")
{
	Graph<int> graph;

	GIVEN("No vertices")
	{
		THEN("The graph is updated and acyclic")
		{
			CHECK(graph.didUpdateCycleStatus());
			CHECK(!graph.hasCycles());

			AND_THEN("The graph can still be checked for cycles")
			{
				CHECK(!graph.checkGraphForCycles());
				CHECK(!graph.hasCycles());
			}
		}

		auto node1 = graph.addVertex(1);
		auto node2 = graph.addVertex(2);
	
		WHEN("Adding a vertex")
		{
			THEN("The graph is updated and acyclic")
			{
				CHECK(graph.didUpdateCycleStatus());
				CHECK(!graph.hasCycles());

				AND_THEN("The graph can still be checked for cycles")
				{
					CHECK(!graph.checkGraphForCycles());
					CHECK(!graph.hasCycles());
				}
			}
		}

		graph.addEdge(node1, node2);

		WHEN("Adding an edge")
		{
			THEN("The graph is unupdated and acyclic")
			{
				CHECK(!graph.didUpdateCycleStatus());
				CHECK(!graph.hasCycles());

				WHEN("The graph is checked for cycles")
				{
					bool hasCycles = graph.checkGraphForCycles();

					THEN("The graph is updated and acyclic")
					{
						CHECK(!hasCycles);
						CHECK(graph.didUpdateCycleStatus());
						CHECK(!graph.hasCycles());
					}
				}
			}
		}

		auto node3 = graph.addVertex(3);
		graph.addEdge(node2, node3);
		graph.addEdge(node3, node1);

		WHEN("Adding a cycle")
		{
			THEN("The graph is unupdated and acyclic")
			{
				CHECK(!graph.didUpdateCycleStatus());
				CHECK(!graph.hasCycles());

				WHEN("The graph is checked for cycles")
				{
					bool hasCycles;
					REQUIRE_NOTHROW(hasCycles = graph.checkGraphForCycles());

					THEN("The graph is updated and cyclic")
					{
						CHECK(hasCycles);
						CHECK(graph.didUpdateCycleStatus());
						CHECK(graph.hasCycles());
					}

					AND_WHEN("Removing a cycle")
					{
						graph.removeEdge(node2, node3);

						THEN("The graph is unupdated and cyclic")
						{
							CHECK(!graph.didUpdateCycleStatus());
							CHECK(graph.hasCycles());

							WHEN("The graph is checked for cycles")
							{
								bool hasCycles;
								REQUIRE_NOTHROW(hasCycles = graph.checkGraphForCycles());

								THEN("The graph is updated and acyclic")
								{
									CHECK(!hasCycles);
									CHECK(graph.didUpdateCycleStatus());
									CHECK(!graph.hasCycles());
								}
							}
						}
					}
				}
			}
		}
	}

	GIVEN("An acyclic graph with many any vertices and edges")
	{
		Graph<int>* graph;
		vector<GraphNode<int>*> nodes;
		tie(graph, nodes) = graphWithManyVerticesAndEdges();

		THEN("The graph is unupdated and acyclic")
		{
			CHECK(!graph->didUpdateCycleStatus());
			CHECK(!graph->hasCycles());

			WHEN("The graph is checked for cycles")
			{
				bool hasCycles;
				REQUIRE_NOTHROW(hasCycles = graph->checkGraphForCycles());

				THEN("The graph is still acyclic")
				{
					CHECK(!hasCycles);
					CHECK(graph->didUpdateCycleStatus());
					CHECK(!graph->hasCycles());
				}
			}
		}

		graph->checkGraphForCycles();

		WHEN("Adding a vertex")
		{
			graph->addVertex(10);

			THEN("The graph is still updated and acyclic")
			{
				CHECK(graph->didUpdateCycleStatus());
				CHECK(!graph->checkGraphForCycles());
			}
		}

		WHEN("Adding an edge which doesn't cause a cycle")
		{
			graph->addEdge(nodes[8], nodes[9]);

			THEN("The graph is initially unupdated and acyclic after update")
			{
				CHECK(!graph->didUpdateCycleStatus());
				CHECK(!graph->checkGraphForCycles());
			}
		}

		WHEN("Adding an edge which causes a long cycle")
		{
			graph->addEdge(nodes[6], nodes[7]);

			THEN("The graph is cyclic")
			{
				CHECK(graph->checkGraphForCycles());
			}

			AND_WHEN("Removing an edge in a long cycle")
			{
				graph->removeEdge(nodes[5], nodes[6]);

				THEN("The graph is acyclic")
				{
					CHECK(!graph->checkGraphForCycles());
				}
			}

			AND_WHEN("Removing a vertex in a long cycle")
			{
				graph->removeVertex(nodes[3]);

				THEN("The graph is acyclic")
				{
					CHECK(!graph->checkGraphForCycles());
				}
			}
		}

		WHEN("Removing a vertex")
		{
			graph->removeVertex(nodes[3]);
			graph->removeVertex(nodes[7]);
			graph->removeVertex(nodes[4]);

			THEN("The graph is still updated")
			{
				CHECK(graph->didUpdateCycleStatus());
			}
		}

		WHEN("Removing an edge")
		{
			graph->removeEdge(nodes[3], nodes[4]);
			graph->removeEdge(nodes[1], nodes[3]);

			THEN("The graph is still updated")
			{
				CHECK(graph->didUpdateCycleStatus());
			}
		}
	}
}

SCENARIO("Graph - Detecting the nodes in a cycle")
{
	auto [graph, nodes] = graphWithManyVerticesAndEdges();

	GIVEN("No cycle")
	{
		THEN("No cycle returned")
		{
			CHECK(!graph->getCycle().has_value());
		}
	}

	GIVEN("A long cycle")
	{
		graph->addEdge(nodes[6], nodes[7]);

		THEN("The cycle is found")
		{
			auto cycle = graph->getCycle();
			REQUIRE(cycle.has_value());
			REQUIRE(cycle.value().size() == 6);

			// Matches cycle against expected result.
			CHECK(cycle.value() == std::vector<GraphNode<int> const*>
			{
				nodes[1],
				nodes[2],
				nodes[3],
				nodes[5],
				nodes[6],
				nodes[7]
			});
		}
	}

	GIVEN("A cycle not beginning at 1")
	{
		graph->addEdge(nodes[6], nodes[7]);
		graph->removeVertex(nodes[3]);
		graph->addEdge(nodes[7], nodes[9]);
		
		THEN("The cycle is found")
		{
			auto cycle = graph->getCycle();
			REQUIRE(cycle.has_value());
			REQUIRE(cycle.value().size() == 3);

			// Matches cycle against expected result.
			CHECK(cycle.value() == std::vector<GraphNode<int> const*>
			{
				nodes[6],
				nodes[7],
				nodes[9]
			});
		}
	}

	GIVEN("A single-node cycle")
	{
		graph->addEdge(nodes[5], nodes[5]);
		
		THEN("The cycle is found")
		{
			auto cycle = graph->getCycle();
			REQUIRE(cycle.has_value());
			REQUIRE(cycle.value().size() == 1);
			CHECK(cycle.value().at(0)->getValue() == 5);
		}
	}
}

SCENARIO("Graph - Copying and moving")
{
	auto [graph, nodes] = graphWithManyVerticesAndEdges();

	GIVEN("A graph")
	{
		Graph<int>* newGraph = new Graph<int>(*graph);

		WHEN("The graph is copied")
		{
			THEN("The new graph is a different object")
			{
				REQUIRE(newGraph != graph);
			}

			THEN("The vertices are copied")
			{
				REQUIRE(newGraph->getVertexCount() == graph->getVertexCount());

				// The first vertex is copied.
				CHECK(newGraph->getVertex(0) != graph->getVertex(0));
				CHECK(newGraph->getVertex(0)->getValue() == graph->getVertex(0)->getValue());
				CHECK(newGraph->getVertex(0)->getGraph() == newGraph);
				CHECK(graph->getVertex(0)->getGraph() == graph);

				// A central vertex is copied.
				CHECK(newGraph->getVertex(3) != graph->getVertex(3));
				CHECK(newGraph->getVertex(3)->getValue() == graph->getVertex(3)->getValue());
				CHECK(newGraph->getVertex(3)->getGraph() == newGraph);
				CHECK(graph->getVertex(3)->getGraph() == graph);

				// The last vertex is copied.
				size_t lastIndex = newGraph->getVertexCount() - 1;
				CHECK(newGraph->getVertex(lastIndex) != graph->getVertex(lastIndex));
				CHECK(newGraph->getVertex(lastIndex)->getValue() == graph->getVertex(lastIndex)->getValue());
				CHECK(newGraph->getVertex(lastIndex)->getGraph() == newGraph);
				CHECK(graph->getVertex(lastIndex)->getGraph() == graph);
			}

			AND_WHEN("Editing a vertex of the new graph")
			{
				newGraph->getMutableVertex(4)->setValue(15);
				REQUIRE(newGraph->getVertex(4)->getValue() == 15);

				THEN("Doesn't affect the vertex of the old graph")
				{
					CHECK(graph->getVertex(4)->getValue() != 15);
				}
			}

			AND_WHEN("Adding a vertex to the new graph")
			{
				newGraph->addVertex(10);
				REQUIRE(newGraph->getVertexCount() == 10);

				THEN("Doesn't affect the old graph")
				{
					CHECK(graph->getVertexCount() == 9);
				}
			}

			AND_WHEN("Removing a vertex from the new graph")
			{
				REQUIRE(newGraph->getVertex(3)->getValue() == 4);

				newGraph->removeVertex(newGraph->getMutableVertex(3));
				REQUIRE(newGraph->getVertex(3)->getValue() != 4);

				THEN("Doesn't affect the old graph")
				{
					CHECK(graph->getVertex(3)->getValue() == 4);
				}
			}

			THEN("The edges are copied")
			{
				// The outgoing connection of the first edge is copied.
				CHECK(newGraph->getVertex(0)->getOutgoingNode(0) != graph->getVertex(0)->getOutgoingNode(0));
				CHECK(newGraph->getVertex(0)->getOutgoingNode(0)->getValue() == 2);
				CHECK(graph->getVertex(0)->getOutgoingNode(0)->getValue() == 2);

				// The incoming connection of the first edge is copied.
				CHECK(newGraph->getVertex(1)->getIncomingNode(0) != graph->getVertex(0)->getIncomingNode(0));
				CHECK(newGraph->getVertex(1)->getIncomingNode(0)->getValue() == 1);
				CHECK(graph->getVertex(1)->getIncomingNode(0)->getValue() == 1);

				// The outgoing connection of a middle edge is copied.
				CHECK(newGraph->getVertex(2)->getOutgoingNode(1) != graph->getVertex(2)->getOutgoingNode(1));
				CHECK(newGraph->getVertex(2)->getOutgoingNode(1)->getValue() == 5);
				CHECK(graph->getVertex(2)->getOutgoingNode(1)->getValue() == 5);

				// The incoming connection of a middle edge is copied.
				CHECK(newGraph->getVertex(4)->getIncomingNode(0) != graph->getVertex(0)->getIncomingNode(0));
				CHECK(newGraph->getVertex(4)->getIncomingNode(0)->getValue() == 3);
				CHECK(graph->getVertex(4)->getIncomingNode(0)->getValue() == 3);
			}

			AND_WHEN("Adding an edge to the new graph")
			{
				newGraph->addEdge(newGraph->getMutableVertex(2), newGraph->getMutableVertex(8));
				REQUIRE(newGraph->getVertex(2)->getOutgoingNode(2) == newGraph->getVertex(8));
				REQUIRE(newGraph->getVertex(8)->getIncomingNode(0) == newGraph->getVertex(2));

				THEN("Doesn't affect the old graph")
				{
					CHECK_THROWS(graph->getVertex(2)->getOutgoingNode(2));
					CHECK_THROWS(graph->getVertex(8)->getIncomingNode(0));
				}
			}

			AND_WHEN("Removing an edge from the new graph")
			{
				REQUIRE(newGraph->getVertex(0)->getOutgoingNode(1) == newGraph->getVertex(2));
				REQUIRE(newGraph->getVertex(2)->getIncomingNode(0) == newGraph->getVertex(0));

				newGraph->removeEdge(newGraph->getMutableVertex(0), newGraph->getMutableVertex(2));
				REQUIRE(newGraph->getVertex(0)->getOutgoingNode(1) != newGraph->getVertex(2));
				REQUIRE(newGraph->getVertex(2)->getIncomingNode(0) != newGraph->getVertex(0));

				THEN("Doesn't affect the old graph")
				{
					CHECK(graph->getVertex(0)->getOutgoingNode(1) == graph->getVertex(2));
					CHECK(graph->getVertex(2)->getIncomingNode(0) == graph->getVertex(0));
				}
			}

			THEN("The sinks and sources are copied")
			{
				CHECK(newGraph->getSourceCount() == graph->getSourceCount());
				CHECK(newGraph->getSource(1) != graph->getSource(1));
				CHECK(newGraph->getSource(1)->getValue() == graph->getSource(1)->getValue());

				CHECK(newGraph->getSinkCount() == graph->getSinkCount());
				CHECK(newGraph->getSink(1) != graph->getSink(1));
				CHECK(newGraph->getSink(1)->getValue() == graph->getSink(1)->getValue());
			}

			THEN("The update and cycle statuses are copied")
			{
				CHECK(newGraph->didUpdateCycleStatus() == graph->didUpdateCycleStatus());
				CHECK(newGraph->hasCycles() == graph->hasCycles());
			}
		}

		WHEN("The graph is moved")
		{
			Graph<int> movedGraph = std::move(*graph);

			THEN("The vertices and edges are moved")
			{
				// The vertices are moved.
				CHECK(movedGraph.getVertexCount() == 9);
				CHECK(movedGraph.getVertex(3)->getGraph() == &movedGraph);

				// The edges are consistent.
				CHECK(movedGraph.getVertex(2)->getOutgoingNode(1)->getGraph() == &movedGraph);
				CHECK(movedGraph.getVertex(4)->getIncomingNode(0)->getGraph() == &movedGraph);
				CHECK(movedGraph.getVertex(2)->getOutgoingNode(1) == movedGraph.getVertex(4));
				CHECK(movedGraph.getVertex(4)->getIncomingNode(0) == movedGraph.getVertex(2));
			}

			THEN("The sinks and sources are consistent")
			{
				CHECK(movedGraph.getSourceCount() == 2);
				CHECK(movedGraph.getSource(1)->getValue() == 9);
				CHECK(movedGraph.getSource(1)->getGraph() == &movedGraph);

				CHECK(movedGraph.getSinkCount() == 2);
				CHECK(movedGraph.getSink(0)->getValue() == 4);
				CHECK(movedGraph.getSink(0)->getGraph() == &movedGraph);
			}

			THEN("The update and cycle statuses are moved")
			{
				CHECK(!movedGraph.didUpdateCycleStatus());
				CHECK(!movedGraph.hasCycles());
			}
		}
	}
}

SCENARIO("Graph - Getting vertices by hash value")
{
	GIVEN("No key function and a line")
	{
		Graph<int> graph = Graph<int>();
		auto node0 = graph.addVertex(0);
		auto node1 = graph.addVertex(1);
		auto node2 = graph.addVertex(2);
		graph.addEdge(node0, node1);
		graph.addEdge(node1, node2);

		WHEN("Getting a vertex by key")
		{
			THEN ("Error")
			{
				REQUIRE_NOTHROW(graph.getVertex(0));
				REQUIRE_THROWS(graph.getVertexWithKey(0));
			}
		
		}
		WHEN("Getting an edge by key")
		{
			THEN("Error")
			{
				REQUIRE_NOTHROW(graph.getVertex(1)->getIncomingNode(0));
				REQUIRE_THROWS(graph.getVertex(1)->getIncomingNodeWithKey(0));
				REQUIRE_NOTHROW(graph.getVertex(1)->getOutgoingNode(0));
				REQUIRE_THROWS(graph.getVertex(1)->getOutgoingNodeWithKey(0));
			}
		}
	}
	
	GIVEN("No key function and no vertices")
	{
		Graph<int> graph = Graph<int>();

		WHEN("Setting a key function")
		{
			THEN("The key function is set")
			{
				REQUIRE_NOTHROW(graph.setKeyFunction([](int const& value){ return value; }));
			}
		}
	}

	GIVEN("No key function and a single vertex")
	{
		Graph<int> graph = Graph<int>();
		graph.addVertex(0);

		WHEN("Setting a key function")
		{
			THEN("Error")
			{
				REQUIRE_THROWS(graph.setKeyFunction([](int const& value){ return value; }));
			}
		}
	}

	GIVEN("A key function and multiple vertices")
	{
		Graph<int> graph = Graph<int>();
		graph.setKeyFunction([](int const& value){ return (value % 10) * 11; });
		graph.addVertex(0);
		graph.addVertex(1);
		graph.addVertex(2);
		graph.addVertex(31);

		WHEN("Getting a vertex by an existing key with no duplicates")
		{
			THEN("Correct single vertex is returned")
			{
				REQUIRE_THROWS(graph.getVertex(22));
				REQUIRE(graph.getVertexWithKey(22)->getValue() == 2);
			}
		}

		WHEN("Getting vertices by an existing key with duplicates")
		{
			THEN("Correct list of vertices is returned")
			{
				REQUIRE_THROWS(graph.getVertex(11));
				REQUIRE(graph.getVertexWithKey(11)->getValue() == 1);
				REQUIRE(graph.getVertexWithKey(11, 1)->getValue() == 31);
			}
		}

		WHEN("Getting a vertex by a non-existent key")
		{
			THEN("Error")
			{
				REQUIRE_NOTHROW(graph.getVertex(2));
				REQUIRE_THROWS(graph.getVertexWithKey(2));
				REQUIRE_THROWS(graph.getVertexWithKey(33));
			}
		}

		WHEN("Removing a vertex")
		{
			graph.removeVertex(graph.getMutableVertex(2));

			AND_WHEN("Getting the vertex by its old key")
			{
				THEN("Error")
				{
					REQUIRE_THROWS(graph.getVertexWithKey(22));
				}
			}
		}

		WHEN("Removing a vertex for a duplicate key")
		{
			graph.removeVertex(graph.getMutableVertex(1));

			AND_WHEN("Getting the vertex by its old key")
			{
				THEN("Remaining single vertex is returned")
				{
					REQUIRE(graph.getVertexWithKey(11)->getValue() == 31);
					REQUIRE_THROWS(graph.getVertexWithKey(11, 1));
				}
			}
		}
	}

	GIVEN("A key function and multiple edges")
	{
		Graph<int> graph = Graph<int>();
		graph.setKeyFunction([](int const& value){ return (value % 10) * 11; });
		auto node0 = graph.addVertex(4);
		auto node1 = graph.addVertex(1);
		auto node2 = graph.addVertex(2);
		auto node3 = graph.addVertex(31);
		graph.addEdge(node0, node1);
		graph.addEdge(node0, node2);
		graph.addEdge(node0, node3);
		graph.addEdge(node1, node2);
		graph.addEdge(node3, node2);

		WHEN("Getting an edge by an existing key with no duplicates")
		{
			THEN("Correct single edge is returned")
			{
				REQUIRE_THROWS(node0->getOutgoingNode(22));
				REQUIRE(node0->getOutgoingNodeWithKey(22)->getValue() == 2);
				REQUIRE_THROWS(node2->getIncomingNode(44));
				REQUIRE(node2->getIncomingNodeWithKey(44)->getValue() == 4);
			}
		}

		WHEN("Getting edges by an existing key with duplicates")
		{
			THEN("Correct list of edges is returned")
			{
				REQUIRE_THROWS(node0->getOutgoingNode(11));
				REQUIRE(node0->getOutgoingNodeWithKey(11)->getValue() == 1);
				REQUIRE(node0->getOutgoingNodeWithKey(11, 1)->getValue() == 31);
				REQUIRE_THROWS(node2->getIncomingNode(11));
				REQUIRE(node2->getIncomingNodeWithKey(11)->getValue() == 1);
				REQUIRE(node2->getIncomingNodeWithKey(11, 1)->getValue() == 31);
			}
		}

		WHEN("Getting an edge by a non-existent key")
		{
			THEN("Error")
			{
				REQUIRE_NOTHROW(node0->getOutgoingNode(2));
				REQUIRE_THROWS(node0->getOutgoingNodeWithKey(2));
				REQUIRE_THROWS(node0->getOutgoingNodeWithKey(33));
				REQUIRE_NOTHROW(node2->getIncomingNode(2));
				REQUIRE_THROWS(node2->getIncomingNodeWithKey(2));
				REQUIRE_THROWS(node2->getIncomingNodeWithKey(33));
			}
		}

		WHEN("Removing an edge")
		{
			graph.removeEdge(node0, node2);

			AND_WHEN("Getting the edge by its old key")
			{
				THEN("Error")
				{
					REQUIRE_THROWS(node0->getOutgoingNodeWithKey(22));
					REQUIRE_THROWS(node2->getIncomingNodeWithKey(44));
				}
			}
		}

		WHEN("Removing an edge for a duplicate key")
		{
			graph.removeEdge(node0, node1);
			graph.removeEdge(node1, node2);

			AND_WHEN("Getting the edge by its old key")
			{
				THEN("Remaining single edge is returned")
				{
					REQUIRE(node0->getOutgoingNodeWithKey(11)->getValue() == 31);
					REQUIRE_THROWS(node0->getOutgoingNodeWithKey(11, 1));
					REQUIRE(node2->getIncomingNodeWithKey(11)->getValue() == 31);
					REQUIRE_THROWS(node0->getOutgoingNodeWithKey(11, 1));
				}
			}
		}
	}
}
