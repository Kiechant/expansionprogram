#include <catch2/catch.hpp>

#include "EnumStringMap.tpp"

using namespace std;

enum class MyEnum
{
	Zero,
	One,
	Two,
	Three,
	Four,
	Five
};

SCENARIO("EnumStringMap")
{
	GIVEN("Enum string map")
	{
		EnumStringMap enumStrMap = EnumStringMap<MyEnum>(vector<string>{"zero", "one", "two", "three", "four"});

		WHEN("Converting to string from valid enum")
		{
			string val1, val2, val3;

			CHECK_NOTHROW(val1 = enumStrMap.getString(MyEnum::Zero));
			CHECK_NOTHROW(val2 = enumStrMap.getString(MyEnum::Two));
			CHECK_NOTHROW(val3 = enumStrMap.getString(MyEnum::Four));

			THEN("Converts to correct string")
			{
				CHECK(val1 == "zero");
				CHECK(val2 == "two");
				CHECK(val3 == "four");
			}
		}

		WHEN("Converting to string from invalid enum")
		{
			THEN("Raises error")
			{
				CHECK_THROWS(enumStrMap.getString((MyEnum)-1));
				CHECK_THROWS(enumStrMap.getString(MyEnum::Five));
				CHECK_THROWS(enumStrMap.getString((MyEnum)numeric_limits<int>::max()));
				CHECK_THROWS(enumStrMap.getString((MyEnum)numeric_limits<int>::min()));
			}
		}

		WHEN("Converting to enum from valid string")
		{
			MyEnum val1, val2, val3;

			CHECK_NOTHROW(val1 = enumStrMap.getEnum("zero"));
			CHECK_NOTHROW(val2 = enumStrMap.getEnum("two"));
			CHECK_NOTHROW(val3 = enumStrMap.getEnum("four"));

			THEN("Converts to correct string")
			{
				CHECK(val1 == MyEnum::Zero);
				CHECK(val2 == MyEnum::Two);
				CHECK(val3 == MyEnum::Four);
			}
		}

		WHEN("Converting to enum from invalid string")
		{
			THEN("Raises error")
			{
				CHECK_THROWS(enumStrMap.getEnum(""));
				CHECK_THROWS(enumStrMap.getEnum("five"));
			}
		}
	}
}
