#include <catch2/catch.hpp>

#include "Comparison.hpp"
#include "StdComparison.hpp"
#include "StdStringConversion.hpp"
#include "Tree.tpp"

#include <utility>

using namespace std;

pair<Tree<int>, vector<TreeNode<int>*>> makeTree()
{
	vector<TreeNode<int>*> nodes(9);
	auto tree = Tree<int>(0);

	nodes[0] = tree.getRoot();
	nodes[1] = nodes[0]->addChild(1);
	nodes[6] = nodes[0]->addChild(6);
	nodes[2] = nodes[1]->addChild(2);
	nodes[3] = nodes[1]->addChild(3);
	nodes[5] = nodes[1]->addChild(5);
	nodes[7] = nodes[6]->addChild(7);
	nodes[8] = nodes[6]->addChild(8);
	nodes[4] = nodes[3]->addChild(4);

	return {move(tree), nodes};
}

TEST_CASE("Tree Constructors")
{
	CHECK(Tree<int>().getRoot()->getItem() == 0);
	CHECK(Tree<int>(new TreeNode<int>(3)).getRoot()->getItem() == 3);
	CHECK_THROWS_MATCHES(Tree<int>(nullptr), invalid_argument,
		Catch::Message("Cannot construct tree with null root."));
	CHECK(Tree<int>(3).getRoot()->getItem() == 3);
}

TEST_CASE("Tree Traversal")
{
	auto [tree, nodes] = makeTree();

	auto depthNodes = vector<pair<TreeNode<int>*, size_t>>
	{
		{nodes[0], 0}, {nodes[1], 1}, {nodes[2], 2}, {nodes[3], 2},
		{nodes[4], 3}, {nodes[5], 2}, {nodes[6], 1}, {nodes[7], 2},
		{nodes[8], 2}
	};

	SECTION("Depth First Search")
	{
		// Includes all nodes.
		auto const& expectedFull = depthNodes;
		auto actualFull = Tree<int>::traverseAll(TreeTraversalMode::DepthFirst, nodes[0]);
		CHECK(actualFull == expectedFull);

		// Cancels branching at nodes 2 and 6. Children of 6 (7 and 8) are ignored.
		auto expectedCulled = vector<pair<TreeNode<int>*, size_t>>
		{
			depthNodes[0], depthNodes[1], depthNodes[3], depthNodes[4],
			depthNodes[5]
		};
		auto actualCulled = Tree<int>::traverseAll(TreeTraversalMode::DepthFirst, nodes[0],
			[](TreeNode<int> const* node){ return node->getItem() == 2 || node->getItem() == 6; });
		CHECK(actualCulled == expectedCulled);
	}

	SECTION("Breadth First Search")
	{
		// Includes all nodes.
		auto expectedFull = vector<pair<TreeNode<int>*, size_t>>
		{
			depthNodes[0], depthNodes[1], depthNodes[6], depthNodes[2],
			depthNodes[3], depthNodes[5], depthNodes[7], depthNodes[8],
			depthNodes[4]
		};
		auto actualFull = Tree<int>::traverseAll(TreeTraversalMode::BreadthFirst, nodes[0]);
		CHECK(actualFull == expectedFull);

		// Cancels branching at nodes 2 and 6. Children of 6 (7 and 8) are ignored.
		auto expectedCulled = vector<pair<TreeNode<int>*, size_t>>
		{
			depthNodes[0], depthNodes[1], depthNodes[3], depthNodes[5],
			depthNodes[4]
		};
		auto actualCulled = Tree<int>::traverseAll(TreeTraversalMode::BreadthFirst, nodes[0],
			[](TreeNode<int> const* node){ return node->getItem() == 2 || node->getItem() == 6; });
		CHECK(actualCulled == expectedCulled);
	}

	SECTION("Post Order Search")
	{
		 // Includes all nodes.
		auto expectedFull = vector<pair<TreeNode<int>*, size_t>>
		{
			depthNodes[2], depthNodes[4], depthNodes[3], depthNodes[5],
			depthNodes[1], depthNodes[7], depthNodes[8], depthNodes[6],
			depthNodes[0]
		};
		auto actualFull = Tree<int>::traverseAll(TreeTraversalMode::PostOrder, nodes[0]);
		CHECK(actualFull == expectedFull);

		// Cancels branching at nodes 2 and 6. Children of 6 (7 and 8) are ignored.
		auto expectedCulled = vector<pair<TreeNode<int>*, size_t>>
		{
			depthNodes[4], depthNodes[3], depthNodes[5], depthNodes[1],
			depthNodes[0]
		};
		auto actualCulled = Tree<int>::traverseAll(TreeTraversalMode::PostOrder, nodes[0],
			[](TreeNode<int> const* node){ return node->getItem() == 2 || node->getItem() == 6; });
		CHECK(actualCulled == expectedCulled);
	}
}

TEST_CASE("Tree Copying and Comparison")
{
	auto [tree1, nodes1] = makeTree();
	auto [tree2, nodes2] = makeTree();

	SECTION("Shallow and Deep Comparison")
	{
		CHECK(&tree1 != &tree2);
		CHECK(tree1 == tree2);
	}

	SECTION("Unequal with Different Node Arrangement")
	{
		// Adds child to lowermost node 4 of tree 2.
		nodes2[4]->addChild(9);
		CHECK(tree1 < tree2);
	}

	SECTION("Unequal with Different Node Arrangement")
	{
		// Adds child to final node 8 of tree 2.
		nodes2[8]->addChild(9);
		CHECK(tree1 < tree2);
	}

	SECTION("Unequal with Different Node Values")
	{
		// Changes value of node 6 in tree 2.
		nodes2[6]->setItem(9);
		CHECK(tree1 != tree2);
	}
}
