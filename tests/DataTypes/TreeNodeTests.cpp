#include <catch2/catch.hpp>

#include "TreeNode.tpp"

#include <vector>

using namespace std;

TEST_CASE("TreeNode Constructors")
{
    // Default constructor and constructor with item.
    CHECK(TreeNode<int>().getItem() == 0);
    CHECK(TreeNode<int>(3).getItem() == 3);

    // Constructor with key function.
    auto node1 = TreeNode<int>([](int const&){ return 3; });
    node1.addChild(10);
    CHECK(node1.getItem() == 0);
    CHECK(node1.getChildWithKey(3, 0)->getItem() == 10);
    node1.deepDeleteChildren();

    // Constructor with item and key function.
    auto node2 = TreeNode<int>(7, [](int const&){ return 3; });
    node2.addChild(10);
    CHECK(node2.getItem() == 7);
    CHECK(node2.getChildWithKey(3, 0)->getItem() == 10);
    node2.deepDeleteChildren();
}

TEST_CASE("TreeNode Adding and Finding Children")
{
    CHECK(TreeNode<int>().getChildCount() == 0);

    auto keyNode = TreeNode<int>([](int const& item){ return item % 10; });
    keyNode.addChild(3);
    keyNode.addChild(new TreeNode<int>(8));
    keyNode.addChild(5);
    keyNode.addChild(23);

    SECTION("Parent is Set When Adding Child")
    {
        CHECK(keyNode.getChild(0)->getParent() == &keyNode);
        CHECK(keyNode.getChild(1)->getParent() == &keyNode);
    }

    SECTION("Root")
    {
        CHECK(keyNode.isRoot());
        CHECK(!keyNode.getChild(0)->isRoot());
    }

    SECTION("Getting Child Count, Position and Iterator")
    {
        CHECK(keyNode.getChildCount() == 4);
        CHECK((keyNode.getChildEnd() - keyNode.getChildBegin()) == 4);

        CHECK((*keyNode.getChildBegin())->getItem() == 3);
        CHECK(keyNode.getChild(2)->getItem() == 5);
        CHECK(keyNode.getChild(4) == nullptr);
    }

    SECTION("Getting Child with Key")
    {
        CHECK(keyNode.getChildCountWithKey(1) == 0);
        CHECK(keyNode.getChildCountWithKey(3) == 2);
        CHECK(keyNode.getChildCountWithKey(8) == 1);

        CHECK(keyNode.getChildWithKey(3, 0)->getItem() == 3);
        CHECK(keyNode.getChildWithKey(3, 1)->getItem() == 23);
        CHECK(keyNode.getChildWithKey(3, 2) == nullptr);
    }

    SECTION("Finding Child with Key")
    {
        SECTION("By Index")
        {
            size_t pos = keyNode.findChild(3, 0);
            CHECK(keyNode.getChild(pos)->getItem() == 3);
            pos = keyNode.findChild(3, pos + 1);
            CHECK(keyNode.getChild(pos)->getItem() == 23);
            pos = keyNode.findChild(3, pos + 1);
            CHECK(pos == keyNode.getChildCount());
        }
        SECTION("By Iterator")
        {
            auto it = keyNode.findChild(3);
            CHECK((*it)->getItem() == 3);
            it = keyNode.findChild(3, it + 1);
            CHECK((*it)->getItem() == 23);
            it = keyNode.findChild(3, it + 1);
            CHECK(it == keyNode.getChildEnd());
        }
    }

    SECTION("Adding Children By List")
    {
        // Adding by TreeNode iterator.
        auto children = vector{new TreeNode<int>(1), new TreeNode<int>(2), new TreeNode<int>(3)};
        keyNode.addChildren(children.begin(), children.end());
        CHECK(keyNode.getChildCount() == 7);
        CHECK(keyNode.getChild(5)->getItem() == 2);

        // Adding by TreeNode vector.
        keyNode.addChildren({new TreeNode<int>(4), new TreeNode<int>(5), new TreeNode<int>(6)});
        CHECK(keyNode.getChildCount() == 10);
        CHECK(keyNode.getChild(9)->getItem() == 6);

        // Adding by Item vector.
        keyNode.addChildren({7, 8, 9});
        CHECK(keyNode.getChildCount() == 13);
        CHECK(keyNode.getChild(11)->getItem() == 8);
    }

    SECTION("Adding and Accessing Branches")
    {
        // Adding by TreeNode iterator.
        auto branch = vector{new TreeNode<int>(1), new TreeNode<int>(2), new TreeNode<int>(3)};
        keyNode.addBranch(branch.begin(), branch.end());
        CHECK(keyNode.getChildCount() == 5);
        CHECK(keyNode.getChild(4)->getItem() == 1);
        CHECK(keyNode.getChild(4)->getChildCount() == 1);
        CHECK(keyNode.getChild(4)->getChild(0)->getChild(0)->getItem() == 3);
        CHECK(keyNode.getChild(4)->getChild(0)->getChild(0)->isLeaf());

        // Accessing newly created branch.
        CHECK(keyNode.getBranch({4})->getItem() == 1);
        CHECK(keyNode.getBranch({4, 0, 0})->getItem() == 3);
        CHECK(keyNode.getBranchWithKey({1})->getItem() == 1);
        CHECK(keyNode.getBranchWithKey({1, 2, 3})->getItem() == 3);

        // Adding by TreeNode vector.
        keyNode.addBranch({new TreeNode<int>(4), new TreeNode<int>(5), new TreeNode<int>(6)});
        CHECK(keyNode.getChildCount() == 6);
        CHECK(keyNode.getChild(5)->getChild(0)->getChild(0)->getItem() == 6);

        // Adding by Item vector.
        keyNode.addBranch({7, 8, 9});
        CHECK(keyNode.getChildCount() == 7);
        CHECK(keyNode.getChild(6)->getChild(0)->getChild(0)->getItem() == 9);
    }

    SECTION("Child Keys Update after Setting Item")
    {
        // Changes node's value from 5 to 53.
        // Removes the now empty entry for key 5, and adds this node to key 3.
        keyNode.getChild(2)->setItem(53);
        CHECK(keyNode.getChild(2)->getItem() == 53);
        CHECK(keyNode.getChildCountWithKey(5) == 0);
        CHECK(keyNode.getChildCountWithKey(3) == 3);
        CHECK(keyNode.getChildWithKey(3, 2)->getItem() == 53);
    }

    SECTION("Child Keys Update after Modifying Item")
    {
        // Changes node's value from 5 to 53.
        // Removes the now empty entry for key 5, and adds this node to key 3.
        keyNode.getChild(2)->modifyItem([](int* item){ *item = 53; });
        CHECK(keyNode.getChild(2)->getItem() == 53);
        CHECK(keyNode.getChildCountWithKey(5) == 0);
        CHECK(keyNode.getChildCountWithKey(3) == 3);
        CHECK(keyNode.getChildWithKey(3, 2)->getItem() == 53);
    }

    keyNode.deepDeleteChildren();
}
