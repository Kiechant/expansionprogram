#include <catch2/catch.hpp>

#include "FileUtils.hpp"
#include "MiscUtils.hpp"

#include "ExpansionTestUtils.hpp"
#include "FileDifference.hpp"

#include <filesystem>

using namespace std;
namespace fs = filesystem;

const fs::path fileComparisonsDir = ExpansionTestUtils::getTestResDir() / "file_comparisons";

fs::path getFirstFile(string const& caseDir)
{
	return fileComparisonsDir / caseDir / "a.txt";
}

fs::path getSecondFile(string const& caseDir)
{
	return fileComparisonsDir / caseDir / "b.txt";
}

fs::path getFirstDir(string const& caseDir)
{
	return fileComparisonsDir / caseDir / "a";
}

fs::path getSecondDir(string const& caseDir)
{
	return fileComparisonsDir / caseDir / "b";
}

fs::path getExpectedFile(string const& caseDir)
{
	return fileComparisonsDir / caseDir / "out.txt";
}

string readExpectedFile(string const& caseDir)
{
	return FileUtils::readFile(getExpectedFile(caseDir));
}

SCENARIO("FileUtils - Comparison of file contents")
{
	GIVEN("Same contents")
	{
		const auto case1 = "equal_1";
		THEN("No difference")
		{
			CHECK(FileUtils::compareFiles(getFirstFile(case1), getSecondFile(case1)).getMessage() == readExpectedFile(case1));
		}
	}

	GIVEN("Character mismatch in a line")
	{
		auto case1 = "char_mismatch_1";
		THEN("Character mismatch displayed")
		{
			CHECK(FileUtils::compareFiles(getFirstFile(case1), getSecondFile(case1)).getMessage() == readExpectedFile(case1));
		}
	}

	GIVEN("Different line length with matching characters")
	{
		auto case1 = "line_count_diff_1";
		auto case2 = "line_count_diff_2";
		auto case3 = "line_count_diff_3";
		THEN("Line length difference displayed")
		{
			CHECK(FileUtils::compareFiles(getFirstFile(case1), getSecondFile(case1)).getMessage() == readExpectedFile(case1));
			CHECK(FileUtils::compareFiles(getFirstFile(case2), getSecondFile(case2)).getMessage() == readExpectedFile(case2));
			CHECK(FileUtils::compareFiles(getFirstFile(case3), getSecondFile(case3)).getMessage() == readExpectedFile(case3));
		}
	}

	GIVEN("Different number of lines with matching contents")
	{
		auto case1 = "line_length_diff_1";
		auto case2 = "line_length_diff_2";
		THEN("Line count difference displayed")
		{
			CHECK(FileUtils::compareFiles(getFirstFile(case1), getSecondFile(case1)).getMessage() == readExpectedFile(case1));
			CHECK(FileUtils::compareFiles(getFirstFile(case2), getSecondFile(case2)).getMessage() == readExpectedFile(case2));
		}
	}
}

SCENARIO("FileUtils - Comparison of file existence or types")
{
	GIVEN("The expected file does not exist")
	{
		auto case1 = "existence_1";
		auto case2 = "existence_2";
		THEN("Existence difference displayed")
		{
			CHECK(FileUtils::compareActualAndExpectedFile(getFirstFile(case1), getSecondFile(case1)).getMessage() == readExpectedFile(case1));
			CHECK(FileUtils::compareActualAndExpectedFile(getFirstFile(case2), getSecondFile(case2)).getMessage() == readExpectedFile(case2));
		}
	}

	GIVEN("Different file types")
	{
		auto case1 = "file_type_1";
		THEN("File type displayed")
		{
			CHECK(FileUtils::compareAnyFiles(getFirstFile(case1), getSecondFile(case1)).getMessage() == readExpectedFile(case1));
		}
	}
}

SCENARIO("FileUtils - Comparison of directories")
{
	GIVEN("Same contents of recursive files and directories")
	{
		auto case1 = "equal_directories_1";
		THEN("Only similarities displayed")
		{
			vector<FileDifference> diffs;
			REQUIRE_NOTHROW(diffs = FileUtils::compareDirectories(getFirstDir(case1), getSecondDir(case1)));
			stringstream ss;
			for (auto const& diff : diffs)
				ss << diff.getMessage() << endl;
			CHECK(ss.str() == readExpectedFile(case1));
		}
	}

	GIVEN("Difference contents")
	{
		auto case1 = "diff_directories_1";
		THEN("List of differences displayed")
		{
			vector<FileDifference> diffs;
			REQUIRE_NOTHROW(diffs = FileUtils::compareDirectories(getFirstDir(case1), getSecondDir(case1)));
			stringstream ss;
			for (auto const& diff : diffs)
				if (diff.isDifference())
					ss << diff.getMessage() << endl;
			CHECK(ss.str() == readExpectedFile(case1));
		}
	}
}
