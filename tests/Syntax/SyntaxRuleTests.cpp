#include <catch2/catch.hpp>

#include "MemoryUtils.hpp"
#include "MiscUtils.hpp"
#include "SyntaxConstituent.tpp"
#include "SyntaxRule.hpp"

using namespace std;

using FieldFormat = SyntaxConstituentFormat;
using ValueType = SyntaxConstituentValueType;
using SetFunctor = SyntaxSet;

class SyntaxRuleDummy : public SyntaxRule
{
public:
    SyntaxRuleDummy(string name, vector<SyntaxConstituent const*>* constituents, bool isAnonymous)
    {
        this->name = name;
        this->constituents = constituents;
        this->_isAnonymous = isAnonymous;
    }

    ~SyntaxRuleDummy() {}
};

#define CHECK_RULE_NOTHROW(ruleName, ruleDef) \
    CHECK_NOTHROW(SyntaxRule(ruleName, ruleDef, false));

#define CHECK_RULE_THROWS(ruleName, ruleDef, errPos, errMsg) \
    CHECK_THROWS_MATCHES(SyntaxRule(ruleName, ruleDef, false), \
        std::invalid_argument, \
        Catch::Message("Error in parsing syntax rule at character " + \
            to_string(errPos) + ": " + errMsg + "."));

TEST_CASE("SyntaxRule Empty String and Enclosures")
{
    SECTION("Empty String")
    {
        CHECK_RULE_NOTHROW("unempty_str", "a");
        CHECK_RULE_THROWS("empty_str", "", 0, "empty string");
    }

    SECTION("Null Unempty String")
    {
        CHECK_RULE_NOTHROW("unempty_str", string(1, '\0'));
    }

    SECTION("Empty Group")
    {
        CHECK_RULE_NOTHROW("unempty_group", "(a)");
        CHECK_RULE_THROWS("empty_group", "()", 1, "empty group");
    }

    SECTION("Empty Set")
    {
        CHECK_RULE_NOTHROW("unempty_set", "[a]");
        CHECK_RULE_THROWS("empty_set", "[]", 1, "empty set");
    }

    SECTION("Empty Rule")
    {
        CHECK_RULE_NOTHROW("unempty_rule", "<a>");
        CHECK_RULE_THROWS("empty_rule", "<>", 1, "empty rule");
    }
}

TEST_CASE("SyntaxRule Enclosure Order and Nesting")
{
    SECTION("Basic Valid Syntax")
    {
        CHECK_RULE_NOTHROW("good_enclosures", "(a)[b]<c>((a)[b]<c>)");
    }

    SECTION("Invalid Group Brackets")
    {
        CHECK_RULE_THROWS("bad_group", ")",         0, "group close ')' before group open '('");
        CHECK_RULE_THROWS("bad_group", "(",         1, "end encountered before group close ')'");
        CHECK_RULE_THROWS("bad_group", "(abc",      4, "end encountered before group close ')'");
        CHECK_RULE_THROWS("bad_group", "(a))",      3, "group close ')' before group open '('");
        CHECK_RULE_THROWS("bad_group", "(((a))(b)", 9, "end encountered before group close ')'");
    }

    SECTION("Invalid Set Brackets")
    {
        CHECK_RULE_THROWS("bad_set", "]",    0, "set close ']' before set open '['");
        CHECK_RULE_THROWS("bad_set", "[",    1, "end encountered before set close ']'");
        CHECK_RULE_THROWS("bad_set", "[abc", 4, "end encountered before set close ']'");
        CHECK_RULE_THROWS("bad_set", "[a]]", 3, "set close ']' before set open '['");
        CHECK_RULE_THROWS("bad_set", "[[",   1, "set open '[' before set close ']'");
    }

    SECTION("Invalid Rule Brackets")
    {
        CHECK_RULE_THROWS("bad_rule", ">",    0, "rule close '>' before rule open '<'");
        CHECK_RULE_THROWS("bad_rule", "<",    1, "end encountered before rule close '>'");
        CHECK_RULE_THROWS("bad_rule", "<abc", 4, "end encountered before rule close '>'");
        CHECK_RULE_THROWS("bad_rule", "<a>>", 3, "rule close '>' before rule open '<'");
        CHECK_RULE_THROWS("bad_rule", "<<",   1, "rule open '<' before rule close '>'");
    }
}

TEST_CASE("SyntaxRule Set")
{
    SECTION("Basic Valid Syntax")
    {
        CHECK_RULE_NOTHROW("plain_chars", "[abc_!]");
        CHECK_RULE_NOTHROW("symbols", "[()?+*]");
        CHECK_RULE_NOTHROW("range", "[a-z]");
        CHECK_RULE_NOTHROW("chars_and_range", "[012_a-zA-Z]");
    }

    SECTION("Missing Operands in Range")
    {
        CHECK_RULE_THROWS("bad_set", "[-]",     1, "range specifier '-' missing left operand");
        CHECK_RULE_THROWS("bad_set", "[a-]",    3, "range specifier '-' missing right operand");
        CHECK_RULE_THROWS("bad_set", "[-z]",    1, "range specifier '-' missing left operand");
        CHECK_RULE_THROWS("bad_set", "[a-b-c]", 4, "range specifier '-' missing left operand");
        CHECK_RULE_THROWS("bad_set", "[--]",    1, "range specifier '-' missing left operand");
        CHECK_RULE_THROWS("bad_set", "[a--]",   3, "range specifier '-' repeated");
        CHECK_RULE_THROWS("bad_set", "[a--z]",  3, "range specifier '-' repeated");
    }

    SECTION("Non-increasing Range")
    {
        CHECK_RULE_THROWS("bad_set", "[a-a]", 3, "range must be from a lower to higher character");
        CHECK_RULE_THROWS("bad_set", "[b-a]", 3, "range must be from a lower to higher character");
    }

    SECTION("Escaping")
    {
        CHECK_RULE_NOTHROW("escaped_symbols", "[\\[\\]\\-]");
        CHECK_RULE_NOTHROW("escaped_group", "[()\\(\\)]");
        CHECK_RULE_NOTHROW("escaped_char", "[\\a]");
        CHECK_RULE_NOTHROW("escaped_escape", "[\\\\]");

        CHECK_RULE_THROWS("bad_escape", "[\\]",   3, "end encountered before set close ']'");
        CHECK_RULE_THROWS("bad_escape", "[a-\\]", 5, "end encountered before set close ']'");

        CHECK_RULE_THROWS("good_escape", "[\\]-\\[]", 5, "range must be from a lower to higher character");
        CHECK_RULE_NOTHROW("good_escape", "[\\--\\\\]");
        CHECK_RULE_NOTHROW("good_escape", "[*-+]");
    }
}

TEST_CASE("SyntaxRule Rule")
{
    SECTION("Invalid Characters")
    {
        CHECK_RULE_THROWS("bad_rule", "<@abc", 1, "invalid character '@' in rule");
        CHECK_RULE_THROWS("bad_rule", "<abc%", 4, "invalid character '\%' in rule");
    }
}

TEST_CASE("SyntaxRule Alternative")
{
    CHECK_RULE_NOTHROW("two_operands", "a|b");
    CHECK_RULE_NOTHROW("four_operands", "a|b|c|d");
    CHECK_RULE_NOTHROW("complex", "a|[b]|(c)|d?");

    CHECK_RULE_THROWS("bad_alt", "|",  0, "alternative '|' missing left operand");
    CHECK_RULE_THROWS("bad_alt", "a|", 2, "alternative '|' missing right operand");
    CHECK_RULE_THROWS("bad_alt", "|b", 0, "alternative '|' missing left operand");
}

TEST_CASE("SyntaxRule Postfix Operators")
{
    CHECK_RULE_NOTHROW("good_opt",      "a?");
    CHECK_RULE_NOTHROW("good_list",     "(a)+");
    CHECK_RULE_NOTHROW("good_opt_list", "[a]*");

    CHECK_RULE_THROWS("bad_opt",      "?",    0, "optional '?' missing left operand");
    CHECK_RULE_THROWS("bad_list",     "a?+*", 2, "list '+' missing left operand");
    CHECK_RULE_THROWS("bad_opt_list", "(*)",  1, "optional list '*' missing left operand");
}

TEST_CASE("SyntaxRule Basic Equivalence")
{
    SyntaxRule basic = SyntaxRule("apple", "red", false);

    SECTION("Equality Comparisons")
    {
        CHECK(basic == SyntaxRule("apple", "red", false));
        CHECK(basic != SyntaxRule("apple", "red", true));
        CHECK(basic != SyntaxRule("orange", "red", false));
        CHECK(basic != SyntaxRule("apple", "green", false));
    }

    SECTION("Copying and Pointer Comparison")
    {
        SyntaxRule basicCopy = SyntaxRule(basic);
        CHECK(&basic != &basicCopy);
        CHECK(basic == basicCopy);
    }
}

TEST_CASE("SyntaxRule Complex")
{
    using Con = SyntaxConstituent;
    using ListIndirect = remove_pointer_t<ValueType::List>;

    // Words segment using set and list: words '[_a-zA-Z ]+'
    Con const* wordsStr1 = Con::create<FieldFormat::String>("words '");
    Con const* wordsStr2 = Con::create<FieldFormat::String>("'");

    SetFunctor set;
    set.equals = "_% ";
    set.ranges = {{'a', 'z'}, {'A', 'Z'}};
    Con const* wordsSet = Con::create<FieldFormat::Set>(set);
    Con const* wordsSetList = Con::create<FieldFormat::List>(wordsSet);

    auto wordsSegment = new ListIndirect{wordsStr1, wordsSetList, wordsStr2};

    // Escaped question mark segment: {\?}
    Con const* token = Con::create<FieldFormat::String>("{?}");
    auto tokenSegment = new ListIndirect{token};

    // Symbols segment using rule: & <symbols>
    Con const* symbolsStr = Con::create<FieldFormat::String>("& ");
    Con const* symbolsRule = Con::create<FieldFormat::Rule>("symbols");
    auto symbolsSegment = new ListIndirect{symbolsStr, symbolsRule};

    // Alternative combines words, token and symbols segments.
    auto altList = new vector{wordsSegment, tokenSegment, symbolsSegment};
    Con const* alt = Con::create<FieldFormat::Alternative>(altList);
    auto altGroupList = new ListIndirect{alt};
    Con const* altGroup = Con::create<FieldFormat::Group>(altGroupList);
    
    // Zero colon segment using optional list: (0:)*
    Con const* zeroColon = Con::create<FieldFormat::String>("0:");
    auto zeroColonGroupList = new ListIndirect{zeroColon};
    Con const* zeroColonGroup = Con::create<FieldFormat::Group>(zeroColonGroupList);
    Con const* zeroColonOptList = Con::create<FieldFormat::OptionalList>(zeroColonGroup);

    // 'Anything' optional group combines zero colon, space and alternative group.    
    Con const* spaceStr = Con::create<FieldFormat::String>(" ");
    auto anythingGroupList = new ListIndirect{zeroColonOptList, spaceStr, altGroup};
    Con const* anythingGroup = Con::create<FieldFormat::Group>(anythingGroupList);
    Con const* anythingOpt = Con::create<FieldFormat::Optional>(anythingGroup);

    // Compiles everything into a single list of constituents that makes up the rule.
    Con const* anythingStr = Con::create<FieldFormat::String>("Anything :");
    auto expectedCons = new vector<SyntaxConstituent const*>{anythingStr, anythingOpt};

    auto expected = SyntaxRuleDummy("complex", expectedCons, false);
    
    auto actual = SyntaxRule("complex", "Anything :((0:)* (words '[_a-z\%A-Z ]+'|{\\?}|& <symbols>))?", false);
    auto close1 = SyntaxRule("complex", "Anything :((0:)+ (words '[_a-z\%A-Z ]+'|{\\?}|& <symbols>))?", false);
    auto close2 = SyntaxRule("complex", "Anything :((0:)* (words '[_a-y\%A-Z ]+'|{\\?}|& <symbols>))?", false);
    auto close3 = SyntaxRule("complex", "Anythang :((0:)* (words '[_a-z\%A-Z ]+'|{\\?}|& <symbols>))?", false);

    CHECK(actual == dynamic_cast<SyntaxRule const&>(expected));
    CHECK(close1 != dynamic_cast<SyntaxRule const&>(expected));
    CHECK(close2 != dynamic_cast<SyntaxRule const&>(expected));
    CHECK(close3 != dynamic_cast<SyntaxRule const&>(expected));
}