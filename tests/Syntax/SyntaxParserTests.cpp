#include <catch2/catch.hpp>

#include <any>

#include "ErrorMatchers.hpp"
#include "Matcher.hpp"
#include "MemoryUtils.hpp"
#include "MiscUtils.hpp"
#include "StdStringConversion.hpp"
#include "SyntaxRule.hpp"
#include "SyntaxParser.hpp"
#include "SyntaxParserDiagnostic.hpp"
#include "SyntaxParserError.hpp"
#include "Tree.tpp"
#include "TreeNode.tpp"
#include "TypeUtils.hpp"

using namespace std;
using DiagFormat = SyntaxParserDiagnostic::Format;
using ParsingError = SyntaxParserError;

MatcherWrapper<ParsingError> WrapParsingErrorContains(string const& match)
{
	return wrapMatcher<ParsingError>(MessageContainsMatcher<ParsingError>(match));
}

Tree<string> makeTree(vector<string> const& branch)
{
	assertMsg(!branch.empty(), "Syntax tree cannot be constructed from an empty branch.");
	Tree<string> tree = Tree<string>(branch[0]);
	TreeNode<string>* node = tree.getRoot();
	for (size_t i = 1; i < branch.size(); i++)
	{
		node = node->addChild(branch[i]);
	}
	return tree;
}

MatcherWrapper<ParsingError> parsingErrorMatcher(string const& rule, string const& query, size_t errPos, DiagFormat failType)
{
	if (failType == DiagFormat::Null)
	{
		return wrapMatcher<ParsingError>(FalseMatcher<ParsingError>());
	}

	MatcherWrapper<ParsingError> matchExpr = wrapMatcher<ParsingError>(TrueMatcher<ParsingError>());
	if (failType == DiagFormat::TrailingChars)
	{
		// Trailing characters list the position in the query string after which no rules are defined.
		matchExpr.andAssign(WrapParsingErrorContains("trailing characters"));
		matchExpr.andAssign(WrapParsingErrorContains(to_string(errPos)));
	}
	if (failType == DiagFormat::StringNoMatch || failType == DiagFormat::StringEnd ||
		failType == DiagFormat::SetNoMatch    || failType == DiagFormat::SetEnd ||
		failType == DiagFormat::Alternative)
	{
		// All non-trailing-character errors occur when parsing a particular rule.
		matchExpr.andAssign(WrapParsingErrorContains(rule));
	}
	if (failType == DiagFormat::StringNoMatch || failType == DiagFormat::SetNoMatch)
	{
		// No-match errors fail at a position in the query string and print the offending value.
		matchExpr.andAssign(WrapParsingErrorContains("query[" + to_string(errPos) + "]"));
		matchExpr.andAssign(WrapParsingErrorContains(MiscUtils::charToPrintableString(query[errPos])));
	}
	
	// TODO: If an alternative fails to parse due to an unmatched string, the error matcher should ensure
	// the format like 'expected abc' instead of 'expected to match set [a-z]' is displayed. This was removed
	// so error matchers can be chained without the 'no set' and 'has set' conditions conflicting.
	// Suggestion: Check the failed alternatives line-by-line, instead of the whole error message at once.

	if (failType == DiagFormat::SetNoMatch)
	{
		matchExpr.andAssign(WrapParsingErrorContains("set"));
	}
	if (failType == DiagFormat::StringEnd || failType == DiagFormat::SetEnd)
	{
		// When end of query string is reached, simply prints that it ended.
		matchExpr.andAssign(WrapParsingErrorContains("end"));
	}
	if (failType == DiagFormat::Alternative)
	{
		// Alternatives are listed recursively underneath first line.
		matchExpr.andAssign(WrapParsingErrorContains("alternatives"));
	}
	return matchExpr;
}

struct FailInfo
	{
	FailInfo(DiagFormat failType, size_t errPos, string failRule)
		: failType(failType), errPos(errPos), failRule(failRule) {}
	DiagFormat failType;
	size_t errPos;
	string failRule;
};

MatcherWrapper<ParsingError> alternativeParsingErrorMatcher(string const& rule,
	string const& query, vector<FailInfo> const& failList)
{
	MatcherWrapper<ParsingError> matchExpr = WrapParsingErrorContains("alternatives");
	matchExpr.andAssign(WrapParsingErrorContains(rule));
	for (auto const& failInfo : failList)
	{
		matchExpr.andAssign(parsingErrorMatcher(failInfo.failRule, query, failInfo.errPos, failInfo.failType));
	}
	return matchExpr;
}

#define CHECK_NESTED_RULE(rule, query, syntax, ...) \
	CHECK(SyntaxParser::parse(query, syntax, rule) == \
		makeTree({rule, __VA_ARGS__, query}));

#define CHECK_SINGLE_RULE(rule, query, syntax) \
	CHECK(SyntaxParser::parse(query, syntax, rule) == \
		makeTree({rule, query}));

#define CHECK_RULE_THROWS(rule, query, syntax, errPos, failType) \
	CHECK_THROWS_MATCHES(SyntaxParser::parse(query, syntax, rule), \
		ParsingError, parsingErrorMatcher(rule, query, errPos, failType));

#define CHECK_NESTED_RULE_THROWS(rule, query, syntax, errPos, failType, failRule) \
	CHECK_THROWS_MATCHES(SyntaxParser::parse(query, syntax, rule), \
		ParsingError, parsingErrorMatcher(failRule, query, errPos, failType));

#define CHECK_ALTERNATIVE_RULE_THROWS(rule, query, syntax, failRule, failList) \
	CHECK_THROWS_MATCHES(SyntaxParser::parse(query, syntax, rule), \
		ParsingError, alternativeParsingErrorMatcher(failRule, query, failList));

// Note: ASCII characters followed by a single backslash are interpreted in
// octal format for up to three digits afterwards.
// Control character limits are \0 (null) to \37 (unit separator), and \177 (delete).
const string ch0 = string(1, '\0');

const vector<SyntaxRule> syntaxRules = {
	// Plaintext
	SyntaxRule("readable_str", "azAZ .!}{~09", false),
	SyntaxRule("control_str", ch0 + "\1\37\177", false),
	SyntaxRule("escaped_str", "\\)\\(\\]\\[\\+\\*\\?\\\\", false),

	// Sets
	SyntaxRule("char_set", "[bde]+", false),
	SyntaxRule("fixed_sized_char_sets", "[ab][cd]", false),
	SyntaxRule("range_set", "[b-yB-Z1-8]+", false),
	SyntaxRule("fixed_sized_range_sets", "[a-z][0-9]", false),
	SyntaxRule("char_and_range_set", "[1-8bde]+", false),
	SyntaxRule("readable_set", "[ -~]+", false),
	SyntaxRule("control_set", "[" + ch0 + "-\37\177]+", false),
	SyntaxRule("escaped_set", "[\\-\\]\\[\\\\]+", false),
	SyntaxRule("symbol_set", "[)(+*?]+", false),
	SyntaxRule("complex_set", "[\\-0-9a-fBC-F ,]+", false),

	// Helper Sets
	SyntaxRule("lowercase", "[a-z]+", false),
	SyntaxRule("uppercase", "[A-Z]+", false),
	SyntaxRule("digits", "[0-9]+", false),

	// Rule Composition
	SyntaxRule("single_rule", "<lowercase>", false),
	SyntaxRule("bad_rule", "<non_existent>", false),
	SyntaxRule("multi_rule_together", "<lowercase><digits><uppercase>", false),
	SyntaxRule("multi_rule_apart", "Lower: <lowercase>, Digits: <digits> & Upper: <uppercase>.", false),
	SyntaxRule("nested_rule_child", "\\{<lowercase>\\}", false),
	SyntaxRule("nested_rule", "<nested_rule_child> <nested_rule_child>", false),

	// Anonymous Rules
	SyntaxRule("anon", "end", true),
	SyntaxRule("double_anon", "second <anon>", true),
	SyntaxRule("non_anon", "<double_anon>", false),
	SyntaxRule("complex_anon", "The <anon> before the <non_anon>.", true),

	// Lists and Optionals
	SyntaxRule("group_list", "(again and )+again", false),
	SyntaxRule("set_optional", "[\\-0]?", false),
	SyntaxRule("char_optional_list", "\\+*", false),
	SyntaxRule("alternative_list", "(yes |no |maybe )+", false),

	// Alternatives
	SyntaxRule("char_alt", "a|b", false),
	SyntaxRule("str_alt", "da|dum", false), // This should be effectively be the same as 'group_alt'.
	SyntaxRule("group_alt", "(da)|(dum)", false),
	SyntaxRule("multi_alt", "(hello|hi|bye) (friend|enemy)", false),

	// Complex rules for black box testing
	SyntaxRule("complex_sentence", "(a ((big|small|crazy) )*cow|the hippo(pota)+mus|some [a-z]+) ate (me alive|grass).?", false),
	SyntaxRule("complex_id", "id: (x<digits>|<lowercase>)", false),
	SyntaxRule("complex_pass", "pass: (<uppercase>|<lowercase>|<digits>)*", false),
	SyntaxRule("complex_logon", "Logon: <complex_id>, <complex_pass>", false)
};

auto syntax = MemoryUtils::mapItems<string, SyntaxRule>(syntaxRules,
	[](SyntaxRule const& rule){ return rule.getName(); });

TEST_CASE("SyntaxParser Plaintext")
{
	SECTION("Readable Plaintext")
	{
		CHECK_SINGLE_RULE("readable_str", "azAZ .!}{~09", syntax);

		// Empty string.
		CHECK_RULE_THROWS("readable_str", "", syntax, 0, DiagFormat::StringEnd);
	
		// Substituted plaintext characters.
		CHECK_RULE_THROWS("readable_str", "bzAZ .!}{~09", syntax, 0, DiagFormat::StringNoMatch);
		CHECK_RULE_THROWS("readable_str", "azAZ .!}{~0:", syntax, 11, DiagFormat::StringNoMatch);
		CHECK_RULE_THROWS("readable_str", "azAZ .?}{~09", syntax, 6, DiagFormat::StringNoMatch);

		// Missing characters.
		CHECK_RULE_THROWS("readable_str", "zAZ .!}{~09", syntax, 0, DiagFormat::StringNoMatch);
		CHECK_RULE_THROWS("readable_str", "azAZ .!}{~0", syntax, 11, DiagFormat::StringEnd);
		CHECK_RULE_THROWS("readable_str", "azAZ .}{~09", syntax, 6, DiagFormat::StringNoMatch);

		// Repeated characters.
		CHECK_RULE_THROWS("readable_str", "aazAZ .!}{~09", syntax, 1, DiagFormat::StringNoMatch);
		CHECK_RULE_THROWS("readable_str", "azAZ .!}{~099", syntax, 12, DiagFormat::TrailingChars);
		CHECK_RULE_THROWS("readable_str", "azAZ ..!}{~09", syntax, 6, DiagFormat::StringNoMatch);

		// Additional whitespace.
		CHECK_RULE_THROWS("readable_str", " azAZ .!}{~09", syntax, 0, DiagFormat::StringNoMatch);
		CHECK_RULE_THROWS("readable_str", "azAZ .!}{~09 ", syntax, 12, DiagFormat::TrailingChars);
		CHECK_RULE_THROWS("readable_str", "azAZ  .!}{~09", syntax, 5, DiagFormat::StringNoMatch);
		CHECK_RULE_THROWS("readable_str", "azAZ .!} {~09", syntax, 8, DiagFormat::StringNoMatch);

		// Control characters.
		CHECK_RULE_THROWS("readable_str", ch0,                  syntax, 0, DiagFormat::StringNoMatch);
		CHECK_RULE_THROWS("readable_str", "azAZ .!}{~09" + ch0, syntax, 12, DiagFormat::TrailingChars);
		CHECK_RULE_THROWS("readable_str", "azAZ\177.!}{~09",    syntax, 4, DiagFormat::StringNoMatch);
	}

	SECTION("Control Plaintext")
	{
		CHECK_SINGLE_RULE("control_str", ch0 + "\1\37\177", syntax);

		CHECK_RULE_THROWS("control_str", "\1\37\177",             syntax, 0, DiagFormat::StringNoMatch);
		CHECK_RULE_THROWS("control_str", ch0 + "\1\37\177" + ch0, syntax, 4, DiagFormat::TrailingChars);
		// ' ': ASCII 40. Proceeds \37.
		CHECK_RULE_THROWS("control_str", ch0 + "\1 \177",         syntax, 2, DiagFormat::StringNoMatch);
		// '~': ASCII 176. Preceeds \177.
		CHECK_RULE_THROWS("control_str", ch0 + "\1\37" + "~",     syntax, 3, DiagFormat::StringNoMatch);
	}

	SECTION("Escaped Plaintext")
	{
		CHECK_SINGLE_RULE("escaped_str", ")(][+*?\\", syntax);

		CHECK_RULE_THROWS("escaped_str", "&)(][+*?\\",  syntax, 0, DiagFormat::StringNoMatch);
		CHECK_RULE_THROWS("escaped_str", ")(][+*?\\^",  syntax, 8, DiagFormat::TrailingChars);
		CHECK_RULE_THROWS("escaped_str", ")(][*+?\\",   syntax, 4, DiagFormat::StringNoMatch);
		CHECK_RULE_THROWS("escaped_str", ")(]\\[*+?\\", syntax, 3, DiagFormat::StringNoMatch);
	}
}

TEST_CASE("SyntaxParser Set")
{
	SECTION("Character Set")
	{
		CHECK_SINGLE_RULE("char_set", "bdeeebed", syntax);

		// Bounds checking for valid b, d, and e characters.
		CHECK_RULE_THROWS("char_set", "beabd", syntax, 2, DiagFormat::TrailingChars);
		CHECK_RULE_THROWS("char_set", "cbded", syntax, 0, DiagFormat::SetNoMatch);
		CHECK_RULE_THROWS("char_set", "bdeef", syntax, 4, DiagFormat::TrailingChars);
		CHECK_RULE_THROWS("char_set", "bdee#", syntax, 4, DiagFormat::TrailingChars);
	}

	SECTION("Fixed Sized Character Sets")
	{
		CHECK_SINGLE_RULE("fixed_sized_char_sets", "ad", syntax);
		CHECK_SINGLE_RULE("fixed_sized_char_sets", "bc", syntax);

		// Invalid characters.
		CHECK_RULE_THROWS("fixed_sized_char_sets", "cd", syntax, 0, DiagFormat::SetNoMatch);
		CHECK_RULE_THROWS("fixed_sized_char_sets", "ae", syntax, 1, DiagFormat::SetNoMatch);

		// Query string finishes when set item is expected.
		CHECK_RULE_THROWS("fixed_sized_char_sets", "", syntax, 0, DiagFormat::SetEnd);
		CHECK_RULE_THROWS("fixed_sized_char_sets", "a", syntax, 1, DiagFormat::SetEnd);
	}

	SECTION("Range Set")
	{
		CHECK_SINGLE_RULE("range_set", "6bBF12ufoyE8UY", syntax);

		// Bounds checking for b-y, B-Y and 1-8 ranges.
		CHECK_RULE_THROWS("range_set", "abBFU1O", syntax, 0, DiagFormat::SetNoMatch);
		CHECK_RULE_THROWS("range_set", "bBFU1O9", syntax, 6, DiagFormat::TrailingChars);
		CHECK_RULE_THROWS("range_set", "bBFAU1O", syntax, 3, DiagFormat::TrailingChars);
		CHECK_RULE_THROWS("range_set", "bBF?U1O", syntax, 3, DiagFormat::TrailingChars);
	}

	SECTION("Fixed Sized Character Sets")
	{
		CHECK_SINGLE_RULE("fixed_sized_range_sets", "a9", syntax);
		CHECK_SINGLE_RULE("fixed_sized_range_sets", "z0", syntax);
		CHECK_SINGLE_RULE("fixed_sized_range_sets", "t1", syntax);

		// Bounds checking for a-z and 0-9. '{' proceeds 'z', '/' preceeds '0'.
		CHECK_RULE_THROWS("fixed_sized_range_sets", "(3", syntax, 0, DiagFormat::SetNoMatch);
		CHECK_RULE_THROWS("fixed_sized_range_sets", "f/", syntax, 1, DiagFormat::SetNoMatch);

		// Query string finishes when set item is expected.
		CHECK_RULE_THROWS("fixed_sized_range_sets", "", syntax, 0, DiagFormat::SetEnd);
		CHECK_RULE_THROWS("fixed_sized_range_sets", "a", syntax, 1, DiagFormat::SetEnd);
	}

	SECTION("Character and Range Set")
	{
		CHECK_SINGLE_RULE("char_and_range_set", "be87de15", syntax);

		// Bounds checking for b, d and e characters and 1-8 range.
		CHECK_RULE_THROWS("char_and_range_set", "ce87de15", syntax, 0, DiagFormat::SetNoMatch);
		CHECK_RULE_THROWS("char_and_range_set", "be87de10", syntax, 7, DiagFormat::TrailingChars);
		CHECK_RULE_THROWS("char_and_range_set", "be97de15", syntax, 2, DiagFormat::TrailingChars);
		CHECK_RULE_THROWS("char_and_range_set", "be87De15", syntax, 4, DiagFormat::TrailingChars);
	}

	SECTION("Readable Set")
	{
		CHECK_SINGLE_RULE("readable_set", "B@d! A[pp]-1es~", syntax);

		// Bounds checking for readable ASCII characters.
		CHECK_RULE_THROWS("readable_set", "\177~ ", syntax, 0, DiagFormat::SetNoMatch);
		CHECK_RULE_THROWS("readable_set", "~ \37",  syntax, 2, DiagFormat::TrailingChars);
		CHECK_RULE_THROWS("readable_set", "~\20 ",  syntax, 1, DiagFormat::TrailingChars);
	}

	SECTION("Control Set")
	{
		CHECK_SINGLE_RULE("control_set", "\1\37\20" + ch0 + "\177\36", syntax);

		// Bounds checking for control ASCII characters. ' ' is ASCII character 38.
		CHECK_RULE_THROWS("control_set", " \1\37",           syntax, 0, DiagFormat::SetNoMatch);
		CHECK_RULE_THROWS("control_set", "\177\176",         syntax, 1, DiagFormat::TrailingChars);
		CHECK_RULE_THROWS("control_set", ch0 + "\177\70\37", syntax, 2, DiagFormat::TrailingChars);
	}

	SECTION("Escaped Set")
	{
		CHECK_SINGLE_RULE("escaped_set", "-]\\[]-\\", syntax);

		// Bounds checking for escaped ASCII characters.
		CHECK_RULE_THROWS("escaped_set", "Z[-\\]", syntax, 0, DiagFormat::SetNoMatch);
		CHECK_RULE_THROWS("escaped_set", "[-\\]^", syntax, 4, DiagFormat::TrailingChars);
		CHECK_RULE_THROWS("escaped_set", "[-,\\]", syntax, 2, DiagFormat::TrailingChars);
	}

	SECTION("Symbol Set")
	{
		CHECK_SINGLE_RULE("symbol_set", ")(*?)+*", syntax);

		// Bounds checking for characters not in symbol set.
		CHECK_RULE_THROWS("symbol_set", "&)+", syntax, 0, DiagFormat::SetNoMatch);
		CHECK_RULE_THROWS("symbol_set", "(*%?)", syntax, 2, DiagFormat::TrailingChars);
		CHECK_RULE_THROWS("symbol_set", "(*8", syntax, 2, DiagFormat::TrailingChars);
	}

	SECTION("Complex Set")
	{
		CHECK_SINGLE_RULE("complex_set", "-- 059, adf, BDF --", syntax);

		// Bounds checking for escaped ASCII characters.
		CHECK_RULE_THROWS("complex_set", "-- 059, adf, ACF --", syntax, 13, DiagFormat::TrailingChars);
		CHECK_RULE_THROWS("complex_set", "-- 059, adg, BCF --", syntax, 10, DiagFormat::TrailingChars);
		CHECK_RULE_THROWS("complex_set", "-- 059. adf, BCF --", syntax, 6, DiagFormat::TrailingChars);
	}
}

TEST_CASE("SyntaxParser Non-anonymous Subrules")
{
	SECTION("Single Rule")
	{
		CHECK_NESTED_RULE("single_rule", "apples", syntax, "lowercase");

		CHECK_NESTED_RULE_THROWS("single_rule", "Apples", syntax, 0, DiagFormat::SetNoMatch, "lowercase");
		CHECK_NESTED_RULE_THROWS("single_rule", "appl3s", syntax, 4, DiagFormat::TrailingChars, "lowercase");
	}

	SECTION("Non-Existent Rule Fails")
	{
		CHECK_THROWS_MATCHES(SyntaxParser::parse("literally anything should throw", syntax, "bad_rule"), invalid_argument, Catch::Message("No rule found of the name 'non_existent'."));
	}

	SECTION("Multiple Rules Together")
	{
		Tree<string> tree = Tree<string>("multi_rule_together");
		TreeNode<string>* node = tree.getRoot();
		node->addChild("lowercase")->addChild("apples");
		node->addChild("digits")->addChild("94");
		node->addChild("uppercase")->addChild("BANANAS");

		CHECK(SyntaxParser::parse("apples94BANANAS", syntax, "multi_rule_together") == tree);

		CHECK_NESTED_RULE_THROWS("multi_rule_together", "Apples94BANANAS",  syntax, 0, DiagFormat::SetNoMatch, "lowercase");
		CHECK_NESTED_RULE_THROWS("multi_rule_together", "apples94 BANANAS", syntax, 8, DiagFormat::SetNoMatch, "uppercase");
	}

	SECTION("Multiple Rules Apart")
	{
		Tree<string> tree = Tree<string>("multi_rule_apart");
		TreeNode<string>* node = tree.getRoot();
		node->addChild("Lower: ");
		node->addChild("lowercase")->addChild("apples");
		node->addChild(", Digits: ");
		node->addChild("digits")->addChild("94");
		node->addChild(" & Upper: ");
		node->addChild("uppercase")->addChild("BANANAS");
		node->addChild(".");

		CHECK(SyntaxParser::parse("Lower: apples, Digits: 94 & Upper: BANANAS.",
			syntax, "multi_rule_apart") == tree);

		CHECK_RULE_THROWS("multi_rule_apart", "Lo#er: apples, Digits: 94 & Upper: BANANAS.", syntax, 2,  DiagFormat::StringNoMatch);
		CHECK_RULE_THROWS("multi_rule_apart", "Lower: apples, Digits: 94& Upper: BANANAS.",  syntax, 25, DiagFormat::StringNoMatch);
		CHECK_RULE_THROWS("multi_rule_apart", "Lower: apples, Digits: 94 & Upper: BANANAS",  syntax, 41, DiagFormat::StringEnd);
	}

	SECTION("Nested Rules")
	{
		Tree<string> tree = Tree<string>("nested_rule");
		TreeNode<string>* parent = tree.getRoot();
		
		TreeNode<string>* child = parent->addChild("nested_rule_child");
		child->addChild("{");
		child->addChild("lowercase")->addChild("first");
		child->addChild("}");

		parent->addChild(" ");

		child = parent->addChild("nested_rule_child");
		child->addChild("{");
		child->addChild("lowercase")->addChild("second");
		child->addChild("}");

		CHECK(SyntaxParser::parse("{first} {second}", syntax, "nested_rule") == tree);

		CHECK_NESTED_RULE_THROWS("nested_rule", "{first] {second}", syntax, 6,  DiagFormat::StringNoMatch, "nested_rule_child");
		CHECK_NESTED_RULE_THROWS("nested_rule", "{first} {sec0nd}", syntax, 12, DiagFormat::StringNoMatch, "nested_rule_child");
	}
}

TEST_CASE("SyntaxParser Anonymous Subrules")
{
	SECTION("Single Anonymous Rule")
	{
		Tree<string> tree = Tree<string>("end");

		CHECK(SyntaxParser::parse("end", syntax, "anon") == tree);

		CHECK_RULE_THROWS("anon", "not the end", syntax, 0, DiagFormat::StringNoMatch);
	}

	SECTION("Nested Anonymous Rule")
	{
		Tree<string> tree = Tree<string>("second end");

		CHECK(SyntaxParser::parse("second end", syntax, "double_anon") == tree);

		CHECK_NESTED_RULE_THROWS("double_anon", "first end",  syntax, 0, DiagFormat::StringNoMatch, "double_anon");
		CHECK_NESTED_RULE_THROWS("double_anon", "second enD", syntax, 9, DiagFormat::StringNoMatch, "anon");
	}

	SECTION("Complex Anonymous Rule")
	{
		// An anonymous rule adopts the child's value unless
		// there is more than one child node.
		Tree<string> tree = Tree<string>("complex_anon");
		TreeNode<string>* root = tree.getRoot();
		root->addChild("The end before the ");
		root->addChild("non_anon")->addChild("second end");
		root->addChild(".");

		CHECK(SyntaxParser::parse("The end before the second end.", syntax, "complex_anon") == tree);

		CHECK_NESTED_RULE_THROWS("complex_anon", "The start before the second end.", syntax, 4,  DiagFormat::StringNoMatch, "anon");
		CHECK_NESTED_RULE_THROWS("complex_anon", "The end before the secondend.",    syntax, 25, DiagFormat::StringNoMatch, "double_anon");
		CHECK_NESTED_RULE_THROWS("complex_anon", "second end.",                      syntax, 0,  DiagFormat::StringNoMatch,  "complex_anon");
	}
}

TEST_CASE("SyntaxParser Postfix Operators")
{
	SECTION("List of Group")
	{
		CHECK_SINGLE_RULE("group_list", "again and again", syntax);
		CHECK_SINGLE_RULE("group_list", "again and again and again and again", syntax);

		CHECK_RULE_THROWS("group_list", "again",           syntax, 0,  DiagFormat::StringEnd);
		CHECK_RULE_THROWS("group_list", "again and akain", syntax, 11, DiagFormat::StringNoMatch);
	}

	SECTION("Optional of Set")
	{
		CHECK_SINGLE_RULE("set_optional", "-", syntax);
		CHECK(SyntaxParser::parse("", syntax, "set_optional") == Tree<string>(""));

		CHECK_RULE_THROWS("set_optional", "--", syntax, 1, DiagFormat::TrailingChars);
		CHECK_RULE_THROWS("set_optional", "1",  syntax, 0, DiagFormat::TrailingChars);
	}

	SECTION("Optional List of Single Escaped Character")
	{
		CHECK(SyntaxParser::parse("", syntax, "char_optional_list") == Tree<string>(""));
		CHECK_SINGLE_RULE("char_optional_list", "+", syntax);
		CHECK_SINGLE_RULE("char_optional_list", "+++++", syntax);

		CHECK_RULE_THROWS("char_optional_list", "-",     syntax, 0, DiagFormat::TrailingChars);
		CHECK_RULE_THROWS("char_optional_list", "++++-", syntax, 4, DiagFormat::TrailingChars);
	}

	SECTION("List of Grouped Alternative")
	{
		CHECK_SINGLE_RULE("alternative_list", "yes no maybe no yes ", syntax);
		CHECK_SINGLE_RULE("alternative_list", "maybe ", syntax);
		CHECK_SINGLE_RULE("alternative_list", "yes yes yes no ", syntax);

		CHECK_RULE_THROWS("alternative_list", "",                  syntax, 0, DiagFormat::StringEnd);
		CHECK_RULE_THROWS("alternative_list", "maybe yes no why ", syntax, 13, DiagFormat::TrailingChars);
	}
}

TEST_CASE("SyntaxParser Alternatives")
{
	SECTION("Single-Character Alternative")
	{
		CHECK_SINGLE_RULE("char_alt", "a", syntax);
		CHECK_SINGLE_RULE("char_alt", "b", syntax);

		CHECK_RULE_THROWS("char_alt", "c", syntax, 0, DiagFormat::Alternative);
	}

	SECTION("String and Grouped Alternative")
	{
		// Ungrouped alternative da|dum is equivalent to grouped alternative (da)|(dum).

		CHECK_SINGLE_RULE("str_alt", "da", syntax);
		CHECK_SINGLE_RULE("group_alt", "dum", syntax);

		CHECK_RULE_THROWS("str_alt",   "d",   syntax, 0, DiagFormat::Alternative);
		CHECK_RULE_THROWS("group_alt", "dam", syntax, 2, DiagFormat::TrailingChars);
		CHECK_RULE_THROWS("str_alt",   "dun", syntax, 2, DiagFormat::Alternative);
	}

	SECTION("Multiple-Choice Alternative")
	{
		CHECK_SINGLE_RULE("multi_alt", "hello friend", syntax);
		CHECK_SINGLE_RULE("multi_alt", "bye enemy", syntax);
		CHECK_SINGLE_RULE("multi_alt", "hi friend", syntax);

		CHECK_RULE_THROWS("multi_alt", "by3 friend", syntax, 2, DiagFormat::Alternative);
		CHECK_RULE_THROWS("multi_alt", "hi ene",     syntax, 3, DiagFormat::Alternative);
		CHECK_RULE_THROWS("multi_alt", " friend",    syntax, 0, DiagFormat::Alternative);
	}
}

TEST_CASE("SyntaxParser Complex")
{
	SECTION("Complex Sentence with No Rule")
	{
		CHECK_SINGLE_RULE("complex_sentence", "a cow ate grass", syntax);
		CHECK_SINGLE_RULE("complex_sentence", "a small big crazy small cow ate me alive.", syntax);
		CHECK_SINGLE_RULE("complex_sentence", "the hippopotapotamus ate me alive", syntax);
		CHECK_SINGLE_RULE("complex_sentence", "some freak ate grass.", syntax);

		// Non-alternative patterns fail when alterantives are fulfilled.
		CHECK_RULE_THROWS("complex_sentence", "the hippopotapotamus ate me alive..", syntax, 34, DiagFormat::TrailingChars);
		CHECK_RULE_THROWS("complex_sentence", "a cow  ate me alive.", syntax, 6, DiagFormat::StringNoMatch);
		CHECK_RULE_THROWS("complex_sentence", "some parRot ate grass", syntax, 8, DiagFormat::StringNoMatch);

		// All alternatives in a group fail: Nested error messages are generated.
		CHECK_ALTERNATIVE_RULE_THROWS("complex_sentence", "the hippomus ate grass", syntax, "complex_sentence",
			(vector<FailInfo>{
				FailInfo(DiagFormat::StringNoMatch, 0, "complex_sentence"),
				FailInfo(DiagFormat::StringNoMatch, 9, "complex_sentence"),
				FailInfo(DiagFormat::StringNoMatch, 0, "complex_sentence")
			}));
		CHECK_ALTERNATIVE_RULE_THROWS("complex_sentence", "a cow ate me allive.", syntax, "complex_sentence",
			(vector<FailInfo>{
				FailInfo(DiagFormat::StringNoMatch, 15, "complex_sentence"),
				FailInfo(DiagFormat::StringNoMatch, 10, "complex_sentence")
			}));
		CHECK_ALTERNATIVE_RULE_THROWS("complex_sentence", "some Dinosaur ate me alive.", syntax, "complex_sentence",
			(vector<FailInfo>{
				FailInfo(DiagFormat::StringNoMatch, 0, "complex_sentence"),
				FailInfo(DiagFormat::StringNoMatch, 0, "complex_sentence"),
				FailInfo(DiagFormat::SetNoMatch,    5, "complex_sentence")
			}));
	}

	SECTION("Complex Rule with Nested Subrules")
	{
		auto expBase = Tree<string>("complex_logon");
		auto root = expBase.getRoot();
		root->addChild("Logon: ");
		auto complexId = root->addChild("complex_id");
		complexId->addChild("id: ");
		root->addChild(", ");
		auto complexPass = root->addChild("complex_pass");
		complexPass->addChild("pass: ");

		auto exp1 = Tree<string>(expBase);
		complexId = exp1.getRoot()->getChildWithKey("complex_id", 0);
		complexId->addChild("lowercase")->addChild("admin");

		auto exp2 = Tree<string>(expBase);
		complexId = exp2.getRoot()->getChildWithKey("complex_id", 0);
		complexId->addChild("lowercase")->addChild("puttyput");
		complexPass = exp2.getRoot()->getChildWithKey("complex_pass", 0);
		complexPass->addChild("uppercase")->addChild("C");
		complexPass->addChild("lowercase")->addChild("razy");
		complexPass->addChild("uppercase")->addChild("MICE");
		complexPass->addChild("digits")->addChild("83");

		auto exp3 = Tree<string>(expBase);
		complexId = exp3.getRoot()->getChildWithKey("complex_id", 0);
		complexId->getChildWithKey("id: ", 0)->setItem("id: x");
		complexId->addChild("digits")->addChild("800");
		complexPass = exp3.getRoot()->getChildWithKey("complex_pass", 0);
		complexPass->addChild("lowercase")->addChild("first");
		complexPass->addChild("digits")->addChild("1");
		complexPass->addChild("lowercase")->addChild("m");
		complexPass->addChild("digits")->addChild("8");

		CHECK(SyntaxParser::parse("Logon: id: admin, pass: ", syntax, "complex_logon") == exp1);
		CHECK(SyntaxParser::parse("Logon: id: puttyput, pass: CrazyMICE83", syntax, "complex_logon") == exp2);
		CHECK(SyntaxParser::parse("Logon: id: x800, pass: first1m8", syntax, "complex_logon") == exp3);

		CHECK_NESTED_RULE_THROWS("complex_logon", "Logon: id: puttyput, pass CrazyMICE83", syntax, 25, DiagFormat::StringNoMatch, "complex_pass");
		CHECK_RULE_THROWS("complex_logon", "Logon: id: x800, pass: first1m8.", syntax, 31, DiagFormat::TrailingChars);
		CHECK_RULE_THROWS("complex_logon", "Login: id: puttyput, pass: CrazyMICE83", syntax, 3,  DiagFormat::StringNoMatch);

		CHECK_ALTERNATIVE_RULE_THROWS("complex_logon", "Logon: id: 800, pass: first1m8", syntax, "complex_id",
			(vector<FailInfo>{
				FailInfo(DiagFormat::StringNoMatch, 11, "complex_id"),
				FailInfo(DiagFormat::SetNoMatch,    11, "lowercase")
			}));
	}
}
