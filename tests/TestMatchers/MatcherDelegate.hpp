//
//  MatcherDelegate.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 12/03/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef MATCHER_DELEGATE_HPP
#define MATCHER_DELEGATE_HPP

#include <catch2/catch.hpp>

#include "MatcherHandler.hpp"

#include <any>

/** Pre-declarations **/

template<typename ArgT>
class MatcherDelegate;
template<typename ArgT>
void swap(MatcherDelegate<ArgT>&, MatcherDelegate<ArgT>&);


/** Matcher Delegate Definition **/

template<typename ArgT>
class MatcherDelegate : public Catch::MatcherBase<ArgT>
{
public:
    /** Constructors **/

    MatcherDelegate();

    template<typename Matcher>
    static MatcherDelegate make(std::enable_if_t<
        std::is_base_of_v<Catch::MatcherBase<ArgT>, Matcher>, std::any*> matcher);    

    /** Copy and Move Semantics **/

    // NOTE: Cannot deep copy internally because the matcher is a reference to an external object.
    MatcherDelegate copyWithNewMatcher(std::any* matcher) const;

    friend void swap<>(MatcherDelegate&, MatcherDelegate&);
    MatcherDelegate(MatcherDelegate const& other);
    MatcherDelegate(MatcherDelegate&& other);
    MatcherDelegate& operator=(MatcherDelegate other);


    /** Matcher Interface **/

    bool match(ArgT const& val) const;
    std::string describe() const;

protected:
    MatcherMatchHandler<ArgT> handleMatch;
    MatcherDescribeHandler handleDescribe;
};

/** Constructors **/

template<typename ArgT>
MatcherDelegate<ArgT>::MatcherDelegate() {}

template<typename ArgT>
template<typename Matcher>
MatcherDelegate<ArgT> MatcherDelegate<ArgT>::make(std::enable_if_t<
    std::is_base_of_v<Catch::MatcherBase<ArgT>, Matcher>, std::any*> matcher)
{
    MatcherDelegate newDelegate;

    typename MatcherMatchHandler<ArgT>::HandlerT matchHandler =
        [](std::any const* matcher, ArgT const& val) -> bool
        { return std::any_cast<Matcher const>(matcher)->match(val); };
    newDelegate.handleMatch = makeMatcherHandler<MatcherMatchHandler<ArgT>>(
        matcher, matchHandler);
    
    MatcherDescribeHandler::HandlerT describeHandler =
        [](std::any const* matcher) -> std::string
        { return std::any_cast<Matcher const>(matcher)->describe(); };
    newDelegate.handleDescribe = makeMatcherHandler<MatcherDescribeHandler>(
        matcher, describeHandler);

    return move(newDelegate);
}


/** Copy and Move Semantics **/

// NOTE: Cannot deep copy internally because the matcher is a reference to an external object.
template<typename ArgT>
MatcherDelegate<ArgT> MatcherDelegate<ArgT>::copyWithNewMatcher(std::any* matcher) const
{
    MatcherDelegate newDelegate;
    newDelegate.handleMatch = copyMatcherHandlerWithNewMatcher<
        MatcherMatchHandler<ArgT>>(this->handleMatch, matcher);
    newDelegate.handleDescribe = copyMatcherHandlerWithNewMatcher<
        MatcherDescribeHandler>(this->handleDescribe, matcher);
    return newDelegate;
}

template<typename ArgT>
void swap(MatcherDelegate<ArgT>& first, MatcherDelegate<ArgT>& second)
{
    using std::swap;
    using MatchHandlerBaseT = MatcherHandler<typename MatcherMatchHandler<ArgT>::HandlerT>;
    using DescribeHandlerBaseT = MatcherHandler<typename MatcherDescribeHandler::HandlerT>;

    swap(dynamic_cast<MatchHandlerBaseT&>(first.handleMatch),
         dynamic_cast<MatchHandlerBaseT&>(second.handleMatch));
    swap(dynamic_cast<DescribeHandlerBaseT&>(first.handleDescribe),
         dynamic_cast<DescribeHandlerBaseT&>(second.handleDescribe));
}

template<typename ArgT>
MatcherDelegate<ArgT>::MatcherDelegate(MatcherDelegate const& other)
{
    this->handleMatch = other.handleMatch;
    this->handleDescribe = other.handleDescribe;
}

template<typename ArgT>
MatcherDelegate<ArgT>::MatcherDelegate(MatcherDelegate&& other)
{
    swap(*this, other);
}

template<typename ArgT>
MatcherDelegate<ArgT>& MatcherDelegate<ArgT>::operator=(MatcherDelegate other)
{
    swap(*this, other);
    return *this;
}


/** Matcher Interface **/

template<typename ArgT>
bool MatcherDelegate<ArgT>::match(ArgT const& val) const
{
    return handleMatch(val);
}

template<typename ArgT>
std::string MatcherDelegate<ArgT>::describe() const
{
    return handleDescribe();
}

#endif /* MATCHER_DELEGATE_HPP */
