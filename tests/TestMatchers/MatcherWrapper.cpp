//
//  MatcherWrapper.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 12/03/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "MatcherWrapper.hpp"

using namespace std;

std::invalid_argument andUnwrappedMatcherError()
{
    return std::invalid_argument("Cannot call 'and' on a MatcherWrapper with an unwrapped Matcher.");
}

std::invalid_argument orUnwrappedMatcherError()
{
    return std::invalid_argument("Cannot call 'or' on a MatcherWrapper with an unwrapped Matcher.");
}
