//
//  BooleanMatchers.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 21/3/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef BOOLEAN_MATCHERS_HPP
#define BOOLEAN_MATCHERS_HPP

#include <catch2/catch.hpp>

#include <string>

/** Boolean Matcher Class Definitions **/

/* A matcher with a set value of either true or false. Used for initialising a
   series of logical operations on matchers. */
template<typename ArgT>
class BooleanMatcher : public Catch::MatcherBase<ArgT>
{
public:
    BooleanMatcher(bool value);

    bool match(ArgT const&) const;
    virtual std::string describe() const;

private:
    bool value;
};

/* A boolean matcher with the value true. */
template<typename ArgT>
class TrueMatcher : public BooleanMatcher<ArgT>
{
public:
    TrueMatcher() : BooleanMatcher<ArgT>(true) {}
};

/* A boolean matcher with the value false. */
template<typename ArgT>
class FalseMatcher : public BooleanMatcher<ArgT>
{
public:
    FalseMatcher() : BooleanMatcher<ArgT>(false) {}
};


/** Boolean Matcher Class Implementations **/

template<typename ArgT>
BooleanMatcher<ArgT>::BooleanMatcher(bool value) : value(value) {}

template<typename ArgT>
bool BooleanMatcher<ArgT>::match(ArgT const&) const
{
    return value;
}

template<typename ArgT>
std::string BooleanMatcher<ArgT>::describe() const
{
    return value ? "true" : "false";
}


/** True and False Matcher Constructor Wrappers **/

template<typename ArgT>
TrueMatcher<ArgT> MatchesTrue()
{
    return TrueMatcher<ArgT>();
}

template<typename ArgT>
FalseMatcher<ArgT> MatchesFalse()
{
    return FalseMatcher<ArgT>();
}

#endif /* BOOLEAN_MATCHERS_HPP */
