//
//  MatcherWrapper.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 12/03/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef MATCHER_WRAPPER_HPP
#define MATCHER_WRAPPER_HPP

#include <catch2/catch.hpp>

#include <any>
#include <exception>

/** Pre-declarations **/

template<typename ArgT> class DynamicMatchAllOf;
template<typename ArgT> class DynamicMatchAnyOf;
template<typename ArgT> class DynamicMatchNotOf;
template<typename ArgT> class MatcherDelegate;
template<typename ArgT> class MatcherWrapper;

template<typename ArgT>
void swap(MatcherWrapper<ArgT>&, MatcherWrapper<ArgT>&);


/** Matcher Type Queries **/

template<typename ArgT, typename MatcherT>
constexpr bool IsMatcherV = std::is_base_of_v<Catch::MatcherBase<ArgT>, MatcherT>;

template<typename ArgT, typename MatcherT>
constexpr bool IsWrappedMatcherV = std::is_same_v<MatcherT, MatcherWrapper<ArgT>>;

template<typename ArgT, typename MatcherT>
constexpr bool IsUnwrappedMatcherV = IsMatcherV<ArgT, MatcherT> && !IsWrappedMatcherV<ArgT, MatcherT>;


/** Matcher Wrapper Implementation **/

/* Stores a matcher internally and wraps its behaviour.
   Supports copy, assignment and move operations.
   Supports 'dynamic' union, intersection and inverse operations through
   the &&, || and ! operators, without altering currently existing matchers. */
template<typename ArgT>
class MatcherWrapper : public Catch::MatcherBase<ArgT>
{
public:
    /** Constructors and Destructors **/

    template<typename Matcher>
    static std::enable_if_t<
        std::is_base_of_v<Catch::MatcherBase<ArgT>, std::decay_t<Matcher>>,
        MatcherWrapper<ArgT>> make(Matcher&& matcher);


    /** Copy and Move Semantics **/

    friend void swap<>(MatcherWrapper&, MatcherWrapper&);
    MatcherWrapper(MatcherWrapper<ArgT> const& other);
    MatcherWrapper(MatcherWrapper<ArgT>&& other);
    MatcherWrapper<ArgT>& operator=(MatcherWrapper<ArgT> other);


    /** Matcher Wrapper Interface **/

    template<typename MatcherT>
    std::enable_if_t<
        std::is_base_of_v<Catch::MatcherBase<ArgT>, std::decay_t<MatcherT>>,
        void> setUnderlyingMatcher(MatcherT&& matcher);


    /** Matcher Interface **/

    bool match(ArgT const& val) const;
    std::string describe() const;


    /** Matcher Intersection **/

    void andAssign(MatcherWrapper<ArgT> const* andMatcher);

    template<typename ParamT>
    std::enable_if_t<IsWrappedMatcherV<ArgT, std::decay_t<ParamT>>,
        void> andAssign(ParamT&& andMatcher);


    /** Matcher Union **/

    void orAssign(MatcherWrapper<ArgT> const* orMatcher);

    template<typename ParamT>
    std::enable_if_t<IsWrappedMatcherV<ArgT, std::decay_t<ParamT>>,
        void> orAssign(ParamT&& orMatcher);


    /** Matcher Inverse **/

    void notMatcher();

protected:
    std::any underlyingMatcher;
    MatcherDelegate<ArgT> matcherDelegate;

    MatcherWrapper();
};


/** Constructors and Destructors **/

template<typename ArgT>
MatcherWrapper<ArgT>::MatcherWrapper() {}

template<typename ArgT>
template<typename Matcher>
std::enable_if_t<std::is_base_of_v<Catch::MatcherBase<ArgT>, std::decay_t<Matcher>>, MatcherWrapper<ArgT>>
    MatcherWrapper<ArgT>::make(Matcher&& matcher)
{
    auto wrapper = MatcherWrapper<ArgT>();
    wrapper.setUnderlyingMatcher(std::forward<Matcher>(matcher));
    return wrapper;
}


/** Copy and Move Semantics **/

template<typename ArgT>
void swap(MatcherWrapper<ArgT>& first, MatcherWrapper<ArgT>& second)
{
    using std::swap;

    swap(first.underlyingMatcher, second.underlyingMatcher);
    
    MatcherDelegate<ArgT> tempDelegate = first.matcherDelegate;
    first.matcherDelegate = second.matcherDelegate.copyWithNewMatcher(&first.underlyingMatcher);
    second.matcherDelegate = tempDelegate.copyWithNewMatcher(&second.underlyingMatcher);
}

template<typename ArgT>
MatcherWrapper<ArgT>::MatcherWrapper(MatcherWrapper<ArgT> const& other)
{
    this->underlyingMatcher = other.underlyingMatcher;
    this->matcherDelegate = other.matcherDelegate.copyWithNewMatcher(&this->underlyingMatcher);
}

template<typename ArgT>
MatcherWrapper<ArgT>::MatcherWrapper(MatcherWrapper<ArgT>&& other)
{
    swap(*this, other);
}

template<typename ArgT>
MatcherWrapper<ArgT>& MatcherWrapper<ArgT>::operator=(MatcherWrapper<ArgT> other)
{
    swap(*this, other);
    return *this;
}


/** Matcher Wrapper Interface **/

template<typename ArgT>
template<typename MatcherT>
std::enable_if_t<
    std::is_base_of_v<Catch::MatcherBase<ArgT>, std::decay_t<MatcherT>>,
    void> MatcherWrapper<ArgT>::setUnderlyingMatcher(MatcherT&& matcher)
{
    this->underlyingMatcher = std::forward<MatcherT>(matcher);
    this->matcherDelegate = MatcherDelegate<ArgT>::template make<std::decay_t<MatcherT>>(&this->underlyingMatcher);
}


/** Matcher Interface **/

template<typename ArgT>
bool MatcherWrapper<ArgT>::match(ArgT const& val) const
{
    return matcherDelegate.match(val);
}

template<typename ArgT>
std::string MatcherWrapper<ArgT>::describe() const
{
    return matcherDelegate.describe();
}


/** Matcher Intersection **/

template<typename ArgT>
void MatcherWrapper<ArgT>::andAssign(MatcherWrapper<ArgT> const* andMatcher)
{
    if (typeid(DynamicMatchAllOf<ArgT>) == underlyingMatcher.type())
    {
        DynamicMatchAllOf<ArgT>* allOfMatcher = std::any_cast<DynamicMatchAllOf<ArgT>>(&underlyingMatcher);
        allOfMatcher->andAssign(andMatcher);
    }
    else
    {
        DynamicMatchAllOf<ArgT> allOfMatcher = DynamicMatchAllOf<ArgT>();
        allOfMatcher.andAssign(new MatcherWrapper(move(*this)));
        allOfMatcher.andAssign(andMatcher);
        this->setUnderlyingMatcher(move(allOfMatcher));
    }
}

template<typename ArgT>
template<typename ParamT>
std::enable_if_t<IsWrappedMatcherV<ArgT, std::decay_t<ParamT>>,
void> MatcherWrapper<ArgT>::andAssign(ParamT&& andMatcher)
{
    this->andAssign(new MatcherWrapper(std::forward<ParamT>(andMatcher)));
}

std::invalid_argument andUnwrappedMatcherError();

template<typename ArgT, typename SecondMatcherT>
std::enable_if_t<IsUnwrappedMatcherV<ArgT, std::decay_t<SecondMatcherT>>,
MatcherWrapper<ArgT>> operator&&(MatcherWrapper<ArgT>, SecondMatcherT&&)
{
    throw andUnwrappedMatcherError();
}

template<typename ArgT, typename FirstMatcherT>
std::enable_if_t<IsUnwrappedMatcherV<ArgT, std::decay_t<FirstMatcherT>>,
MatcherWrapper<ArgT>> operator&&(FirstMatcherT&&, MatcherWrapper<ArgT>)
{
    throw andUnwrappedMatcherError();
}

template<typename ArgT, typename SecondMatcherT>
std::enable_if_t<IsWrappedMatcherV<ArgT, std::decay_t<SecondMatcherT>>,
MatcherWrapper<ArgT>> operator&&(MatcherWrapper<ArgT> const& first, SecondMatcherT&& second)
{
    auto newWrapper = MatcherWrapper<ArgT>(first);
    newWrapper.andAssign(std::forward<SecondMatcherT>(second));
    return newWrapper;
}

template<typename ArgT, typename SecondMatcherT>
std::enable_if_t<IsWrappedMatcherV<ArgT, std::decay_t<SecondMatcherT>>,
MatcherWrapper<ArgT>> operator&&(MatcherWrapper<ArgT>&& first, SecondMatcherT&& second)
{
    first.andAssign(std::forward<SecondMatcherT>(second));
    return move(first);
}


/** Matcher Union **/

template<typename ArgT>
void MatcherWrapper<ArgT>::orAssign(MatcherWrapper<ArgT> const* orMatcher)
{
    if (typeid(DynamicMatchAnyOf<ArgT>) == underlyingMatcher.type())
    {
        DynamicMatchAnyOf<ArgT>* anyOfMatcher = std::any_cast<DynamicMatchAnyOf<ArgT>>(&underlyingMatcher);
        anyOfMatcher->orAssign(orMatcher);
    }
    else
    {
        DynamicMatchAnyOf<ArgT> anyOfMatcher = DynamicMatchAnyOf<ArgT>();
        anyOfMatcher.orAssign(new MatcherWrapper(move(*this)));
        anyOfMatcher.orAssign(orMatcher);
        this->setUnderlyingMatcher(move(anyOfMatcher));
    }
}

template<typename ArgT>
template<typename ParamT>
std::enable_if_t<IsWrappedMatcherV<ArgT, std::decay_t<ParamT>>,
void> MatcherWrapper<ArgT>::orAssign(ParamT&& orMatcher)
{
    this->orAssign(new MatcherWrapper(std::forward<ParamT>(orMatcher)));
}

std::invalid_argument orUnwrappedMatcherError();

template<typename ArgT, typename SecondMatcherT>
std::enable_if_t<IsUnwrappedMatcherV<ArgT, std::decay_t<SecondMatcherT>>,
MatcherWrapper<ArgT>> operator||(MatcherWrapper<ArgT>, SecondMatcherT&&)
{
    throw orUnwrappedMatcherError();
}

template<typename ArgT, typename FirstMatcherT>
std::enable_if_t<IsUnwrappedMatcherV<ArgT, std::decay_t<FirstMatcherT>>,
MatcherWrapper<ArgT>> operator||(FirstMatcherT&&, MatcherWrapper<ArgT>)
{
    throw orUnwrappedMatcherError();
}

template<typename ArgT, typename SecondMatcherT>
std::enable_if_t<IsWrappedMatcherV<ArgT, std::decay_t<SecondMatcherT>>,
MatcherWrapper<ArgT>> operator||(MatcherWrapper<ArgT> const& first, SecondMatcherT&& second)
{
    auto newWrapper = MatcherWrapper<ArgT>(first);
    newWrapper.orAssign(std::forward<SecondMatcherT>(second));
    return newWrapper;
}

template<typename ArgT, typename SecondMatcherT>
std::enable_if_t<IsWrappedMatcherV<ArgT, std::decay_t<SecondMatcherT>>,
MatcherWrapper<ArgT>> operator||(MatcherWrapper<ArgT>&& first, SecondMatcherT&& second)
{
    first.orAssign(std::forward<SecondMatcherT>(second));
    return move(first);
}


/** Matcher Inverse **/

template<typename ArgT>
void MatcherWrapper<ArgT>::notMatcher()
{
    DynamicMatchNotOf<ArgT> notOfMatcher = DynamicMatchNotOf<ArgT>(new MatcherWrapper(move(*this)));
    this->setUnderlyingMatcher(move(notOfMatcher));
}

template<typename ArgT>
MatcherWrapper<ArgT> operator!(MatcherWrapper<ArgT> const& matcher)
{
    auto newWrapper = MatcherWrapper<ArgT>(matcher);
    newWrapper.notMatcher();
    return newWrapper;
}

template<typename ArgT>
MatcherWrapper<ArgT> operator!(MatcherWrapper<ArgT>&& matcher)
{
    matcher.notMatcher();
    return move(matcher);
}


/** Matcher Constructor Wrapper **/

template<typename ArgT, typename Matcher>
MatcherWrapper<ArgT> wrapMatcher(Matcher const& matcher)
{
    return MatcherWrapper<ArgT>::make(matcher);
}

#endif /* MATCHER_WRAPPER_HPP */
