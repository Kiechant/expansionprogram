
#ifndef MATCHER_HANDLER_HPP
#define MATCHER_HANDLER_HPP

#include <any>
#include <functional>
#include <string>

/** Pre-declarations **/

template<typename ArgT>
class MatcherHandler;
template<typename ArgT>
void swap(MatcherHandler<ArgT>&, MatcherHandler<ArgT>&);


/** Matcher Handler Class Definitions **/

template<typename HandlerT>
class MatcherHandler
{
public:
    MatcherHandler();
    MatcherHandler(std::any* matcher, HandlerT const& handler);
    friend void swap<>(MatcherHandler&, MatcherHandler&);

    std::any* matcher = nullptr;
    HandlerT handler;
};

template<typename ArgT>
class MatcherMatchHandler : public MatcherHandler<std::function<bool(std::any const*, ArgT const&)>>
{
public:
    using HandlerT = typename std::function<bool(std::any const*, ArgT const&)>;

    bool operator()(ArgT const& val) const
    {
        return this->handler(this->matcher, val);
    }
};

class MatcherDescribeHandler : public MatcherHandler<std::function<std::string(std::any const*)>>
{
public:
    using HandlerT = std::function<std::string(std::any const*)>;

    std::string operator()() const
    {
        return this->handler(this->matcher);
    }
};


/** Matcher Handler Method Implementations **/

template<typename HandlerT>
MatcherHandler<HandlerT>::MatcherHandler()
{
    this->matcher = nullptr;
}

template<typename HandlerT>
MatcherHandler<HandlerT>::MatcherHandler(std::any* matcher, HandlerT const& handler)
{
    this->matcher = matcher;
    this->handler = handler;
}

template<typename HandlerT>
void swap(MatcherHandler<HandlerT>& first, MatcherHandler<HandlerT>& second)
{
    using std::swap;
    swap(first.matcher, second.matcher);
    swap(first.handler, second.handler);
}


/** Matcher Handler Wrappers **/

template<typename DerivedT>
DerivedT makeMatcherHandler(std::any* matcher, typename DerivedT::HandlerT const& handler)
{
    DerivedT newHandler;
    newHandler.matcher = matcher;
    newHandler.handler = handler;
    return newHandler;
}

template<typename DerivedT>
DerivedT copyMatcherHandlerWithNewMatcher(DerivedT const& other, std::any* newMatcher)
{
    return makeMatcherHandler<DerivedT>(newMatcher, other.handler);
}

#endif /* MATCHER_HANDLER_HPP */
