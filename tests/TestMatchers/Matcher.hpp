//
//  Matcher.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 18/2/20.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#ifndef MATCHER_HPP
#define MATCHER_HPP

/* Required for functionality of dynamic matchers.
   DynamicMatchers and MatcherWrapper are interdependent.
   MatcherWrapper uses functionality from MatcherDelegate.
   MatcherDelegate includes MatcherHandler internally. */
#include "DynamicMatchers.hpp"
#include "MatcherDelegate.hpp"
#include "MatcherWrapper.hpp"

/* Allows initialising a logical sequence of matchers. */
#include "BooleanMatchers.hpp"

#endif /* MATCHER_HPP */
