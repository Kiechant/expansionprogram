//
//  DynamicMatchers.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 12/03/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef DYNAMIC_MATCHERS_HPP
#define DYNAMIC_MATCHERS_HPP

#include <catch2/catch.hpp>

#include "MemoryUtils.hpp"

/* Implementation of 'dynamic' matchers which support the &&, || and !
   operations on matcher wrappers without altering currently exisitng matchers.
   Essentially, performs deep copies on l-value references to matchers in
   logical expressions, and moves on r-value references to matchers. */


/** Pre-declarations **/

template<typename ArgT>
class MatcherWrapper;

template<typename ArgT>
class DynamicMatchAllOf;
template<typename ArgT>
void swap(DynamicMatchAllOf<ArgT>&, DynamicMatchAllOf<ArgT>&);

template<typename ArgT>
class DynamicMatchAnyOf;
template<typename ArgT>
void swap(DynamicMatchAnyOf<ArgT>&, DynamicMatchAnyOf<ArgT>&);

template<typename ArgT>
class DynamicMatchNotOf;
template<typename ArgT>
void swap(DynamicMatchNotOf<ArgT>&, DynamicMatchNotOf<ArgT>&);


/** Dynamic Matcher Class Definitions **/

/* An intersection of matchers. Matches true if all underlying matchers match true. */
template<typename ArgT>
class DynamicMatchAllOf : public Catch::Matchers::Impl::MatchAllOf<ArgT>
{
public:
    /** Constructors and Destructors **/

    DynamicMatchAllOf();
    ~DynamicMatchAllOf();


    /** Copy and Move Semantics **/

    friend void swap<>(DynamicMatchAllOf&, DynamicMatchAllOf&);
    DynamicMatchAllOf(DynamicMatchAllOf const& other);
    DynamicMatchAllOf(DynamicMatchAllOf&& other);
    DynamicMatchAllOf& operator=(DynamicMatchAllOf other);


    /** Matcher Intersection **/

    DynamicMatchAllOf& operator&&(Catch::MatcherBase<ArgT> const&) = delete;
    void andAssign(MatcherWrapper<ArgT> const* andMatcher);
};

/* A union of matchers. Matches true if any underlying matcher matches true. */
template<typename ArgT>
class DynamicMatchAnyOf : public Catch::Matchers::Impl::MatchAnyOf<ArgT>
{
public:
    /** Constructors and Destructors **/

    DynamicMatchAnyOf();
    ~DynamicMatchAnyOf();


    /** Copy and Move Semantics **/

    friend void swap<>(DynamicMatchAnyOf&, DynamicMatchAnyOf&);
    DynamicMatchAnyOf(DynamicMatchAnyOf const& other);
    DynamicMatchAnyOf(DynamicMatchAnyOf&& other);
    DynamicMatchAnyOf& operator=(DynamicMatchAnyOf other);


    /** Matcher Union **/

    DynamicMatchAnyOf& operator||(Catch::MatcherBase<ArgT> const&) = delete;
    void orAssign(MatcherWrapper<ArgT> const* orMatcher);
};

/* An inverse of a matcher. Matches true if the underlying matcher matches false. */
template<typename ArgT>
class DynamicMatchNotOf : public Catch::Matchers::Impl::MatchNotOf<ArgT>
{
public:
    /** Constructors and Destructors **/

    DynamicMatchNotOf(MatcherWrapper<ArgT> const* notMatcher);
    ~DynamicMatchNotOf();


    /** Copy and Move Semantics **/

    friend void swap<>(DynamicMatchNotOf&, DynamicMatchNotOf&);
    DynamicMatchNotOf(DynamicMatchNotOf const& other);
    DynamicMatchNotOf(DynamicMatchNotOf&& other);
    DynamicMatchNotOf& operator=(DynamicMatchNotOf other);
};


/** Dynamic Matcher Method Implementations **/

/** DynamicMatchAllOf **/

/** Constructors and Destructors **/

template<typename ArgT>
DynamicMatchAllOf<ArgT>::DynamicMatchAllOf()
    : Catch::Matchers::Impl::MatchAllOf<ArgT>() {}

template<typename ArgT>
DynamicMatchAllOf<ArgT>::~DynamicMatchAllOf()
{
    MemoryUtils::deepDelete(this->m_matchers, [](Catch::MatcherBase<ArgT> const* mat)
    {
        delete mat;
    });
}


/** Copy and Move Semantics **/

template<typename ArgT>
void swap(DynamicMatchAllOf<ArgT>& first, DynamicMatchAllOf<ArgT>& second)
{
    using std::swap;
    swap(first.m_matchers, second.m_matchers);
}

template<typename ArgT>
DynamicMatchAllOf<ArgT>::DynamicMatchAllOf(DynamicMatchAllOf const& other)
{
    this->m_matchers = MemoryUtils::deepCopy(other.m_matchers,
    [](Catch::MatcherBase<ArgT> const* mat) -> Catch::MatcherBase<ArgT> const*
    {
        return new MatcherWrapper(*reinterpret_cast<MatcherWrapper<ArgT> const*>(mat));
    });
}

template<typename ArgT>
DynamicMatchAllOf<ArgT>::DynamicMatchAllOf(DynamicMatchAllOf&& other)
{
    swap(*this, other);
}

template<typename ArgT>
DynamicMatchAllOf<ArgT>& DynamicMatchAllOf<ArgT>::operator=(DynamicMatchAllOf other)
{
    swap(*this, other);
    return *this;
}


/** Matcher Intersection **/

template<typename ArgT>
void DynamicMatchAllOf<ArgT>::andAssign(MatcherWrapper<ArgT> const* andMatcher)
{
    this->m_matchers.push_back(andMatcher);
}


/** DynamicMatchAnyOf **/

/** Constructors and Destructors **/

template<typename ArgT>
DynamicMatchAnyOf<ArgT>::DynamicMatchAnyOf() : Catch::Matchers::Impl::MatchAnyOf<ArgT>() {}

template<typename ArgT>
DynamicMatchAnyOf<ArgT>::~DynamicMatchAnyOf()
{
    MemoryUtils::deepDelete(this->m_matchers, [](Catch::MatcherBase<ArgT> const* mat)
    {
        delete mat;
    });
}


/** Copy and Move Semantics **/

template<typename ArgT>
void swap(DynamicMatchAnyOf<ArgT>& first, DynamicMatchAnyOf<ArgT>& second)
{
    using std::swap;
    swap(first.m_matchers, second.m_matchers);
}

template<typename ArgT>
DynamicMatchAnyOf<ArgT>::DynamicMatchAnyOf(DynamicMatchAnyOf const& other)
{
    this->m_matchers = MemoryUtils::deepCopy(other.m_matchers,
    [](Catch::MatcherBase<ArgT> const* mat) -> Catch::MatcherBase<ArgT> const*
    {
        return new MatcherWrapper(*reinterpret_cast<MatcherWrapper<ArgT> const*>(mat));
    });
}

template<typename ArgT>
DynamicMatchAnyOf<ArgT>::DynamicMatchAnyOf(DynamicMatchAnyOf&& other)
{
    swap(*this, other);
}

template<typename ArgT>
DynamicMatchAnyOf<ArgT>& DynamicMatchAnyOf<ArgT>::operator=(DynamicMatchAnyOf other)
{
    swap(*this, other);
    return *this;
}


/** Matcher Union **/

template<typename ArgT>
void DynamicMatchAnyOf<ArgT>::orAssign(MatcherWrapper<ArgT> const* orMatcher)
{
    this->m_matchers.push_back(orMatcher);
}


/** DynamicMatchNotOf **/

/** Constructors and Destructors **/

template<typename ArgT>
DynamicMatchNotOf<ArgT>::DynamicMatchNotOf(MatcherWrapper<ArgT> const* notMatcher)
    : Catch::Matchers::Impl::MatchNotOf<ArgT>(*notMatcher) {}

template<typename ArgT>
DynamicMatchNotOf<ArgT>::~DynamicMatchNotOf()
{
    delete &this->m_underlyingMatcher;
}


/** Copy and Move Semantics **/

template<typename ArgT>
void swap(DynamicMatchNotOf<ArgT>& first, DynamicMatchNotOf<ArgT>& second)
{
    using std::swap;
    swap(reinterpret_cast<MatcherWrapper<ArgT>&>(
            const_cast<Catch::MatcherBase<ArgT>&>(first.m_underlyingMatcher)),
         reinterpret_cast<MatcherWrapper<ArgT>&>(
            const_cast<Catch::MatcherBase<ArgT>&>(second.m_underlyingMatcher)));
}

template<typename ArgT>
DynamicMatchNotOf<ArgT>::DynamicMatchNotOf(DynamicMatchNotOf const& other)
    : Catch::Matchers::Impl::MatchNotOf<ArgT>(
    *new MatcherWrapper(
        reinterpret_cast<MatcherWrapper<ArgT> const&>(other.m_underlyingMatcher))) {}

template<typename ArgT>
DynamicMatchNotOf<ArgT>::DynamicMatchNotOf(DynamicMatchNotOf&& other)
    : Catch::Matchers::Impl::MatchNotOf<ArgT>(
    *new MatcherWrapper(move(
        reinterpret_cast<MatcherWrapper<ArgT>&>(
            const_cast<Catch::MatcherBase<ArgT>&>(other.m_underlyingMatcher))))) {}

template<typename ArgT>
DynamicMatchNotOf<ArgT>& DynamicMatchNotOf<ArgT>::operator=(DynamicMatchNotOf other)
{
    swap(*this, other);
    return *this;
}

#endif /* DYNAMIC_MATCHERS_HPP */
