//
//  ErrorMatchers.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 22/3/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef ERROR_MATCHERS_HPP
#define ERROR_MATCHERS_HPP

#include <catch2/catch.hpp>

#include <exception>
#include <sstream>
#include <string>

#include "TypeUtils.hpp"

#define MESSAGE_CONTAINS_MATCHER_T MessageContainsMatcher< \
    ArgT, std::enable_if_t<std::is_base_of_v<std::exception, ArgT>>>

template<typename ArgT, typename = void>
class MessageContainsMatcher;

template<typename ArgT>
class MESSAGE_CONTAINS_MATCHER_T : public Catch::MatcherBase<ArgT>
{
public:
    MessageContainsMatcher(std::string const& match);

    bool match(ArgT const& err) const;
    virtual std::string describe() const;

protected:
    std::string matchAgainst;
};

template<typename ArgT>
MESSAGE_CONTAINS_MATCHER_T::MessageContainsMatcher(std::string const& match)
    : matchAgainst(match) {}

template<typename ArgT>
bool MESSAGE_CONTAINS_MATCHER_T::match(ArgT const& err) const
{
    return Catch::Contains(matchAgainst).match(err.what());
}

template<typename ArgT>
std::string MESSAGE_CONTAINS_MATCHER_T::describe() const
{
    std::stringstream ss;
    ss << "contains '" << matchAgainst << "'";
    return ss.str();
}

template<typename ArgT>
MESSAGE_CONTAINS_MATCHER_T MessageContains(std::string const& match)
{
    return MESSAGE_CONTAINS_MATCHER_T(match);
}

#undef MESSAGE_CONTAINS_MATCHER_T

#endif /* ERROR_MATCHERS_HPP */
