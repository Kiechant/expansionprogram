#include <catch2/catch.hpp>

#include "ExpansionScheme.hpp"
#include "ExpansionTestUtils.hpp"
#include "MiscUtils.hpp"

#include <filesystem>

using namespace std;
namespace fs = filesystem;
using Catch::Contains;

fs::path const schemeTestDir = ExpansionTestUtils::getTestResDir() / "scheme_tests";

TEST_CASE("ExpansionScheme All")
{
	auto scheme = ExpansionScheme(ExpansionTestUtils::getDefnDir() / "scheme.json", "universal", "template", "expansion");

	// Elements (subdirectories)
	CHECK(scheme.getElementCount() == 7);
	CHECK_THROWS(scheme.getElement(8));
	CHECK(scheme.getElement(0).templateSubpath == ".");
	CHECK(scheme.getElement(1).templateSubpath == "car");
	CHECK(scheme.getElement(1).pipelineId == "full_expand");
	CHECK(scheme.getElement(1).dictionarySelectors.size() == 1);
	CHECK(scheme.getElement(1).dictionarySelectors.at(0) == "#default");
	CHECK(scheme.getElement(4).templateSubpath == "car/finishes");
	CHECK(scheme.getElement(6).pipelineId == "substitute_only");

	// Pipelines
	CHECK_THROWS(scheme.getPipelineOfId("non_existent"));
	CHECK(scheme.getPipelineOfId("all").operations == vector{ExpansionOperation::ExpandExternal, ExpansionOperation::ExpandInternal, ExpansionOperation::Substitute});
	CHECK(scheme.getPipelineOfId("substitute_only").operations == vector{ExpansionOperation::Substitute});
}

SCENARIO("ExpansionScheme - Subdirectory validation for valid subdirectories")
{
	ExpansionScheme scheme = ExpansionScheme(schemeTestDir / "scheme.json", "universal");

	GIVEN("Empty subdirectory")
	{
		THEN("Resolves to current directory '.'")
		{
			CHECK_NOTHROW(scheme.getElementOfSubdirectory("."));
		}
	}

	GIVEN("Valid subdirectories in format 'a' or 'a/b/c'")
	{
		THEN("Resolves to verbatim subdirectory")
		{
			CHECK_NOTHROW(scheme.getElementOfSubdirectory("a"));
			CHECK_NOTHROW(scheme.getElementOfSubdirectory("a/b/c/d"));
		}
	}
}

SCENARIO("ExpansionScheme - Subdirectory validation for special symbols")
{
	optional<ExpansionScheme> scheme;
	string message;
	try
	{
		scheme = ExpansionScheme(schemeTestDir / "invalid_schemes" / "special_symbols.json", "universal");
	}
	catch(const std::exception& e)
	{
		message = e.what();
	}
	REQUIRE(!scheme.has_value());

	GIVEN("Lone special symbols")
	{
		CHECK_THAT(message, Catch::Contains("'.'"));
		CHECK_THAT(message, Catch::Contains("'..'"));
		CHECK_THAT(message, Catch::Contains("'/'"));
	}

	GIVEN("Special symbols at beginning")
	{
		CHECK_THAT(message, Catch::Contains("'./a'"));
		CHECK_THAT(message, Catch::Contains("'../b'"));
		CHECK_THAT(message, Catch::Contains("'/c'"));
	}

	GIVEN("Special symbols in path")
	{
		CHECK_THAT(message, Catch::Contains("'a/b/./c'"));
		CHECK_THAT(message, Catch::Contains("'a/b/c/..'"));
		CHECK_THAT(message, Catch::Contains("'a/b//c'"));
		CHECK_THAT(message, Catch::Contains("'a/b/c//'"));
	}

	GIVEN("File name '...' at beginning or in path")
	{
		CHECK_THAT(message, !Catch::Contains("'...'"));
		CHECK_THAT(message, !Catch::Contains("'a/b/.../c'"));
	}
}

SCENARIO("ExpansionScheme - Subdirectory validation for alternate file names")
{
	optional<ExpansionScheme> scheme;
	REQUIRE_NOTHROW(scheme = ExpansionScheme(schemeTestDir / "invalid_schemes" / "alternate_file_names.json", "universal"));

	GIVEN("Hidden file names")
	{
		CHECK_NOTHROW(scheme.value().getElementOfSubdirectory(".a"));
	}

	GIVEN("Extensions")
	{
		CHECK_NOTHROW(scheme.value().getElementOfSubdirectory("a.ext"));
	}

	GIVEN("Combined hidden files and extensions in path")
	{
		CHECK_NOTHROW(scheme.value().getElementOfSubdirectory("a.ext.ext/.b/.c.ext"));
	}
}

SCENARIO("ExpansionScheme - Subdirectory validation for final slash")
{
	optional<ExpansionScheme> scheme;
	string message;
	try
	{
		scheme = ExpansionScheme(schemeTestDir / "invalid_schemes" / "final_slash.json", "universal");
	}
	catch(const std::exception& e)
	{
		message = e.what();
	}
	REQUIRE(!scheme.has_value());

	GIVEN("Single final slash")
	{
		CHECK_THAT(message, Catch::Contains("'a/b/c/'"));
	}
}

SCENARIO("ExpansionScheme - Subdirectory validation for missing file or different type")
{
	optional<ExpansionScheme> scheme;
	REQUIRE_NOTHROW(scheme = ExpansionScheme(schemeTestDir / "invalid_schemes" / "missing_or_diff_files.json", "universal"));

	string message = "";
	try
	{
		scheme.value().validateSubdirectoriesInParentExist(schemeTestDir / "templates");
	}
	catch(const std::exception& e)
	{
		message = e.what();
	}
	REQUIRE(message != "");

	REQUIRE(fs::is_regular_file(schemeTestDir / "templates" / "file"));
	REQUIRE(!fs::exists(schemeTestDir / "templates" / "missing"));

	CHECK_THAT(message, Catch::Contains("file"));
	CHECK_THAT(message, Catch::Contains("missing"));
	CHECK_THAT(message, Catch::Contains("file/b/c"));
	CHECK_THAT(message, Catch::Contains("a/missing/c"));
}

SCENARIO("ExpansionScheme - Dictionary selection by expansion scheme")
{
	GIVEN("Dictionary of alphanumeric id with no leading digit for path")
	{
		THEN("Dictionary id added to scheme element")
		{
			optional<ExpansionScheme> scheme;
			REQUIRE_NOTHROW(scheme = ExpansionScheme(schemeTestDir / "valid_schemes" / "good_dict_ids.json", "universal"));
			CHECK(scheme.value().getElement(0).dictionarySelectors.at(0) == "_my_dictionary_id");
			CHECK(scheme.value().getElement(0).dictionarySelectors.at(1) == "a12345");
		}
	}

	GIVEN("Dictionary not of alphanumeric id or with leading digit for path")
	{
		THEN("Raises error")
		{
			CHECK_THROWS(ExpansionScheme(schemeTestDir / "invalid_schemes" / "bad_dict_ids" / "empty.json", "universal"));
			CHECK_THROWS(ExpansionScheme(schemeTestDir / "invalid_schemes" / "bad_dict_ids" / "unsupported_chars.json", "universal"));
			CHECK_THROWS(ExpansionScheme(schemeTestDir / "invalid_schemes" / "bad_dict_ids" / "leading_digits.json", "universal"));
			CHECK_THROWS(ExpansionScheme(schemeTestDir / "invalid_schemes" / "bad_dict_ids" / "unsupported_chars_in_dict_set.json", "universal"));
		}
	}

	GIVEN("Dictionary set of alphanumeric id with no leading digit")
	{
		THEN("Dictionary set added to scheme")
		{
			optional<ExpansionScheme> scheme;
			REQUIRE_NOTHROW(scheme = ExpansionScheme(schemeTestDir / "valid_schemes" / "good_dict_set_ids.json", "universal"));
			CHECK_NOTHROW(scheme.value().getDictionarySetOfId("_my_dictionary_set"));
			CHECK_NOTHROW(scheme.value().getDictionarySetOfId("a12345"));
		}
	}

	GIVEN("Valid dictionary id in dictionary set")
	{
		THEN("Dictionary id added to dictionary set")
		{
			optional<ExpansionScheme> scheme;
			REQUIRE_NOTHROW(scheme = ExpansionScheme(schemeTestDir / "valid_schemes" / "good_dict_ids_in_set.json", "universal"));
			optional<ExpansionSchemeDictionarySet const*> dictSet;
			REQUIRE_NOTHROW(dictSet = &scheme.value().getDictionarySetOfId("my_dict_set"));
			CHECK(dictSet.value()->dictionaryIds.at(1) == "dict_a");
			CHECK(dictSet.value()->dictionaryIds.at(2) == "dict_b");
		}
	}

	GIVEN("Dictionary set not of alphanumeric id or with leading digit")
	{
		THEN("Raises error")
		{
			CHECK_THROWS(ExpansionScheme(schemeTestDir / "invalid_schemes" / "bad_dict_set_ids" / "empty.json", "universal"));
			CHECK_THROWS(ExpansionScheme(schemeTestDir / "invalid_schemes" / "bad_dict_set_ids" / "unsupported_chars.json", "universal"));
			CHECK_THROWS(ExpansionScheme(schemeTestDir / "invalid_schemes" / "bad_dict_set_ids" / "leading_digits.json", "universal"));
		}
	}

	GIVEN("Existent dictionary set selected for path with leading # and valid dictionary id pattern")
	{
		THEN("Dictionary set id added to element")
		{
			optional<ExpansionScheme> scheme;
			REQUIRE_NOTHROW(scheme = ExpansionScheme(schemeTestDir / "valid_schemes" / "existing_dict_id_selected.json", "universal"));
			CHECK(scheme.value().getElement(0).dictionarySelectors.at(0) == "#a");
			CHECK(scheme.value().getElement(0).dictionarySelectors.at(1) == "#b");
		}
	}

	GIVEN("Non-existent dictionary set selected for path")
	{
		THEN("Raises error")
		{
			CHECK_THROWS(ExpansionScheme(schemeTestDir / "invalid_schemes" / "missing_dict_set.json", "universal"));
		}
	}

	GIVEN("Dictionary set selected for path with leading # but invalid dictionary id pattern")
	{
		THEN("Raises error")
		{
			CHECK_THROWS(ExpansionScheme(schemeTestDir / "invalid_schemes" / "bad_dict_set_id_selectors" / "empty.json", "universal"));
			CHECK_THROWS(ExpansionScheme(schemeTestDir / "invalid_schemes" / "bad_dict_set_id_selectors" / "unsupported_chars.json", "universal"));
			CHECK_THROWS(ExpansionScheme(schemeTestDir / "invalid_schemes" / "bad_dict_set_id_selectors" / "leading_digits.json", "universal"));
		}
	}

	GIVEN("Repeated dictionary set or dictionary id selected for path")
	{
		THEN("Raises error")
		{
			CHECK_THROWS(ExpansionScheme(schemeTestDir / "invalid_schemes" / "repeated_dicts" / "repeated_dict_id.json", "universal"));
			CHECK_THROWS(ExpansionScheme(schemeTestDir / "invalid_schemes" / "repeated_dicts" / "repeated_dict_set.json", "universal"));
		}
	}

	GIVEN("Dictionary id recurs in dictionary set inclusions")
	{
		THEN("Dictionary ids and sets added to element")
		{
			optional<ExpansionScheme> scheme;
			REQUIRE_NOTHROW(scheme = ExpansionScheme(schemeTestDir / "valid_schemes" / "dict_ids_recur_in_dict_sets.json", "universal"));
			CHECK(scheme.value().getElement(0).dictionarySelectors.at(0) == "a");
			REQUIRE_NOTHROW(scheme.value().getDictionarySetOfId("c"));
			CHECK(scheme.value().getDictionarySetOfId("c").dictionaryIds.at(1) == "a");
		}
	}

	GIVEN("Dictionary set id selected in dictionary set")
	{
		THEN("Raises error")
		{
			CHECK_THROWS(ExpansionScheme(schemeTestDir / "invalid_schemes" / "nested_dict_set.json", "universal"));
		}
	}
}

SCENARIO("ExpansionScheme - Default dictionary set inclusions")
{
	GIVEN("Valid scheme with multiple elements")
	{
		optional<ExpansionScheme> scheme;
		REQUIRE_NOTHROW(scheme = ExpansionScheme(schemeTestDir / "valid_schemes" / "multi_elements.json", "universal"));

		WHEN("Dictionary id or set selected for path")
		{
			THEN("Doesn't add default dictionary set to element")
			{
				CHECK(!MiscUtils::containsElement<string>(scheme.value().getElementOfSubdirectory("a").dictionarySelectors, "#default"));
				CHECK(!MiscUtils::containsElement<string>(scheme.value().getElementOfSubdirectory("b").dictionarySelectors, "#default"));
			}
		}

		WHEN("No dictionary id or set selected for path")
		{
			THEN("Adds default dictionary set to element")
			{
				CHECK(scheme.value().getElementOfSubdirectory("c").dictionarySelectors.at(0) == "#default");
			}
		}
	}

	GIVEN("Default dictionary set selected for path")
	{
		optional<ExpansionScheme> scheme;
		REQUIRE_NOTHROW(scheme = ExpansionScheme(schemeTestDir / "valid_schemes" / "explicit_default_dict_set.json", "universal"));

		THEN("Adds default dictionary set to element")
		{
			CHECK(scheme.value().getElementOfSubdirectory("a").dictionarySelectors.at(0) == "#default");
		}
	}

	GIVEN("Default dictionary selected for path without a set qualifier")
	{
		THEN("Raises error")
		{
			CHECK_THROWS_WITH(ExpansionScheme(schemeTestDir / "invalid_schemes" / "default_dict_set" / "default_as_dict_id.json", "universal"),
				Contains("default") && Contains("reserved"));
		}
	}
}

SCENARIO("ExpansionScheme - Universal dictionary inclusions")
{
	GIVEN("Valid scheme with multiple dictionary sets")
	{
		THEN("Adds universal dictionary to all dictionary sets")
		{
			optional<ExpansionScheme> scheme;
			REQUIRE_NOTHROW(scheme = ExpansionScheme(schemeTestDir / "valid_schemes" / "multi_dict_sets.json", "universal"));
			CHECK(scheme.value().getDictionarySetOfId("default").dictionaryIds.at(0) == "universal");
			CHECK(scheme.value().getDictionarySetOfId("set_a").dictionaryIds.at(0) == "universal");
		}
	}

	GIVEN("Valid scheme with custom-named universal dictionary and custom default dictionary set")
	{
		THEN("Adds universal dictionary to all dictionary sets")
		{
			optional<ExpansionScheme> scheme;
			REQUIRE_NOTHROW(scheme = ExpansionScheme(schemeTestDir / "valid_schemes" / "multi_dict_sets_with_custom_default.json", "my_universal"));
			CHECK(scheme.value().getDictionarySetOfId("default").dictionaryIds.at(0) == "my_universal");
			CHECK(scheme.value().getDictionarySetOfId("set_a").dictionaryIds.at(0) == "my_universal");
		}
	}

	GIVEN("Universal dictionary selected")
	{
		THEN("Raises error")
		{
			CHECK_THROWS_WITH(ExpansionScheme(schemeTestDir / "invalid_schemes" / "universal_dict" / "explicit_universal_dict.json", "universal"),
				Contains("universal") && Contains("implicitly included"));
		}
	}

	GIVEN("Custom-named universal dictionary selected")
	{
		THEN("Raises error")
		{
			CHECK_THROWS_WITH(ExpansionScheme(schemeTestDir / "invalid_schemes" / "universal_dict" / "explicit_custom_universal_dict.json", "my_universal"),
				Contains("my_universal") && Contains("implicitly included"));
			CHECK_THROWS_WITH(ExpansionScheme(schemeTestDir / "invalid_schemes" / "universal_dict" / "explicit_custom_universal_dict_in_dict_set.json", "my_universal"),
				Contains("my_universal") && Contains("implicitly included"));
		}
	}

	GIVEN("Default universal dictionary selected for a custom-named universal dictionary")
	{
		THEN("Adds dictionary id to element")
		{
			CHECK_NOTHROW(ExpansionScheme(schemeTestDir / "valid_schemes" / "default_universal_dict_allowed_when_customised.json", "my_universal"));
		}
	}

	GIVEN("Custom-named universal dictionary selected with a set qualifier")
	{
		THEN("Raises error")
		{
			CHECK_THROWS_WITH(ExpansionScheme(schemeTestDir / "invalid_schemes" / "universal_dict" / "custom_universal_as_dict_set.json", "my_universal"),
				Contains("my_universal") && Contains("reserved"));
		}
	}
}
