#include <catch2/catch.hpp>

#include "ExpansionGroup.tpp"
#include "ExpansionTestUtils.hpp"
#include "TemplateTree.hpp"
#include "Tree.tpp"
#include "TreeNode.tpp"

using namespace std;

TEST_CASE("TemplateTree Parse Template Name")
{
	auto basicName = ExpansionTestUtils::getBasicName();
	TemplateTree* actual;
	REQUIRE_NOTHROW(actual = TemplateTree::parseTemplateName(basicName));

	TemplateTree expected = TemplateTree(new Tree<string>("template_name"));
	auto templateName = expected.tree->getRoot();
	templateName->addBranch({"group_spec", "group_expansion", "group", "car"});
	templateName->addChild("@");
	auto fileName = templateName->addChild("file_name");
	auto expandableName = fileName->addChild("expandable_name");
	expandableName->addChild("my_");
	auto propertyToken = expandableName->addBranch({"token", "property_token"});
	propertyToken->addChild("<");
	auto groupRef = propertyToken->addChild("group_ref");
	groupRef->addBranch({"group", "car"});
	groupRef->addChild(":");
	groupRef->addBranch({"copy_ref", "0"});
	propertyToken->addChild("@");
	auto property = propertyToken->addChild("property");
	property->addBranch({"term", "field", "name"});
	propertyToken->addChild(">");
	expandableName->addChild("_description");
	fileName->addBranch({"extension", ".template"});

	CHECK(*actual->tree == *expected.tree);

	delete actual;
}

TEST_CASE("TemplateTree Parse Template Contents")
{
	auto basicName = ExpansionTestUtils::getBasicName();
	auto basicPath = ExpansionTestUtils::getTmplDir() / basicName;
	TemplateTree* actual;
	REQUIRE_NOTHROW(actual = TemplateTree::parseTemplateContentsAtPath(basicPath));

	TemplateTree expected = TemplateTree(new Tree<string>("file_contents"));
	auto fileContents = expected.tree->getRoot();

	// Line 0
	auto expLine0 = fileContents->addBranch({"contents_line", "expandable_line"});
	expLine0->addChild("Application 0: ");
	auto propToken0 = expLine0->addBranch({"token", "property_token"});
	propToken0->addChild("<");
	auto term0 = propToken0->addBranch({"property", "term"});
	term0->addBranch({"field", "application"});
	auto indexer0 = term0->addChild("indexer");
	indexer0->addChild("[");
	indexer0->addBranch({"index", "0"});
	indexer0->addChild("]");
	propToken0->addChild(">");
	fileContents->addChild("\n");

	// Line 1
	auto expLine1 = fileContents->addBranch({"contents_line", "expandable_line"});
	expLine1->addChild("Colour: ");
	auto propToken1 = expLine1->addBranch({"token", "property_token"});
	propToken1->addChild("<");
	propToken1->addBranch({"group_ref", "group", "car"});
	propToken1->addChild("@");
	auto term1 = propToken1->addBranch({"property", "term"});
	term1->addBranch({"field", "colour"});
	auto indexer1 = term1->addChild("indexer");
	indexer1->addChild({"["});
	indexer1->addBranch({"index", "2"});
	indexer1->addChild({"]"});
	propToken1->addChild(">");
	fileContents->addChild("\n");

	// Line 2
	auto expLine2 = fileContents->addBranch({"contents_line", "expandable_line"});
	expLine2->addChild("Model: ");
	auto propToken2 = expLine2->addBranch({"token", "property_token"});
	propToken2->addChild("<");
	propToken2->addBranch({"group_ref", "index_ref", "0"});
	propToken2->addChild("@");
	propToken2->addBranch({"property", "term", "field", "model"});
	propToken2->addChild(">");
	fileContents->addChild("\n");

	// Line 3
	auto expLine3 = fileContents->addBranch({"contents_line", "expandable_line"});
	expLine3->addChild("Body Weight: ");
	auto propToken3 = expLine3->addBranch({"token", "property_token"});
	propToken3->addChild("<");
	auto groupRef3 = propToken3->addChild("group_ref");
	groupRef3->addBranch({"group", "car"});
	groupRef3->addChild(":");
	groupRef3->addBranch({"copy_ref", "0"});
	propToken3->addChild("@");
	propToken3->addBranch({"property", "term", "field", "body_weight"});
	propToken3->addChild(">");
	fileContents->addChild("\n");

	CHECK(*actual->tree == *expected.tree);

	delete actual;
}

TEST_CASE("TemplateTree Navigation of Group Specifier")
{
	auto multiGroupSpecName = ExpansionTestUtils::getMultiGroupSpecName();
	shared_ptr<TemplateTree> templateNameTree;
	REQUIRE_NOTHROW(templateNameTree = ExpansionTestUtils::getTemplateNameTree(multiGroupSpecName));
	shared_ptr<ExpansionDefinition> definition;
	REQUIRE_NOTHROW(definition = ExpansionTestUtils::getDefinition());

	TemplateNode* groupSpec;
	REQUIRE_NOTHROW(groupSpec = TemplateTree::getGroupSpecifier(templateNameTree->tree->getRoot()));

	SECTION("Get Group Expansions")
	{
		// Extracts expansion groups as a node.
		vector<TemplateNode*> groupExpansions;
		REQUIRE_NOTHROW(groupExpansions = TemplateTree::getGroupExpansions(groupSpec));
		CHECK(groupExpansions.size() == 3);
		CHECK(groupExpansions.at(0)->getItem() == "group_expansion");
		CHECK(groupExpansions.at(2)->getItem() == "group_expansion");
	}

	SECTION("Get Groups")
	{
		// Extracts expansion groups from group specifier of the template name tree.
		vector<ExpansionGroup const*> groups;
		REQUIRE_NOTHROW(groups = TemplateTree::getGroups(groupSpec, definition.get()));
		CHECK(groups.size() == 3);
		CHECK(groups.at(0)->getId() == "car");
		CHECK(groups.at(1)->getId() == "car_engine");
		CHECK(groups.at(2)->getId() == "person");
	}

	SECTION("Get Group Ids")
	{
		// Extracts expansion group ids from group specifier of the template name tree. 
		vector<string> groupIds;
		REQUIRE_NOTHROW(groupIds = TemplateTree::getGroupIds(groupSpec, definition.get()));
		CHECK(groupIds.size() == 3);
		CHECK(groupIds.at(0) == "car");
		CHECK(groupIds.at(1) == "car_engine");
		CHECK(groupIds.at(2) == "person");
	}
}

TEST_CASE("TemplateTree Navigation of Nested Expansions")
{
	auto multiNestedSpecName = ExpansionTestUtils::getMultiNestedSpecName();
	shared_ptr<TemplateTree> templateNameTree;
	REQUIRE_NOTHROW(templateNameTree = ExpansionTestUtils::getTemplateNameTree(multiNestedSpecName));

	// Extracts nested expansions.
	auto groupSpec = TemplateTree::getGroupSpecifier(templateNameTree->tree->getRoot());
	auto groupExpansions = TemplateTree::getGroupExpansions(groupSpec);
	string groupLabel;
	REQUIRE_NOTHROW(groupLabel = TemplateTree::getGroupNode(groupExpansions.at(0))->getItem());
	CHECK(groupLabel == "driver");

	// Layer 1
	vector<TemplateNode*> nestedExpansions1;
	REQUIRE_NOTHROW(nestedExpansions1 = TemplateTree::getNestedExpansions(groupExpansions.at(0)));
	CHECK(nestedExpansions1.size() == 1);
	auto nestedProp = TemplateTree::getProperty(nestedExpansions1.at(0));
	CHECK(TemplateTree::flatten(nestedProp) == "garage[0].car");

	// Layer 2
	vector<TemplateNode*> nestedExpansions2;
	REQUIRE_NOTHROW(nestedExpansions2 = TemplateTree::getNestedExpansions(nestedExpansions1.at(0)));
	CHECK(nestedExpansions2.size() == 3);
	nestedProp = TemplateTree::getProperty(nestedExpansions2.at(0));
	CHECK(TemplateTree::flatten(nestedProp) == "colour");
	nestedProp = TemplateTree::getProperty(nestedExpansions2.at(1));
	CHECK(TemplateTree::flatten(nestedProp) == "wheels[0].trim");
	nestedProp = TemplateTree::getProperty(nestedExpansions2.at(2));
	CHECK(TemplateTree::flatten(nestedProp) == "engine");
}

TEST_CASE("TemplateTree Navigation of Basic Property")
{
	auto basicName = ExpansionTestUtils::getBasicName();
	shared_ptr<TemplateTree> templateNameTree;
	REQUIRE_NOTHROW(templateNameTree = ExpansionTestUtils::getTemplateNameTree(basicName));

	// Extracts the expandable part of the file name of the template name tree.
	TemplateNode* expandableName;
	REQUIRE_NOTHROW(expandableName = TemplateTree::getExpandableName(templateNameTree->tree->getRoot()));
	CHECK(expandableName->getItem() == "expandable_name");
	CHECK(TemplateTree::flatten(expandableName) == "my_<car:0@name>_description");

	// Extracts the property tokens from the expandable part of the expandable name node.
	auto propTokens = TemplateTree::getPropertyTokens(expandableName);
	REQUIRE(propTokens.size() == 1);
	auto propToken = propTokens.at(0);
	CHECK(propToken->getItem() == "property_token");
	CHECK(TemplateTree::flatten(propToken) == "<car:0@name>");

	SECTION("Group Referencing")
	{
		// Extracts the group reference, copy number and group index from a property token node.
		auto [groupId, copyNum, groupIndex] = TemplateTree::getGroupRef(propToken);
		CHECK(groupId == "car");
		CHECK(copyNum == 0);
		CHECK(groupIndex == numeric_limits<size_t>::max());
	}

	SECTION("Field Access")
	{
		// Extracts the terms of a property node.
		auto property = TemplateTree::getProperty(propToken);
		CHECK(property->getItem() == "property");
		auto terms = TemplateTree::getPropertyTerms(property);
		REQUIRE(terms.size() == 1);
		auto term = terms.at(0);
		CHECK(term->getItem() == "term");
		CHECK(TemplateTree::flatten(term) == "name");

		// Extracts the field and indexer of a term node.
		auto [field, indexer] = TemplateTree::getFieldAndIndexer(term);
		CHECK(field == "name");
		CHECK(indexer == numeric_limits<size_t>::max());
	}
}

TEST_CASE("TemplateTree Navigation of Complex Property")
{
	auto multiPropertyName = ExpansionTestUtils::getMultiPropertyName();
	shared_ptr<TemplateTree> templateNameTree;
	REQUIRE_NOTHROW(templateNameTree = ExpansionTestUtils::getTemplateNameTree(multiPropertyName));

	TemplateNode* expandableName;
	REQUIRE_NOTHROW(expandableName = TemplateTree::getExpandableName(templateNameTree->tree->getRoot()));
	CHECK(TemplateTree::flatten(expandableName) == "<a.b.c>_<x@a>_<5@a.b[0].c.d[2]>_<y:2@a[3]>");

	auto propTokens = TemplateTree::getPropertyTokens(expandableName);
	REQUIRE(propTokens.size() == 4);
	CHECK(TemplateTree::flatten(propTokens.at(0)) == "<a.b.c>");
	CHECK(TemplateTree::flatten(propTokens.at(1)) == "<x@a>");
	CHECK(TemplateTree::flatten(propTokens.at(2)) == "<5@a.b[0].c.d[2]>");
	CHECK(TemplateTree::flatten(propTokens.at(3)) == "<y:2@a[3]>");

	const size_t unsetSize = numeric_limits<size_t>::max();

	SECTION("Group Referencing")
	{
		// No reference.
		CHECK(TemplateTree::getGroupRef(propTokens.at(0)) == make_tuple("", unsetSize, unsetSize));

		// Reference by name.
		CHECK(TemplateTree::getGroupRef(propTokens.at(1)) == make_tuple("x", unsetSize, unsetSize));

		// Reference by index.
		CHECK(TemplateTree::getGroupRef(propTokens.at(2)) == make_tuple("", unsetSize, 5));

		// Reference by name and copy number.
		CHECK(TemplateTree::getGroupRef(propTokens.at(3)) == make_tuple("y", 2, unsetSize));
	}
	
	SECTION("Field Access")
	{
		// Single term.
		auto terms = TemplateTree::getPropertyTerms(TemplateTree::getProperty(propTokens.at(1)));
		REQUIRE(terms.size() == 1);
		CHECK(TemplateTree::getFieldAndIndexer(terms.at(0)) == make_tuple("a", unsetSize));

		// Nested terms.
		terms = TemplateTree::getPropertyTerms(TemplateTree::getProperty(propTokens.at(0)));
		REQUIRE(terms.size() == 3);
		CHECK(TemplateTree::getFieldAndIndexer(terms.at(0)) == make_tuple("a", unsetSize));
		CHECK(TemplateTree::getFieldAndIndexer(terms.at(2)) == make_tuple("c", unsetSize));

		// Single indexed term.
		terms = TemplateTree::getPropertyTerms(TemplateTree::getProperty(propTokens.at(3)));
		REQUIRE(terms.size() == 1);
		CHECK(TemplateTree::getFieldAndIndexer(terms.at(0)) == make_tuple("a", 3));

		// Nested indexed terms.
		terms = TemplateTree::getPropertyTerms(TemplateTree::getProperty(propTokens.at(2)));
		REQUIRE(terms.size() == 4);
		CHECK(TemplateTree::getFieldAndIndexer(terms.at(0)) == make_tuple("a", unsetSize));
		CHECK(TemplateTree::getFieldAndIndexer(terms.at(1)) == make_tuple("b", 0));
		CHECK(TemplateTree::getFieldAndIndexer(terms.at(3)) == make_tuple("d", 2));
	}
}
