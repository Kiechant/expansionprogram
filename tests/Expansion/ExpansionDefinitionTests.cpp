#include <catch2/catch.hpp>

#include "ExpansionDefinition.hpp"

#include "Comparison.hpp"
#include "ExpanderOptions.hpp"
#include "ExpansionMember.hpp"
#include "ExpansionFieldEntry.hpp"
#include "ExpansionMemberFieldMap.tpp"
#include "ExpansionGroupFieldEntry.tpp"
#include "ExpansionTestUtils.hpp"
#include "NestedExpansionGroup.hpp"
#include "SubstitutionDictionary.hpp"
#include "TemplateTree.hpp"
#include "Tree.tpp"
#include "UserFunction.tpp"
#include "ValueEntry.tpp"

using namespace std;
using Catch::Contains;
namespace fs = filesystem;
using FieldFormat = ExpansionFieldEntryFormat;

class ExpansionDefinitionDummy : public ExpansionDefinition
{
public:
	ExpansionDefinitionDummy() : ExpansionDefinition() {}
	using ExpansionDefinition::readGroupsAndCategories;
	using ExpansionDefinition::readMembersOfGroup;
	using ExpansionDefinition::readFunctionsOfGroup;
	using ExpansionDefinition::readSubstitutionDictionaries;
};

static const fs::path defnPath = "../test_resources/definition";

const fs::path circularFuncTestDir = ExpansionTestUtils::getTestResDir() / "definition_tests/circular_functions";
const fs::path circularSubTestDir = ExpansionTestUtils::getTestResDir() / "dictionary_tests";
const fs::path schemeDictRefTestDir = ExpansionTestUtils::getTestResDir() / "definition_tests/scheme_dictionary_referencing";

enum class DefinitionMode
{
	Function,
	Subproperty
};

fs::path getSubdirForDefinitionMode(string const& prefix, DefinitionMode mode)
{
	string subdir = prefix;
	switch (mode)
	{
	case DefinitionMode::Function:
		break;
	case DefinitionMode::Subproperty:
		subdir += "_with_subprops";
		break;
	}
	return subdir;
}

shared_ptr<ExpanderOptions>
setupFunctionCycleDetectionTestForDefinition(string const& defnName, DefinitionMode mode = DefinitionMode::Function, string additional = "")
{
	auto opts = make_shared<ExpanderOptions>();
	opts->addPath(ExpanderOptions::PathCategory::Groups, fs::absolute(circularFuncTestDir / defnName));
	opts->addPath(ExpanderOptions::PathCategory::Functions, fs::absolute(circularFuncTestDir / getSubdirForDefinitionMode("functions", mode)));
	opts->addPath(ExpanderOptions::PathCategory::Members, fs::absolute(circularFuncTestDir / getSubdirForDefinitionMode("members", mode)));
	if (additional != "")
		opts->addPath(ExpanderOptions::PathCategory::Members, fs::absolute(circularFuncTestDir / additional));
	return opts;
}

shared_ptr<ExpanderOptions>
setupSubstitutionDictCycleDetectionTestForDefinition(string const& defnName, fs::path const& altSchemePath = "")
{
	auto opts = make_shared<ExpanderOptions>();
	opts->addPath(ExpanderOptions::PathCategory::Substitutions, fs::absolute(circularSubTestDir / defnName));
	if (altSchemePath != "")
		opts->schemePath = fs::absolute(circularSubTestDir / altSchemePath);
	else
		opts->schemePath = fs::absolute(circularSubTestDir / "scheme.json");
	return opts;
}

shared_ptr<ExpanderOptions>
setupSchemeDictRefTestForDefinition(string const& schemePath, vector<string> const& substitutionDirs, string const& universalDictId = "")
{
	auto opts = make_shared<ExpanderOptions>();
	for (auto const& dir : substitutionDirs)
	{
		opts->addPath(ExpanderOptions::PathCategory::Substitutions, fs::absolute(schemeDictRefTestDir / dir));
	}
	if (!universalDictId.empty())
	{
		opts->universalDictName = universalDictId;
	}
	opts->schemePath = fs::absolute(schemeDictRefTestDir / schemePath);
	return opts;
}

TEST_CASE("ExpansionDefinition Reading Groups")
{
	auto[groupOfId, categoryOfId, nestedGroupOfId] =
		ExpansionDefinitionDummy().readGroupsAndCategories(defnPath, vector<fs::path>{"groups"});
	ExpansionDefinition::mapNestedGroupsToOwner(&groupOfId, &nestedGroupOfId);

	CHECK(groupOfId.count("car") == 1);
	CHECK(groupOfId.count("car_engine") == 1);
	CHECK(nestedGroupOfId.count("car_wheels") == 1);

	// Car tests.
	ExpansionGroup const* car = groupOfId.at("car");

	// Id and name.
	CHECK(car->getId() == "car");
	CHECK(car->getName() == "car");

	// Static value.
	CHECK(car->getStaticFieldMap()->getFormat("application") == FieldFormat::Value);
	CHECK(car->getStaticFieldMap()->isStatic("application"));
	CHECK(car->getValueType("application") == ValueCategory::String);
	CHECK(car->getStaticFieldMap()->getListSize("application") == 2);
	CHECK(toString(*car->getStaticFieldMap()->getValue("application", 0)) == "transport");
	CHECK(toString(*car->getStaticFieldMap()->getValue("application", 1)) == "racing");

	// Value.
	CHECK(car->getFormat("colour") == FieldFormat::Value);
	CHECK(!car->isStatic("colour"));
	CHECK(car->getValueType("colour") == ValueCategory::String);
	CHECK(car->getListSize("colour") == 3);
	CHECK(car->getValueType("model") == ValueCategory::String);
	CHECK(car->getListSize("model") == 1);
	CHECK(car->hasField("body_weight"));
	CHECK(car->getFormat("body_weight") == FieldFormat::Value);
	CHECK(car->getValueType("body_weight") ==  ValueCategory::Int);

	// Foreign member.
	CHECK(car->getFormat("engine") == FieldFormat::ForeignMember);
	CHECK(!car->isStatic("engine"));
	CHECK(*car->getForeignGroupId("engine") == "car_engine");
	CHECK(car->getListSize("engine") == 3);

	// Nested member.
	CHECK(car->getFormat("wheels") == FieldFormat::NestedMember);
	CHECK(!car->isStatic("wheels"));
	CHECK(*car->getNestedGroupId("wheels") == "car_wheels");
	CHECK(car->getListSize("wheels") == 3);

	// Nested template.
	CHECK(car->hasField("description"));
	CHECK(car->getFormat("description") == FieldFormat::NestedTemplate);
	CHECK(car->isStatic("description"));
	CHECK_THROWS(car->getListSize("speed"));

	// Function.
	// TODO: Get actual executable function and test its operation.
	CHECK(car->getFormat("speed") == FieldFormat::Function);
	CHECK_THROWS(car->isStatic("speed"));
	CHECK(*car->getFunctionName("speed") == "calc_speed");
	CHECK(car->getFunctionReturnType("speed") == ValueCategory::Float);

	auto* actualArgs = car->getFunctionArgs("speed");
	auto expectedArgs = vector<string>{"total_weight", "engine_power", "wheels.friction"};
	REQUIRE(actualArgs->size() == expectedArgs.size());
	for (size_t i = 0 ; i < actualArgs->size(); i++)
	{
		CHECK(*actualArgs->at(i).get()->tree == *TemplateTree::parseProperty(expectedArgs.at(i))->tree);
	}

	CHECK_THROWS(car->getListSize("speed"));
	CHECK(car->hasField("total_weight"));
	CHECK(car->getFormat("total_weight") == FieldFormat::Function);

	// Alias.
	CHECK(car->getFormat("engine_power") == FieldFormat::Alias);
	CHECK_THROWS(car->isStatic("engine_power"));
	CHECK(TemplateTree::flatten((*car->getAlias("engine_power"))->tree->getRoot()) == "engine.horsepower");
	CHECK_THROWS(car->getListSize("engine_power"));

	// Unique fields.
	CHECK(car->isUnique("id"));
	CHECK(car->isUnique("name"));
	CHECK(car->isUnique("model"));
	CHECK(!car->isUnique("colour"));
}

TEST_CASE("ExpansionDefinition Reading Members")
{
	auto[groupOfId, categoryOfId, nestedGroupOfId] =
		ExpansionDefinitionDummy().readGroupsAndCategories(defnPath, vector<fs::path>{"groups"});
	ExpansionDefinition::mapNestedGroupsToOwner(&groupOfId, &nestedGroupOfId);

	ExpansionGroup* carGroup = groupOfId.at("car");
	auto carMemberOfId = ExpansionDefinitionDummy::readMembersOfGroup(defnPath, vector<fs::path>{"members"}, carGroup);
	CHECK(carGroup->memberIds.size() == 2);
	CHECK(carMemberOfId.count("car_buggy") == 1);
	CHECK(carGroup->indexOfMemberId.count("car_buggy") > 0);
	CHECK(carMemberOfId.count("car_hatchback") == 1);
	CHECK(carGroup->indexOfMemberId.count("car_hatchback") > 0);

	auto buggy = carMemberOfId.at("car_buggy");

	// Basic member fields/properties.
	CHECK(buggy->getFieldMap()->areFieldsComplete());
	CHECK(buggy->getId() == "car_buggy");
	CHECK(buggy->getName() == "buggy");
	CHECK(buggy->getGroup() == carGroup);

	// Group lists members.
	// CHECK(carGroup.hasMemberId("buggy"));
	// CHECK(carGroup.listMemberIds() == {"buggy", "hatchback"});

	// Has field.
	CHECK(buggy->getFieldMap()->hasField("colour"));
	CHECK(buggy->getFieldMap()->hasField("engine"));
	CHECK(!buggy->getFieldMap()->hasField("absent"));

	// Values.
	CHECK(buggy->getFieldMap()->getFormat("colour") == FieldFormat::Value);
	CHECK(buggy->getFieldMap()->getListSize("colour") == 3);
	CHECK(buggy->getFieldMap()->getValue("colour", 0)->getValue<ValueCategory::String>() == "hotrod_red");
	CHECK(buggy->getFieldMap()->getValue("colour", 1)->getValue<ValueCategory::String>() == "silver");
	CHECK(buggy->getFieldMap()->getValue("colour", 2)->getValue<ValueCategory::String>() == "metallic_blue");
	CHECK_THROWS(buggy->getFieldMap()->getValue("colour", 0)->getValue<ValueCategory::Int>());
	CHECK_THROWS(buggy->getFieldMap()->getValue("colour", 3));

	CHECK(buggy->getFieldMap()->getListSize("model") == 1);
	CHECK(buggy->getFieldMap()->getValue("model", 0)->getValue<ValueCategory::String>() == "buggy");
	CHECK_THROWS(buggy->getFieldMap()->getValue("model", 1));

	CHECK(buggy->getFieldMap()->getListSize("body_weight") == 1);
	CHECK(buggy->getFieldMap()->getValue("body_weight", 0)->getValue<ValueCategory::Int>() == 300);
	CHECK_THROWS(buggy->getFieldMap()->getValue("body_weight", 0)->getValue<ValueCategory::String>());
	CHECK_THROWS(buggy->getFieldMap()->getValue("body_weight", 0)->getValue<ValueCategory::Unsigned>());

	// Foreign members.
	CHECK(buggy->getFieldMap()->getFormat("engine") == FieldFormat::ForeignMember);
	CHECK(buggy->getFieldMap()->getListSize("engine") == 3);
	CHECK(*buggy->getFieldMap()->getForeignMemberId("engine", 0) == "v4");
	CHECK(*buggy->getFieldMap()->getForeignMemberId("engine", 1) == "v6");
	CHECK(*buggy->getFieldMap()->getForeignMemberId("engine", 2) == "v8");

	// Nested members.
	CHECK(buggy->getFieldMap()->getFormat("wheels") == FieldFormat::NestedMember);
	CHECK(buggy->getFieldMap()->getListSize("wheels") == 3);

	// TODO: Test actual nested member creation.
	// auto spoked = NestedExpansionMember("spoked", nullptr, "car_wheels", 0);
	// spoked.fieldMap->setName(10); // Arg 1
	// spoked.fieldMap->setValue("friction", 30); // Arg 2
	// spoked.fieldMap->setValue("weight", 10); // Arg 3
	// CHECK(*buggy->getFieldMap()->getNestedMember("wheels", 0) == spoked);
}

TEST_CASE("ExpansionDefinition Reading Functions")
{
	auto[groupOfId, categoryOfId, nestedGroupOfId] =
        ExpansionDefinitionDummy().readGroupsAndCategories(defnPath, vector<fs::path>{"groups"});
	ExpansionDefinition::mapNestedGroupsToOwner(&groupOfId, &nestedGroupOfId);

	ExpansionGroup* carGroup = groupOfId.at("car");
	auto carMemberOfId = ExpansionDefinitionDummy::readMembersOfGroup(defnPath, vector<fs::path>{"members"}, carGroup);
	ExpansionDefinitionDummy::readFunctionsOfGroup(defnPath, vector<fs::path>{"functions"}, carGroup);

	CHECK_NOTHROW(carGroup->getFunction("speed"));
	CHECK_NOTHROW(carGroup->getFunctionOfName("calc_speed"));

	CHECK(carGroup->evaluateFunction("speed", vector<EpValue>{
		makeEpValue<EpValueCategory::Float>(100), // weight
		makeEpValue<EpValueCategory::Float>(300), // power
		makeEpValue<EpValueCategory::Float>(5)    // friction
		}).getValue<EpValueCategory::Float>() ==
		Approx((ValueType::Float)(0.6))); // speed = weight / (power * friction)

	CHECK(carGroup->evaluateFunction("total_weight", vector<EpValue>{
		makeEpValue<EpValueCategory::Float>(1),
		makeEpValue<EpValueCategory::Float>(2),
		makeEpValue<EpValueCategory::Float>(4)
		}).getValue<EpValueCategory::Float>() ==
		Approx((ValueType::Float)(7)));
}

TEST_CASE("ExpansionDefinition Reading Substitutions")
{
	auto dictsOfId = ExpansionDefinitionDummy::readSubstitutionDictionaries(
		defnPath,
		vector<fs::path>{"substitutions"},
		"universal",
		set<string>{"universal", "game"});

	CHECK(dictsOfId.size() == 2);
	CHECK(dictsOfId.find("universal") != dictsOfId.end());
	CHECK(dictsOfId.find("game") != dictsOfId.end());

	auto const& universalDict = dictsOfId.find("universal")->second;
	auto const& gameDict = dictsOfId.find("game")->second;

	// Universal dictionary.

	CHECK(universalDict->hasField("greeting"));
	CHECK(!universalDict->isTemplate("greeting"));
	CHECK(*universalDict->getReplacementString("greeting") == "Hello world");
	CHECK_THROWS(*universalDict->getReplacementTree("greeting"));

	CHECK(universalDict->hasField("long_greeting"));
	CHECK(universalDict->isTemplate("long_greeting"));
	CHECK_THROWS(universalDict->getReplacementString("long_greeting"));

	// TODO: Make sure tree is created and appropriately parsed after behaviour
	// is implemented in substitution dictionary.
	CHECK_NOTHROW(universalDict->getReplacementTree("long_greeting"));

	// Game dictionary.

	CHECK(*gameDict->getReplacementString("hp") == "health points");

	// TODO: Tree parsing required. See above.
	CHECK_NOTHROW(gameDict->getReplacementTree("player_display"));
}

SCENARIO("ExpansionDefinition - Cycle detection of functions")
{
	GIVEN("Single-layered function")
	{
		auto opts = setupFunctionCycleDetectionTestForDefinition("single_layer_func");

		WHEN("Validating definition")
		{
			THEN("No cycle detected")
			{
				REQUIRE_NOTHROW(ExpansionDefinition(ExpansionTestUtils::getDefnDummyDir(), opts.get()));
			}
		}
	}

	GIVEN("Double-layered function")
	{
		auto opts = setupFunctionCycleDetectionTestForDefinition("double_layer_func");

		WHEN("Validating definition")
		{
			THEN("No cycle detected")
			{
				REQUIRE_NOTHROW(ExpansionDefinition(ExpansionTestUtils::getDefnDummyDir(), opts.get()));
			}
		}
	}

	GIVEN("Multi-layered function with non-tree arrangement and no cycle")
	{
		auto opts = setupFunctionCycleDetectionTestForDefinition("multi_layer_func_no_cycle");

		WHEN("Validating definition")
		{
			THEN("No cycle detected")
			{
				REQUIRE_NOTHROW(ExpansionDefinition(ExpansionTestUtils::getDefnDummyDir(), opts.get()));
			}
		}
	}

	GIVEN("Multi-layered function with cycle between a function and itself")
	{
		auto opts = setupFunctionCycleDetectionTestForDefinition("multi_layer_func_with_self_cycle");

		WHEN("Validating definition")
		{
			THEN("Cycle detected")
			{
				REQUIRE_THROWS_WITH(ExpansionDefinition(ExpansionTestUtils::getDefnDummyDir(), opts.get()),
					Contains("cycle") &&
					!Contains("group 'a'") && !Contains("group 'b'") && Contains("group 'c'") &&
					!Contains("group 'd'") && !Contains("group 'e'") && !Contains("group 'nc'"));
			}
		}
	}

	GIVEN("Multi-layered function with cycle between a function and itself with duplicate edges")
	{
		auto opts = setupFunctionCycleDetectionTestForDefinition("multi_layer_func_with_duplicate_self_cycle");

		WHEN("Validating definition")
		{
			THEN("Cycle detected")
			{
				REQUIRE_THROWS_WITH(ExpansionDefinition(ExpansionTestUtils::getDefnDummyDir(), opts.get()),
					Contains("cycle") &&
					!Contains("group 'a'") && !Contains("group 'b'") && Contains("group 'c'") &&
					!Contains("group 'd'") && !Contains("group 'e'") && !Contains("group 'nc'"));
			}
		}
	}

	GIVEN("Multi-layered function with cycle over two functions")
	{
		auto opts = setupFunctionCycleDetectionTestForDefinition("multi_layer_func_with_double_node_cycle");

		WHEN("Validating definition")
		{
			THEN("Cycle detected")
			{
				REQUIRE_THROWS_WITH(ExpansionDefinition(ExpansionTestUtils::getDefnDummyDir(), opts.get()),
					Contains("cycle") &&
					Contains("group 'a'") && !Contains("group 'b'") && Contains("group 'c'") &&
					!Contains("group 'd'") && !Contains("group 'e'") && !Contains("group 'nc'"));
			}
		}
	}

	GIVEN("Multi-layered function with cycle over multiple functions")
	{
		auto opts = setupFunctionCycleDetectionTestForDefinition("multi_layer_func_with_long_cycle");

		WHEN("Validating definition")
		{
			THEN("Cycle detected")
			{
				REQUIRE_THROWS_WITH(ExpansionDefinition(ExpansionTestUtils::getDefnDummyDir(), opts.get()),
					Contains("cycle") &&
					Contains("group 'a'") && !Contains("group 'b'") && Contains("group 'c'") &&
					!Contains("group 'd'") && Contains("group 'e'") && Contains("group 'nc'"));
			}
		}
	}

	GIVEN("Multi-layered function with cycle over multiple functions with duplicate edges")
	{
		auto opts = setupFunctionCycleDetectionTestForDefinition("multi_layer_func_with_duplicate_long_cycle");

		WHEN("Validating definition")
		{
			THEN("Cycle detected")
			{
				REQUIRE_THROWS_WITH(ExpansionDefinition(ExpansionTestUtils::getDefnDummyDir(), opts.get()),
					Contains("cycle") &&
					Contains("group 'a'") && !Contains("group 'b'") && Contains("group 'c'") &&
					!Contains("group 'd'") && Contains("group 'e'") && Contains("group 'nc'"));
			}
		}
	}
}

SCENARIO("ExpansionDefinition - Cycle detection of functions, aliases and nested templates")
{
	GIVEN("Multi-layered dependencies with non-tree arrangement and no cycle")
	{
		auto opts = setupFunctionCycleDetectionTestForDefinition(
			"multi_layer_subprops_no_cycle",
			DefinitionMode::Subproperty,
			"members_with_subprops_with_no_cycle");

		WHEN("Validating definition")
		{
			THEN("No cycle detected")
			{
				REQUIRE_NOTHROW(ExpansionDefinition(ExpansionTestUtils::getDefnDummyDir(), opts.get()));
			}
		}
	}

	GIVEN("Multi-layered dependencies with non-tree arrangement and no cycle")
	{
		auto opts = setupFunctionCycleDetectionTestForDefinition(
			"multi_layer_subprops_no_cycle",
			DefinitionMode::Subproperty,
			"members_with_subprops_with_no_cycle");

		WHEN("Validating definition")
		{
			THEN("No cycle detected")
			{
				REQUIRE_NOTHROW(ExpansionDefinition(ExpansionTestUtils::getDefnDummyDir(), opts.get()));
			}
		}
	}

	GIVEN("Cycle between two aliases")
	{
		auto opts = setupFunctionCycleDetectionTestForDefinition(
			"multi_layer_subprops_with_two_alias_cycle",
			DefinitionMode::Subproperty,
			"members_with_subprops_with_no_cycle");

		WHEN("Validating definition")
		{
			THEN("Cycle detected")
			{
				REQUIRE_THROWS_WITH(ExpansionDefinition(ExpansionTestUtils::getDefnDummyDir(), opts.get()),
					Contains("cycle") &&
					Contains("group 'd'") && Contains("member 'c1.nc.0'") && Contains("group 'nc'"));
			}
		}
	}

	GIVEN("Cycle between three member nested templates")
	{
		auto opts = setupFunctionCycleDetectionTestForDefinition(
			"multi_layer_subprops_with_three_nested_template_cycle",
			DefinitionMode::Subproperty,
			"members_with_subprops_with_three_nested_template_cycle");

		WHEN("Validating definition")
		{
			THEN("Cycle detected")
			{
				REQUIRE_THROWS_WITH(ExpansionDefinition(ExpansionTestUtils::getDefnDummyDir(), opts.get()),
					Contains("cycle") &&
					Contains("group 'a'") && Contains("member 'e1'") && Contains("group 'e'") &&
					Contains("member 'c1'") && Contains("member 'a1'"));
			}
		}
	}

	GIVEN("Long cycle including alias, member nested template and group nested template")
	{
		auto opts = setupFunctionCycleDetectionTestForDefinition(
			"multi_layer_subprops_with_long_cycle",
			DefinitionMode::Subproperty,
			"members_with_subprops_with_long_cycle");

		WHEN("Validating definition")
		{
			THEN("Cycle detected")
			{
				REQUIRE_THROWS_WITH(ExpansionDefinition(ExpansionTestUtils::getDefnDummyDir(), opts.get()),
					Contains("cycle") &&
					Contains("group 'a'") && Contains("member 'c1'") && Contains("member 'c1.nc.0'") &&
					Contains("group 'nc'") && Contains("group 'e'") && Contains("member 'g1'") &&
					Contains("group 'g'") && Contains("group 'a'") && Contains("member 'a1'") &&
					Contains("member 'a1'"));
			}
		}
	}
}

SCENARIO("ExpansionDefinition - Cycle detection of substitution dictionaries")
{
	GIVEN("No cycle in a large dictionary graph")
	{
		auto opts = setupSubstitutionDictCycleDetectionTestForDefinition("no_cycle");

		THEN("No cycle detected")
		{
			REQUIRE_NOTHROW(ExpansionDefinition(ExpansionTestUtils::getDefnDummyDir(), opts.get()));
		}
	}

	GIVEN("Cycle between the universal dictionary and itself")
	{
		auto opts = setupSubstitutionDictCycleDetectionTestForDefinition("self_cycle_universal", "scheme_universal_dict_only.json");

		THEN("Cycle detected")
		{
			REQUIRE_THROWS_WITH(ExpansionDefinition(ExpansionTestUtils::getDefnDummyDir(), opts.get()),
				Contains("cycle") &&
				Contains("dict 'universal'") && Contains("key 'sub'"));
		}
	}

	GIVEN("Cycle between a local dictionary and itself")
	{
		auto opts = setupSubstitutionDictCycleDetectionTestForDefinition("self_cycle_local", "scheme_single_dict.json");

		THEN("Cycle detected")
		{
			REQUIRE_THROWS_WITH(ExpansionDefinition(ExpansionTestUtils::getDefnDummyDir(), opts.get()),
				Contains("cycle") &&
				Contains("dict 'dict_a'") && Contains("key 'sub'") && !Contains("dict 'universal'"));
		}
	}

	GIVEN("Cycle in large dictionary graph across universal and local dictionaries")
	{
		auto opts = setupSubstitutionDictCycleDetectionTestForDefinition("long_cycle");

		THEN("Cycle detected")
		{
			REQUIRE_THROWS_WITH(ExpansionDefinition(ExpansionTestUtils::getDefnDummyDir(), opts.get()),
				Contains("cycle") &&
				Contains("dict 'dict_a'") && Contains("dict 'dict_b'") && Contains("dict 'universal'") && Contains("dict 'dict_c'") &&
				Contains("key 'sub'") && Contains("key 'sub_2'") && Contains("key 'sub_others_2'") && Contains("key 'sub_others_1'"));
		}
	}

	GIVEN("Cycle containing a dictionary not included in the expansion scheme")
	{
		auto opts = setupSubstitutionDictCycleDetectionTestForDefinition("long_cycle_with_excluded_dict",
			"scheme_long_cycle_with_excluded_dict.json");

		THEN("No cycle")
		{
			REQUIRE_NOTHROW(ExpansionDefinition(ExpansionTestUtils::getDefnDummyDir(), opts.get()));
		}
	}
}

SCENARIO("ExpansionDefinition - Scheme dictionary referencing")
{
	GIVEN("All existent dictionaries selected for path or included in dictionary set")
	{
		auto opts = setupSchemeDictRefTestForDefinition(
			"valid_schemes/all_dicts_selected_plus_custom_universal.jsonc",
			vector<string>{"multi_dicts", "universal_dict", "custom_universal_dict"});

		THEN("Valid")
		{
			CHECK_NOTHROW(ExpansionDefinition(ExpansionTestUtils::getDefnDummyDir(), opts.get()));
		}
	}

	GIVEN("Non-existent universal dictionary")
	{
		auto opts = setupSchemeDictRefTestForDefinition(
			"valid_schemes/all_dicts_selected.jsonc",
			vector<string>{"multi_dicts", "custom_universal_dict"});

		THEN("Raises error")
		{
			CHECK_THROWS_WITH(ExpansionDefinition(ExpansionTestUtils::getDefnDummyDir(), opts.get()),
				Contains("universal.json") && Contains("not found"));
		}
	}

	GIVEN("Non-existent custom-named universal dictionary")
	{
		auto opts = setupSchemeDictRefTestForDefinition(
			"valid_schemes/all_dicts_selected.jsonc",
			vector<string>{"multi_dicts", "universal_dict"},
			"my_universal");

		THEN("Raises error")
		{
			CHECK_THROWS_WITH(ExpansionDefinition(ExpansionTestUtils::getDefnDummyDir(), opts.get()),
				Contains("my_universal.json") && Contains("not found"));
		}
	}

	GIVEN("Non-existent dictionary selected for path")
	{
		auto opts = setupSchemeDictRefTestForDefinition(
			"invalid_schemes/missing_dict_in_subdir.jsonc",
			vector<string>{"multi_dicts", "universal_dict", "custom_universal_dict"});

		THEN("Raises error")
		{
			CHECK_THROWS_WITH(ExpansionDefinition(ExpansionTestUtils::getDefnDummyDir(), opts.get()),
				Contains("id 'd'") && Contains("subdirectory '.'"));
		}
	}

	GIVEN("Non-existent dictionary included in selected dictionary set")
	{
		auto opts = setupSchemeDictRefTestForDefinition(
			"invalid_schemes/missing_dict_in_set.jsonc",
			vector<string>{"multi_dicts", "universal_dict", "custom_universal_dict"});

		THEN("Raises error")
		{
			CHECK_THROWS_WITH(ExpansionDefinition(ExpansionTestUtils::getDefnDummyDir(), opts.get()),
				Contains("id 'd'") && Contains("dictionary set 'set_a'"));
		}
	}

	GIVEN("Non-existent dictionary used by substitution dictionary entry")
	{
		auto opts = setupSchemeDictRefTestForDefinition(
			"invalid_schemes/excluded_dict_in_nested_substitution.json",
			vector<string>{"universal_dict", "invalid_dicts"});

		THEN("Raises error")
		{
			CHECK_THROWS_WITH(ExpansionDefinition(ExpansionTestUtils::getDefnDummyDir(), opts.get()),
				Contains("dictionary 'ref_to_excluded_dict'") &&
				Contains("key 'bad_ref'") &&
				Contains("<<missing@missing>>"));
		}
	}

	GIVEN("Non-existent dictionary entry used by substitution dictionary entry")
	{
		auto opts = setupSchemeDictRefTestForDefinition(
			"invalid_schemes/missing_field_in_nested_substitution.json",
			vector<string>{"universal_dict", "invalid_dicts"});

		THEN("Raises error")
		{
			CHECK_THROWS_WITH(ExpansionDefinition(ExpansionTestUtils::getDefnDummyDir(), opts.get()),
				Contains("dictionary 'ref_to_missing_field'") &&
				Contains("key 'bad_ref'") &&
				Contains("<<missing>>"));
		}
	}
}
