#include <catch2/catch.hpp>

#include "BasicTermResolver.hpp"
#include "Cache.tpp"
#include "CacheMap.tpp"
#include "ContextualEntryReference.hpp"
#include "ContextualTermResolver.hpp"
#include "EntryReference.hpp"
#include "ExpansionContext.hpp"
#include "ExpansionContextItem.hpp"
#include "ExpansionDefinition.hpp"
#include "ExpansionMember.hpp"
#include "ExpansionMemberFieldMap.tpp"
#include "ExpansionGroup.tpp"
#include "ExpansionTestUtils.hpp"
#include "NestedExpansionMember.hpp"
#include "NestedExpansionGroup.hpp"
#include "PropertyResolver.tpp"
#include "TemplateTree.hpp"
#include "Tree.tpp"
#include "TreeNode.tpp"

#include <vector>

using namespace std;
namespace fs = filesystem;

fs::path const resolverTestDir = "property_resolver";
size_t const invalidSize = numeric_limits<size_t>::max();

void mapIndicesToTree(Tree<ExpansionContextItem>* tree, vector<size_t> const& indices)
{
	auto contextNodes = Tree<ExpansionContextItem>::traverseAllDepthFirst(tree->getRoot());
	for (size_t i = 0; i < indices.size(); i++)
	{
		if (indices.at(i) != invalidSize)
		{
			contextNodes.at(i).first->modifyItem(
				[&indices, i](ExpansionContextItem* item)
				{ item->setIndex(indices.at(i)); });
		}
	}
}

auto singleTermContext = Cache<shared_ptr<ExpansionContext>>([]()
{
	auto context = make_shared<ExpansionContext>(*ExpansionTestUtils::defnTestContext().get(make_pair(resolverTestDir, string("") +
		"a(fm3_b(fm3_c,sfm4_c),fm3_b[1].v4_f,sfm3_c[1].nm3_nc,v2_i,sfm3_c(v4_u))," +
		"b(sv3_s,nm1_nb.sfm3_c,v4_f,sfm4_c[2].v4_u)," +
		"c(nm3_nc(sfm1_a.v2_i,fm3_a,fm3_a[0].fm3_b))," +
		"d" +
		"@x.template"
	)));

	// Populates the context tree with indices to simulate a member combination.
	auto indices = vector<size_t>{
		invalidSize,										 // root
		1, 2, 1, 3, invalidSize, 3, invalidSize, 1, 1, 2, 3, // a & subproperties
		3, 2, invalidSize, 1, 2, invalidSize, 3,			 // b & subproperties
		2, 1, invalidSize, 1, 2, invalidSize, 2,			 // c & subproperties
		3													 // d
	};
	mapIndicesToTree(context->tree, indices);

	return context;
});

auto fullPropertyContext = Cache<shared_ptr<ExpansionContext>>([]()
{
	auto context = make_shared<ExpansionContext>(*ExpansionTestUtils::defnTestContext().get(make_pair(resolverTestDir, string("") +
		"a(" +
			"sfm3_c[0].nm3_nc(" +
				"fm3_d(fm4_a,v3_s)," +
				"v4_f," +
				"sfm1_a.fm3_b" +
			")," +
			"sfm3_c(v4_u)" +
		")," +
		"b(" +
			"nm1_nb.fm4_d(" +
				"fm4_a(sfm1_b.v4_f,fm3_b)," +
				"v3_s" +
			")" +
		")" +
		"@x.template"
	)));

	// Populates the context tree with indices to simulate a member combination.
	auto indices = vector<size_t>{
		invalidSize,
		1, // a
			invalidSize, 2, 	// sfm3_c[0].nm3_nc
				0, 1, 1, 		// fm3_d(fm4_a,v3_s)
				3, 				// v4_f
				invalidSize, 2, // sfm1_a.fm3_b
			2, 3, 				// sfm3_c(v4_u)
		3, // b
			invalidSize, 1, 			// nm1_nb.fm4_d
				0, invalidSize, 2, 1, 	// fm4_a(sfm1_b.v4_f,fm3_b)
				2 						// v3_s
	};
	mapIndicesToTree(context->tree, indices);

	return context;
});

SCENARIO("ContextualPropertyResolver - Group Referencing", "[!mayfail]")
{
	REQUIRE(false);
}

SCENARIO("ContextualPropertyResolver - Single Term Resolution")
{
	auto defn = ExpansionTestUtils::defnTestDefinition().get(resolverTestDir);
	auto context = singleTermContext.get();
	auto termResolver = ContextualTermResolver(defn.get(), context.get());

	auto aNode = context->tree->getRoot()->getChild(0);
	auto bNode = context->tree->getRoot()->getChild(1);
	auto cNode = context->tree->getRoot()->getChild(2);
	auto dNode = context->tree->getRoot()->getChild(3);

	auto aGroup = aNode->getItem().getEntryRef().getGroup();
	auto bGroup = bNode->getItem().getEntryRef().getGroup();

	auto aRef = ContextualEntryReference::initFromGroupId(context.get(), "a");
	auto bRef = ContextualEntryReference::initFromGroupId(context.get(), "b");
	auto cRef = ContextualEntryReference::initFromGroupId(context.get(), "c");
	auto dRef = ContextualEntryReference::initFromGroupId(context.get(), "d");
	
	GIVEN("Entry reference of group context node with index x")
	{
		WHEN("Resolving term (mf, -) belonging to an expanded child A")
		{
			auto ref1 = termResolver.resolveTerm(aRef, "fm3_b", invalidSize);
			auto ref2 = termResolver.resolveTerm(aRef, "v2_i", invalidSize);
			auto ref3 = termResolver.resolveTerm(cRef, "nm3_nc", invalidSize);

			THEN("Returns entry reference with x-th member of group and index of node A")
			{
				CHECK(!ref1.getContextRef()->isFinalised());
				CHECK(ref1.getContextRef()->getContextNode() == aNode->getChild(0));
				CHECK(*ref1.getEntryRef() == EntryReference(defn->getMemberOfId("a2"), "fm3_b", 2));

				CHECK(!ref2.getContextRef()->isFinalised());
				CHECK(ref2.getContextRef()->getContextNode() == aNode->getChild(3));
				CHECK(*ref2.getEntryRef() == EntryReference(defn->getMemberOfId("a2"), "v2_i", 1));

				CHECK(!ref3.getContextRef()->isFinalised());
				CHECK(ref3.getContextRef()->getContextNode() == cNode->getChild(0));
				CHECK(*ref3.getEntryRef() == EntryReference(defn->getMemberOfId("c3"), "nm3_nc", 1));
			}
		}

		WHEN("Resolving term (gf, -) belonging to an expanded child A")
		{
			auto ref1 = termResolver.resolveTerm(bRef, "sv3_s", invalidSize);

			THEN("Returns entry reference with group and index of node A")
			{
				CHECK(!ref1.getContextRef()->isFinalised());
				CHECK(ref1.getContextRef()->getContextNode() == bNode->getChild(0));
				CHECK(*ref1.getEntryRef() == EntryReference(bGroup, "sv3_s", 2));
			}
		}

		WHEN("Resolving term (mf, n) with field belonging to an expanded child A")
		{	
			auto ref1 = termResolver.resolveTerm(aRef, "fm3_b", 2);
			auto ref2 = termResolver.resolveTerm(aRef, "v2_i", 0);
			auto ref3 = termResolver.resolveTerm(cRef, "nm3_nc", 0);

			THEN("Returns finalised entry reference with x-th member of group and supplied index")
			{
				CHECK(ref1.getContextRef()->isFinalised());
				CHECK(ref1.getContextRef()->getContextNode() == aNode);
				CHECK(*ref1.getEntryRef() == EntryReference(defn->getMemberOfId("a2"), "fm3_b", 2));

				CHECK(ref2.getContextRef()->isFinalised());
				CHECK(ref2.getContextRef()->getContextNode() == aNode);
				CHECK(*ref2.getEntryRef() == EntryReference(defn->getMemberOfId("a2"), "v2_i", 0));

				CHECK(ref3.getContextRef()->isFinalised());
				CHECK(ref3.getContextRef()->getContextNode() == cNode);
				CHECK(*ref3.getEntryRef() == EntryReference(defn->getMemberOfId("c3"), "nm3_nc", 0));
			}
		}

		WHEN("Resolving term (gf, n) with field belonging to an expanded child A")
		{
			auto ref1 = termResolver.resolveTerm(aRef, "sfm3_c", 2);

			THEN("Returns finalised entry reference with group and supplied index")
			{
				CHECK(ref1.getContextRef()->isFinalised());
				CHECK(ref1.getContextRef()->getContextNode() == aNode);
				CHECK(*ref1.getEntryRef() == EntryReference(aGroup, "sfm3_c", 2));
			}
		}

		WHEN("Resolving term (mf, -/n) belonging to a non-expanded child A")
		{	
			auto ref1 = termResolver.resolveTerm(aRef, "fm3_b", 1);
			auto ref2 = termResolver.resolveTerm(bRef, "nm1_nb", invalidSize);

			THEN("Returns entry reference with x-th member of group and supplied index")
			{
				CHECK(!ref1.getContextRef()->isFinalised());
				CHECK(ref1.getContextRef()->getContextNode() == aNode->getChild(1));
				CHECK(*ref1.getEntryRef() == EntryReference(defn->getMemberOfId("a2"), "fm3_b", 1));

				CHECK(!ref2.getContextRef()->isFinalised());
				CHECK(ref2.getContextRef()->getContextNode() == bNode->getChild(1));
				CHECK(*ref2.getEntryRef() == EntryReference(defn->getMemberOfId("b4"), "nm1_nb", invalidSize));
			}
		}

		WHEN("Resolving term (gf, -/n) belonging to a non-expanded child A")
		{	
			auto ref1 = termResolver.resolveTerm(bRef, "sfm4_c", 2);

			THEN("Returns entry reference with group and supplied index")
			{
				CHECK(!ref1.getContextRef()->isFinalised());
				CHECK(ref1.getContextRef()->getContextNode() == bNode->getChild(3));
				CHECK(*ref1.getEntryRef() == EntryReference(bGroup, "sfm4_c", 2));
			}
		}

		WHEN("Resolving valid term (mf, -/n) outside the context tree")
		{
			auto ref1 = termResolver.resolveTerm(bRef, "alias", invalidSize);
			auto ref2 = termResolver.resolveTerm(dRef, "fm4_a", 1);

			THEN("Returns finalised entry reference with x-th member of group")
			{
				CHECK(ref1.getContextRef()->isFinalised());
				CHECK(ref1.getContextRef()->getContextNode() == bNode);
				CHECK(*ref1.getEntryRef() == EntryReference(defn->getMemberOfId("b4"), "alias", invalidSize));

				CHECK(ref2.getContextRef()->isFinalised());
				CHECK(ref2.getContextRef()->getContextNode() == dNode);
				CHECK(*ref2.getEntryRef() == EntryReference(defn->getMemberOfId("d4"), "fm4_a", 1));
			}
		}

		WHEN("Resolving valid term (gf, -/n) outside the context tree")
		{
			auto ref1 = termResolver.resolveTerm(aRef, "sfm1_b", invalidSize);

			THEN("Returns finalised entry reference with group")
			{
				CHECK(ref1.getContextRef()->isFinalised());
				CHECK(ref1.getContextRef()->getContextNode() == aNode);
				CHECK(*ref1.getEntryRef() == EntryReference(aGroup, "sfm1_b", invalidSize));
			}
		}

		WHEN("Resolving invalid term with non-existent field")
		{	
			THEN("Raises error")
			{
				CHECK_THROWS(termResolver.resolveTerm(aRef, "missing", invalidSize));
				CHECK_THROWS(termResolver.resolveTerm(aRef, "missing", 0));
			}
		}

		WHEN("Resolving invalid term with field matching a context node")
		{	
			THEN("Raises error")
			{
				CHECK_THROWS(termResolver.resolveTerm(bRef, "v4_f", 4));
				CHECK_THROWS(termResolver.resolveTerm(bRef, "sfm4_c", 4));
				CHECK_THROWS(termResolver.resolveTerm(bRef, "alias", 0));
			}
		}

		WHEN("Resolving invalid term with field outside of the context tree")
		{	
			THEN("Raises error")
			{
				CHECK_THROWS(termResolver.resolveTerm(aRef, "sv2_s", invalidSize));
				CHECK_THROWS(termResolver.resolveTerm(aRef, "fm1_b", 0));
			}
		}
	}

	GIVEN("Entry reference with member context node")
	{
		auto mem_a_b_1 = termResolver.resolveTerm(aRef, "fm3_b", invalidSize);
		auto mem_a_b_2 = termResolver.resolveTerm(aRef, "fm3_b", 1);
		auto mem_a_c_1 = termResolver.resolveTerm(aRef, "sfm3_c", invalidSize);
		auto mem_a_c_2 = termResolver.resolveTerm(aRef, "sfm3_c", 1);
		auto mem_c_nc_1 = termResolver.resolveTerm(cRef, "nm3_nc", invalidSize);

		WHEN("Resolving term to expanding context node")
		{
			auto ref1 = termResolver.resolveTerm(mem_a_b_1, "sfm4_c", invalidSize);
			auto ref2 = termResolver.resolveTerm(mem_a_c_1, "v4_u", invalidSize);
			auto ref3 = termResolver.resolveTerm(mem_a_c_2, "nm3_nc", invalidSize);
			auto ref4 = termResolver.resolveTerm(mem_c_nc_1, "fm3_a", invalidSize);

			THEN("Returns entry reference with group or expanded member")
			{
				CHECK(!ref1.getContextRef()->isFinalised());
				CHECK(ref1.getContextRef()->getContextNode() == aNode->getChild(0)->getChild(1));
				CHECK(*ref1.getEntryRef() == EntryReference(defn.get()->getForeignGroupOfId("b"), "sfm4_c", 3));

				CHECK(!ref2.getContextRef()->isFinalised());
				CHECK(ref2.getContextRef()->getContextNode() == aNode->getChild(4)->getChild(0));
				CHECK(*ref2.getEntryRef() == EntryReference(defn.get()->getMemberOfId("c3"), "v4_u", 3));

				CHECK(!ref3.getContextRef()->isFinalised());
				CHECK(ref3.getContextRef()->getContextNode() == aNode->getChild(2)->getChild(0));
				CHECK(*ref3.getEntryRef() == EntryReference(defn.get()->getMemberOfId("c2"), "nm3_nc", 1));

				CHECK(!ref4.getContextRef()->isFinalised());
				CHECK(ref4.getContextRef()->getContextNode() == cNode->getChild(0)->getChild(1));
				CHECK(*ref4.getEntryRef() == EntryReference(
					defn.get()->getMemberOfId("c3")->getFieldMap()->getNestedMember("nm3_nc", 1), "fm3_a", 2));
			}
		}

		WHEN("Resolving term to non-expanding context node")
		{
			auto ref1 = termResolver.resolveTerm(mem_c_nc_1, "sfm1_a", invalidSize);
			auto ref2 = termResolver.resolveTerm(mem_c_nc_1, "fm3_a", 0);

			THEN("Returns entry reference with group or indexed member")
			{
				CHECK(!ref1.getContextRef()->isFinalised());
				CHECK(ref1.getContextRef()->getContextNode() == cNode->getChild(0)->getChild(0));
				CHECK(*ref1.getEntryRef() == EntryReference(defn.get()->getNestedGroupOfId("nc"), "sfm1_a", invalidSize));

				CHECK(!ref2.getContextRef()->isFinalised());
				CHECK(ref2.getContextRef()->getContextNode() == cNode->getChild(0)->getChild(2));
				CHECK(*ref2.getEntryRef() == EntryReference(
					defn.get()->getMemberOfId("c3")->getFieldMap()->getNestedMember("nm3_nc", 1), "fm3_a", 0));
			}
		}

		WHEN("Resolving term to outside context tree")
		{
			auto ref1 = termResolver.resolveTerm(mem_a_b_1, "alias", invalidSize);
			auto ref2 = termResolver.resolveTerm(mem_c_nc_1, "fm3_a", 1);
			auto ref3 = termResolver.resolveTerm(mem_a_b_1, "sv3_s", 2);

			THEN("Returns finalised entry reference with group or indexed member")
			{
				CHECK(ref1.getContextRef()->isFinalised());
				CHECK(ref1.getContextRef()->getContextNode() == aNode->getChild(0));
				CHECK(*ref1.getEntryRef() == EntryReference(defn.get()->getMemberOfId("b4"), "alias", invalidSize));

				CHECK(ref2.getContextRef()->isFinalised());
				CHECK(ref2.getContextRef()->getContextNode() == cNode->getChild(0));
				CHECK(*ref2.getEntryRef() == EntryReference(
					defn.get()->getMemberOfId("c3")->getFieldMap()->getNestedMember("nm3_nc", 1), "fm3_a", 1));

				CHECK(ref3.getContextRef()->isFinalised());
				CHECK(ref3.getContextRef()->getContextNode() == aNode->getChild(0));
				CHECK(*ref3.getEntryRef() == EntryReference(defn.get()->getForeignGroupOfId("b"), "sv3_s", 2));
			}
		}

		WHEN("Resolving invalid term")
		{
			THEN("Raises error")
			{
				CHECK_THROWS(termResolver.resolveTerm(mem_a_b_1, "alias", 0));
				CHECK_THROWS(termResolver.resolveTerm(mem_c_nc_1, "fm3_a", 3));
				CHECK_THROWS(termResolver.resolveTerm(mem_a_b_1, "sv3_s", invalidSize));
			}
		}
	}

	GIVEN("Finalised entry reference with group or member context node")
	{
		auto mem_d_a = termResolver.resolveTerm(dRef, "fm4_a", 0);
		auto mem_a_b = termResolver.resolveTerm(aRef, "fm3_b", invalidSize);
		auto mem_a_b_nb = termResolver.resolveTerm(mem_a_b, "nm1_nb", invalidSize);
		auto mem_a_c = termResolver.resolveTerm(aRef, "sfm3_c", 2);

		WHEN("Resolving term")
		{
			auto ref1 = termResolver.resolveTerm(mem_d_a, "v2_i", 1);
			auto ref2 = termResolver.resolveTerm(mem_a_b, "alias", invalidSize);
			auto ref3 = termResolver.resolveTerm(mem_a_b_nb, "sfm3_c", 0);
			auto ref4 = termResolver.resolveTerm(mem_a_c, "func", invalidSize);

			THEN("Original context node remains")
			{
				CHECK(ref1.getContextRef()->isFinalised());
				CHECK(ref1.getContextRef()->getContextNode() == dNode);
				CHECK(*ref1.getEntryRef() == EntryReference(defn->getMemberOfId("a4"), "v2_i", 1));

				CHECK(ref2.getContextRef()->isFinalised());
				CHECK(ref2.getContextRef()->getContextNode() == aNode->getChild(0));
				CHECK(*ref2.getEntryRef() == EntryReference(defn->getMemberOfId("b4"), "alias", invalidSize));

				CHECK(ref3.getContextRef()->isFinalised());
				CHECK(ref3.getContextRef()->getContextNode() == aNode->getChild(0));
				CHECK(*ref3.getEntryRef() == EntryReference(defn->getNestedGroupOfId("nb"), "sfm3_c", 0));

				CHECK(ref4.getContextRef()->isFinalised());
				CHECK(ref4.getContextRef()->getContextNode() == aNode);
				CHECK(*ref4.getEntryRef() == EntryReference(defn->getMemberOfId("c3"), "func", invalidSize));
			}
		}
	}

	GIVEN("Entry reference with value context node")
	{
		auto val1 = termResolver.resolveTerm(aRef, "v2_i", invalidSize);
		auto val2 = termResolver.resolveTerm(bRef, "sv3_s", invalidSize);

		WHEN("Resolving")
		{
			THEN("Raises error")
			{
				CHECK_THROWS(termResolver.resolveTerm(val1, "anything", 0));
				CHECK_THROWS(termResolver.resolveTerm(val2, "anything", invalidSize));
			}
		}
	}
}

SCENARIO("ContextualPropertyResolver - Full Property Resolution")
{
	auto defn = ExpansionTestUtils::defnTestDefinition().get(resolverTestDir);
	auto context = fullPropertyContext.get();

	auto termResolver = ContextualTermResolver(defn.get(), context.get());
	auto propResolver = PropertyResolver<ContextualEntryReference>(termResolver);

	auto aNode = context->tree->getRoot()->getChild(0);

	auto aRef = ContextualEntryReference::initFromGroupId(context.get(), "a");
	auto bRef = ContextualEntryReference::initFromGroupId(context.get(), "b");

	GIVEN("Valid property ending with value")
	{
		// a
		auto prop1 = TemplateTree::parseProperty("sfm3_c[0].nm3_nc.fm3_d.fm4_a.fm3_b[2].v1_f");
		auto prop2 = TemplateTree::parseProperty("sfm3_c[0].nm3_nc.sfm1_a.fm3_b.nm1_nb.name");
		auto prop3 = TemplateTree::parseProperty("sfm3_c[0].nm3_nc.fm3_d.fm4_a[0].v2_i[1]");
		auto prop4 = TemplateTree::parseProperty("sfm3_c[0].nm3_nc[1].fm3_d[1].v3_s[0]");
		auto prop5 = TemplateTree::parseProperty("sfm3_c.v4_u");
		auto prop6 = TemplateTree::parseProperty("sfm3_c.fm1_a.fm3_b[2].fm3_c[0].func");

		// b
		auto prop7 = TemplateTree::parseProperty("nm1_nb.fm4_d.fm4_a.sfm1_b.v4_f");
		auto prop8 = TemplateTree::parseProperty("nm1_nb.fm4_d.fm4_a.fm3_b.alias");
		auto prop9 = TemplateTree::parseProperty("nm1_nb.sfm3_c[1].sv3_s[0]");
		auto prop10 = TemplateTree::parseProperty("nm1_nb.fm4_d.fm4_a[3].nt_1");

		WHEN("Resolving final term")
		{
			THEN("Resolves to correct entry reference")
			{
				auto contextNode = aNode->getChild(0)->getChild(0)->getChild(0)->getChild(0);
				auto viewNode = aRef.getContextRef()->getViewNode()->getChild(0)->getChild(0)->getChild(0);
				CHECK(propResolver.resolveFinalTerm(aRef, prop1->tree->getRoot(), 0).value() ==
					ContextualEntryReference(
						ContextReference(contextNode, viewNode, true),
						EntryReference(defn->getMemberOfId("b2"), "v1_f", invalidSize)));
			}
		}

		WHEN("Resolving final value")
		{
			THEN("Resolves to correct value")
			{
				CHECK(propResolver.resolveFinalValueString(aRef, prop1->tree->getRoot(), 0) == "220");
				CHECK(propResolver.resolveFinalValueString(aRef, prop2->tree->getRoot(), 0) == "nb1");
				CHECK(propResolver.resolveFinalValueString(aRef, prop3->tree->getRoot(), 0) == "131");
				CHECK(propResolver.resolveFinalValueString(aRef, prop4->tree->getRoot(), 0) == "d3-one");
				CHECK(propResolver.resolveFinalValueString(aRef, prop5->tree->getRoot(), 0) == "434");
				CHECK(propResolver.resolveFinalValueString(aRef, prop6->tree->getRoot(), 0) == "131, " + to_string(210.0) + ", c4");
				CHECK(propResolver.resolveFinalValueString(bRef, prop7->tree->getRoot(), 0) == "211.3");
				CHECK(propResolver.resolveFinalValueString(bRef, prop8->tree->getRoot(), 0) == "220");
				CHECK(propResolver.resolveFinalValueString(bRef, prop9->tree->getRoot(), 0) == "apple");
				CHECK(propResolver.resolveFinalValueString(bRef, prop10->tree->getRoot(), 0) == "a4 in the house: nc2_1 and 140 and nb4_1");
			}
		}
	}

	GIVEN("Valid property ending with member inside context tree")
	{
		auto prop1 = TemplateTree::parseProperty("sfm3_c[0].nm3_nc.fm3_d.fm4_a");

		WHEN("Resolving final entry reference")
		{
			THEN("Resolves to correct entry reference")
			{
				auto contextNode = aNode->getChild(0)->getChild(0)->getChild(0)->getChild(0);
				auto viewNode = aRef.getContextRef()->getViewNode()->getChild(0)->getChild(0)->getChild(0);
				CHECK(propResolver.resolveFinalTerm(aRef, prop1->tree->getRoot(), 0).value() ==
					ContextualEntryReference(
						ContextReference(contextNode, viewNode, false),
						EntryReference(defn->getMemberOfId("d3"), "fm4_a", 1)));
			}
		}
		
		WHEN("Resolving final value")
		{
			THEN("Resolves to correct member id")
			{
				CHECK(propResolver.resolveFinalValueString(aRef, prop1->tree->getRoot(), 0) == "a4");
			}
		}
	}
}

SCENARIO("ContextualPropertyResolver - Errors in Invalid Full Property Resolution", "[!mayfail]")
{
	auto defn = ExpansionTestUtils::defnTestDefinition().get(resolverTestDir);
	auto context = fullPropertyContext.get();

	auto termResolver = ContextualTermResolver(defn.get(), context.get());
	auto propResolver = PropertyResolver<ContextualEntryReference>(termResolver);

	auto aRef = ContextualEntryReference::initFromGroupId(context.get(), "a");
	auto bRef = ContextualEntryReference::initFromGroupId(context.get(), "b");

	// a

	// Member following value v4_f[2].
	auto prop1 = TemplateTree::parseProperty("sfm3_c[0].nm3_nc.v4_f[2].fm4_a.fm3_b[2].v1_f");
	
	// No index for non-expanded f3_a.
	auto prop2 = TemplateTree::parseProperty("sfm3_c[0].nm3_nc.fm3_a.v2_i[1]");

	// b

	// Index for unindexed and non-expanded member sfm1_b[0].
	auto prop3 = TemplateTree::parseProperty("nm1_nb.fm4_d.fm4_a.sfm1_b[0].v4_f");

	WHEN("Resolving property")
	{
		THEN("Raises error")
		{
			CHECK_THROWS(propResolver.resolveFinalValueString(aRef, prop1->tree->getRoot(), 0));
			CHECK_THROWS(propResolver.resolveFinalValueString(aRef, prop2->tree->getRoot(), 0));
			CHECK_THROWS(propResolver.resolveFinalValueString(aRef, prop3->tree->getRoot(), 0));
		}
	}
}
