#include <catch2/catch.hpp>

#include "BasicTermResolver.hpp"
#include "ContextualEntryReference.hpp"
#include "Expander.hpp"
#include "ExpanderOptions.hpp"
#include "ExpansionContext.hpp"
#include "ExpansionContextItem.hpp"
#include "ExpansionDefinition.hpp"
#include "ExpansionMember.hpp"
#include "ExpansionTestUtils.hpp"
#include "ExpansionTree.hpp"
#include "FileDifference.hpp"
#include "FileUtils.hpp"
#include "MemberCombination.hpp"
#include "NestedExpansionGroup.hpp"
#include "PropertyResolver.tpp"
#include "StdComparison.hpp"
#include "StdStringConversion.hpp"
#include "Tree.tpp"
#include "ValueEntry.tpp"

#include <fstream>
#include <memory>

using namespace std;
using Catch::Contains;
using Catch::CaseSensitive;
namespace fs = filesystem;

// TODO: Move scheme test directory under expander tests.
const fs::path schemeTestDir = ExpansionTestUtils::getTestResDir() / "scheme_tests";

fs::path const expanderTestDir = ExpansionTestUtils::getTestResDir() / "expander_tests";
fs::path const schemeDictRefTestDir = expanderTestDir / "scheme_dictionary_substitutions";
fs::path const subnTestDir = expanderTestDir / "substitutions";

tuple<shared_ptr<ExpanderOptions>, shared_ptr<ExpansionDefinition>>
setupSchemeTestDefn()
{
	auto opts = make_shared<ExpanderOptions>();
	opts->mode = ExpanderOptions::Mode::CleanThenExpand;
	opts->schemePath = fs::absolute(schemeTestDir / "scheme.json");
	auto defn = make_shared<ExpansionDefinition>(ExpansionTestUtils::getDefnDir(), opts.get());
	return make_tuple(opts, defn);
}

tuple<shared_ptr<ExpanderOptions>, shared_ptr<ExpansionDefinition>>
setupSchemeDictRefTestDefn()
{
	auto opts = make_shared<ExpanderOptions>();
	opts->mode = ExpanderOptions::Mode::CleanThenExpand;
	opts->schemePath = fs::absolute(schemeDictRefTestDir / "scheme.jsonc");
	auto defn = make_shared<ExpansionDefinition>(ExpansionTestUtils::getDefnDir(), opts.get());
	return make_tuple(opts, defn);
}

tuple<shared_ptr<ExpanderOptions>, shared_ptr<ExpansionDefinition>, shared_ptr<Expander>>
setupSubnTestExpander()
{
	auto opts = make_shared<ExpanderOptions>();
	opts->mode = ExpanderOptions::Mode::CleanThenExpand;
	opts->schemePath = fs::absolute(subnTestDir / "scheme.json");
	auto defn = make_shared<ExpansionDefinition>(ExpansionTestUtils::getDefnDir(), opts.get());
	auto expander = make_shared<Expander>(Expander(defn.get(), subnTestDir / "templates", subnTestDir / "actual_expansions"));
	return make_tuple(opts, defn, expander);
}

TEST_CASE("Expander Property Token Validation")
{
	// TODO: Test with multiple groups and nested groups in specifier.

	auto basicName = ExpansionTestUtils::getBasicName();
	auto definition = ExpansionTestUtils::getDefinition();
	auto context = ExpansionTestUtils::getContext(basicName);
	auto expander = ExpansionTestUtils::getExpander();

	// Extracts template nodes for following tests.
	auto templateNameTree = ExpansionTestUtils::getTemplateNameTree(basicName);
	auto groupSpec = TemplateTree::getGroupSpecifier(templateNameTree->tree->getRoot());
	auto const* expandableName = TemplateTree::getExpandableName(templateNameTree->tree->getRoot());
	auto propTokens = TemplateTree::getPropertyTokens(expandableName);
	auto propToken = propTokens.at(0);

	
	auto groups = TemplateTree::getGroups(groupSpec, definition.get());

	// Finds groups containing field in the definition. Checks a non-existent field doesn't match anything.
	CHECK(definition->getForeignGroupOfId(ExpansionGroup::findGroupsContainingField(groups, "model").at(0))->getId() == "car");
	CHECK(definition->getForeignGroupOfId(ExpansionGroup::findGroupsContainingField(groups, "speed").at(0))->getId() == "car");
	CHECK(ExpansionGroup::findGroupsContainingField(groups, "non_existent").size() == 0);

	// Dereferences the property token to the unique index of the property token.
	CHECK(ContextualEntryReference::getIndexOfUniqueGroupReference(groups, propToken) == 0);

	// Resolves foreign or nested group contained in field.
	auto const* carGroup = definition->getForeignGroupOfId("car");
	CHECK(BasicTermResolver(definition.get()).getGroup(EntryReference(carGroup, "engine", 0))->getId() == "car_engine");
	CHECK(BasicTermResolver(definition.get()).getGroup(EntryReference(carGroup, "wheels", 0))->getId() == "car_wheels");

	// Checks the property token expands as unique.
	CHECK_NOTHROW(expander->checkPropertyTokensExpand(context.get(), propTokens, true));
}

TEST_CASE("Expander Expand Property Tokens")
{
	auto multiGroupSpecName = ExpansionTestUtils::getValidMultiGroupName();
	shared_ptr<TemplateTree> templateNameTree = ExpansionTestUtils::getTemplateNameTree(multiGroupSpecName);
	shared_ptr<TemplateTree> templateContentsTree = ExpansionTestUtils::getTemplateContentsTree(multiGroupSpecName);
	shared_ptr<ExpansionContext> context = ExpansionTestUtils::getContext(multiGroupSpecName);
	shared_ptr<Expander> expander = ExpansionTestUtils::getExpander();

	// Checks expansion of basic property token in template name.
	// TODO: Use more complex examples.

	auto const* expandableName = TemplateTree::getExpandableName(templateNameTree->tree->getRoot());
	MemberCombination* memberCombo = MemberCombination::initFromContext(context.get());
	auto repls = expander->expandPropertyTokens(memberCombo, TemplateTree::getPropertyTokens(expandableName));
	CHECK(repls.size() == 5);
	CHECK(repls.at(0) == "car_buggy");
	CHECK(repls.at(1) == "adam");
	CHECK(repls.at(4) == "v4");

	// Checks expansion of more complex property tokens in contents of template.

	auto contentsLines = TemplateTree::getContentsLines(templateContentsTree->tree->getRoot());
	auto expandableLines = TemplateTree::filterExpandableLines(contentsLines);
	vector<TemplateNode const*> contentsPropTokens;
	for (auto const* line : expandableLines)
	{
		auto propTokens = TemplateTree::getPropertyTokens(line);
		contentsPropTokens.insert(contentsPropTokens.end(), propTokens.begin(), propTokens.end());
	}

	repls = expander->expandPropertyTokens(memberCombo, contentsPropTokens);
	CHECK(repls.size() == 7);
	CHECK(repls.at(0) == "basic_trim");
	CHECK(repls.at(1) == "30");
	CHECK(repls.at(2) == "rubber");
	CHECK(repls.at(3) == "100");
	CHECK(repls.at(4) == "Adam");
	CHECK(repls.at(5) == "Philippa");
	CHECK(repls.at(6) == "80");

	delete memberCombo;
}

TEST_CASE("Expander Expand Region")
{
	auto multiGroupSpecName = ExpansionTestUtils::getValidMultiGroupName();
	auto templateNameTree = ExpansionTestUtils::getTemplateNameTree(multiGroupSpecName);
	auto templateContentsTree = ExpansionTestUtils::getTemplateContentsTree(multiGroupSpecName);
	auto context = ExpansionTestUtils::getContext(multiGroupSpecName);
	auto expander = ExpansionTestUtils::getExpander();

	// Expansion tree creation from contents of file.
	ExpansionTree* expnTree;
	REQUIRE_NOTHROW(expnTree = ExpansionTree::initWithFileContents(
		templateContentsTree->tree->getRoot(), context.get(), context->definition, nullptr));

	// Using the expansion tree and member combination to populate the file.
	auto memberCombo = MemberCombination::initFromContext(context.get());
	auto ofs = ofstream(ExpansionTestUtils::getActualExpnDir() / "actual_1.expansion");
	REQUIRE_NOTHROW(expander->expandRegionIntoStream(ofs, expnTree->tree->getRoot()));

	delete memberCombo;
}

TEST_CASE("Expander External Expansion", "[.]")
{
	auto validMultiGroupSpecName = ExpansionTestUtils::getValidMultiGroupName();
	auto expander = ExpansionTestUtils::getExpander();

	REQUIRE_NOTHROW(expander->expandFile(validMultiGroupSpecName));
}

TEST_CASE("Expander Internal Expansion", "[.]")
{
	auto expander = ExpansionTestUtils::getExpander();

	string const templateName = "internal_expansion_test.template";
	REQUIRE_NOTHROW(expander->expandFile(templateName));
	
	string const templateName2 = "internal_expansion_test_2.template";
	REQUIRE_NOTHROW(expander->expandFile(templateName2));
}

SCENARIO("Expander - Expand schemes")
{
	// TODO: Move scheme test directory under expander tests.
	// This is a test for the expander using the scheme, not a unit test for the scheme itself.

	auto [opts, defn] = setupSchemeTestDefn();
	auto expander = Expander(defn.get(), schemeTestDir / "templates", schemeTestDir / "actual_expansions");
	
	REQUIRE_NOTHROW(expander.expandWithScheme(defn->getScheme()));

	CHECK(FileUtils::getDirectoryDifferenceMessage(FileUtils::compareDirectories(
		schemeTestDir / "expected_expansions",
		schemeTestDir / "actual_expansions")) == "");
}

SCENARIO("Expander - Expands substitutions for selected dictionaries in scheme")
{
	GIVEN("Dictionary(s) selected for path in scheme")
	{
		auto [opts, defn] = setupSchemeDictRefTestDefn();

		WHEN("Using selected dictionary entries in expansion")
		{
			auto expander = Expander(defn.get(),
				schemeDictRefTestDir / "templates" / "valid_all_included_dicts",
				schemeDictRefTestDir / "actual_expansions" / "valid_all_included_dicts");

			REQUIRE_NOTHROW(expander.expandWithScheme(defn->getScheme()));

			THEN("Substitutes correct values")
			{
				CHECK(!FileUtils::compareFiles(
					schemeDictRefTestDir / "expected_expansions" / "valid_all_included_dicts" / "page.expansion",
					schemeDictRefTestDir / "actual_expansions" / "valid_all_included_dicts" / "page.expansion")
						.isDifference());
			}
		}

		WHEN("Using selected dictionary entries inside a substitution")
		{
			auto expander = Expander(defn.get(),
				schemeDictRefTestDir / "templates" / "valid_nested_substitution",
				schemeDictRefTestDir / "actual_expansions" / "valid_nested_substitution");

			REQUIRE_NOTHROW(expander.expandWithScheme(defn->getScheme()));

			THEN("Substitutes correct values")
			{
				CHECK(!FileUtils::compareFiles(
					schemeDictRefTestDir / "expected_expansions" / "valid_nested_substitution" / "page.expansion",
					schemeDictRefTestDir / "actual_expansions" / "valid_nested_substitution" / "page.expansion")
						.isDifference());
			}
		}

		WHEN("Using any excluded dictionary")
		{
			auto expander = Expander(defn.get(),
				schemeDictRefTestDir / "templates" / "invalid_excluded_dict",
				schemeDictRefTestDir / "actual_expansions" / "invalid_excluded_dict");

			THEN("Raises error")
			{
				REQUIRE_THROWS_WITH(expander.expandWithScheme(defn->getScheme()),
					Contains("dictionary", CaseSensitive::No) && Contains("'geography'"));
			}
		}

		WHEN("Using the universal dictionary explicitly")
		{
			auto expander = Expander(defn.get(),
				schemeDictRefTestDir / "templates" / "invalid_explicit_universal_dict",
				schemeDictRefTestDir / "actual_expansions" / "invalid_explicit_universal_dict");

			THEN("Raises error")
			{
				REQUIRE_THROWS_WITH(expander.expandWithScheme(defn->getScheme()),
					Contains("explicit") && Contains("universal"));
			}
		}

		WHEN("Using any excluded dictionary inside a substitution")
		{
			auto expander = Expander(defn.get(),
				schemeDictRefTestDir / "templates" / "invalid_excluded_dict_in_nested_substitution",
				schemeDictRefTestDir / "actual_expansions" / "invalid_excluded_dict_in_nested_substitution");

			THEN("Raises error")
			{
				REQUIRE_THROWS_WITH(expander.expandWithScheme(defn->getScheme()),
					Contains("<<geography@igneous>>") && Contains("'geography'"));
			}
		}

		WHEN("Using a non-existent entry in a selected dictionary")
		{
			auto expander = Expander(defn.get(),
				schemeDictRefTestDir / "templates" / "invalid_missing_field_in_dict",
				schemeDictRefTestDir / "actual_expansions" / "invalid_missing_field_in_dict");

			THEN("Raises error")
			{
				REQUIRE_THROWS_WITH(expander.expandWithScheme(defn->getScheme()),
					Contains("non-existent") && Contains("'game'") && Contains("'missing'"));
			}
		}
	}
}

SCENARIO("Expander - Expands substitutions containing template text")
{
	GIVEN("Multi-line text")
	{
		auto [opts, defn, expander] = setupSubnTestExpander();

		THEN("Expands multiple lines")
		{
			REQUIRE_NOTHROW(expander->expandFile(fs::path("multi_line_text") / "page.template"));
			REQUIRE(FileUtils::getDirectoryDifferenceMessage(FileUtils::compareDirectories(
				subnTestDir / "actual_expansions" / "multi_line_text",
				subnTestDir / "expected_expansions" / "multi_line_text"
			)) == "");
		}
	}

	GIVEN("Property token in substitution")
	{
		auto [opts, defn, expander] = setupSubnTestExpander();

		THEN("Expands property token")
		{
			REQUIRE_NOTHROW(expander->expandFile(fs::path("property_token") / "king@<id>_laws.template"));
			REQUIRE(FileUtils::getDirectoryDifferenceMessage(FileUtils::compareDirectories(
				subnTestDir / "actual_expansions" / "property_token",
				subnTestDir / "expected_expansions" / "property_token"
			)) == "");
		}
	}

	GIVEN("Internal expansion directive in substitution")
	{
		auto [opts, defn, expander] = setupSubnTestExpander();

		THEN("Expands internal expansion directives")
		{
			REQUIRE_NOTHROW(expander->expandFile(fs::path("internal_expansion_directives") / "page.template"));
			REQUIRE(FileUtils::getDirectoryDifferenceMessage(FileUtils::compareDirectories(
				subnTestDir / "actual_expansions" / "internal_expansion_directives",
				subnTestDir / "expected_expansions" / "internal_expansion_directives"
			)) == "");
		}
	}

	GIVEN("Internal property token in substitution")
	{
		auto [opts, defn, expander] = setupSubnTestExpander();

		THEN("Expands internal property token")
		{
			REQUIRE_NOTHROW(expander->expandFile(fs::path("internal_property_token") / "page.template"));
			REQUIRE(FileUtils::getDirectoryDifferenceMessage(FileUtils::compareDirectories(
				subnTestDir / "actual_expansions" / "internal_property_token",
				subnTestDir / "expected_expansions" / "internal_property_token"
			)) == "");
		}
	}

	GIVEN("Nested substitution")
	{
		auto [opts, defn, expander] = setupSubnTestExpander();

		THEN("Recursively substitutes values")
		{
			REQUIRE_NOTHROW(expander->expandFile(fs::path("nested_substitution") / "page.template"));
			REQUIRE(FileUtils::getDirectoryDifferenceMessage(FileUtils::compareDirectories(
				subnTestDir / "actual_expansions" / "nested_substitution",
				subnTestDir / "expected_expansions" / "nested_substitution"
			)) == "");
		}
	}

	GIVEN("Everything: Multi-line nested substitutions with expansion directives and all property tokens")
	{
		auto [opts, defn, expander] = setupSubnTestExpander();

		THEN("Substitutes correct values")
		{
			REQUIRE_NOTHROW(expander->expandFile(fs::path("all_in_one") / "king@<id>s_kingdom.template"));
			REQUIRE(FileUtils::getDirectoryDifferenceMessage(FileUtils::compareDirectories(
				subnTestDir / "actual_expansions" / "all_in_one",
				subnTestDir / "expected_expansions" / "all_in_one"
			)) == "");
		}
	}
}
