#include <catch2/catch.hpp>

#include "BasicTermResolver.hpp"
#include "CacheMap.tpp"
#include "EntryReference.hpp"
#include "ExpanderOptions.hpp"
#include "ExpansionTestUtils.hpp"
#include "ExpansionDefinition.hpp"
#include "NestedExpansionGroup.hpp"
#include "PropertyResolver.tpp"
#include "TemplateTree.hpp"
#include "Tree.tpp"
#include "ValueEntry.tpp"

using namespace std;
namespace fs = filesystem;
using Catch::Contains;


fs::path const resolverTestDir = "property_resolver";
size_t const invalidSize = numeric_limits<size_t>::max();

SCENARIO("BasicPropertyResolver - Single Term Resolution")
{
	auto defn = ExpansionTestUtils::defnTestDefinition().get(resolverTestDir);
	auto termResolver = BasicTermResolver(defn.get());

	GIVEN("A reference to a group G")
	{
		auto ref1 = EntryReference(defn->getForeignGroupOfId("a"), "", invalidSize);
		auto ref2 = EntryReference(defn->getForeignGroupOfId("a"), "fm3_b", 2);

		WHEN("Resolving a group-contained term (gf, -/n)")
		{
			THEN("Resolves to (G, gf, -/n)")
			{
				CHECK(termResolver.resolveTerm(ref1, "sv2_s", 1) ==
					EntryReference(defn->getForeignGroupOfId("a"), "sv2_s", 1));
				CHECK(termResolver.resolveTerm(ref2, "alias", invalidSize) ==
					EntryReference(defn->getForeignGroupOfId("b"), "alias", invalidSize));
			}
		}

		WHEN("Resolving a member-contained term (mf, -/n)")
		{
			THEN("Resolves to (G, mf, -/n)")
			{
				CHECK(termResolver.resolveTerm(ref1, "v2_i", 0) ==
					EntryReference(defn->getForeignGroupOfId("a"), "v2_i", 0));
				CHECK(termResolver.resolveTerm(ref2, "name", invalidSize) ==
					EntryReference(defn->getForeignGroupOfId("b"), "name", invalidSize));
			}
		}
	}

	GIVEN("A reference to a member M")
	{
		auto ref1 = EntryReference(defn->getMemberOfId("a1"), "", invalidSize);
		auto ref2 = EntryReference(defn->getForeignGroupOfId("a"), "sfm3_c", 2);

		WHEN("Resolving a group-contained term (gf, -/n)")
		{
			THEN("Resolves to (G, gf, -/n)")
			{
				CHECK(termResolver.resolveTerm(ref1, "sv2_s", 1) ==
					EntryReference(defn->getForeignGroupOfId("a"), "sv2_s", 1));
			}
		}

		WHEN("Resolving a member-contained term (mf, -/n)")
		{
			THEN("Resolves to (M, mf, -/n)")
			{
				CHECK(termResolver.resolveTerm(ref1, "v2_i", 0) ==
					EntryReference(defn->getMemberOfId("a1"), "v2_i", 0));
				CHECK(termResolver.resolveTerm(ref2, "nm3_nc", 1) ==
					EntryReference(defn->getMemberOfId("c3"), "nm3_nc", 1));
				CHECK(termResolver.resolveTerm(ref2, "func", invalidSize) ==
					EntryReference(defn->getMemberOfId("c3"), "func", invalidSize));
			}
		}
	}

	GIVEN("A reference to a value")
	{
		auto ref1 = EntryReference(defn->getMemberOfId("a1"), "v2_i", 0);
		auto ref2 = EntryReference(defn->getForeignGroupOfId("c"), "func", invalidSize);

		WHEN("Resolvng any term (f, -/n)")
		{
			THEN("Error")
			{
				CHECK_THROWS(termResolver.resolveTerm(ref1, "fm1_b", invalidSize));
				CHECK_THROWS(termResolver.resolveTerm(ref2, "sfv3_s", 0));
			}
		}
	}
}

SCENARIO("BasicPropertyResolver - Full Property Resolution")
{
	auto defn = ExpansionTestUtils::defnTestDefinition().get(resolverTestDir);
	auto propResolver = PropertyResolver<EntryReference>(BasicTermResolver(defn.get()));

	GIVEN("Valid property ending with value")
	{
		auto mem1 = EntryReference(defn->getMemberOfId("a1"), "", invalidSize);
		auto prop1 = TemplateTree::parseProperty("sfm1_b.sfm4_c[3].fm1_a.v2_i[1]");

		auto mem2 = EntryReference(defn->getAnyGroupOfId("a"), "", invalidSize);
		auto prop2 = TemplateTree::parseProperty("sfm3_c[0].nm3_nc[0].name");

		auto mem3 = EntryReference(defn->getMemberOfId("b3"), "", invalidSize);
		auto prop3 = TemplateTree::parseProperty("nm1_nb.sfm3_c[2].func");

		auto mem4 = EntryReference(defn->getAnyGroupOfId("b"), "", invalidSize);
		auto prop4 = TemplateTree::parseProperty("sfm4_c[0].fm1_a.sv2_s[1]");

		auto mem5 = EntryReference(defn->getMemberOfId("c2"), "", invalidSize);
		auto prop5 = TemplateTree::parseProperty("sfm4_c[1].sfm4_c[1].nm3_nc[2].alias");

		auto mem6 = EntryReference(defn->getAnyGroupOfId("a"), "", invalidSize);
		auto prop6 = TemplateTree::parseProperty("sfm3_c[2].nm3_nc[1].v4_f[3]");

		WHEN("Resolving final term")
		{
			THEN("Resolves to correct entry reference")
			{
				CHECK(propResolver.resolveFinalTerm(mem1, prop1->tree->getRoot(), 0).value() ==
					EntryReference(defn->getMemberOfId("a4"), "v2_i", 1));
			}
		}

		WHEN("Resolving final value")
		{
			THEN("Resolves to correct value")
			{
				CHECK(propResolver.resolveFinalValueString(mem1, prop1->tree->getRoot(), 0) == "141");
				CHECK(propResolver.resolveFinalValueString(mem2, prop2->tree->getRoot(), 0) == "nc1_1");
				CHECK(propResolver.resolveFinalValueString(mem3, prop3->tree->getRoot(), 0) == "131, " + to_string(230.0) + ", c4");
				CHECK(propResolver.resolveFinalValueString(mem4, prop4->tree->getRoot(), 0) == "pasty");
				CHECK(propResolver.resolveFinalValueString(mem5, prop5->tree->getRoot(), 0) == "c4");
				CHECK(propResolver.resolveFinalValueString(mem6, prop6->tree->getRoot(), 0) == "43.32");
			}
		}
	}

	GIVEN("Valid property ending with nested template")
	{
		// Instance nested template of a1.
		auto mem1 = EntryReference(defn->getMemberOfId("b1"), "", invalidSize);
		auto prop1 = TemplateTree::parseProperty("fm3_c[0].nm3_nc[0].fm3_a[0].nt_1");

		// Instance nested template of a2.
		auto mem2 = EntryReference(defn->getMemberOfId("b1"), "", invalidSize);
		auto prop2 = TemplateTree::parseProperty("fm3_c[1].nm3_nc[0].fm3_a[0].nt_1");

		// Static nested template with no member-contained fields from a1.
		auto mem3 = EntryReference(defn->getMemberOfId("b1"), "", invalidSize);
		auto prop3 = TemplateTree::parseProperty("fm3_c[0].nm3_nc[0].fm3_a[0].snt_1");

		// Static nested template with member-contained fields from a2.
		auto mem4 = EntryReference(defn->getMemberOfId("b1"), "", invalidSize);
		auto prop4 = TemplateTree::parseProperty("fm3_c[1].nm3_nc[0].fm3_a[0].snt_2");

		// Static nested template with no member-contained fields from group a.
		auto mem5 = EntryReference(defn->getForeignGroupOfId("b"), "", invalidSize);
		auto prop5 = TemplateTree::parseProperty("fm3_c[0].nm3_nc[0].fm3_a[0].snt_1");

		WHEN("Resolving final value")
		{
			THEN("Resolves value and subproperties")
			{
				CHECK(propResolver.resolveFinalValueString(mem1, prop1->tree->getRoot(), 0) == "a1 is here: 220 and 110 and nb1_3");
				CHECK(propResolver.resolveFinalValueString(mem2, prop2->tree->getRoot(), 0) == "My name is a2: b4 and pie and b1.nm1_nb.0!");
				CHECK(propResolver.resolveFinalValueString(mem3, prop3->tree->getRoot(), 0) == "The Big Bad A: a and pasty and nb1_1");
				CHECK(propResolver.resolveFinalValueString(mem4, prop4->tree->getRoot(), 0) == "A little a: a2 and a2 and 230 and 211.2");
				CHECK(propResolver.resolveFinalValueString(mem5, prop5->tree->getRoot(), 0) == "The Big Bad A: a and pasty and nb1_1");
			}
		}
	}

	GIVEN("Valid property ending with member")
	{
		auto mem1 = EntryReference(defn->getMemberOfId("a1"), "", invalidSize);
		auto prop1 = TemplateTree::parseProperty("sfm1_b.nm1_nb.sfm3_c[2]");

		WHEN("Resolving final term")
		{
			THEN("Resolves to correct entry reference")
			{
				CHECK(propResolver.resolveFinalTerm(mem1, prop1->tree->getRoot(), 0).value() ==
					EntryReference(defn->getForeignGroupOfId("b")->getNestedGroupOfId("nb"), "sfm3_c", 2));
			}
		}

		WHEN("Resolving final value")
		{
			THEN("Resolves to correct member id")
			{
				CHECK(propResolver.resolveFinalValueString(mem1, prop1->tree->getRoot(), 0) == "c3");
			}
		}
	}
}

SCENARIO("BasicPropertyResolver - Errors in Invalid Full Property Resolution", "[!mayfail]")
{
	auto defn = ExpansionTestUtils::defnTestDefinition().get(resolverTestDir);
	auto propResolver = PropertyResolver<EntryReference>(BasicTermResolver(defn.get()));

	GIVEN("Invalid property terminating with group reference to value entry")
	{
		auto mem1 = EntryReference(defn->getAnyGroupOfId("a"), "", invalidSize);
		auto prop1 = TemplateTree::parseProperty("fm1_b.fm3_c[1].fm1_a.v2_i[1]");

		THEN("Error resolving property")
		{
			// TODO: Unimplemented. Throws, but does not describe which members were attempting to
			// be accessed by which group. Should this show the property resolution path below?
			//   group a -> group b -> group a -> value (resolution impossible)
			// Fails at EntryReference::getMember()
			REQUIRE_THROWS_WITH(propResolver.resolveFinalValueString(mem1, prop1->tree->getRoot(), 0),
				Contains("group") && Contains("a") && Contains("v2_i") && Contains("1"));
		}
	}

	GIVEN("Invalid property with index for unlisted entry")
	{
		auto mem1 = EntryReference(defn->getMemberOfId("a1"), "", invalidSize);
		auto prop1 = TemplateTree::parseProperty("sfm1_b[0].sfm4_c[1].fm1_a.v2_i[1]");

		auto mem2 = EntryReference(defn->getMemberOfId("a1"), "", invalidSize);
		auto prop2 = TemplateTree::parseProperty("sfm1_b.sfm4_c[1].func[1]");

		auto mem3 = EntryReference(defn->getMemberOfId("a1"), "", invalidSize);
		auto prop3 = TemplateTree::parseProperty("sfm1_b.sfm4_c[1].nm3_nc[2].alias[0].v4_f[2]");

		THEN("Error resolving property")
		{
			// TODO: Pass these tests too. An informative error message needs to be provided,
			//   and some tests don't actually fail. Different problems are causing this.
			//   The benefits of thorough testing!
			CHECK_THROWS_WITH(propResolver.resolveFinalValueString(mem1, prop1->tree->getRoot(), 0),
				Contains("member") && Contains("a1") && Contains("sfm1_b") && Contains("0") && Contains("unlisted"));
			CHECK_THROWS_WITH(propResolver.resolveFinalValueString(mem2, prop2->tree->getRoot(), 0),
				Contains("member") && Contains("c2") && Contains("func") && Contains("1") && Contains("unlisted"));
			CHECK_THROWS_WITH(propResolver.resolveFinalValueString(mem3, prop3->tree->getRoot(), 0),
				Contains("member") && Contains("nc2_3") && Contains("alias") && Contains("0") && Contains("unlisted"));
		}
	}

	GIVEN("Invalid property with no index for listed entry")
	{
		auto mem1 = EntryReference(defn->getMemberOfId("a1"), "", invalidSize);
		auto prop1 = TemplateTree::parseProperty("sfm1_b.sfm4_c[1].fm1_a.v2_i");

		auto mem2 = EntryReference(defn->getMemberOfId("a1"), "", invalidSize);
		auto prop2 = TemplateTree::parseProperty("sfm1_b.sfm4_c[1].nm3_nc.v4_f[1]");

		auto mem3 = EntryReference(defn->getMemberOfId("a1"), "", invalidSize);
		auto prop3 = TemplateTree::parseProperty("sfm1_b.sfm4_c.nm3_nc[2].v4_f[1]");

		auto mem4 = EntryReference(defn->getMemberOfId("a1"), "", invalidSize);
		auto prop4 = TemplateTree::parseProperty("sfm1_b.sfm4_c[1].nm3_nc[2].v4_f");

		THEN("Error resolving property")
		{
			// TODO: Fail earlier if a listed field is referenced without an index,
			//   and with an informative error message.
			// Currently fails at ExpansionMemberFieldMap::checkIndexWithinList() after
			// attempting to retrieve the value when converting to a value string.
			// Could perform validation prior to attempting to resolve the actual value.
			CHECK_THROWS_WITH(propResolver.resolveFinalValueString(mem1, prop1->tree->getRoot(), 0),
				Contains("member") && Contains("a2") && Contains("v2_i") && Contains(" listed"));
			CHECK_THROWS_WITH(propResolver.resolveFinalValueString(mem2, prop2->tree->getRoot(), 0),
				Contains("member") && Contains("c2") && Contains("nm3_nc") && Contains(" listed"));
			CHECK_THROWS_WITH(propResolver.resolveFinalValueString(mem3, prop3->tree->getRoot(), 0),
				Contains("member") && Contains("b1") && Contains("sfm4_c") && Contains(" listed"));
			CHECK_THROWS_WITH(propResolver.resolveFinalValueString(mem4, prop4->tree->getRoot(), 0),
				Contains("member") && Contains("nc2_3") && Contains("v4_f") && Contains(" listed"));
		}
	}

	GIVEN("Invalid property with term following a value reference")
	{
		auto mem1 = EntryReference(defn->getMemberOfId("a1"), "", invalidSize);
		auto prop1 = TemplateTree::parseProperty("sfm1_b.sv3_s[2].fm1_a.v2_i[1]");

		THEN("Error resolving property")
		{
			REQUIRE_THROWS_WITH(propResolver.resolveFinalValueString(mem1, prop1->tree->getRoot(), 0),
				Contains("member") && Contains("b1") && Contains("sfm3_b") && Contains("2") && Contains("fm1_a"));
		}
	}

	GIVEN("Invalid property with oversized index for listed entry")
	{
		auto mem1 = EntryReference(defn->getMemberOfId("a1"), "", invalidSize);
		auto prop1 = TemplateTree::parseProperty("sfm1_b.sfm4_c[4].fm1_a.v2_i[1]");

		auto mem2 = EntryReference(defn->getMemberOfId("a1"), "", invalidSize);
		auto prop2 = TemplateTree::parseProperty("sfm1_b.sfm4_c[1].fm1_a.v2_i[3]");

		THEN("Error resolving property")
		{
			REQUIRE_THROWS_WITH(propResolver.resolveFinalValueString(mem1, prop1->tree->getRoot(), 0),
				Contains("member") && Contains("b1") && Contains("sfm3_b") && Contains("3") && Contains("large"));
			REQUIRE_THROWS_WITH(propResolver.resolveFinalValueString(mem2, prop2->tree->getRoot(), 0),
				Contains("member") && Contains("a2") && Contains("v2_i") && Contains("3") && Contains("large"));
		}
	}

	GIVEN("Invalid property with group reference to static nested template referencing instance entries")
	{
		// Static nested template with member-contained fields from group a.
		auto mem1 = EntryReference(defn->getForeignGroupOfId("b"), "", invalidSize);
		auto prop1 = TemplateTree::parseProperty("fm3_c[1].nm3_nc[0].fm3_a[2].snt_2");

		THEN("Error resolving property")
		{
			REQUIRE_THROWS_WITH(propResolver.resolveFinalValueString(mem1, prop1->tree->getRoot(), 0),
				Contains("group") && Contains("a") && Contains("static nested template") && Contains("snt_2"));
		}
	}
}
