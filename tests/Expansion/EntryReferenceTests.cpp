#include <catch2/catch.hpp>

#include "EntryReference.hpp"
#include "ExpansionDefinition.hpp"
#include "ExpansionGroup.tpp"
#include "ExpansionMember.hpp"
#include "ExpansionMemberFieldMap.tpp"
#include "ExpansionTestUtils.hpp"
#include "NestedExpansionMember.hpp"
#include "NestedExpansionGroup.hpp"

using namespace std;

using ReferenceCategory = EntryReference::ReferenceCategory;

const size_t invalidSize = numeric_limits<size_t>::max();

SCENARIO("EntryReference")
{
	auto defn = ExpansionTestUtils::getDefinition();
	auto foreignCar = defn->getForeignGroupOfId("car");
	auto nestedWheels = defn->getNestedGroupOfId("car_wheels");
	auto foreignBuggy = defn->getMemberOfId("car_buggy");
	auto nestedSpokedWheels = defn->getMemberOfId("car_buggy")->getFieldMap()->getNestedMember("wheels", 0);

	GIVEN("(A, -, -)")
	{
		auto foreignRef = EntryReference(foreignCar, "", invalidSize);
		auto nestedRef = EntryReference(nestedWheels, "", invalidSize);

		THEN("Does reference group")
		{
			REQUIRE(foreignRef.getReferenceCategory() == ReferenceCategory::Group);
			REQUIRE(nestedRef.getReferenceCategory() == ReferenceCategory::Group);
		}
	}
	
	GIVEN("(A, -, n) for foreign group")
	{
		auto foreignRef = EntryReference(foreignCar, "", 1);

		THEN("Does reference member")
		{
			REQUIRE(foreignRef.getReferenceCategory() == ReferenceCategory::Member);
		}
	}
	
	GIVEN("(A, -, n) for nested group")
	{
		auto nestedRef = EntryReference(nestedWheels, "", 0);

		THEN("Is invalid reference")
		{
			REQUIRE(nestedRef.getReferenceCategory() == ReferenceCategory::Invalid);
		}
	}

	GIVEN("(A, gf, -/n) for member format")
	{
		auto ref1 = EntryReference(foreignCar, "spare_wheel", invalidSize);
		auto ref2 = EntryReference(nestedWheels, "trim", 0);

		THEN("Does reference member")
		{
			REQUIRE(ref1.getReferenceCategory() == ReferenceCategory::Member);
			REQUIRE(ref2.getReferenceCategory() == ReferenceCategory::Member);
		}
	}

	GIVEN("(A, gf, -/n) for non-member format")
	{
		auto ref1 = EntryReference(foreignCar, "application", 4);
		auto ref2 = EntryReference(foreignCar, "engine_power", invalidSize);
		auto ref3 = EntryReference(nestedWheels, "revolutions", invalidSize);

		THEN("Does reference non-member entry")
		{
			REQUIRE(ref1.getReferenceCategory() == ReferenceCategory::Value);
			REQUIRE(ref2.getReferenceCategory() == ReferenceCategory::Value);
			REQUIRE(ref3.getReferenceCategory() == ReferenceCategory::Value);
		}
	}

	GIVEN("(A, mf, -/n) for member format")
	{
		auto ref1 = EntryReference(foreignCar, "engine", 15);

		THEN("Does reference group")
		{
			REQUIRE(ref1.getReferenceCategory() == ReferenceCategory::Group);
		}
	}

	GIVEN("(A, mf, -/n) for non-member format")
	{
		auto ref1 = EntryReference(foreignCar, "colour", 2);
		auto ref2 = EntryReference(nestedWheels, "friction", invalidSize);

		THEN("Is invalid reference")
		{
			REQUIRE(ref1.getReferenceCategory() == ReferenceCategory::Invalid);
			REQUIRE(ref2.getReferenceCategory() == ReferenceCategory::Invalid);
		}
	}

	GIVEN("(a, -, -)")
	{
		auto ref1 = EntryReference(foreignBuggy, "", invalidSize);
		auto ref2 = EntryReference(nestedSpokedWheels, "", invalidSize);

		THEN("Does reference member")
		{
			REQUIRE(ref1.getReferenceCategory() == ReferenceCategory::Member);
			REQUIRE(ref2.getReferenceCategory() == ReferenceCategory::Member);
		}
	}

	GIVEN("(a, -, n)")
	{
		auto ref1 = EntryReference(foreignBuggy, "", 0);
		auto ref2 = EntryReference(nestedSpokedWheels, "", 2);

		THEN("Is invalid reference")
		{
			REQUIRE(ref1.getReferenceCategory() == ReferenceCategory::Invalid);
			REQUIRE(ref2.getReferenceCategory() == ReferenceCategory::Invalid);
		}
	}

	GIVEN("(a, gf, -/n) for member format")
	{
		auto ref1 = EntryReference(foreignBuggy, "spare_wheel", invalidSize);
		auto ref2 = EntryReference(nestedSpokedWheels, "trim", 2);

		THEN("Does reference member")
		{
			REQUIRE(ref1.getReferenceCategory() == ReferenceCategory::Member);
			REQUIRE(ref2.getReferenceCategory() == ReferenceCategory::Member);
		}
	}

	GIVEN("(a, gf, -/n) for non-member format")
	{
		auto ref1 = EntryReference(foreignBuggy, "engine_power", invalidSize);
		auto ref2 = EntryReference(foreignBuggy, "speed", invalidSize);
		auto ref3 = EntryReference(nestedSpokedWheels, "sound", 1);

		THEN("Does reference non-member entry")
		{
			REQUIRE(ref1.getReferenceCategory() == ReferenceCategory::Value);
			REQUIRE(ref2.getReferenceCategory() == ReferenceCategory::Value);
			REQUIRE(ref3.getReferenceCategory() == ReferenceCategory::Value);
		}
	}

	GIVEN("(a, mf, -/n) for member format")
	{
		auto ref1 = EntryReference(foreignBuggy, "wheels", 0);

		THEN("Does reference member")
		{
			REQUIRE(ref1.getReferenceCategory() == ReferenceCategory::Member);
		}
	}

	GIVEN("(a, mf, -/n) for non-member format")
	{
		auto ref1 = EntryReference(foreignBuggy, "name", invalidSize);
		auto ref2 = EntryReference(nestedSpokedWheels, "weight", invalidSize);

		THEN("Does reference non-member entry")
		{
			REQUIRE(ref1.getReferenceCategory() == ReferenceCategory::Value);
			REQUIRE(ref2.getReferenceCategory() == ReferenceCategory::Value);
		}
	}

	GIVEN("An index for an unlisted entry")
	{
		auto ref1 = EntryReference(foreignCar, "name", 1);
		auto ref2 = EntryReference(foreignBuggy, "engine_power", 5);
		auto ref3 = EntryReference(nestedWheels, "revolutions", 0);
		auto ref4 = EntryReference(foreignBuggy, "body_weight", 0);

		THEN("Is invalid reference")
		{
			REQUIRE(ref1.getReferenceCategory() == ReferenceCategory::Invalid);
			REQUIRE(ref2.getReferenceCategory() == ReferenceCategory::Invalid);
			REQUIRE(ref3.getReferenceCategory() == ReferenceCategory::Invalid);
			REQUIRE(ref4.getReferenceCategory() == ReferenceCategory::Invalid);
		}
	}

	GIVEN("No index for a list entry")
	{
		auto ref1 = EntryReference(foreignCar, "colour", invalidSize);
		auto ref2 = EntryReference(nestedWheels, "weight", invalidSize);
		auto ref3 = EntryReference(nestedSpokedWheels, "trim", invalidSize);

		THEN("Is invalid reference")
		{
			REQUIRE(ref1.getReferenceCategory() == ReferenceCategory::Invalid);
			REQUIRE(ref2.getReferenceCategory() == ReferenceCategory::Invalid);
			REQUIRE(ref3.getReferenceCategory() == ReferenceCategory::Invalid);
		}
	}
}
