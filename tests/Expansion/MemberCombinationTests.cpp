#include <catch2/catch.hpp>

#include "BasicTermResolver.hpp"
#include "CacheMap.tpp"
#include "ContextReference.hpp"
#include "ContextualTermResolver.hpp"
#include "ExpanderOptions.hpp"
#include "ExpansionContext.hpp"
#include "ExpansionContextItem.hpp"
#include "ExpansionDefinition.hpp"
#include "ExpansionGroup.tpp"
#include "ExpansionMember.hpp"
#include "ExpansionTestUtils.hpp"
#include "MemberCombination.hpp"
#include "PropertyResolver.tpp"
#include "TreeNode.tpp"
#include "ValueEntry.tpp"

#include <filesystem>
#include <memory>
#include <tuple>

using namespace std;
namespace fs = filesystem;

const fs::path memberCombosTestDir = "member_combos";

const unsigned aCycles = 4;
const unsigned cCycles = 3;
const unsigned dCycles = 3;
const unsigned eCycles = 3;
const unsigned fCycles = 3;

SCENARIO("MemberCombination - Single group")
{
	GIVEN("A single group")
	{
		string templateName = "a@<x>.template";
		auto context = make_shared<ExpansionContext>(*ExpansionTestUtils::defnTestContext().get(make_pair(memberCombosTestDir, templateName)));
		shared_ptr<MemberCombination> memberCombo;
		REQUIRE_NOTHROW(memberCombo = shared_ptr<MemberCombination>(MemberCombination::initFromContext(context.get())));

		auto aNode = memberCombo->context->getRoot().getContextNode()->getChild(0);

		WHEN("Initialised")
		{
			auto const& contextItem = aNode->getItem();

			THEN("Group node has index 0")
			{
				REQUIRE(contextItem.getEntryRef().getIndex() == 0);
			}

			THEN("Group node has group name and no field")
			{
				REQUIRE(contextItem.getEntryRef().getGroup()->getId() == "a");
				REQUIRE(!contextItem.getEntryRef().hasField());
			}

			THEN("Group is expanded")
			{
				REQUIRE(contextItem.doExpand());
			}
		}

		WHEN("Cycling 1 time")
		{
			memberCombo->getNext();

			auto const& contextItem = aNode->getItem();

			THEN("Group node has index 1")
			{
				REQUIRE(contextItem.getEntryRef().getIndex() == 1);
			}
		}

		WHEN("Cycling 1 < x < n times")
		{
			memberCombo->getNext(2);

			auto const& contextItem = aNode->getItem();

			THEN("Group node has index x")
			{
				REQUIRE(contextItem.getEntryRef().getIndex() == 2);
			}

			THEN("Group node has group name and no field")
			{
				REQUIRE(contextItem.getEntryRef().getGroup()->getId() == "a");
				REQUIRE(!contextItem.getEntryRef().hasField());
			}
		}

		WHEN("Cycling n - 1 times")
		{
			bool hasNext = memberCombo->getNext(aCycles - 1);

			auto const& contextItem = aNode->getItem();

			THEN("Group node has index n - 1")
			{
				REQUIRE(hasNext);
				REQUIRE(contextItem.getEntryRef().getIndex() == aCycles - 1);
			}
		}

		WHEN("Cycling n times")
		{
			bool hasNext = memberCombo->getNext(aCycles);

			auto const& contextItem = aNode->getItem();

			THEN("Cycle is done and indices are 0")
			{
				REQUIRE(!hasNext);
				REQUIRE(contextItem.getEntryRef().getIndex() == 0);
			}
		}
	}
}

SCENARIO("MemberCombination - Two groups")
{
	GIVEN("Two groups")
	{
		string templateName = "a,e@<x>.template";
		auto context = make_shared<ExpansionContext>(*ExpansionTestUtils::defnTestContext().get(make_pair(memberCombosTestDir, templateName)));
		shared_ptr<MemberCombination> memberCombo;
		REQUIRE_NOTHROW(memberCombo = shared_ptr<MemberCombination>(MemberCombination::initFromContext(context.get())));

		auto aNode = memberCombo->context->getRoot().getContextNode()->getChild(0);
		auto eNode = memberCombo->context->getRoot().getContextNode()->getChild(1);

		WHEN("Initialised")
		{
			THEN("All nodes have index 0")
			{
				REQUIRE(aNode->getItem().getEntryRef().getIndex() == 0);
				REQUIRE(eNode->getItem().getEntryRef().getIndex() == 0);
			}
		}

		WHEN("Cycling x < cycles(a) times")
		{
			memberCombo->getNext(2);

			THEN("Node a has index x, node e has index 0")
			{
				REQUIRE(aNode->getItem().getEntryRef().getIndex() == 2);
				REQUIRE(eNode->getItem().getEntryRef().getIndex() == 0);
			}
		}

		WHEN("Cycling cycles(a) times")
		{
			memberCombo->getNext(4);

			THEN("Node a has index 0, node e has index 1")
			{
				REQUIRE(aNode->getItem().getEntryRef().getIndex() == 0);
				REQUIRE(eNode->getItem().getEntryRef().getIndex() == 1);
			}
		}

		WHEN("Cycling x * cycles(a) + y times")
		{
			unsigned cycles = 2 * aCycles + 1;
			bool hasNext = memberCombo->getNext(cycles);

			THEN("Node a has index y, node e has index x")
			{
				REQUIRE(hasNext);
				REQUIRE(aNode->getItem().getEntryRef().getIndex() == 1);
				REQUIRE(eNode->getItem().getEntryRef().getIndex() == 2);
			}
		}

		WHEN("Cycling n times")
		{
			bool hasNext = memberCombo->getNext(aCycles * eCycles);

			THEN("Cycle is done, all indices are 0")
			{
				REQUIRE(!hasNext);
				REQUIRE(aNode->getItem().getEntryRef().getIndex() == 0);
				REQUIRE(eNode->getItem().getEntryRef().getIndex() == 0);
			}
		}
	}
}

SCENARIO("MemberCombination - Subproperties")
{
	GIVEN("Subproperties")
	{
		string templateName = "a(b[1].c,d)@<x>.template";
		auto context = make_shared<ExpansionContext>(*ExpansionTestUtils::defnTestContext().get(make_pair(memberCombosTestDir, templateName)));
		shared_ptr<MemberCombination> memberCombo;
		REQUIRE_NOTHROW(memberCombo = shared_ptr<MemberCombination>(MemberCombination::initFromContext(context.get())));

		auto aNode = memberCombo->context->getRoot().getContextNode()->getChild(0);
		auto bNode = aNode->getChild(0);
		auto cNode = bNode->getChild(0);
		auto dNode = aNode->getChild(1);

		WHEN("Intialised")
		{
			THEN("Expandable nodes have index 0")
			{
				REQUIRE(aNode->getItem().getEntryRef().getIndex() == 0);
				REQUIRE(cNode->getItem().getEntryRef().getIndex() == 0);
				REQUIRE(dNode->getItem().getEntryRef().getIndex() == 0);
			}

			THEN("Group and field names are correct, b has index 1")
			{
				REQUIRE(aNode->getItem().getEntryRef().getGroup()->getId() == "a");
				REQUIRE(!aNode->getItem().getEntryRef().hasField());

				REQUIRE(bNode->getItem().getEntryRef().getGroup()->getId() == "a");
				REQUIRE(bNode->getItem().getEntryRef().getField() == "b");
				REQUIRE(bNode->getItem().getEntryRef().getIndex() == 1);

				REQUIRE(cNode->getItem().getEntryRef().getGroup()->getId() == "b");
				REQUIRE(cNode->getItem().getEntryRef().getField() == "c");

				REQUIRE(dNode->getItem().getEntryRef().getGroup()->getId() == "a");
				REQUIRE(dNode->getItem().getEntryRef().getField() == "d");
			}

			THEN("Nodes a, c and d are expanded, node b is not")
			{
				REQUIRE(aNode->getItem().doExpand());
				REQUIRE(!bNode->getItem().doExpand());
				REQUIRE(cNode->getItem().doExpand());
				REQUIRE(dNode->getItem().doExpand());
			}
		}

		WHEN("Cycling x < cycles(c)")
		{
			memberCombo->getNext(2);

			THEN("c has index x, others have index 0")
			{
				REQUIRE(aNode->getItem().getEntryRef().getIndex() == 0);
				REQUIRE(cNode->getItem().getEntryRef().getIndex() == 2);
				REQUIRE(dNode->getItem().getEntryRef().getIndex() == 0);
			}
		}

		WHEN("Cycling cycles(c)")
		{
			memberCombo->getNext(cCycles);

			THEN("Node d has index 1, others have index 0")
			{
				REQUIRE(aNode->getItem().getEntryRef().getIndex() == 0);
				REQUIRE(cNode->getItem().getEntryRef().getIndex() == 0);
				REQUIRE(dNode->getItem().getEntryRef().getIndex() == 1);
			}
		}

		WHEN("Cycling x * cycles(c) + y")
		{
			unsigned cycles = 1 * cCycles + 2;
			memberCombo->getNext(cycles);

			THEN("Node c has index y, node d has index x, node a has index 0")
			{
				REQUIRE(aNode->getItem().getEntryRef().getIndex() == 0);
				REQUIRE(cNode->getItem().getEntryRef().getIndex() == 2);
				REQUIRE(dNode->getItem().getEntryRef().getIndex() == 1);
			}
		}

		WHEN("Cycling (x * cycles(c) * cycles(d)) + (y * cycles(c)) + z")
		{
			unsigned cycles = 3 * cCycles * dCycles + 2 * cCycles + 1;
			bool hasNext = memberCombo->getNext(cycles);

			THEN("Node c has index z, node d has index y, node a has index x")
			{
				REQUIRE(hasNext);
				REQUIRE(aNode->getItem().getEntryRef().getIndex() == 3);
				REQUIRE(cNode->getItem().getEntryRef().getIndex() == 1);
				REQUIRE(dNode->getItem().getEntryRef().getIndex() == 2);
			}

			THEN("Group and field names are unchanged, b has index 1")
			{
				REQUIRE(aNode->getItem().getEntryRef().getGroup()->getId() == "a");
				REQUIRE(!aNode->getItem().getEntryRef().hasField());

				REQUIRE(bNode->getItem().getEntryRef().getGroup()->getId() == "a");
				REQUIRE(bNode->getItem().getEntryRef().getField() == "b");
				REQUIRE(bNode->getItem().getEntryRef().getIndex() == 1);

				REQUIRE(cNode->getItem().getEntryRef().getGroup()->getId() == "b");
				REQUIRE(cNode->getItem().getEntryRef().getField() == "c");

				REQUIRE(dNode->getItem().getEntryRef().getGroup()->getId() == "a");
				REQUIRE(dNode->getItem().getEntryRef().getField() == "d");
			}
		}

		WHEN("Cycling n times")
		{
			unsigned cycles = cCycles * dCycles * aCycles;
			bool hasNext = memberCombo->getNext(cycles);

			THEN("Cycle is done, all expandable indices are 0")
			{
				REQUIRE(!hasNext);
				REQUIRE(aNode->getItem().getEntryRef().getIndex() == 0);
				REQUIRE(cNode->getItem().getEntryRef().getIndex() == 0);
				REQUIRE(dNode->getItem().getEntryRef().getIndex() == 0);
			}
		}
	}
}

SCENARIO("MemberCombination - Multiple groups and properties")
{
	GIVEN("Multiple groups and properties")
	{
		string templateName = "a(b[1].c,d),e,f@<x>.template";
		auto context = make_shared<ExpansionContext>(*ExpansionTestUtils::defnTestContext().get(make_pair(memberCombosTestDir, templateName)));
		shared_ptr<MemberCombination> memberCombo;
		REQUIRE_NOTHROW(memberCombo = shared_ptr<MemberCombination>(MemberCombination::initFromContext(context.get())));

		auto rootNode = memberCombo->context->getRoot().getContextNode();
		auto aNode = rootNode->getChild(0);
		auto bNode = aNode->getChild(0);
		auto cNode = bNode->getChild(0);
		auto dNode = aNode->getChild(1);
		auto eNode = rootNode->getChild(1);
		auto fNode = rootNode->getChild(2);

		WHEN("Cycling such that each element is cycled at least once")
		{
			unsigned cycles = 0;
			cycles = cycles * fCycles + 1;
			cycles = cycles * eCycles + 0;
			cycles = cycles * aCycles + 3;
			cycles = cycles * dCycles + 1;
			cycles = cycles * cCycles + 2;

			bool hasNext = memberCombo->getNext(cycles);

			THEN("All nodes have correct indices")
			{
				REQUIRE(hasNext);

				REQUIRE(fNode->getItem().getEntryRef().getIndex() == 1);
				REQUIRE(eNode->getItem().getEntryRef().getIndex() == 0);
				REQUIRE(aNode->getItem().getEntryRef().getIndex() == 3);
				REQUIRE(dNode->getItem().getEntryRef().getIndex() == 1);
				REQUIRE(cNode->getItem().getEntryRef().getIndex() == 2);

				REQUIRE(bNode->getItem().getEntryRef().getIndex() == 1);
			}
		}

		WHEN("Cycling such that each element is cycled at least once")
		{
			unsigned cycles = aCycles * cCycles * dCycles * eCycles * fCycles;

			bool hasNext = memberCombo->getNext(cycles);
			
			THEN("Cycle is done, all expandable nodes have index 0")
			{
				REQUIRE(!hasNext);
				
				REQUIRE(fNode->getItem().getEntryRef().getIndex() == 0);
				REQUIRE(eNode->getItem().getEntryRef().getIndex() == 0);
				REQUIRE(aNode->getItem().getEntryRef().getIndex() == 0);
				REQUIRE(cNode->getItem().getEntryRef().getIndex() == 0);
				REQUIRE(dNode->getItem().getEntryRef().getIndex() == 0);

				REQUIRE(bNode->getItem().getEntryRef().getIndex() == 1);
			}
		}
	}
}
