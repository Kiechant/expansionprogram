//
//  FileUtils.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 8/5/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef FILE_UTILS_HPP
#define FILE_UTILS_HPP

#include "FileDifferenceDecl.hpp"

#include <filesystem>
#include <functional>
#include <optional>
#include <vector>
#include <utility>

#include <nlohmann/json.hpp>

class ValueEntry;

namespace FileUtils
{
/** Group Definition Readers **/

// TODO: Standardise so checks are performed appropriately at each step,
// instead of the muddle of checking before and after access.
// e.g. do I check something is an object before get(), or after as checkIfObject().
// TODO: standardise errors (not using asserts!).
// TODO: Standardise multi-dimensional array access.
nlohmann::json readJsonFromFile(std::filesystem::path const& path);
std::string getIfJsonString(nlohmann::json const& entry);
nlohmann::json const& getIfJsonObjectHasObject(nlohmann::json const& entry, std::string const& field);
std::string getIfJsonObjectHasString(nlohmann::json const& entry, std::string const& field);
nlohmann::json const& getIfJsonObjectHasArray(nlohmann::json const& entry, std::string const& field);
std::string getIfJsonObjectHasStringOrDefault(nlohmann::json const& entry, std::string const& field, std::string const& alternative);
size_t sizeOfJsonArray(nlohmann::json const& jsonArray);
std::pair<nlohmann::json::const_iterator, nlohmann::json::const_iterator> rangeFromJsonArray(nlohmann::json const& jsonArray);
std::vector<std::string> stringVectorFromJsonRange(nlohmann::json::const_iterator const& begin, nlohmann::json::const_iterator const& end);
std::vector<std::vector<std::vector<ValueEntry>>> argumentVectorFromJsonRange(nlohmann::json::const_iterator const& begin, nlohmann::json::const_iterator const& end);
std::vector<std::vector<ValueEntry>> valueVector2dFromJsonRange(nlohmann::json::const_iterator const& begin, nlohmann::json::const_iterator const& end);
std::vector<ValueEntry> valueVectorFromJsonRange(nlohmann::json::const_iterator const& begin, nlohmann::json::const_iterator const& end);
ValueEntry valueFromJsonObject(nlohmann::json const& jsonObj);
bool isJsonExtension(std::string const& extension);


/** Directory Navigation **/

void forEachFileInDirectoryList(
	std::vector<std::filesystem::path> const& dirList,
	std::filesystem::path const& baseDir,
	std::function<void(std::filesystem::path const&)> onFound,
	std::optional<std::function<void(std::filesystem::path const&)>> onDirNotFound
);


/** File Comparison **/

FileDifference compareFiles(std::filesystem::path const& first, std::filesystem::path const& second);
FileDifference compareActualAndExpectedFile(std::filesystem::path const& actual, std::filesystem::path const& expected);
FileDifference compareActualAndExpectedDirectory(std::filesystem::path const& actual, std::filesystem::path const& expected);
std::vector<FileDifference> compareDirectories(std::filesystem::path const& first, std::filesystem::path const& second);
FileDifference compareAnyFiles(std::filesystem::path const& first, std::filesystem::path const& second);
std::string readFile(std::filesystem::path const& file);
std::optional<std::string> readLine(std::istream* input, size_t lineOffset = 0);
std::vector<std::string> readLines(std::istream* input, size_t lineOffset = 0, std::optional<size_t> lineCount = std::nullopt);
int getSizeOfFile(std::filesystem::path const& file);
int getSizeOfFile(std::ifstream* input);
std::string getFileLocator(std::filesystem::path const& file, size_t lineOffset, std::optional<size_t> charOffset);
std::string getNameOfFileType(std::filesystem::file_type type);
std::vector<std::filesystem::path> getPathsInDirectory(std::filesystem::path const& dir);
std::vector<std::filesystem::path> getSubpathsInDirectory(std::filesystem::path const& dir);
bool doIgnorePathName(std::string const& path);
bool isDifference(std::vector<FileDifference> const& diffs);
std::string getDirectoryDifferenceMessage(std::vector<FileDifference> const&);
std::string getDirectoryComparisonMessage(std::vector<FileDifference> const&);
void checkIsRegularFile(std::filesystem::path const& file);

}

#endif /* FILE_UTILS_HPP */
