//
//  TupleUtils.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 30/5/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef TUPLE_UTILS_HPP
#define TUPLE_UTILS_HPP

#include <tuple>

namespace TupleUtils
{

template<typename FuncT, typename ValueT>
using MappedValueT = decltype(std::declval<FuncT>()(std::declval<ValueT>()));

template<size_t I, typename FuncT, typename... ItemT>
using MappedTupleElementT = MappedValueT<FuncT, std::tuple_element_t<I, std::tuple<ItemT...>>>;

template<typename FuncT, typename... ItemT>
using OutTupleT = std::tuple<MappedValueT<FuncT, ItemT>...>;

template<typename FuncT, typename... ItemT>
using OutVectorT = std::vector<MappedTupleElementT<0, FuncT, ItemT...>>;

template<size_t I = 0, typename FuncT, typename... ItemT>
inline typename std::enable_if_t<I == sizeof...(ItemT), void>
forEach(std::tuple<ItemT...> const&, FuncT const&)
{}

template<size_t I = 0, typename FuncT, typename... ItemT>
inline typename std::enable_if_t<I < sizeof...(ItemT), void>
forEach(std::tuple<ItemT...> const& tuple, FuncT const& func)
{
	func(std::get<I>(tuple));
	forEach<I + 1>(tuple, func);
}

template<size_t I = 0, typename FuncT, typename... ItemT>
inline typename std::enable_if_t<I == sizeof...(ItemT), void>
forEachMutable(std::tuple<ItemT...>&, FuncT const&)
{}

template<size_t I = 0, typename FuncT, typename... ItemT>
inline typename std::enable_if_t<I < sizeof...(ItemT), void>
forEachMutable(std::tuple<ItemT...>& tuple, FuncT const& func)
{
	func(std::get<I>(tuple));
	forEachMutable<I + 1>(tuple, func);
}

template<size_t I = 0, typename FuncT, typename OutTupleT, typename... ItemT>
inline typename std::enable_if_t<I == sizeof...(ItemT), void>
mapHeterogenousInternal(std::tuple<ItemT...> const&, FuncT const&, OutTupleT*)
{}

template<size_t I = 0, typename FuncT, typename OutTupleT, typename... ItemT>
inline typename std::enable_if_t<I < sizeof...(ItemT), void>
mapHeterogenousInternal(std::tuple<ItemT...> const& tuple, FuncT const& func, OutTupleT* outTuple)
{
	std::get<I>(*outTuple) = func(std::get<I>(tuple));
	mapHeterogenousInternal<I + 1>(tuple, func, outTuple);
}

template<size_t I = 0, typename FuncT, typename... ItemT>
inline typename std::enable_if_t<I < sizeof...(ItemT),
OutTupleT<FuncT, ItemT...>>
mapHeterogenous(std::tuple<ItemT...> const& tuple, FuncT const& func)
{
	OutTupleT<FuncT, ItemT...> outTuple{};
	mapHeterogenousInternal<I>(tuple, func, &outTuple);
	return outTuple;
}

template<size_t I = 0, typename FuncT, typename OutVectorT, typename... ItemT>
inline typename std::enable_if_t<I == sizeof...(ItemT), void>
mapHomogenousInternal(std::tuple<ItemT...> const&, FuncT const&, OutVectorT*)
{}

template<size_t I = 0, typename FuncT, typename OutVectorT, typename... ItemT>
inline typename std::enable_if_t<I < sizeof...(ItemT), void>
mapHomogenousInternal(std::tuple<ItemT...> const& tuple, FuncT const& func, OutVectorT* outVector)
{
	(*outVector)[I] = func(std::get<I>(tuple));
	mapHomogenousInternal<I + 1>(tuple, func, outVector);
}

template<size_t I = 0, typename FuncT, typename... ItemT>
inline typename std::enable_if_t<I < sizeof...(ItemT),
OutVectorT<FuncT, ItemT...>>
mapHomogenous(std::tuple<ItemT...> const& tuple, FuncT const& func)
{
	OutVectorT<FuncT, ItemT...> outVector(sizeof...(ItemT));
	mapHomogenousInternal<I>(tuple, func, &outVector);
	return outVector;
}

template<size_t I = 0, typename FuncT, typename OutValueT, typename... ItemT>
inline typename std::enable_if_t<I == sizeof...(ItemT), void>
reduceInternal(std::tuple<ItemT...> const&, FuncT const&, OutValueT*)
{}

template<size_t I = 0, typename FuncT, typename OutValueT, typename... ItemT>
inline typename std::enable_if_t<I < sizeof...(ItemT), void>
reduceInternal(std::tuple<ItemT...> const& tuple, FuncT const& func, OutValueT* currentValue)
{
	*currentValue = func(std::get<I>(tuple), *currentValue);
	reduceInternal<I + 1>(tuple, func, currentValue);
}

template<typename OutValueT, size_t I = 0, typename FuncT, typename... ItemT>
inline typename std::enable_if_t<I < sizeof...(ItemT),
OutValueT>
reduce(std::tuple<ItemT...> const& tuple, OutValueT initValue, FuncT const& func)
{
	reduceInternal<I>(tuple, func, &initValue);
	return initValue;
}

template<typename OutValueT, size_t I = 0, typename FuncT, typename... ItemT>
inline typename std::enable_if_t<I < sizeof...(ItemT),
OutValueT>
reduce(std::tuple<ItemT...> const& tuple, FuncT const& func)
{
	OutValueT initValue = OutValueT();
	reduceInternal<I>(tuple, func, &initValue);
	return initValue;
}

};

#endif /* TUPLE_UTILS_HPP */
