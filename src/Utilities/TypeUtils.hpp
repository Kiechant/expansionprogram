//
//  TypeUtils.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 8/7/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#ifndef TYPE_UTILS_HPP
#define TYPE_UTILS_HPP

#include <iostream>
#include <functional>

namespace TypeUtils
{
	/** General Type Queries **/

	/* True if the type is actually a type. Allows passing a type to the conditional
	   component of an 'std::enable_if' query.

	   Note, if a type is ill-formed in an SFINAE context, the code will fail to
	   compile. In this situation 'std::void_t' or 'VoidT' must be used instead. */
	template<typename T>
	constexpr bool IsTypeV = std::is_same_v<T, T>;

	/* True if a primitive or object type i.e. not a pointer, reference, union etc. */
	template<typename Concrete>
	constexpr bool IsConcreteV =
		std::is_arithmetic_v<Concrete> ||
		std::is_class_v<Concrete> ||
		std::is_enum_v<Concrete>;

	/* True if a primitive character type. */
	template<typename T>
	constexpr bool IsCharV =
		std::is_same_v<T, char> ||
		std::is_same_v<T, char16_t> ||
		std::is_same_v<T, char32_t> ||
		std::is_same_v<T, wchar_t>;

	/* True if a primitive integer type. Characters and integers are both classified
	   as arithmetic types. This takes the difference of the two categories. */
	template<typename T>
	constexpr bool IsIntegerV =
		std::is_integral_v<T> && !IsCharV<T>;

	/* True if a primitive numeric type which includes integers and floating-point types.
	   Floating point types are not classified as arithmetic types. This takes
	   the union of integer and floating-point types. */
	template<typename T>
	constexpr bool IsNumericV =
		IsIntegerV<T> || std::is_floating_point_v<T>;

	/* True if the type is a pointer to a const object. */
	template<typename Ptr>
	constexpr bool IsPtrToConstV = std::is_pointer_v<Ptr> &&
		std::is_const_v<std::remove_pointer_t<Ptr>>;


	/** General Type Aliases **/

	/* An alternative to 'std::void_t' which supports SFINAE.
	   'std::void_t' is aliased as the same type 'void'.
	   Therefore, attempted template specialisations with 'std::void_t' as a
	   type parameter result in a template redefinition. */
	template<typename... T> struct MakeVoid { using type = void; };
	template<typename... T> using VoidT = typename MakeVoid<T...>::type;

	/* Disables a template specialisation for the provided type 'T'.
	   Uses a pointer to 'T' to generate a different compound type which is
	   not the same as itself.
	   Note: cannot disable the base specialisation. To disable a specialisation
	   for a struct or class, provide no implementation,
	   e.g. template<typename T, typename = void> class SomeClass; */
	template<typename T>
	using DisableT = std::enable_if_t<std::is_same_v<T, T*>>;

	/* Removes const-qualification from the pointed-to object. */
	template<typename Ptr, typename = std::enable_if_t<
		std::is_pointer<Ptr>::value &&
		std::is_const<std::remove_pointer_t<Ptr>>::value>>
	using MutableIndirectOfT = std::remove_const_t<std::remove_pointer_t<Ptr>>;


	/** Iterator Traits Aliases **/

	template<typename Iter>
	using IterValueOfT = typename std::iterator_traits<Iter>::value_type;

	template<typename Iter>
	using IterRefOfT = typename std::iterator_traits<Iter>::reference;

	template<typename Iter>
	using IterCategoryOfT = typename std::iterator_traits<Iter>::iterator_category;


	/** Iterator Queries **/

	template<typename Iter, typename Category>
	constexpr bool HasIterCategoryV =
		std::is_convertible_v<IterCategoryOfT<Iter>, Category>;

	template<typename T, typename Iter>
	constexpr bool IsConstructibleWithIterRefOfV =
		std::is_constructible_v<T, IterRefOfT<Iter>>;

	template<typename T, typename Iter>
	constexpr bool IsConstructibleWithIndirectIterValueOfV =
		std::is_constructible_v<T, std::remove_pointer_t<IterValueOfT<Iter>>>;

	template<typename T, typename Iter>
	constexpr bool IsBaseOfIndirectIterValueOfV =
		std::is_base_of_v<std::remove_pointer_t<IterValueOfT<Iter>>, T>;


	/** Macro Writing Helpers **/

	/* Deduces a type enclosed in brackets.
	   Employed by writing 'typename ArgumentType<void(EnclosedType)>::type',
	   where 'EnclosedType' is a type enclosed in any number of brackets.
	   All brackets are stripped during type deduction. */
	template<typename T>
	struct ArgumentType;
	template<typename T, typename U>
	struct ArgumentType<T(U)> { using type = U; };

	/* Unpacks a macro paramter enclosed in brackets.
	   Employed by writing 'UNPACK_ARGUMENTS EnclosedArgs', where 'EnclosedArgs'
	   is enclosed in brackets i.e. EnclosedArgs = (Args).
	   This is interpreted as 'UNPACK_ARGUMENTS(Args)' and the arguments are separated.
	   Only removes one level of brackets. */
	#define UNPACK_ARGUMENTS(...) __VA_ARGS__

	/* Standardised way of forcing a semi-colon for the user of a macro.
	   Gives a simple error message from the compiler.
	   Preferable to an unterminated statement within the macro that would
	   generate an implementation-specific error message. */
	#define FORCE_SEMICOLON() do {} while (false)


	/** Type-Dependent Struct Writing Helpers */

	/* Variant of 'WRAPPED_TYPE_STRUCT' for a single type-to-type definition. */
	#define WRAPPED_TYPE_STRUCT_DEF(Source, Dest, Definition) \
		template<Source Type> struct Dest \
			{ using type = ArgumentType<void(Definition)>(); }; \
		template<Source Type> using Dest##T = typename Dest<Type>::type;


	/** Function Type and Return-Type Deduction **/

	/* Wraps a function in a lambda expression. Allows executing function
	   pointers with default parameters. */
	#define AS_LAMBDA(Function) [](auto&&... args) -> decltype(auto) { \
	   return Function(std::forward<decltype(args)>(args)...); }

	/* Wraps a templated functor in a lambda expression. */
	#define FUNCTOR_TEMPLATE_AS_LAMBDA(FunctorTemplate) [](auto&&... args) -> decltype(auto) { \
		return FunctorTemplate<std::decay_t<decltype(args)>...>()(std::forward<decltype(args)>(args)...); }


	/** Const Conversion **/

	/* Functions for explicitly converting a const pointer or l-value reference
	   to a non-const pointer or l-value reference. */

	template<typename Ret>
	Ret* asMutable(Ret const* ret)
	{
		return const_cast<Ret*>(ret);
	}

	template<typename Ret>
	Ret& asMutable(Ret const& ret)
	{
		return const_cast<Ret&>(ret);
	}

	template<typename Ret>
	std::vector<Ret*> asMutable(std::vector<Ret const*> const& constList)
	{
		auto mutableList = std::vector<Ret*>(constList.size());
		for (size_t i = 0; i < constList.size(); i++)
			mutableList.at(i) = const_cast<Ret*>(constList.at(i));
		return mutableList;
	}

	template<typename Item>
	std::vector<Item const*> asConst(std::vector<Item*> const& mutableList)
	{
		auto constList = std::vector<Item const*>(mutableList.size());
		for (size_t i = 0; i < mutableList.size(); i++)
			constList.at(i) = static_cast<Item const*>(mutableList.at(i));
		return constList;
	}

	template<typename Function, typename... Args>
	auto fromConstFunc(Function function, Args... args)
	{
		return function(std::as_const(args)...);
	}

	template<typename Function, typename... Args>
	auto mutableFromConstFunc(Function function, Args... args)
	{
		return asMutable(fromConstFunc(function, args...));
	}

	template<typename Class, typename Function, typename... Args>
	auto fromConstMemberFunc(Class* self, Function function, Args... args)
	{
		return std::as_const(*self).*function(args...);
	}

	template<typename Class, typename Function, typename... Args>
	auto mutableFromConstMemberFunc(Class* self, Function function, Args... args)
	{
		return asMutable(fromConstMemberFunc(self, function, args...));
	}

	#define FROM_CONST_MEMBER_FUNC(Member, FunctionName, ...) \
		std::as_const(*Member).FunctionName(__VA_ARGS__)

	#define MUTABLE_FROM_CONST_MEMBER_FUNC(Member, FunctionName, ...) \
		TypeUtils::asMutable(FROM_CONST_MEMBER_FUNC(Member, FunctionName, __VA_ARGS__))


	/** Function Type Deduction **/

	template<typename FuncT>
	struct FunctionTraits;

	template<typename RetT, typename... _ArgT>
	struct FunctionTraits<std::function<RetT(_ArgT...)>>
	{
		static constexpr size_t ArgCount = sizeof...(_ArgT);

		using ResultT = RetT;

		template<size_t I>
		using ArgT = std::tuple_element_t<I, std::tuple<_ArgT...>>;
	};

	template<size_t I, typename FuncT>
	using FunctionArgumentT = typename FunctionTraits<FuncT>::template ArgT<I>;
}

#endif /* TYPE_UTILS_HPP */
