//
//  FileDifference.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 21/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "FileDifference.hpp"

#include "Cache.tpp"
#include "EnumStringMap.tpp"
#include "FileUtils.hpp"

using namespace std;
namespace fs = filesystem;

#include <fstream>
#include <functional>
#include <sstream>

FileDifference::FileDifference(fs::path const& firstFile, fs::path const& secondFile,
	FileDifferenceType differenceType, size_t lineOffset, size_t charOffset, bool isFirstSmaller)
	: firstFile(firstFile),
	  secondFile(secondFile),
	  differenceType(differenceType),
	  lineOffset(lineOffset),
	  charOffset(charOffset),
	  isFirstSmaller(isFirstSmaller),
	  message(Cache<string>(bind(mem_fn(&FileDifference::composeMessage), this))) {}

void swap(FileDifference& first, FileDifference& second) 
{
	using std::swap;
	swap(first.firstFile, second.firstFile);
	swap(first.secondFile, second.secondFile);
	swap(first.differenceType, second.differenceType);
	swap(first.lineOffset, second.lineOffset);
	swap(first.charOffset, second.charOffset);
	swap(first.isFirstSmaller, second.isFirstSmaller); 
	swap<string>(first.message, second.message);
}

FileDifference::FileDifference(FileDifference const& other)
	: firstFile(other.firstFile),
	  secondFile(other.secondFile),
	  differenceType(other.differenceType),
	  lineOffset(other.lineOffset),
	  charOffset(other.charOffset),
	  isFirstSmaller(other.isFirstSmaller),
	  message(Cache<string>(other.message, bind(mem_fn(&FileDifference::composeMessage), this))) {}

FileDifference& FileDifference::operator=(FileDifference other) 
{
	swap(*this, other);
	return *this;
}

bool FileDifference::isDifference() const
{
	return differenceType != FileDifferenceType::NoDifference;
}

FileDifferenceType FileDifference::getDifferenceType() const
{
	return differenceType;
}

size_t FileDifference::getLine() const
{
	return lineOffset;
}

size_t FileDifference::getChar() const
{
	return charOffset;
}

string const& FileDifference::getMessage() const
{
	return message.get();
}

string FileDifference::composeMessage() const
{
	stringstream ss;

	if (isDifference())
	{
		ss << "Difference: " << getDifferenceTypeNameMap().getString(differenceType) << endl;
		if (differenceType == FileDifferenceType::CharMismatch || differenceType == FileDifferenceType::LineLength)
		{
			ss << composeLineDifferenceMessage(0);
			ss << composeLineDifferenceMessage(1);
		}
		else if (differenceType == FileDifferenceType::LineCount)
		{
			ss << composeLineCountDifferenceMessage();
		}
		else if (differenceType == FileDifferenceType::Existence)
		{
			ss << composeExistenceMessage();
		}
		else /* differenceType == FileDifferenceType::FileType */
		{
			ss << composeFileTypeMessage();
		}
	}
	else
	{
		ss << "Files '" << firstFile.string() << "' and '" << secondFile.string() << "' are the same." << endl;
	}

	return ss.str();
}

string FileDifference::composeLineDifferenceMessage(int fileNo) const
{
	fs::path const* file = getFile(fileNo);
	ifstream ifs = ifstream(*file);

	optional<string> line = FileUtils::readLine(&ifs, lineOffset);
	if (!line.has_value())
	{
		throw runtime_error("Line " + to_string(lineOffset) + " does not exist in file '" + file->string() + "'.");
	}

	stringstream ss;
	ss << "  " << composeFileLocator(*file, true) << endl;
	ss << "    " << line.value() << endl;
	ss << composeCharMarker();
	return ss.str();
}

string FileDifference::composeLineCountDifferenceMessage() const
{
	auto [smallerFile, largerFile] = getOrderedFiles();
	ifstream ifs = ifstream(*largerFile);

	// The line in question is the one following the final parsed line of the other file.
	size_t nextLineOffset = lineOffset + 1;

	// Reads a window of lines before the spillover line.
	size_t preWindowSize = min(nextLineOffset, (size_t)1);
	size_t startLineOffset = nextLineOffset - preWindowSize;
	vector<string> lines = FileUtils::readLines(&ifs, startLineOffset, preWindowSize + 1);
	if (lines.size() <= preWindowSize)
	{
		throw runtime_error("Line " + to_string(startLineOffset + lines.size() + 1) + " does not exist in file '" + largerFile->string() + "'.");
	}

	stringstream ss;
	ss << "  " << FileUtils::getFileLocator(*largerFile, lineOffset + 1, nullopt) << endl;
	for (size_t i = 0; i < preWindowSize; i++)
		ss << "    " << lines.at(i) << endl;
	ss << " -> " << lines.at(preWindowSize) << endl;
	return ss.str();
}

string FileDifference::composeExistenceMessage() const
{
	auto [smaller, larger] = getOrderedFiles();
	stringstream ss;
	ss << "  File '" << larger->string() << "' exists." << endl;
	ss << "  File '" << smaller->string() << "' does not exist." << endl;
	return ss.str();
}

string FileDifference::composeFileTypeMessage() const
{
	stringstream ss;
	ss << composeTypeDescriptor(firstFile);
	ss << composeTypeDescriptor(secondFile);
	return ss.str();
}

string FileDifference::composeCharMarker() const
{
	return "   " + string(charOffset, ' ') + "-^-\n";
}

string FileDifference::composeFileLocator(fs::path const& file, bool doIncludeCharOffset) const
{
	auto optCharOffset = doIncludeCharOffset ? make_optional(charOffset) : nullopt;
	return FileUtils::getFileLocator(file, lineOffset, optCharOffset);
}

string FileDifference::composeTypeDescriptor(fs::path const& file) const
{
	return "  File '" + file.string() + "' is of type '" + FileUtils::getNameOfFileType(status(file).type()) + "'.\n";
}

fs::path const* FileDifference::getFile(int fileNo) const
{
	fs::path const* file = nullptr;
	if (fileNo == 0)
	{
		file = &firstFile;
	}
	else if (fileNo == 1)
	{
		file = &secondFile;
	}
	return file;
}

pair<fs::path const*, fs::path const*> FileDifference::getOrderedFiles() const
{
	return isFirstSmaller ?
		make_pair(&firstFile, &secondFile) :
		make_pair(&secondFile, &firstFile);
}

EnumStringMap<FileDifferenceType> FileDifference::getDifferenceTypeNameMap() 
{
	static auto _differenceTypeNameMap = EnumStringMap<FileDifferenceType>(vector<string>{
		"no difference",
		"character mismatch",
		"line length",
		"line count",
		"existence",
		"file type"
	});
	return _differenceTypeNameMap;
}
