//
//  WrappedTypeStruct.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef WRAPPED_TYPE_STRUCT_HPP
#define WRAPPED_TYPE_STRUCT_HPP

/* Automatically declares a type struct named Dest of type Source,
   and aliases the struct's contained type as DestT. */
#define WRAPPED_TYPE_STRUCT(Source, Dest) \
	template<Source Type> struct Dest; \
	template<Source Type> using Dest##T = typename Dest<Type>::type;

#endif /* WRAPPED_TYPE_STRUCT_HPP */
