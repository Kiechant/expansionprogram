//
//  MiscUtils.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 13/7/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#include "MiscUtils.hpp"

#include <iostream>

using namespace std;

void __assertMsg(const char* condStr, bool cond, const char* file, int line,
	const char* msg)
{
	if (!cond)
	{
		cerr << "Assertion failed: " << msg << "\n" <<
			"Expected: " << condStr << "\n" <<
			"Source:   " << file << ", line" << line << endl;
		abort();
	}
}

template<typename Ret, typename ExceptionFunc, typename... Args>
Ret MiscUtils::throwOrReturn(Ret ret, ExceptionFunc exceptionFunc,
	bool doThrowException, Args... args)
{
	static_assert(is_base_of<exception, invoke_result_t<ExceptionFunc, Args...>>::value);
	if (doThrowException)
		throw exceptionFunc(args...);
	return ret;
}

string MiscUtils::subChar(string const& formatter, char ch)
{
	string subbed = "";
	size_t subToken = formatter.find('{');
	bool subTokenExists = subToken != string::npos && formatter[subToken + 1] == '}';
	if (subTokenExists)
	{
		subbed.append(formatter.begin(), formatter.begin() + subToken);
		subbed.push_back(ch);
		subbed.append(formatter.begin() + subToken + 2, formatter.end());
	}
	return subTokenExists ? subbed : formatter;
}

string MiscUtils::toOctal(int val)
{
	stringstream ss;
	ss << oct << val;
	return ss.str();
}

string MiscUtils::charToPrintableString(char ch)
{
	return (isgraph(ch) || isspace(ch)) ? string(1, ch) : "\\" + toOctal(int(ch));
}

string MiscUtils::printableString(string const& str)
{
	string printStr = "";
	printStr.reserve(str.size());
	for (char ch : str)
	{
		string printSubstr = charToPrintableString(ch);
		printStr.insert(printStr.end(), printSubstr.begin(), printSubstr.end());
	}
	return printStr;
}
