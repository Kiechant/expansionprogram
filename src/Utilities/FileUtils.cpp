//
//  FileUtils.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 8/5/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "FileUtils.hpp"

#include "FileDifference.hpp"
#include "MiscUtils.hpp"
#include "ValueEntry.tpp"

#include <fstream>
#include <sstream>

using namespace std;
namespace fs = filesystem;
using json_t = nlohmann::json;

/** Group Definition Readers **/

json_t FileUtils::readJsonFromFile(fs::path const& path)
{
	if (!fs::is_regular_file(path))
		throw invalid_argument("Cannot read JSON at non-existent file: " + string(path));
	
	auto ifs = ifstream(path);
	json_t json;

	try
	{
		json = json_t::parse(ifs, nullptr, true, true);
	}
	catch(const json_t::exception& e)
	{
		throw_with_nested(runtime_error("Error parsing JSON file: " + string(path)));
	}

	ifs.close();
	return json;
}

string FileUtils::getIfJsonString(json_t const& entry)
{
	if (!entry.is_string())
		throw invalid_argument("JSON entry not string");
	return entry.get<string>();
}

json_t const& FileUtils::getIfJsonObjectHasObject(json_t const& entry, string const& field)
{
	if (entry.count(field) == 0)
		throw invalid_argument("Expected field '" + field + "' to exist in object.");
	auto const& obj = entry[field];
	if (!obj.is_object())
		throw invalid_argument("Expected entry for field '" + field + "' to be an object.");
	return obj;
}

string FileUtils::getIfJsonObjectHasString(json_t const& entry, string const& field)
{
	if (entry.count(field) == 0)
		throw invalid_argument("Expected field '" + field + "' to exist in object.");
	auto const& obj = entry[field];
	if (!obj.is_string())
		throw invalid_argument("Expected entry for field '" + field + "' to be a string.");
	return obj.get<string>();
}

json_t const& FileUtils::getIfJsonObjectHasArray(json_t const& entry, string const& field)
{
	if (entry.count(field) == 0)
		throw invalid_argument("Expected field '" + field + "' to exist in object.");
	auto const& obj = entry[field];
	if (!obj.is_array())
		throw invalid_argument("Expected entry for field '" + field + "' to be an array.");
	return obj;
}

string FileUtils::getIfJsonObjectHasStringOrDefault(json_t const& entry, string const& field, string const& alternative)
{
	string res = "";
	if (entry.count(field) == 1)
	{
		res = getIfJsonString(entry[field]);
	}
	else res = alternative;
	return res;
}

size_t FileUtils::sizeOfJsonArray(json_t const& jsonArray)
{
	if (!jsonArray.is_array())
		throw invalid_argument("JSON entry not array");
	return jsonArray.size();
}

pair<json_t::const_iterator, json_t::const_iterator> FileUtils::rangeFromJsonArray(json_t const& jsonArray)
{
	if (!jsonArray.is_array())
		throw invalid_argument("JSON entry not array");
	return make_pair(jsonArray.begin(), jsonArray.end());
}

vector<string> FileUtils::stringVectorFromJsonRange(json_t::const_iterator const& begin, json_t::const_iterator const& end)
{
	auto vec = vector<string>();
	for (auto argIt = begin; argIt != end; argIt++)
	{
		vec.push_back(getIfJsonString(*argIt));
	}
	return vec;
}

vector<vector<vector<ValueEntry>>> FileUtils::argumentVectorFromJsonRange(json_t::const_iterator const& begin, json_t::const_iterator const& end) 
{
	auto vec = vector<vector<vector<ValueEntry>>>();
	for (auto it1 = begin; it1 != end; it1++)
	{
		vec.push_back(vector<vector<ValueEntry>>());
		auto& array1 = vec.back();

		auto [begin2, end2] = rangeFromJsonArray(*it1);
		for (auto it2 = begin2; it2 != end2; it2++)
		{
			if (it2->is_array())
			{
				auto [begin3, end3] = rangeFromJsonArray(*it2);
				array1.push_back(valueVectorFromJsonRange(begin3, end3));
			}
			else
			{
				array1.push_back(vector{valueFromJsonObject(*it2)});
			}
		}
	}
	return vec;
}

bool FileUtils::isJsonExtension(string const& extension)
{
	return extension == ".json" || extension == ".jsonc";
}

vector<vector<ValueEntry>> FileUtils::valueVector2dFromJsonRange(json_t::const_iterator const& begin, json_t::const_iterator const& end)
{
	auto vec = vector<vector<ValueEntry>>();
	for (auto subArrayIt = begin; subArrayIt != end; subArrayIt++)
	{
		auto [subArrayBegin, subArrayEnd] = rangeFromJsonArray(*subArrayIt);
		vec.push_back(valueVectorFromJsonRange(subArrayBegin, subArrayEnd));
	}
	return vec;
}

vector<ValueEntry> FileUtils::valueVectorFromJsonRange(json_t::const_iterator const& begin, json_t::const_iterator const& end)
{
	auto vec = vector<ValueEntry>();
	for (auto argIt = begin; argIt != end; argIt++)
	{
		vec.push_back(valueFromJsonObject(*argIt));
	}
	return vec;
}


ValueEntry FileUtils::valueFromJsonObject(json_t const& jsonObj)
{
	ValueEntry value;
	using Category = ValueCategory;
	if (jsonObj.is_string())
	{
		value.setValueWithCategory<Category::String>(
			jsonObj.get<ValueTypeOfCategoryT<Category::String>>());
	}
	else if (jsonObj.is_boolean())
	{
		value.setValueWithCategory<Category::Bool>(
			jsonObj.get<ValueTypeOfCategoryT<Category::Bool>>());
	}
	else if (jsonObj.is_number_integer())
	{
		value.setValueWithCategory<Category::Int>(
			jsonObj.get<ValueTypeOfCategoryT<Category::Int>>());
	}
	else if (jsonObj.is_number_unsigned())
	{
		value.setValueWithCategory<Category::Unsigned>(
			jsonObj.get<ValueTypeOfCategoryT<Category::Unsigned>>());
	}
	else if (jsonObj.is_number_float())
	{
		value.setValueWithCategory<Category::Float>(
			jsonObj.get<ValueTypeOfCategoryT<Category::Float>>());
	}
	else
	{
		throw invalid_argument("Unrecognised argument in json.");
	}
	return value;
}


/** Directory Navigation **/

void FileUtils::forEachFileInDirectoryList(
	vector<fs::path> const& dirList,
	std::filesystem::path const& baseDir,
	function<void(fs::path const&)> onFileFound,
	optional<function<void(fs::path const&)>> onDirNotFound)
{
	for (fs::path const& dir : dirList)
	{
		fs::path fullDir = baseDir / dir;
		if (fs::is_directory(fullDir))
		{
			for (fs::path const& file : fs::directory_iterator(fullDir))
			{
				onFileFound(fs::relative(file, baseDir));
			}
		}
		else /* Listed a directory that does not exist. */
		{
			if (onDirNotFound.has_value())
				onDirNotFound.value()(dir);
			else
				throw runtime_error("Directory '" + dir.string() + "' not found in '" + baseDir.string() + "'.");
		}
	}
}


/** File Comparison **/

FileDifference FileUtils::compareFiles(fs::path const& first, fs::path const& second) 
{
	checkIsRegularFile(first);
	checkIsRegularFile(second);

	ifstream firstIfs = ifstream(first);
	ifstream secondIfs = ifstream(second);
	size_t lineOffset = 0;
	
	while (true)
	{
		string firstLine;
		getline(firstIfs, firstLine);
		string secondLine;
		getline(secondIfs, secondLine);

		// Checks whether an error occurred reading the files.
		// Ignores if EOF, since extracting the last empty line of a file can set the fail bit.
		if ((firstIfs.fail() && !firstIfs.eof()) || (secondIfs.fail() && !secondIfs.eof()))
		{
			throw runtime_error("Failed reading input while comparing files '" + first.string() + "' or '" + second.string() + "'.");
		}

		// Searches for a difference between characters in the line.
		size_t minLength = min(firstLine.size(), secondLine.size());
		auto mismatchIt = mismatch(firstLine.begin(), firstLine.begin() + minLength, secondLine.begin());
		if (mismatchIt.first != firstLine.begin() + minLength)
		{
			return FileDifference(first, second, FileDifferenceType::CharMismatch, lineOffset,
				mismatchIt.first - firstLine.begin());
		}

		// Checks whether the lines are different lengths.
		if (firstLine.size() != secondLine.size())
		{
			return FileDifference(first, second, FileDifferenceType::LineLength, lineOffset,
				mismatchIt.first - firstLine.begin(), firstLine.size() < secondLine.size());
		}
		
		if (firstIfs.eof() || secondIfs.eof())
		{
			// Checks whether one file is longer than the other.
			if (firstIfs.eof() != secondIfs.eof())
			{
				return FileDifference(first, second, FileDifferenceType::LineCount, lineOffset,
					0, firstLine.size() < secondLine.size());
			}
			else /* firstIfs.eof() && secondIfs.eof() */
			{
				// The files have been matched.
				break;
			}
		}

		lineOffset++;
	}

	return FileDifference(first, second, FileDifferenceType::NoDifference, 0, 0);
}

FileDifference FileUtils::compareAnyFiles(fs::path const& first, fs::path const& second) 
{
	auto firstType = status(first).type();
	auto secondType = status(second).type();

	if (firstType != secondType)
	{
		if (firstType == fs::file_type::not_found || secondType == fs::file_type::not_found)
		{
			if (firstType == fs::file_type::not_found && secondType == fs::file_type::not_found)
			{
				throw invalid_argument("Cannot compare two non-existent files '" + first.string() + "' and '" + second.string() + "'.");
			}

			bool firstIsMissing = firstType == fs::file_type::not_found;
			return FileDifference(first, second, FileDifferenceType::Existence, 0, 0, firstIsMissing);
		}

		return FileDifference(first, second, FileDifferenceType::FileType, 0, 0, false);
	}

	return compareFiles(first, second);
}

FileDifference FileUtils::compareActualAndExpectedFile(fs::path const& actual, fs::path const& expected) 
{
	if (!fs::is_regular_file(actual))
	{
		throw invalid_argument("Actual file '" + actual.string() + "' does not exist.");
	}

	if (!fs::is_regular_file(expected))
	{
		return FileDifference(actual, expected, FileDifferenceType::Existence, 0, 0, false);
	}

	return compareFiles(actual, expected);
}

vector<FileDifference> FileUtils::compareDirectories(fs::path const& first, fs::path const& second)
{
	vector<FileDifference> diffs;
	auto firstPaths = FileUtils::getSubpathsInDirectory(first);
	auto secondPaths = FileUtils::getSubpathsInDirectory(second);
	// auto firstPaths = MiscUtils::zipSymmetric();
	sort(firstPaths.begin(), firstPaths.end());
	sort(secondPaths.begin(), secondPaths.end());

	auto symmetricPaths = MiscUtils::zipSymmetric<fs::path>(&firstPaths, &secondPaths);
	for (auto const& symmetricPath : symmetricPaths)
	{
		if (symmetricPath.first == nullptr || symmetricPath.second == nullptr)
		{
			fs::path pathName = (symmetricPath.first != nullptr) ?
				*symmetricPath.first :
				*symmetricPath.second;

			if (!doIgnorePathName(pathName))
			{
				diffs.push_back(FileDifference(first / pathName, second / pathName, FileDifferenceType::Existence, 0, 0, symmetricPath.first == nullptr));
			}
		}
		else if (!doIgnorePathName(*symmetricPath.first))
		{
			auto firstPath = first / *symmetricPath.first;
			auto secondPath = second / *symmetricPath.second;
			auto firstType = status(firstPath).type();
			auto secondType = status(secondPath).type();

			if (firstType != secondType)
			{
				diffs.push_back(FileDifference(firstPath, secondPath, FileDifferenceType::FileType, 0, 0, false));
			}
			else if (firstType == fs::file_type::directory)
			{
				auto recDiffs = compareDirectories(firstPath, secondPath);
				diffs.insert(diffs.end(), recDiffs.begin(), recDiffs.end());
			}
			else if (firstType == fs::file_type::regular)
			{
				diffs.push_back(compareFiles(firstPath, secondPath));
			}
		}
	}

	return diffs;
}

string FileUtils::readFile(fs::path const& file) 
{
	ifstream ifs = ifstream(file);
	string str;

	ifs.seekg(0, ios::end);   
	str.reserve(ifs.tellg());
	ifs.seekg(0, ios::beg);

	str.assign((istreambuf_iterator<char>(ifs)), istreambuf_iterator<char>());

	return str;
}

optional<string> FileUtils::readLine(istream* input, size_t lineOffset)
{
	vector<string> lines = readLines(input, lineOffset, 1);
	return (lines.size() > 0) ?
		make_optional(lines.back()) :
		nullopt;
}

vector<string> FileUtils::readLines(istream* input, size_t lineOffset, optional<size_t> lineCount) 
{
	vector<string> lines;
	size_t currLineOffset = 0;
	size_t endLineOffset = lineCount.has_value() ? lineOffset + lineCount.value() : 0;
	string line;

	while (currLineOffset < lineOffset &&
		getline(*input, line).good())
	{
		currLineOffset++;
	}

	while ((!lineCount.has_value() || currLineOffset < endLineOffset) &&
		getline(*input, line).good())
	{
		lines.push_back(line);
		currLineOffset++;
	}

	if (input->fail() && !input->eof())
	{
		throw runtime_error("Error occurred reading input stream.");
	}

	lines.push_back(line);
	return lines;
}

int FileUtils::getSizeOfFile(fs::path const& file) 
{
	ifstream ifs = ifstream(file);
	return getSizeOfFile(&ifs);
}

int FileUtils::getSizeOfFile(ifstream* input) 
{
	input->seekg(0, ios::end);
	return input->tellg();
}

string FileUtils::getFileLocator(fs::path const& file, size_t lineOffset, optional<size_t> charOffset) 
{
	stringstream ss;
	ss << file.string() << ":" << (lineOffset + 1);
	if (charOffset.has_value())
		ss << ":" << (charOffset.value() + 1);
	return ss.str();
}

string FileUtils::getNameOfFileType(fs::file_type type) 
{
	switch (type)
	{
	case fs::file_type::regular:
		return "regular";
	case fs::file_type::directory:
		return "directory";
	default:
		return "unsupported";
	}
}

vector<fs::path> FileUtils::getPathsInDirectory(fs::path const& dir) 
{
	vector<fs::path> paths;
	for (auto const& dirEntry : fs::directory_iterator(dir))
		paths.push_back(dirEntry.path());
	return paths;
}

vector<fs::path> FileUtils::getSubpathsInDirectory(fs::path const& dir) 
{
	vector<fs::path> paths;
	for (auto const& dirEntry : fs::directory_iterator(dir))
		paths.push_back(dirEntry.path().filename());
	return paths;
}

bool FileUtils::doIgnorePathName(string const& path) 
{
	// Ignores empty path names, special names '.' and '..', and hidden files.
	return path.size() < 0 || path[0] == '.';
}

bool FileUtils::isDifference(vector<FileDifference> const& diffs)
{
	return any_of(diffs.begin(), diffs.end(), [](FileDifference const& diff){ return diff.isDifference(); });
}

string FileUtils::getDirectoryDifferenceMessage(vector<FileDifference> const& diffs) 
{
	stringstream ss;
	for (auto const& diff : diffs)
	{
		if (diff.isDifference())
			ss << diff.getMessage() << endl;
	}
	return ss.str();
}

string FileUtils::getDirectoryComparisonMessage(vector<FileDifference> const& diffs) 
{
	stringstream ss;
	for (auto const& diff : diffs)
	{
		ss << diff.getMessage() << endl;
	}
	return ss.str();
}

void FileUtils::checkIsRegularFile(fs::path const& file)
{
	if (!fs::is_regular_file(file))
		throw invalid_argument("File '" + file.string() + "' does not exist or is not a regular file.");
}
