//
//  MiscUtils.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 13/7/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#ifndef MISC_UTILS_HPP
#define MISC_UTILS_HPP

#include <functional>
#include <map>
#include <queue>
#include <sstream>
#include <unordered_map>
#include <vector>

#ifndef NDEBUG
	#define assertMsg(cond, msg) \
		__assertMsg(#cond, cond, __FILE__, __LINE__, msg)
#else
	#define assertMsg() ;
#endif

void __assertMsg(const char* condStr, bool cond, const char* file, int line,
	const char* msg);

namespace MiscUtils
{
	/** Error Handling **/

	template<typename Ret, typename ExceptionFunc, typename... Args>
		Ret throwOrReturn(Ret ret, ExceptionFunc exceptionFunc,
			bool doThrowException, Args... args);


	/** String Conversion **/

	std::string subChar(std::string const& formatter, char ch);

	template<typename T>
		std::string stringifyVector(std::vector<T> const& list);
	template<typename T>
		std::string stringifyVector(std::vector<T*> const& list);
	template<typename T>
		std::string stringifyVector(
			typename std::vector<T>::const_iterator const& begin,
			typename std::vector<T>::const_iterator const& end);
	template<typename T>
		std::string stringifyVector(
			typename std::vector<T*>::const_iterator const& begin,
			typename std::vector<T*>::const_iterator const& end);

	/* Returns an octal string representation of an integer. */
	std::string toOctal(int val);

	/* Returns a printable string representation of the character. If the character is a
	   printable character i.e. is a visual character (isgraph) or
	   whitespace control code (isspace), returns a string matching the character.
	   Otherwise returns the character code in octal format preceeded by a backslash. */
	std::string charToPrintableString(char ch);

	/* Returns a printable representation of the string.
	   Applies 'charToPrintableString()' to each character in the string. */
	std::string printableString(std::string const& str);


	/** Type Conversion **/

	template<typename T>
		std::queue<T> makeQueue(std::vector<T> const& list);


	/** Vector Value Search **/

	template<typename T>
		typename std::vector<T>::iterator findElement(std::vector<T>* list, T const& element);

	template<typename T>
		typename std::vector<T*>::iterator findElement(std::vector<T*>* list, T const* element);

	template<typename T>
		typename std::vector<T>::const_iterator findElement(std::vector<T> const& list, T const& element);

	template<typename T>
		typename std::vector<T*>::const_iterator findElement(std::vector<T*> const& list, T const* element);

	template<typename T>
		bool containsElement(std::vector<T> const& list, T const& element);

	template<typename T>
		bool containsElement(std::vector<T*> const& list, T const* element);

	template<typename T>
		size_t indexOfElement(std::vector<T> const& list, T const& element);

	template<typename T>
		size_t indexOfElement(std::vector<T*> const& list, T const* element);


	/** Vector Value Erasure **/

	template<typename T>
		void eraseAtIndex(std::vector<T>* list, size_t index);

	template<typename T>
		void eraseElement(std::vector<T>* list, T const& element);

	template<typename T>
		void eraseElement(std::vector<T*>* list, T const* element);

	template<typename T>
		bool eraseIfElement(std::vector<T>* list, T const& element);

	template<typename T>
		bool eraseIfElement(std::vector<T*>* list, T const* element);

	template<typename Key, typename Item, typename Hash>
		void eraseElementInMappedList(std::unordered_map<Key, std::vector<Item>, Hash>* listOfKey, Key const& key, Item const& element);


	/** Map, Filter, Reduce **/

	template<typename T, typename U>
		std::vector<U> transformCopy(std::vector<T> const& list, std::function<U(T const&)> const& transformFunc);


	/** Iteration **/

	template<typename T>
		std::vector<std::pair<T*, T*>> zipSymmetric(std::vector<T>* firstList, std::vector<T>* secondList);

	/* Indexes the items in the map. */
	template<typename Key, typename Item>
		std::vector<Item*> indexMappedItems(std::unordered_map<Key, Item>* mappedItems);


	/** Sorting **/

	template<typename T>
		std::vector<T> sortVectorOfIndexMap(std::map<T, size_t>* indexMap);

	/* Sorts and indexes the items in the map. */
	template<typename Key, typename Item>
		std::vector<Item*> sortAndIndexMappedItems(std::unordered_map<Key, Item>* mappedItems, std::function<bool(Item const*, Item const*)> const& sortFunc);


	/** Pointer Arithmetic **/

	template<typename T>
		const std::function<uintptr_t(T* const&)> offsetOfPtrRef =
			[](T* const& ptr){ return reinterpret_cast<uintptr_t>(ptr); };
}

template<typename T>
std::string MiscUtils::stringifyVector(std::vector<T> const& list)
{
	return stringifyVector<T>(list.begin(), list.end());
}

template<typename T>
std::string MiscUtils::stringifyVector(std::vector<T*> const& list)
{
	return stringifyVector<T>(list.begin(), list.end());
}

template<typename T>
std::string MiscUtils::stringifyVector(
	typename std::vector<T>::const_iterator const& begin,
	typename std::vector<T>::const_iterator const& end)
{
	std::stringstream ss;
	ss << "{";
	for (auto it = begin; it != end; it++)
	{
		ss << std::string(*it);
		if (it < end - 1)
			ss << ", ";
	}
	ss << "}";
	return ss.str();
}

template<typename T>
std::string MiscUtils::stringifyVector(
	typename std::vector<T*>::const_iterator const& begin,
	typename std::vector<T*>::const_iterator const& end)
{
	std::stringstream ss;
	ss << "{";
	for (auto it = begin; it != end; it++)
	{
		ss << *it << " -> " << std::string(**it);
		if (it < end - 1)
			ss << ", ";
	}
	ss << "}";
	return ss.str();
}

template<typename T>
std::queue<T> MiscUtils::makeQueue(std::vector<T> const& list)
{
	std::queue<T> _queue;
	for (auto const& item : list)
	{
		_queue.push(item);
	}
	return _queue;
}

template<typename T>
typename std::vector<T>::iterator MiscUtils::findElement(std::vector<T>* list, T const& element)
{
	return std::find(list->begin(), list->end(), element);
}

template<typename T>
typename std::vector<T*>::iterator MiscUtils::findElement(std::vector<T*>* list, T const* element)
{
	return findElement(list, const_cast<T*>(element));
}

template<typename T>
typename std::vector<T>::const_iterator MiscUtils::findElement(std::vector<T> const& list, T const& element)
{
	return std::find(list.begin(), list.end(), element);
}

template<typename T>
typename std::vector<T*>::const_iterator MiscUtils::findElement(std::vector<T*> const& list, T const* element)
{
	return findElement(list, const_cast<T*>(element));
}

template<typename T>
size_t MiscUtils::indexOfElement(std::vector<T> const& list, T const& element)
{
	auto it = findElement(list, element);
	if (it == list.end())
	{
		throw std::out_of_range("Element not found in list.");
	}
	return it - list.begin();
}

template<typename T>
size_t MiscUtils::indexOfElement(std::vector<T*> const& list, T const* element)
{
	return MiscUtils::indexOfElement(list, const_cast<T*>(element));
}

template<typename T>
bool MiscUtils::containsElement(std::vector<T> const& list, T const& element)
{
	return findElement(list, element) != list.end();
}

template<typename T>
bool MiscUtils::containsElement(std::vector<T*> const& list, T const* element)
{
	return MiscUtils::containsElement(list, const_cast<T*>(element));
}

template<typename T>
void MiscUtils::eraseAtIndex(std::vector<T>* list, size_t index)
{
	if (index >= list->size())
	{
		throw std::out_of_range("Index higher than the end of the list.");
	}
	list->erase(list->begin() + index);
}

template<typename T>
void MiscUtils::eraseElement(std::vector<T>* list, T const& element)
{
	if (!eraseIfElement(list, element))
	{
		throw std::out_of_range("Element not found in list.");
	}
}

template<typename T>
void MiscUtils::eraseElement(std::vector<T*>* list, T const* element)
{
	return MiscUtils::eraseElement(list, const_cast<T*>(element));
}

template<typename T>
bool MiscUtils::eraseIfElement(std::vector<T>* list, T const& element)
{
	auto it = findElement(list, element);
	if (it != list->end())
	{
		list->erase(it);
		return true;
	}
	return false;
}

template<typename T>
bool MiscUtils::eraseIfElement(std::vector<T*>* list, T const* element)
{
	return MiscUtils::eraseIfElement(list, const_cast<T*>(element));
}

template<typename Key, typename Item, typename Hash>
void MiscUtils::eraseElementInMappedList(std::unordered_map<Key, std::vector<Item>, Hash>* listOfKey, Key const& key, Item const& element)
{
	eraseElement(&listOfKey->at(key), element);
	if (listOfKey->at(key).empty())
	{
		listOfKey->erase(key);
	}
}

template<typename T, typename U>
std::vector<U> MiscUtils::transformCopy(std::vector<T> const& list, std::function<U(T const&)> const& transformFunc)
{
	auto newList = std::vector<U>(list.size());
	std::transform(list.begin(), list.end(), newList.begin(), transformFunc);
	return newList;
}

template<typename T>
std::vector<std::pair<T*, T*>> MiscUtils::zipSymmetric(std::vector<T>* firstList, std::vector<T>* secondList)
{
	std::vector<std::pair<T*, T*>> zip;
	size_t i = 0;
	size_t k = 0;
	while (i < firstList->size() && k < secondList->size())
	{
		if (firstList->at(i) == secondList->at(k))
		{
			zip.emplace_back(&firstList->at(i), &secondList->at(k));
			i++;
			k++;
		}
		else if (firstList->at(i) < secondList->at(k))
		{
			zip.emplace_back(&firstList->at(i), nullptr);
			i++;
		}
		else
		{
			zip.emplace_back(nullptr, &secondList->at(k));
			k++;
		}
	}
	while (i < firstList->size())
	{
		zip.emplace_back(&firstList->at(i), nullptr);
		i++;
	}
	while (k < secondList->size())
	{
		zip.emplace_back(nullptr, &secondList->at(k));
		k++;
	}
	return zip;
}

template<typename Key, typename Item>
std::vector<Item*> MiscUtils::indexMappedItems(std::unordered_map<Key, Item>* mappedItems)
{
	std::vector<Item*> indexedItems;
	for (auto& [_, item] : *mappedItems)
	{
		indexedItems.push_back(&item);
	}
	return indexedItems;
}

template<typename T>
std::vector<T> MiscUtils::sortVectorOfIndexMap(std::map<T, size_t>* indexMap)
{
	std::vector<T> values;
	for (auto const& [value, index] : *indexMap)
	{
		indexMap->at(value) = values.size();
		values.push_back(value);
	}
	return values;
}

template<typename Key, typename Item>
std::vector<Item*> MiscUtils::sortAndIndexMappedItems(std::unordered_map<Key, Item>* mappedItems, std::function<bool(Item const*, Item const*)> const& sortFunc)
{
	std::vector<Item*> indexedItems = indexMappedItems(mappedItems);
	sort(indexedItems.begin(), indexedItems.end(), sortFunc);
	return indexedItems;
}

#endif /* MISC_UTILS_HPP */
