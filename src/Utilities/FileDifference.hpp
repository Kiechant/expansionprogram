//
//  FileDifference.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 21/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef FILE_DIFFERENCE_HPP
#define FILE_DIFFERENCE_HPP

#include "FileDifferenceDecl.hpp"

#include "Cache.hpp"
#include "EnumStringMapDecl.hpp"

#include <filesystem>

enum class FileDifferenceType
{
	NoDifference,
	CharMismatch,
	LineLength,
	LineCount,
	Existence,
	FileType
};

class FileDifference
{
public:
	FileDifference(std::filesystem::path const& firstFile, std::filesystem::path const& secondFile,
		FileDifferenceType differenceType, size_t lineOffset, size_t charOffset, bool isFirstSmaller = true);
	
	friend void swap(FileDifference&, FileDifference&);
	FileDifference(FileDifference const&);
	FileDifference& operator=(FileDifference);
	
	bool isDifference() const;
	FileDifferenceType getDifferenceType() const;
	size_t getLine() const;
	size_t getChar() const;
	std::string const& getMessage() const;

private:
	std::filesystem::path firstFile;
	std::filesystem::path secondFile;
	
	FileDifferenceType differenceType;
	size_t lineOffset;
	size_t charOffset;
	bool isFirstSmaller;

	Cache<std::string> message;

	std::string composeMessage() const;
	std::string composeLineDifferenceMessage(int fileNo) const;
	std::string composeLineCountDifferenceMessage() const;
	std::string composeExistenceMessage() const;
	std::string composeFileTypeMessage() const;
	std::string composeCharMarker() const;
	std::string composeFileLocator(std::filesystem::path const& file, bool doIncludeCharOffset) const;
	std::string composeTypeDescriptor(std::filesystem::path const& file) const;

	std::filesystem::path const* getFile(int fileNo) const;
	std::pair<std::filesystem::path const*, std::filesystem::path const*> getOrderedFiles() const;

	static EnumStringMap<FileDifferenceType> getDifferenceTypeNameMap();
};

#endif /* FILE_DIFFERENCE_HPP */
