//
//  FileDifferenceDecl.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 21/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef FILE_DIFFERENCE_DECL_HPP
#define FILE_DIFFERENCE_DECL_HPP

enum class FileDifferenceType;
class FileDifference;

#endif /* FILE_DIFFERENCE_DECL_HPP */
