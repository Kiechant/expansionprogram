//
//  UserFunction.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "UserFunction.hpp"

#include "ValueEntry.tpp"

using namespace std;

EpArgumentList::EpArgumentList(std::vector<EpValue> const& arguments)
	: arguments(arguments) {}
