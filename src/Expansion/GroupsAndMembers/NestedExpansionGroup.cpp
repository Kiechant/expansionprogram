//
//  NestedExpansionGroup.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 8/7/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#include "NestedExpansionGroup.hpp"

#include "ExpansionDefinition.hpp"
#include "ExpansionFieldEntry.hpp"
#include "ExpansionGroup.tpp"
#include "ExpansionGroupFieldEntry.tpp"

#include <iostream>

using namespace std;

using FieldFormat = ExpansionFieldEntryFormat;

NestedExpansionGroup::NestedExpansionGroup(string const& id, string const& name,
	string const& ownerId) : ExpansionGroup(id, name)
{
	this->ownerId = ownerId;
	
	fieldMap.emplace(ExpansionDefinition::idOfReservedField(ExpansionDefinition::ReservedField::Owner), ExpansionGroupFieldEntry(FieldFormat::Owner, false));
}

NestedExpansionGroup::NestedExpansionGroup(string const& id,
	string const& ownerId) : ExpansionGroup(id)
{
	this->ownerId = ownerId;
}

string const& NestedExpansionGroup::getOwnerId() const
{
	return ownerId;
}

void NestedExpansionGroup::addArgument(string const& arg)
{
	arguments.push_back(arg);
}

string NestedExpansionGroup::getArgument(size_t argIndex) const
{
	if (argIndex >= arguments.size())
		throw out_of_range("Argument index of " + to_string(argIndex) + " does not exist in the " + groupInfo() + ".");
	return arguments.at(argIndex);
}

size_t NestedExpansionGroup::getArgumentCount() const
{
	return arguments.size();
}
