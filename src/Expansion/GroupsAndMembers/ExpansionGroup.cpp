//
//  ExpansionGroup.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 8/7/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#include "ExpansionGroup.tpp"

#include "ExpansionDefinition.hpp"
#include "ExpansionFieldEntry.hpp"
#include "ExpansionGroupFieldEntry.tpp"
#include "ExpansionMemberFieldMap.tpp"
#include "ValueEntry.tpp"

#include <iostream>
#include <sstream>
#include <utility>

using namespace std;
using FieldFormat = ExpansionFieldEntryFormat;
using EntryType = ExpansionGroupFieldEntryType;


/** Constructors and Destructors **/

ExpansionGroup::ExpansionGroup()
	: staticFieldMap(new ExpansionMemberFieldMap(this, nullptr, true)) {}

ExpansionGroup::ExpansionGroup(string const& id, string const& name)
	: ExpansionGroup()
{
	this->id = id;
	this->name = name;

	fieldMap = unordered_map<string, ExpansionGroupFieldEntry>();
	functionOfName = unordered_map<string, FunctionType>();

	fieldMap.emplace(ExpansionDefinition::idOfReservedField(ExpansionDefinition::ReservedField::GroupId), ExpansionGroupFieldEntry(FieldFormat::GroupIdentifier, false));
	fieldMap.emplace(ExpansionDefinition::idOfReservedField(ExpansionDefinition::ReservedField::GroupName), ExpansionGroupFieldEntry(FieldFormat::GroupName, false));
	fieldMap.emplace(ExpansionDefinition::idOfReservedField(ExpansionDefinition::ReservedField::Id), ExpansionGroupFieldEntry(FieldFormat::Identifier, false));
	fieldMap.emplace(ExpansionDefinition::idOfReservedField(ExpansionDefinition::ReservedField::Name), ExpansionGroupFieldEntry(FieldFormat::Name, false));
}

ExpansionGroup::ExpansionGroup(string const& id)
	: ExpansionGroup(id, id) {}


/** Copy and Move Semantics **/

ExpansionGroup::ExpansionGroup(ExpansionGroup const& other)
{
	this->staticFieldMap = new ExpansionMemberFieldMap(*other.staticFieldMap);

	// The static field map must point to the newly created object,
	// otherwise it becomes a shallow copy.
	this->staticFieldMap->setGroup(this);

	this->memberIds = other.memberIds;
	this->indexOfMemberId = other.indexOfMemberId;
	this->categoryIds = other.categoryIds;
	this->indexOfCategoryId = other.indexOfCategoryId;
	this->nestedGroupOfId = other.nestedGroupOfId;
	this->id = other.id;
	this->name = other.name;
	this->functionOfName = other.functionOfName;
	this->fieldMap = other.fieldMap;
}

void swap(ExpansionGroup& first, ExpansionGroup& second)
{
	using std::swap;
	swap(first.staticFieldMap, second.staticFieldMap);

	// Required since the static field map must always be pointing to the current group.
	swapGroup(*first.staticFieldMap, *second.staticFieldMap);

	swap(first.memberIds, second.memberIds);
	swap(first.indexOfMemberId, second.indexOfMemberId);
	swap(first.categoryIds, second.categoryIds);
	swap(first.indexOfCategoryId, second.indexOfCategoryId);
	swap(first.nestedGroupOfId, second.nestedGroupOfId);
	swap(first.id, second.id);
	swap(first.name, second.name);
	swap(first.fieldMap, second.fieldMap);
	swap(first.functionOfName, second.functionOfName);
}

ExpansionGroup::ExpansionGroup(ExpansionGroup&& other)
{
	swap(*this, other);
}

ExpansionGroup& ExpansionGroup::operator=(ExpansionGroup other)
{
	swap(*this, other);
	return *this;
}

ExpansionGroup::~ExpansionGroup() {}


/** Basic Accessors **/

string const& ExpansionGroup::getId() const
{
	return id;
}

string const& ExpansionGroup::getName() const
{
	return name;
}

NestedExpansionGroup const* ExpansionGroup::getNestedGroupOfId(string const& id) const
{
	auto it = nestedGroupOfId.find(id);
	if (it == nestedGroupOfId.end())
		throw out_of_range("Attempted to access non-existent nested group of id '" + id + "' in " + groupInfo() + ".");
	return it->second;
}

void ExpansionGroup::setNestedGroupOfId(string const& id, NestedExpansionGroup const* nestedGroup)
{
	if (nestedGroupOfId.count(id) == 0)
	{
		nestedGroupOfId.emplace(id, nestedGroup);
	}
	else
	{
		nestedGroupOfId.at(id) = nestedGroup;
	}
}

ExpansionMemberFieldMap* ExpansionGroup::getMutableStaticFieldMap()
{
	return staticFieldMap;
}

ExpansionMemberFieldMap const* ExpansionGroup::getStaticFieldMap() const
{
	return &dynamic_cast<ExpansionMemberFieldMap const&>(*staticFieldMap);
}



/** General Field and Format Interface **/

void ExpansionGroup::initField(string const& field, FieldFormat format, bool isStatic)
{
	fieldMap.emplace(field, ExpansionGroupFieldEntry(format, isStatic));
	if (isStatic)
	{
		staticFieldMap->initField(field);
	}
}

vector<string> ExpansionGroup::getFields() const
{
	vector<string> fields;
	for (auto const& fieldItem : fieldMap)
	{
		fields.push_back(fieldItem.first);
	}
	return fields;
}

bool ExpansionGroup::hasField(string const& field) const
{
	return fieldMap.find(field) != fieldMap.end() ||
		staticFieldMap->hasField(field) ||
		field == ExpansionDefinition::idOfReservedField(ExpansionDefinition::ReservedField::GroupId) ||
		field == ExpansionDefinition::idOfReservedField(ExpansionDefinition::ReservedField::GroupName);
}

FieldFormat ExpansionGroup::getFormat(string const& field) const
{
	auto groupIt = fieldMap.find(field);
	if (groupIt != fieldMap.end())
	{
		return groupIt->second.getFormat();
	}
	throw fieldNotDefinedError(field, false);
}


/** List Size Interface **/

bool ExpansionGroup::isListedField(string const& field) const
{
	auto format = getFormat(field);
	bool isListedFormat = ExpansionFieldEntry::isListedFormat(format);
	return isListedFormat && (getListSize(field) > 1);
}

size_t ExpansionGroup::getListSize(string const& field) const
{
	auto format = getFormat(field);
	
	switch (format) 
	{
	case FieldFormat::Value:
	case FieldFormat::ForeignMember:
	case FieldFormat::NestedMember:
		return getFieldEntry(field)->getListSize();
	default:
		throw noListSizeError(field);
	}
}

void ExpansionGroup::setListSize(string const& field, size_t size)
{
	auto format = getFormat(field);

	switch(format)
	{
	case FieldFormat::Value:
	case FieldFormat::ForeignMember:
	case FieldFormat::NestedMember:
		getMutableFieldEntry(field)->setListSize(size);
		if (isStatic(field))
			staticFieldMap->setListSize(field, size);
		break;
	default:
		throw noListSizeError(field);
	}
}


/** Static Query Interface **/

bool ExpansionGroup::isGroupContainedEntry(string const& field) const
{
	return !isMemberContainedEntry(field);
}

bool ExpansionGroup::isMemberContainedEntry(string const& field) const
{
	FieldFormat format = getFormat(field);
	return ExpansionFieldEntry::isDefaultMemberContainedFormat(format) ||
		(ExpansionFieldEntry::isMemberDefinedFormat(format) && !isStatic(field));
}

bool ExpansionGroup::isStatic(string const& field) const
{
	return getFieldEntry(field)->isStatic();
}

void ExpansionGroup::isStatic(string const& field, bool _isStatic)
{
	getMutableFieldEntry(field)->isStatic(_isStatic);
}


/** Uniqueness Interface **/

void ExpansionGroup::setUnique(string const& field)
{
	if (isUnique(field))
		throw invalid_argument("Attempting to set field '" + field + "' " +
			"as unique, when it is already unique in " + groupInfo() + ".");
	FieldFormat format = getFormat(field);
	if (ExpansionFieldEntry::isUniqueSupportingFormat(format) || getListSize(field) > 1)
		throw invalid_argument("Attempting to set field '" + field + "' of format '" +
			ExpansionFieldEntry::nameOfFormat(format) + "' as unique, when it does not support uniqueness " +
			"in " + groupInfo() + ".");
	uniqueFieldSet.insert(field);
}

bool ExpansionGroup::isUnique(string const& field) const
{
	return ExpansionFieldEntry::isDefaultUniqueFormat(getFormat(field)) || uniqueFieldSet.count(field) >= 1;
}

vector<string> ExpansionGroup::getUniqueFields() const
{
	auto uniqueFields = vector<string>(uniqueFieldSet.size());
	copy(uniqueFieldSet.begin(), uniqueFieldSet.end(), uniqueFields.begin());
	return uniqueFields;
}


/** Value Field Interface **/

ValueCategory ExpansionGroup::getValueType(string const& field) const
{
	return getFieldEntryWithFormat<FieldFormat::Value>(field)->first;
}

void ExpansionGroup::setValueType(string const& field, string const& valueType)
{
	auto entry = getMutableFieldEntryWithFormat<FieldFormat::Value>(field);
	entry->first = ValueEntry::valueCategoryOfId(valueType);
}


/** Foreign Member Field Interface **/

string const* ExpansionGroup::getForeignGroupId(string const& field) const
{
	return getGroupId(field, FieldFormat::ForeignMember);
}

void ExpansionGroup::setForeignGroupId(string const& field, string const& foreignGroupId)
{
	setGroupId(field, FieldFormat::ForeignMember, foreignGroupId);
}


/** Nested Member Field Interface **/

string const* ExpansionGroup::getNestedGroupId(string const& field) const
{
	return getGroupId(field, FieldFormat::NestedMember);
}

void ExpansionGroup::setNestedGroupId(string const& field, string const& nestedGroupId)
{
	setGroupId(field, FieldFormat::NestedMember, nestedGroupId);
}


/** Function Field Interface **/

ExpansionGroup::FunctionType const* ExpansionGroup::getFunction(string const& field) const
{
	return getFunctionOfName(*getFunctionName(field));
}

string const* ExpansionGroup::getFunctionName(string const& field) const
{
	return &get<0>(*getFieldEntryWithFormat<FieldFormat::Function>(field));
}

void ExpansionGroup::setFunctionName(string const& field, string const& name)
{
	auto funcDef = getMutableFieldEntryWithFormat<FieldFormat::Function>(field);
	get<0>(*funcDef) = name;
}

ValueCategory ExpansionGroup::getFunctionReturnType(string const& field) const
{
	return get<1>(*getFieldEntryWithFormat<FieldFormat::Function>(field));
}

void ExpansionGroup::setFunctionReturnType(string const& field, string const& returnType)
{
	auto funcDef = getMutableFieldEntryWithFormat<FieldFormat::Function>(field);
	get<1>(*funcDef) = ValueEntry::valueCategoryOfId(returnType);
}

vector<shared_ptr<TemplateTree>> const* ExpansionGroup::getFunctionArgs(string const& field) const
{
	return &get<2>(*getFieldEntryWithFormat<FieldFormat::Function>(field));
}

void ExpansionGroup::setFunctionArgs(string const& field,
	vector<string>::const_iterator argsBegin,
	vector<string>::const_iterator argsEnd)
{
	auto funcDef = getMutableFieldEntryWithFormat<FieldFormat::Function>(field);
	auto parsedArgs = vector<shared_ptr<TemplateTree>>(argsEnd - argsBegin);
	transform(argsBegin, argsEnd, parsedArgs.begin(), [](string const& arg)
	{
		return shared_ptr<TemplateTree>(TemplateTree::parseProperty(arg));
	});
	get<2>(*funcDef) = parsedArgs;
}

ExpansionGroup::FunctionType const* ExpansionGroup::getFunctionOfName(string const& name) const
{
	return &functionOfName.at(name);
}

void ExpansionGroup::setFunctionOfName(string const& name, ExpansionGroup::FunctionType const& function)
{
	functionOfName[name] = function;
}

ValueEntry ExpansionGroup::evaluateFunction(string const& field,
	vector<ValueEntry> const& resolvedArgs) const
{
	return (*getFunction(field))(EpArgumentList(resolvedArgs));
}

string ExpansionGroup::evaluateFunctionString(string const& field,
	vector<ValueEntry> const& resolvedArgs) const
{
	return toString(evaluateFunction(field, resolvedArgs));
}


/** Alias Field Interface **/

shared_ptr<TemplateTree> const* ExpansionGroup::getAlias(string const& field) const
{
	return getFieldEntryWithFormat<FieldFormat::Alias>(field);
}

void ExpansionGroup::setAlias(string const& field, string const& subproperty)
{
	auto parsedSubproperty = shared_ptr<TemplateTree>(TemplateTree::parseProperty(subproperty));
	*getMutableFieldEntryWithFormat<FieldFormat::Alias>(field) = parsedSubproperty;
}


/** Object Description Methods **/

string ExpansionGroup::groupInfo() const
{
	stringstream ss;
	ss << "expansion group '" << getName() << "'";
	return ss.str();
}


/** Constant Field Accessors **/

ExpansionGroupFieldEntry const* ExpansionGroup::getFieldEntry(string const& field) const
{
	auto entryIt = fieldMap.find(field);
	if (entryIt == fieldMap.end())
	{
		throw fieldNotDefinedError(field, false);
	}
	return &entryIt->second;
}

ExpansionGroupFieldEntry const* ExpansionGroup::getFieldEntryWithFormat(
	string const& field, FieldFormat format) const
{
	auto entry = getFieldEntry(field);
	if (format != entry->getFormat())
	{
		throw badFormatError(field, format, entry->getFormat());
	}
	return entry;
}


/** Mutable Field Accessors **/

ExpansionGroupFieldEntry* ExpansionGroup::getMutableFieldEntry(string const& field)
{
	return MUTABLE_FROM_CONST_MEMBER_FUNC(this, getFieldEntry, field);
}

ExpansionGroupFieldEntry* ExpansionGroup::getMutableFieldEntryWithFormat(
	string const& field, FieldFormat format)
{
	return MUTABLE_FROM_CONST_MEMBER_FUNC(this, getFieldEntryWithFormat,
		field, format);
}


/** Error Handling **/

out_of_range ExpansionGroup::fieldNotDefinedError(string const& field, bool inStatic) const
{
	string inStaticMsg = inStatic ? "static " : "";
	return out_of_range("The " + inStaticMsg + "field '" + field + "' is not defined " +
		"in the " + groupInfo() + ".");
}

out_of_range ExpansionGroup::badFormatError(string const& field, FieldFormat requested, FieldFormat expected) const
{
	return out_of_range("The field '" + field + "' of the " + groupInfo() +
		" is of the requested format '" + ExpansionFieldEntry::nameOfFormat(requested) + "'. " +
		"Expected '" + ExpansionFieldEntry::nameOfFormat(expected) + "'.");
}

out_of_range ExpansionGroup::noListSizeError(string const& field) const
{
	return out_of_range("The field '" + field + "' in the " + groupInfo() +
		" is of the format '" + ExpansionFieldEntry::nameOfFormat(getFormat(field)) +
		"', which does not define a list of values.");
}


/** Field Checking Helpers **/

void ExpansionGroup::checkHasField(string const& field) const
{
	getFieldEntry(field);
}

void ExpansionGroup::checkHasFieldWithFormat(string const& field,
	FieldFormat format) const
{
	getFieldEntryWithFormat(field, format);
}


/** Misc Helpers **/

vector<string> ExpansionGroup::findGroupsContainingField(
	vector<ExpansionGroup const*> const& groups, string const& field)
{
	auto groupsWithField = vector<string>();
	for_each(groups.begin(), groups.end(),
	[&field, &groupsWithField](auto group)
	{
		if (group->hasField(field))
			groupsWithField.push_back(group->getName());
	});
	return groupsWithField;
}


/** Group Referencing Helpers **/

string const* ExpansionGroup::getGroupId(string const& field, FieldFormat format) const
{
	return &getFieldEntryWithFormat(field, format)->getEntry<EntryType::StringSizePair>()->first;
}

string* ExpansionGroup::getGroupId(string const& field, FieldFormat format)
{
	return &getMutableFieldEntryWithFormat(field, format)->getMutableEntry<EntryType::StringSizePair>()->first;
}

void ExpansionGroup::setGroupId(string const& field, FieldFormat format, string const& groupId)
{
	auto id = getGroupId(field, format);
	*id = groupId;
}
