//
//  ExpansionMember.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 5/3/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#include "ExpansionMember.hpp"

#include "ExpansionMemberFieldMap.tpp"
#include "ExpansionGroup.tpp"

#include <iostream>
#include <sstream>
#include <string>

using namespace std;
using FieldFormat = ExpansionFieldEntryFormat;


/** Constructors and Destructors **/

ExpansionMember::ExpansionMember(string const& id, string const& name,
	ExpansionGroup const* group)
		: fieldMap(new ExpansionMemberFieldMap(group, this, false)), id(id), name(name), group(group) {}

ExpansionMember::ExpansionMember(string const& id, ExpansionGroup const* group)
	: ExpansionMember(id, id, group) {}

ExpansionMember::~ExpansionMember() {}


/** Copy and Move Semantics **/

void swap(ExpansionMember& first, ExpansionMember& second)
{
	using std::swap;

	// Must update the references of the contained field maps to point
	// to the correct member and group.
	swap(first.fieldMap, second.fieldMap);
	swapGroup(*first.fieldMap, *second.fieldMap);
	swapMember(*first.fieldMap, *second.fieldMap);

	swap(first.id, second.id);
	swap(first.name, second.name);
	swap(first.group, second.group);
}

ExpansionMember::ExpansionMember(ExpansionMember const& other)
{
	this->fieldMap = other.fieldMap;
	this->fieldMap->setGroup(other.group);
	this->fieldMap->setMember(this);

	this->id = other.id;
	this->name = other.name;
	this->group = other.group;
}

ExpansionMember::ExpansionMember(ExpansionMember&& other)
{
	swap(*this, other);
}

ExpansionMember& ExpansionMember::operator=(ExpansionMember other)
{
	swap(*this, other);
	return *this;
}



/** Basic Accessors **/

string const& ExpansionMember::getId() const
{
	return id;
}

string const& ExpansionMember::getName() const
{
	return name;
}

ExpansionGroup const* ExpansionMember::getGroup() const
{
	return group;
}

ExpansionMemberFieldMap* ExpansionMember::getMutableFieldMap()
{
	return fieldMap;
}

ExpansionMemberFieldMap const* ExpansionMember::getFieldMap() const
{
	return &dynamic_cast<ExpansionMemberFieldMap const&>(*fieldMap);
}


/** Object Description Methods **/

string ExpansionMember::memberInfo() const
{
	return "expansion member '" + getName() + "' of " + group->groupInfo();
}
