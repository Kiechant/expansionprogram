//
//  ExpansionMemberFieldMap.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 21/4/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef EXPANSION_MEMBER_FIELD_MAP_HPP
#define EXPANSION_MEMBER_FIELD_MAP_HPP

#include "ExpansionMemberFieldEntryDecl.hpp"
#include "TemplateTreeDecl.hpp"

class ExpansionMember;
class ExpansionGroup;
class ValueEntry;

#include <map>
#include <unordered_map>

class ExpansionMemberFieldMap
{
public:
	/** Constructors and Destructors **/

	ExpansionMemberFieldMap(ExpansionGroup const* group, ExpansionMember const* member, bool isStatic);
	ExpansionMemberFieldMap(ExpansionMemberFieldMap const& other);
	ExpansionMemberFieldMap(ExpansionMemberFieldMap&& other);
	ExpansionMemberFieldMap& operator=(ExpansionMemberFieldMap other);
	friend void swap(ExpansionMemberFieldMap& first, ExpansionMemberFieldMap& second);


	/** Group and Member Management **/

	ExpansionGroup const* getGroup() const;
	void setGroup(ExpansionGroup const* group);
	friend void swapGroup(ExpansionMemberFieldMap& first, ExpansionMemberFieldMap& second);

	ExpansionMember const* getMember() const;
	void setMember(ExpansionMember const* member);
	friend void swapMember(ExpansionMemberFieldMap& first, ExpansionMemberFieldMap& second);


	/** Field Query Methods **/

	void initField(std::string const& field);
	bool hasField(std::string const& field) const;
	ExpansionFieldEntryFormat getFormat(std::string const& field) const;
	bool areFieldsComplete() const;


	/** List Size Interface **/

	size_t getListSize(std::string const& field) const;
	void setListSize(std::string const& field, size_t size);


	/** Static Query Interface **/

	bool isStatic(std::string const& field) const;
	void isStatic(std::string const& field, bool);


	/** Value Field Interface **/

	ValueEntry const* getValue(std::string const& field, size_t index = 0) const;
	void setValue(std::string const& field, ValueEntry const& value, size_t index = 0);
	std::string getValueString(std::string const& field, size_t index = 0) const;

	// Returns a list of all value indexers in the field map.
	std::vector<std::pair<std::string, size_t>> getAllValueIndexers() const;

	// Casts the value to the type requested in the group, if the type conversion can be performed.
	// For instance, signed integers may be cast to unsigned integers if they are non-negative.
	void tryCastValueToRequestedType(std::string const& field, size_t index = 0);

	// Tries to cast the value to the requested type for all value entries in the field map.
	void tryCastAllValuesToRequestedType();

	// Validates a value has the same requested type in the group.
	void validateValueHasRequestedType(std::string const& field, size_t index = 0) const;

	// Validates the all values in the field map have the same type requested in the group.
	void validateAllValuesHaveRequestedType() const;


	/** Foreign Member Field Interface **/

	std::string const* getForeignMemberId(std::string const& field, size_t index = 0) const;
	void setForeignMemberId(std::string const& field, std::string const& foreignMemberId, size_t index = 0);


	/** Nested Member Field Interface **/

	NestedExpansionMember const* getNestedMember(std::string const& field, size_t index = 0) const;
	NestedExpansionMember* getMutableNestedMember(std::string const& field, size_t index);
	void createNestedMember(std::string const& field, std::vector<std::vector<ValueEntry>> const& nestedArgs, size_t index = 0);


	/** Nested Template Field Interface **/

	const std::shared_ptr<TemplateTree> getNestedTemplate(std::string const& field) const;
	void parseNestedTemplate(std::string const& field, std::string const& nestedTemplate);
	std::string evaluateNestedTemplateString(std::string const& field, std::vector<std::string> const& replacements) const;
	
private:
	struct FieldCompletion
	{
		std::vector<bool> bits;
		unsigned char divisionCount;
		unsigned char listSize;
		static const unsigned char offset = 2;

		FieldCompletion(unsigned char divisionCount, unsigned char listSize);
		unsigned char getDivisionCount() const;
		unsigned char getListSize() const;
		void setField(unsigned char divisionNumber, unsigned char index);
		bool isFieldUpdated() const;
		void isFieldUpdated(bool _isFieldUpdated);
		bool isFieldCompleted();
	};


	/** Members **/

	// NOTE: Booleans must be initialised. Without initialising these values in a constructor,
	// a runtime error will be generated whenever the user attempt to access their value.
	// The temporary object created in a move constructor caused a runtime error when these properties
	// weren't explicitly instantiated here or in the move constructor's initialisation list,
	// because the value must be accessed to be swapped.
	//
	// Error generated:
	// runtime error: load of value 190, which is not a valid value for type 'typename remove_reference<bool &>::type' (aka 'bool')

	bool _isStatic = false;
	ExpansionGroup const* group = nullptr;
	ExpansionMember const* member = nullptr;
	std::map<std::string, ExpansionMemberFieldEntry> fieldMap;
	mutable bool areFieldsUpdated = false;
	mutable bool _areFieldsComplete = false;
	mutable std::unordered_map<std::string, FieldCompletion> fieldCompletionMap;


	/** Object Description Methods **/

	std::string ownerInfo() const;


	/** Field Completion **/

	void markFieldUpdate(std::string const& field, unsigned char divisionNumber, size_t index);


	/** Constant Field Access Methods **/

	ExpansionMemberFieldEntry const*
		getFieldEntry(std::string const& field) const;
	ExpansionMemberFieldEntry const*
		getFieldEntry(std::string const& field, ExpansionFieldEntryFormat format) const;
	template<ExpansionFieldEntryFormat F>
	ExpansionMemberFieldEntryTypeOfFormatT<F> const*
		getFieldEntry(std::string const& field) const;
	template<ExpansionFieldEntryFormat F>
	typename ExpansionMemberFieldEntryTypeOfFormatT<F>::value_type const*
		getListedFieldEntry(std::string const& field, size_t index) const;


	/** Mutable Field Access Methods **/

	ExpansionMemberFieldEntry*
		getMutableFieldEntry(std::string const& field);
	ExpansionMemberFieldEntry*
		getMutableFieldEntry(std::string const& field, ExpansionFieldEntryFormat format);
	template<ExpansionFieldEntryFormat F>
	ExpansionMemberFieldEntryTypeOfFormatT<F>*
		getMutableFieldEntry(std::string const& field);
	template<ExpansionFieldEntryFormat F>
	typename ExpansionMemberFieldEntryTypeOfFormatT<F>::value_type*
		getMutableListedFieldEntry(std::string const& field, size_t index);


	/** Format Query Methods **/

	void checkIndexWithinList(std::string const& field, size_t index) const;
	void checkFieldEntryHasFormatAndIndex(std::string const& field, ExpansionFieldEntryFormat format, size_t index) const;


	/** Error Handling **/

	std::out_of_range fieldNotFoundError(std::string const& field) const;
	std::out_of_range badFormatError(std::string const& field, ExpansionFieldEntryFormat format) const;
	std::out_of_range badIndexError(std::string const& field, size_t index) const;
	std::out_of_range groupDefinedFieldError(std::string const& field, ExpansionFieldEntryFormat format) const;
};

#endif /* EXPANSION_MEMBER_FIELD_MAP_HPP */
