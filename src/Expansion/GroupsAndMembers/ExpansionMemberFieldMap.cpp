//
//  ExpansionMemberFieldMap.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 21/4/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "ExpansionMemberFieldMap.tpp"

// TODO: Remove reliance on ExpansionDefinition.
#include "ExpansionDefinition.hpp"

#include "ExpansionFieldEntry.hpp"
#include "ExpansionMemberFieldEntry.tpp"
#include "ExpansionGroup.tpp"
#include "NestedExpansionMember.hpp"
#include "NestedExpansionGroup.hpp"
#include "TemplateTree.hpp"
#include "Tree.tpp"
#include "ValueEntry.tpp"

#include <any>
#include <sstream>

using namespace std;

using FieldFormat = ExpansionFieldEntryFormat;
using EntryType = ExpansionMemberFieldEntryType;


ExpansionMemberFieldMap::FieldCompletion::FieldCompletion(unsigned char divisionCount, unsigned char listSize)
	: divisionCount(divisionCount), listSize(listSize)
{
	bits = vector<bool>(divisionCount * listSize + offset);
}

unsigned char ExpansionMemberFieldMap::FieldCompletion::getDivisionCount() const
{
	return divisionCount;
}

unsigned char ExpansionMemberFieldMap::FieldCompletion::getListSize() const
{
	return listSize;
}

void ExpansionMemberFieldMap::FieldCompletion::setField(unsigned char divisionNumber, unsigned char index)
{
	isFieldUpdated(true);
	bits[divisionNumber * index + index + offset] = true;
}

bool ExpansionMemberFieldMap::FieldCompletion::isFieldUpdated() const
{
	return bits[0];
}

void ExpansionMemberFieldMap::FieldCompletion::isFieldUpdated(bool _isFieldUpdated)
{
	bits[0] = _isFieldUpdated;
}

bool ExpansionMemberFieldMap::FieldCompletion::isFieldCompleted()
{
	if (!bits[1] && isFieldUpdated())
	{
		bits[1] = !any_of(bits.begin() + offset, bits.end(), logical_not());
		isFieldUpdated(false);
	}
	return bits[1];
}


/** Constructors and Destructors **/

ExpansionMemberFieldMap::ExpansionMemberFieldMap(ExpansionGroup const* group, ExpansionMember const* member, bool isStatic)
	: _isStatic(isStatic), group(group)
{
	if (!isStatic)
	{
		// Initialises field map with lists equal to the sizes defined in the group.
		this->member = member;

		// TODO: Fix error here for nested group.
		vector<string> fields = group->getFields();
		
		for (auto it = fields.begin(); it != fields.end(); it++)
		{
			string field = *it;
			auto format = group->getFormat(*it);
			if (ExpansionFieldEntry::isMemberDefinedFormat(format))
			{
				if (!group->isStatic(field))
				{
					initField(field);
					size_t listSize = 1;
					if (ExpansionFieldEntry::isListedFormat(format))
					{
						listSize = group->getListSize(field);
						setListSize(field, listSize);
					}
					unsigned char divs = ExpansionMemberFieldEntry::getDivisionCount(format);
					fieldCompletionMap.emplace(field, FieldCompletion(divs, listSize));
				}
			}
		}
	}
	else
	{
		this->member = nullptr;
	}
}

ExpansionMemberFieldMap::ExpansionMemberFieldMap(ExpansionMemberFieldMap const& other)
{
	this->_isStatic = other._isStatic;
	this->group = other.group;
	this->fieldMap = other.fieldMap;
	this->areFieldsUpdated = other.areFieldsUpdated;
	this->_areFieldsComplete = other._areFieldsComplete;
	this->fieldCompletionMap = other.fieldCompletionMap;
}
ExpansionMemberFieldMap::ExpansionMemberFieldMap(ExpansionMemberFieldMap&& other)
{
	swap(*this, other);
}
ExpansionMemberFieldMap& ExpansionMemberFieldMap::operator=(ExpansionMemberFieldMap other)
{
	swap(*this, other);
	return *this;
}
void swap(ExpansionMemberFieldMap& first, ExpansionMemberFieldMap& second)
{
	using std::swap;
	swap(first._isStatic, second._isStatic);
	swap(first.group, second.group);
	swap(first.fieldMap, second.fieldMap);
	swap(first.areFieldsUpdated, second.areFieldsUpdated);
	swap(first._areFieldsComplete, second._areFieldsComplete);
	swap(first.fieldCompletionMap, second.fieldCompletionMap);
}


/** Group and Member Management **/

ExpansionGroup const* ExpansionMemberFieldMap::getGroup() const
{
	return group;
}

void ExpansionMemberFieldMap::setGroup(ExpansionGroup const* group)
{
	this->group = group;
}

void swapGroup(ExpansionMemberFieldMap& first, ExpansionMemberFieldMap& second)
{
	swap(first.group, second.group);
}

ExpansionMember const* ExpansionMemberFieldMap::getMember() const
{
	return member;
}

void ExpansionMemberFieldMap::setMember(ExpansionMember const* member)
{
	this->member = member;
}


void swapMember(ExpansionMemberFieldMap& first, ExpansionMemberFieldMap& second)
{
	swap(first.member, second.member);
}



/** Field Query Methods **/

void ExpansionMemberFieldMap::initField(string const& field)
{
	if (group->hasField(field))
	{
		FieldFormat format = group->getFormat(field);
		if (ExpansionFieldEntry::isGroupDefinedFormat(format))
			throw groupDefinedFieldError(field, format);
		fieldMap[field] = ExpansionMemberFieldEntry::makeFromFormat(format, _isStatic);
	}
	else throw group->fieldNotDefinedError(field, false);
}

bool ExpansionMemberFieldMap::hasField(string const& field) const
{
	return fieldMap.find(field) != fieldMap.end() ||
		(!_isStatic && (field == "id" || field == "name"));
}

FieldFormat ExpansionMemberFieldMap::getFormat(string const& field) const
{
	auto groupIt = fieldMap.find(field);
	if (groupIt != fieldMap.end())
	{
		return groupIt->second.getFormat();
	}
	throw group->fieldNotDefinedError(field, _isStatic);
}

bool ExpansionMemberFieldMap::areFieldsComplete() const
{
	if (!_areFieldsComplete && areFieldsUpdated)
	{
		bool foundIncomplete = false;
		for (auto& [field, completion] : fieldCompletionMap)
		{
			if (!completion.isFieldCompleted())
			{
				foundIncomplete = true;
				break;
			}
		}
		_areFieldsComplete = !foundIncomplete;
		areFieldsUpdated = false;
	}
	return _areFieldsComplete;
}


/** List Size Interface **/

size_t ExpansionMemberFieldMap::getListSize(string const& field) const
{
	return getFieldEntry(field)->getListSize();
}

void ExpansionMemberFieldMap::setListSize(string const& field, size_t size)
{
	getMutableFieldEntry(field)->setListSize(size);
}


/** Static Query Interface **/

bool ExpansionMemberFieldMap::isStatic(string const& field) const
{
	return getFieldEntry(field)->isStatic();
}

void ExpansionMemberFieldMap::isStatic(string const& field, bool _isStatic)
{
	getMutableFieldEntry(field)->isStatic(_isStatic);
}



/** Value Field Interface **/

ValueEntry const* ExpansionMemberFieldMap::getValue(string const& field, size_t index) const
{
	return getListedFieldEntry<FieldFormat::Value>(field, index);
}

void ExpansionMemberFieldMap::setValue(string const& field, ValueEntry const& value, size_t index)
{
	*getMutableListedFieldEntry<FieldFormat::Value>(field, index) = value;
	if (!_isStatic)
		markFieldUpdate(field, 0, index);
}

string ExpansionMemberFieldMap::getValueString(string const& field, size_t index) const
{
	return toString(*getValue(field, index));
}

vector<pair<string, size_t>> ExpansionMemberFieldMap::getAllValueIndexers() const
{
	vector<pair<string, size_t>> indexers;
	for (string const& field : group->getFields())
	{
		if (group->getFormat(field) == FieldFormat::Value)
		{
			if (_isStatic == group->isStatic(field))
			{
				if (group->isListedField(field))
				{
					for (size_t index = 0; index < group->getListSize(field); index++)
					{
						indexers.push_back(make_pair(field, index));
					}
				}
				else
				{
					indexers.push_back(make_pair(field, 0));
				}
			}
		}
	}
	return indexers;
}

void ExpansionMemberFieldMap::tryCastValueToRequestedType(string const& field, size_t index)
{
	auto const* value = getValue(field, index);
	auto const valueCategory = value->getCategory();
	auto const requestedValueCategory = group->getValueType(field);

	if (valueCategory != requestedValueCategory)
	{
		auto const castedValue = value->tryCastCopy(requestedValueCategory);
		if (castedValue.has_value())
		{

			setValue(field, castedValue.value(), index);
		}
	}
}

void ExpansionMemberFieldMap::tryCastAllValuesToRequestedType()
{
	for (auto const& valueIndexer : getAllValueIndexers())
	{
		tryCastValueToRequestedType(valueIndexer.first, valueIndexer.second);
	}
}

void ExpansionMemberFieldMap::validateValueHasRequestedType(string const& field, size_t index) const
{
	auto const* value = getValue(field, index);
	auto const valueCategory = value->getCategory();
	auto const requestedValueCategory = group->getValueType(field);

	if (valueCategory != requestedValueCategory)
	{
		throw invalid_argument("Invalid value type for " + field + "[" + to_string(index) + "] in " + getMember()->memberInfo() + ". " +
			"Got value '" + toString(*value) + "' of type '" + ValueEntry::idOfValueCategory(valueCategory) + "'. " +
			"Expected '" + ValueEntry::idOfValueCategory(requestedValueCategory) + "'.");
	}
}

void ExpansionMemberFieldMap::validateAllValuesHaveRequestedType() const
{
	for (auto const& valueIndexer : getAllValueIndexers())
	{
		validateValueHasRequestedType(valueIndexer.first, valueIndexer.second);
	}
}


/** Foreign Member Field Interface **/

string const* ExpansionMemberFieldMap::getForeignMemberId(string const& field, size_t index) const
{
	return getListedFieldEntry<FieldFormat::ForeignMember>(field, index);
}

void ExpansionMemberFieldMap::setForeignMemberId(string const& field,
	string const& foreignMemberId, size_t index)
{
	*getMutableListedFieldEntry<FieldFormat::ForeignMember>(field, index) = foreignMemberId;
	if (!_isStatic)
		markFieldUpdate(field, 0, index);
}


/** Nested Member Field Interface **/

NestedExpansionMember const* ExpansionMemberFieldMap::getNestedMember(string const& field, size_t index) const
{
	auto nestedMember = *getListedFieldEntry<FieldFormat::NestedMember>(field, index);
	return nestedMember.get();
}

NestedExpansionMember* ExpansionMemberFieldMap::getMutableNestedMember(string const& field, size_t index)
{
	auto nestedMember = *getMutableListedFieldEntry<FieldFormat::NestedMember>(field, index);
	return nestedMember.get();
}

void ExpansionMemberFieldMap::createNestedMember(string const& field,
	vector<vector<ValueEntry>> const& nestedArgs, size_t index)
{
	checkIndexWithinList(field, index);

	NestedExpansionGroup const* nestedGroup = group->getNestedGroupOfId(*group->getNestedGroupId(field));

	string idField = ExpansionDefinition::idOfReservedField(ExpansionDefinition::ReservedField::Id);
	string nameField = ExpansionDefinition::idOfReservedField(ExpansionDefinition::ReservedField::Name);

	// Maps the special arguments of the nested member (id and name) to their positions in the argument list.
	// They must appear in sequence at the beginning of the argument list with optional ommission.
	size_t nextArgIndex = 0;
	size_t specialArgIndex = 0;
	auto const specialArgs = vector<string>{idField, nameField};
	auto specialArgPos = unordered_map<string, size_t>();

	while (nextArgIndex < nestedGroup->getArgumentCount() && specialArgIndex < specialArgs.size())
	{
		if (nestedGroup->getArgument(nextArgIndex) == specialArgs[specialArgIndex])
		{
			specialArgPos.emplace(specialArgs[specialArgIndex], nextArgIndex);
			nextArgIndex += 1;
		}
		specialArgIndex += 1;
	}
	
	// Id defaults to a representation with the owner id and containing entry (field and index).
	string ownerIdPrefix = _isStatic ? group->getId() : member->getId();
	string nestedMemberId = specialArgPos.count(idField) > 0 ?
		nestedArgs.at(specialArgPos.at(idField)).front().getValue<ValueCategory::String>() :
		ownerIdPrefix + "." + field + "." + to_string(index);

	// Name defaults to the id.
	string nestedMemberName = specialArgPos.count(nameField) > 0 ?
		nestedArgs.at(specialArgPos.at(nameField)).front().getValue<ValueCategory::String>() :
		nestedMemberId;

	// TODO: Remove static nested members.
	// Initialises the owner id to nothing if the nested member is static.
	// In theory, there should be no static nested member.
	// This behaviour can be summarised in a foreign group.
	string ownerId = _isStatic ? "" : member->getId();

	// Creates the nested member from the default or custom id and name.
	auto nestedMember = make_shared<NestedExpansionMember>(nestedMemberId, nestedMemberName, nestedGroup, ownerId, index);

	// Assigns the values of the remaining arguments.
	for (size_t i = nextArgIndex; i < nestedArgs.size(); i++)
	{
		vector<ValueEntry> const& entries = nestedArgs.at(i);
		string const& arg = nestedGroup->getArgument(i);

		// TODO: Handle validation and parsing using an specialised class.
		//   This behaviour is shared by the definition.

		switch (nestedGroup->getFormat(arg))
		{
		case FieldFormat::Value:
			// Validates the value of the nested member argument is of the type specified by the nested group.
			if (entries.front().getCategory() != nestedGroup->getValueType(arg))
			{
				throw invalid_argument("In " + this->ownerInfo() + ":\n" +
					"In term " + field + "[" + to_string(index) + "]:\n" +
					"Value type '" + ValueEntry::idOfValueCategory(entries.front().getCategory()) + "' " +
					"received when '" + ValueEntry::idOfValueCategory(nestedGroup->getValueType(arg)) + "' " +
					"was expected for argument '" + arg + "' of nested member.");
			}
			for (size_t j = 0; j < entries.size(); j++)
			{
				nestedMember->getMutableFieldMap()->setValue(arg, entries.at(j), j);
			}
			break;
		case FieldFormat::ForeignMember:
			for (size_t j = 0; j < entries.size(); j++)
			{
				nestedMember->getMutableFieldMap()->setForeignMemberId(arg, entries.at(j).getValue<ValueCategory::String>(), j);
			}
			break;
		default:
			throw invalid_argument("Unsupported argument type when creating nested member.");
		}
	}

	getMutableFieldEntry<FieldFormat::NestedMember>(field)->at(index) = nestedMember;

	if (!_isStatic)
		markFieldUpdate(field, 0, index);
}


/** Nested Template Field Interface **/

const shared_ptr<TemplateTree> ExpansionMemberFieldMap::getNestedTemplate(string const& field) const
{
	return *getFieldEntry<FieldFormat::NestedTemplate>(field);
}

void ExpansionMemberFieldMap::parseNestedTemplate(string const& field, string const& nestedTemplate)
{
	*getMutableFieldEntry<FieldFormat::NestedTemplate>(field) =
		shared_ptr<TemplateTree>(TemplateTree::parseSimpleTemplateContents(nestedTemplate));
	if (!_isStatic)
		markFieldUpdate(field, 0, 0);
}

string ExpansionMemberFieldMap::evaluateNestedTemplateString(string const& field, vector<string> const& replacements) const
{
	stringstream ss;
	try
	{
		size_t nextReplInd = 0;
		auto templateContents = getNestedTemplate(field)->tree->getRoot();
		for (size_t i = 0; i < templateContents->getChildCount(); i++)
		{
			auto line = templateContents->getChild(i);
			if (line->getItem() == "expandable_line")
			{
				try
				{
					for (size_t k = 0; k < line->getChildCount(); k++)
					{
						auto textOrToken = line->getChild(k);
						if (textOrToken->isLeaf())
						{
							ss << textOrToken->getItem();
						}
						else if (textOrToken->getItem() == "token")
						{
							try
							{
								auto token = textOrToken->getChild(0);
								if (token->getItem() == "property_token")
								{
									if (nextReplInd < replacements.size())
									{
										ss << replacements.at(nextReplInd);
										nextReplInd++;
									}
									else throw invalid_argument("More property tokens found than replacements.");
								}
								else throw invalid_argument("Unrecognised node '" + line->getItem() + "'.");
							}
							catch (...)
							{
								throw_with_nested(invalid_argument("Error in property_token " + to_string(i) + "."));
							}
						}
						else throw invalid_argument("Unrecognised node '" + line->getItem() + "'.");
					}
				}
				catch (...)
				{
					throw_with_nested(invalid_argument("Error in expandable_line " + to_string(i) + "."));
				}
				
			}
			else if (line->getItem() == "new_line")
			{
				ss << line->getItem();
			}
			else throw invalid_argument("Unrecognised node '" + line->getItem() + "'.");
		}
		if (nextReplInd < replacements.size())
		{
			throw invalid_argument("More replacements supplied than property tokens.");
		}
	}
	catch (...)
	{
		throw_with_nested(invalid_argument("Error processing nested template in " + ownerInfo() + "."));
	}
	return ss.str();
}


/** Object Description Methods **/

string ExpansionMemberFieldMap::ownerInfo() const
{
	stringstream ss;
	if (_isStatic)
	{
		ss << group->groupInfo();
	}
	else
	{
		ss << member->memberInfo();
	}
	return ss.str();
}


/** Field Completion **/

void ExpansionMemberFieldMap::markFieldUpdate(string const& field, unsigned char divisionNumber, size_t index)
{
	areFieldsUpdated = true;
	fieldCompletionMap.at(field).setField(divisionNumber, index);
}


/** Constant Field Access Methods **/

ExpansionMemberFieldEntry const* ExpansionMemberFieldMap::getFieldEntry(string const& field) const
{
	if (hasField(field))
		return &fieldMap.at(field);
	else if (!_isStatic && group->getStaticFieldMap()->hasField(field))
		return group->getStaticFieldMap()->getFieldEntry(field);
	throw fieldNotFoundError(field);
}

ExpansionMemberFieldEntry const* ExpansionMemberFieldMap::getFieldEntry(
	string const& field, FieldFormat format) const
{
	auto actualFormat = group->getFormat(field);
	if (format != actualFormat)
		throw group->badFormatError(field, format, actualFormat);
	return getFieldEntry(field);
}


/** Mutable Field Access Methods **/

ExpansionMemberFieldEntry* ExpansionMemberFieldMap::getMutableFieldEntry(string const& field)
{
	return MUTABLE_FROM_CONST_MEMBER_FUNC(this, getFieldEntry, field);
}

ExpansionMemberFieldEntry* ExpansionMemberFieldMap::getMutableFieldEntry(
	string const& field, FieldFormat format)
{
	return MUTABLE_FROM_CONST_MEMBER_FUNC(this, getFieldEntry, field, format);
}


/** Format Query Methods **/

void ExpansionMemberFieldMap::checkFieldEntryHasFormatAndIndex(
	string const& field, FieldFormat format, size_t index) const
{
	group->checkHasFieldWithFormat(field, format);
	
	// Checks list size defined in the group field map supports the index.
	if (index >= group->getListSize(field))
	{
		throw badIndexError(field, index);
	}
}

void ExpansionMemberFieldMap::checkIndexWithinList(string const& field, size_t index) const
{
	if (!ExpansionFieldEntry::isListedFormat(group->getFormat(field)))
		throw group->noListSizeError(field);
	if (index >= group->getListSize(field))
		throw badIndexError(field, index);
}


/** Error Handling **/

out_of_range ExpansionMemberFieldMap::fieldNotFoundError(string const& field) const
{
	return out_of_range("The field '" + field + "' has not been set for the " +
		ownerInfo() + ".");
}

out_of_range ExpansionMemberFieldMap::badFormatError(string const& field, FieldFormat format) const
{
	return out_of_range("The field '" + field + "' is not defined with the format '" +
		ExpansionFieldEntry::nameOfFormat(format) + "' for the " +
		group->groupInfo() + ".");
}

out_of_range ExpansionMemberFieldMap::badIndexError(string const& field, size_t index) const
{
	return out_of_range("The field '" + field + "' does not have an index of " +
		to_string(index) + " for the " + group->groupInfo() + ".");
}

out_of_range ExpansionMemberFieldMap::groupDefinedFieldError(string const& field, FieldFormat format) const
{
	return out_of_range("The field '" + field + "' is a group-defined field of " +
		"format '" + ExpansionFieldEntry::nameOfFormat(format) + "' for the " +
		group->groupInfo() + ".");
}
