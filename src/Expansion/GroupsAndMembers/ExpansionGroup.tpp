//
//  ExpansionGroup.tpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef EXPANSION_GROUP_TPP
#define EXPANSION_GROUP_TPP

#include "ExpansionGroup.hpp"

#include "ExpansionGroupFieldEntry.tpp"
#include "TypeUtils.hpp"
#include "UserFunction.tpp"

/** Constant Field Accessors **/

template<ExpansionFieldEntryFormat F>
	ExpansionGroupFieldEntryTypeOfFormatT<F> const* ExpansionGroup::
		getFieldEntryWithFormat(std::string const& field) const
{
	auto entry = getFieldEntryWithFormat(field, F);
	return entry->getEntry<ExpansionGroupFieldEntryTypeOfFormatT<F>>();
}

/** Mutable Field Accessors **/

template<ExpansionFieldEntryFormat F>
	ExpansionGroupFieldEntryTypeOfFormatT<F>* ExpansionGroup::
		getMutableFieldEntryWithFormat(std::string const& field)
{
	return MUTABLE_FROM_CONST_MEMBER_FUNC(this,
		getFieldEntryWithFormat<F>, field);
}

#endif /* EXPANSION_GROUP_TPP */
