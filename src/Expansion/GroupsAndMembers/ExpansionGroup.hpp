//
//  ExpansionGroup.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 8/7/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#ifndef EXPANSION_GROUP_HPP
#define EXPANSION_GROUP_HPP

#include "ExpansionGroupFieldEntryDecl.hpp"
#include "UserFunction.hpp"

class ExpansionMember;
class ExpansionMemberFieldMap;
class NestedExpansionGroup;
class ValueEntry;

#include <any>
#include <map>
#include <set>
#include <unordered_map>

class ExpansionGroup
{
public:
	using FunctionPtrType = EpValue(*)(EpArgumentList);
	using FunctionType = std::function<EpValue(EpArgumentList)>;
	
	std::vector<std::string> memberIds;
	std::map<std::string, size_t> indexOfMemberId;
	std::vector<std::string> categoryIds;
	std::map<std::string, size_t> indexOfCategoryId;

	/** Constructors and Destructors **/

	ExpansionGroup();
	ExpansionGroup(std::string const& id, std::string const& name);
	ExpansionGroup(std::string const& id);
	virtual ~ExpansionGroup();

	/** Copy and Move Semantics **/

	friend void swap(ExpansionGroup&, ExpansionGroup&);
	ExpansionGroup(ExpansionGroup const&);
	ExpansionGroup(ExpansionGroup&&);
	ExpansionGroup& operator=(ExpansionGroup);
	
	/** Basic Accessors **/

	std::string const& getId() const;
	std::string const& getName() const;
	NestedExpansionGroup const* getNestedGroupOfId(std::string const& id) const;
	void setNestedGroupOfId(std::string const& id, NestedExpansionGroup const* nestedGroup);
	ExpansionMemberFieldMap* getMutableStaticFieldMap();
	ExpansionMemberFieldMap const* getStaticFieldMap() const;
	
	/** General Field and Format Interface **/

	void initField(std::string const& field, ExpansionFieldEntryFormat format, bool isStatic);
	std::vector<std::string> getFields() const;
	bool hasField(std::string const& field) const;
	ExpansionFieldEntryFormat getFormat(std::string const& field) const;

	/** List Size Interface **/

	bool isListedField(std::string const& field) const;
	size_t getListSize(std::string const& field) const;
	void setListSize(std::string const& field, size_t size);

	/** Static Query Interface **/

	// Returns if the field is instantiated in the group.
	bool isGroupContainedEntry(std::string const& field) const;

	// Returns if the field is instantiated in the member.
	bool isMemberContainedEntry(std::string const& field) const;

	bool isStatic(std::string const& field) const;
	void isStatic(std::string const& field, bool _isStatic);
	
	/** Uniqueness Interface **/

	void setUnique(std::string const& field);
	bool isUnique(std::string const& field) const;
	std::vector<std::string> getUniqueFields() const;

	/** Value Field Interface **/

	ValueCategory getValueType(std::string const& field) const;
	void setValueType(std::string const& field, std::string const& valueType);
	
	/** Foreign Member Field Interface **/

	std::string const* getForeignGroupId(std::string const& field) const;
	void setForeignGroupId(std::string const& field,
		std::string const& foreignGroupId);
	
	/** Nested Member Field Interface **/
	
	std::string const* getNestedGroupId(std::string const& field) const;
	void setNestedGroupId(std::string const& field, std::string const& nestedGroupId);
	
	/** Function Field Interface **/

	FunctionType const* getFunction(std::string const& field) const;
	std::string const* getFunctionName(std::string const& field) const;
	void setFunctionName(std::string const& field, std::string const& name);
	ValueCategory getFunctionReturnType(std::string const& field) const;
	void setFunctionReturnType(std::string const& field, std::string const& returnType);
	std::vector<std::shared_ptr<TemplateTree>> const* getFunctionArgs(std::string const& field) const;
	void setFunctionArgs(std::string const& field,
		std::vector<std::string>::const_iterator argsBegin,
		std::vector<std::string>::const_iterator argsEnd);
	FunctionType const* getFunctionOfName(std::string const& name) const;
	void setFunctionOfName(std::string const& name, FunctionType const& function);
	ValueEntry evaluateFunction(std::string const& field,
		std::vector<ValueEntry> const& resolvedArgs) const;
	std::string evaluateFunctionString(std::string const& field,
		std::vector<ValueEntry> const& resolvedArgs) const;

	/** Alias Field Interface **/

	std::shared_ptr<TemplateTree> const* getAlias(std::string const& field) const;
	void setAlias(std::string const& field, std::string const& subproperty);

	/** Object Description Methods **/

	std::string groupInfo() const;

	/** Constant Field Accessors **/

	ExpansionGroupFieldEntry const*
	getFieldEntry(std::string const& field) const;
	ExpansionGroupFieldEntry const*
	getFieldEntryWithFormat(std::string const& field,
		ExpansionFieldEntryFormat format) const;
	template<ExpansionFieldEntryFormat F>
	ExpansionGroupFieldEntryTypeOfFormatT<F> const*
	getFieldEntryWithFormat(
		std::string const& field) const;

	/** Mutable Field Accessors **/

	ExpansionGroupFieldEntry*
	getMutableFieldEntry(std::string const& field);
	ExpansionGroupFieldEntry*
	getMutableFieldEntryWithFormat(std::string const& field,
		ExpansionFieldEntryFormat format);
	template<ExpansionFieldEntryFormat F>
	ExpansionGroupFieldEntryTypeOfFormatT<F>*
	getMutableFieldEntryWithFormat(std::string const& field);
	
	/** Error Handling **/
	
	std::out_of_range fieldNotDefinedError(std::string const& field, bool inStatic) const;
	std::out_of_range badFormatError(std::string const& field, ExpansionFieldEntryFormat requested, ExpansionFieldEntryFormat expected) const;
	std::out_of_range noListSizeError(std::string const& field) const;

	/** Field Checking Helpers **/

	void checkHasField(std::string const& field) const;
	void checkHasFieldWithFormat(std::string const& field,
		ExpansionFieldEntryFormat format) const;
	
	/** Misc Helpers **/

	static std::vector<std::string> findGroupsContainingField(
		std::vector<ExpansionGroup const*> const& groups, std::string const& field);

protected:
	std::unordered_map<std::string, ExpansionGroupFieldEntry> fieldMap;
	
private:
	ExpansionMemberFieldMap* staticFieldMap = nullptr;
	std::string id;
	std::string name;
	std::unordered_map<std::string, NestedExpansionGroup const*> nestedGroupOfId;
	std::unordered_map<std::string, FunctionType> functionOfName;
	std::set<std::string> uniqueFieldSet;
	
	/** Group Referencing Helpers **/

	std::string const* getGroupId(std::string const& field, ExpansionFieldEntryFormat format) const;
	std::string* getGroupId(std::string const& field, ExpansionFieldEntryFormat format);
	void setGroupId(std::string const& field, ExpansionFieldEntryFormat format, std::string const& id);
};

#endif /* EXPANSION_GROUP_HPP */
