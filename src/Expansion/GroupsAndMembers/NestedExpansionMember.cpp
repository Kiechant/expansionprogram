//
//  NestedExpansionMember.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 8/7/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#include "NestedExpansionMember.hpp"

#include <iostream>

using namespace std;

NestedExpansionMember::NestedExpansionMember(string const& id, string const& name,
	ExpansionGroup const* group, string const& ownerId, size_t position)
		: ExpansionMember(id, name, group)
{
	this->ownerId = ownerId;
}

NestedExpansionMember::NestedExpansionMember(string const& id,
	ExpansionGroup const* group, string const& ownerId, size_t position)
		: ExpansionMember(id, group)
{
	this->ownerId = ownerId;
}

string const& NestedExpansionMember::getOwnerId() const
{
	return ownerId;
}
