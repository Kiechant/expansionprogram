//
//  NestedExpansionMember.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 8/7/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#ifndef NESTED_EXPANSION_MEMBER_HPP
#define NESTED_EXPANSION_MEMBER_HPP

#include "ExpansionMember.hpp"

class NestedExpansionMember : public ExpansionMember
{
public:
	NestedExpansionMember(std::string const& id, std::string const& name,
		ExpansionGroup const* group, std::string const& ownerId, size_t position);
	NestedExpansionMember(std::string const& id, ExpansionGroup const* group,
		std::string const& ownerId, size_t position);

	std::string const& getOwnerId() const;

private:
	std::string ownerId;
};

#endif /* NESTED_EXPANSION_MEMBER_HPP */
