//
//  NestedExpansionGroup.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 8/7/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#ifndef NESTED_EXPANSION_GROUP_HPP
#define NESTED_EXPANSION_GROUP_HPP

#include "ExpansionGroup.hpp"

class NestedExpansionGroup : public ExpansionGroup
{
public:
	NestedExpansionGroup() = default;
	NestedExpansionGroup(std::string const& id, std::string const& name,
		std::string const& ownerId);
	NestedExpansionGroup(std::string const& id, std::string const& ownerId);

	std::string const& getOwnerId() const;

	void addArgument(std::string const& arg);
	std::string getArgument(size_t argIndex) const;
	size_t getArgumentCount() const;

private:
	std::string ownerId;
	std::vector<std::string> arguments;
};

#endif /* NESTED_EXPANSION_GROUP_HPP */
