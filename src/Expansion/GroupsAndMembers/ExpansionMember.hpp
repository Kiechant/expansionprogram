//
//  ExpansionMember.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 5/3/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#ifndef EXPANSION_MEMBER_HPP
#define EXPANSION_MEMBER_HPP

#include "ExpansionMemberFieldEntryDecl.hpp"

class ExpansionMemberFieldMap;
class ExpansionGroup;
class NestedExpansionMember;

#include <exception>
#include <map>

class ExpansionMember
{
public:
	/** Constructors and Destructors **/
	
	ExpansionMember(std::string const& id, std::string const& name,
		ExpansionGroup const* group);
	ExpansionMember(std::string const& id, ExpansionGroup const* group);
	~ExpansionMember();


	/** Copy and Move Semantics **/

	friend void swap(ExpansionMember&, ExpansionMember&);
	ExpansionMember(ExpansionMember const&);
	ExpansionMember(ExpansionMember&&);
	ExpansionMember& operator=(ExpansionMember);
	

	/** Basic Acessors **/

	std::string const& getId() const;
	std::string const& getName() const;
	ExpansionGroup const* getGroup() const;
	ExpansionMemberFieldMap* getMutableFieldMap();
	ExpansionMemberFieldMap const* getFieldMap() const;


	/** Object Description Methods **/

	std::string memberInfo() const;

private:
	ExpansionMemberFieldMap* fieldMap = nullptr;
	std::string id;
	std::string name;
	ExpansionGroup const* group = nullptr;
};

#endif /* EXPANSION_MEMBER_HPP */
