//
//  ExpansionMemberFieldMap.tpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef EXPANSION_MEMBER_FIELD_MAP_TPP
#define EXPANSION_MEMBER_FIELD_MAP_TPP

#include "ExpansionMemberFieldMap.hpp"

#include "ExpansionFieldEntry.hpp"
#include "ExpansionMemberFieldEntry.tpp"
#include "TemplateTree.hpp"
#include "TypeUtils.hpp"

/** Constant Field Access Methods **/

template<ExpansionFieldEntryFormat F>
ExpansionMemberFieldEntryTypeOfFormatT<F> const* ExpansionMemberFieldMap::
getFieldEntry(std::string const& field) const
{
	auto entry = getFieldEntry(field, F);
	return entry->getEntry<F>();
}

template<ExpansionFieldEntryFormat F>
typename ExpansionMemberFieldEntryTypeOfFormatT<F>::value_type const* ExpansionMemberFieldMap::
getListedFieldEntry(std::string const& field, size_t index) const
{
	checkIndexWithinList(field, index);
	return &getFieldEntry<F>(field)->at(index);
}


/** Mutable Field Access Methods **/

template<ExpansionFieldEntryFormat F>
ExpansionMemberFieldEntryTypeOfFormatT<F>* ExpansionMemberFieldMap::
getMutableFieldEntry(std::string const& field)
{
	return MUTABLE_FROM_CONST_MEMBER_FUNC(this, getFieldEntry<F>, field);
}

template<ExpansionFieldEntryFormat F>
typename ExpansionMemberFieldEntryTypeOfFormatT<F>::value_type* ExpansionMemberFieldMap::
getMutableListedFieldEntry(std::string const& field, size_t index)
{
	return MUTABLE_FROM_CONST_MEMBER_FUNC(this, getListedFieldEntry<F>, field, index);
}

#endif /* EXPANSION_MEMBER_FIELD_MAP_TPP */
