//
//  ExpansionContextItem.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 27/6/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef EXPANSION_CONTEXT_ITEM_HPP
#define EXPANSION_CONTEXT_ITEM_HPP

#include "ComparisonOperatorMethodsDecl.hpp"
#include "EntryReference.hpp"
#include "GraphNodeDecl.hpp"
#include "TreeNodeDecl.hpp"

class ExpansionGroup;
class ExpansionMember;
class ValueEntry;

#include <optional>
#include <string>

class ExpansionContextItem;
using ExpansionContextNode = TreeNode<ExpansionContextItem>;
using ExpansionViewNode = TreeNode<ExpansionContextNode*>;

class ExpansionContextItem
{
public:
	enum class ContextType
	{
		GroupSpecifier,	// The root node representing the list of groups in the group specifier.
						// All fields are unused and should be default.
		GroupReference, // A direct reference to a group or category in the group specifier.
						// Cycles through each member of that group or category.
		EntryReference, // A subproperty pointing to a list of expandable entries in a member and field.
						// Cycles through each entry.
	};

	ExpansionContextItem();

	static ExpansionContextItem makeGroupSpecifier();
	static ExpansionContextItem makeGroupReference(std::string const& alias, ExpansionGroup const* group);
	static ExpansionContextItem makeEntryReference(bool doExpand, EntryReference const& entryRef);

	static std::string nameOfContextType(ContextType contextType);

	ContextType getContextType() const;
	std::string const& getAlias() const;
	bool doExpand() const;
	EntryReference const& getEntryRef() const;
	bool hasEntryRef() const;

	size_t getIndex() const;
	void setIndex(size_t index);

	friend int compare(ExpansionContextItem const& first, ExpansionContextItem const& second);
	COMPARISON_OPERATOR_METHODS_DECL(ExpansionContextItem)

	friend std::string toString(ExpansionContextItem const&);

	friend size_t toHash(ExpansionContextItem const&);

	// Processing value. Indicates whether the branching contexts require an update
	// after changing the current node's value.
	bool doUpdateBranches = false;

private:
	ExpansionContextItem(ContextType contextType, bool doExpand, std::string const& alias, EntryReference const& entryRef);

	ContextType contextType = ContextType::GroupSpecifier;

	// Whether a property is expanded.
	bool _doExpand = false;

	// The alias for the group, member or value in the group specifier.
	// Defaults to the group or property name.
	std::string alias = "";

	// The group, field and index. Field and index are blank for group references.
	std::optional<EntryReference> entryRef;
};

#endif /* EXPANSION_CONTEXT_ITEM_HPP */
