//
//  Cache.tpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 25/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef CACHE_TPP
#define CACHE_TPP

#include "Cache.hpp"

#include <utility>

CACHE_TMPL
CACHE_T::Cache(std::function<Item()> const& generate) 
	: generateItem(generate) {}

CACHE_TMPL
void swap(CACHE_T& first, CACHE_T& second) 
{
	using std::swap;
	swap(first.generateItem, second.generateItem);
	swap(first.cachedItem, second.cachedItem);
}

CACHE_TMPL
CACHE_T::Cache(CACHE_T const& other)
	: generateItem(other.generateItem),
	  cachedItem(other.cachedItem) {}

CACHE_TMPL
CACHE_T::Cache(CACHE_T const& other, std::function<Item()> const& generate) 
	: Cache(other)
{
	this->generateItem = generate;
}

CACHE_TMPL
CACHE_T& CACHE_T::operator=(CACHE_T other) 
{
	swap(*this, other);
	return *this;
}

CACHE_TMPL
Item const& CACHE_T::get() const
{
	if (!cachedItem.has_value())
	{
		cachedItem.emplace(generateItem());
	}
	return cachedItem.value();
}

#endif /* CACHE_TPP */
