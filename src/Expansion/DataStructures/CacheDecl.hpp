//
//  CacheDecl.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 25/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef CACHE_DECL_HPP
#define CACHE_DECL_HPP

template<typename Item>
class Cache;

#define CACHE_TMPL template<typename Item>
#define CACHE_T Cache<Item>

CACHE_TMPL
void swap(CACHE_T&, CACHE_T&);

#endif /* CACHE_DECL_HPP */
