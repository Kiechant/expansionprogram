//
//  ContextReference.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 11/1/21.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "ContextReference.hpp"

#include "ExpansionContextItem.hpp"
#include "StdComparison.hpp"
#include "StdHashing.hpp"
#include "TreeNode.tpp"

using namespace std;

ContextReference::ContextReference(ExpansionContextNode* contextNode, ExpansionViewNode* viewNode, bool isFinalised) 
	: contextNode(contextNode), viewNode(viewNode), _isFinalised(isFinalised) {}

ExpansionContextNode* ContextReference::getContextNode() const
{
	return contextNode;
}

ExpansionViewNode* ContextReference::getViewNode() const
{
	return viewNode;
}

bool ContextReference::isFinalised() const
{
	return _isFinalised;
}

int compare(ContextReference const& first, ContextReference const& second) 
{
	RETURN_IF_UNEQUAL_COMPARE(first.contextNode, second.contextNode);
	RETURN_IF_UNEQUAL_COMPARE(first.viewNode, second.viewNode);
	RETURN_IF_UNEQUAL_COMPARE(first._isFinalised, second._isFinalised);
	return 0;
}

COMPARISON_OPERATOR_METHODS_IMPL(ContextReference)

int toHash(ContextReference const& obj)
{
	size_t res = initHash();
	res = combineHash(res, toHash(obj.contextNode));
	res = combineHash(res, toHash(obj.viewNode));
	res = combineHash(res, toHash(obj._isFinalised));
	return res;
}
