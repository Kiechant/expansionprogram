//
//  ExpansionContextDecl.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef EXPANSION_CONTEXT_DECL_HPP
#define EXPANSION_CONTEXT_DECL_HPP

#include "TreeNodeDecl.hpp"

class ExpansionContextItem;

using ExpansionContextNode = TreeNode<ExpansionContextItem>;
using ExpansionViewNode = TreeNode<ExpansionContextNode*>;

class ExpansionContext;

#endif /* EXPANSION_CONTEXT_DECL_HPP */
