//
//  TemplateTree.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 28/7/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "TemplateTree.hpp"

#include "EnumStringMap.tpp"
#include "ExpansionDefinition.hpp"
#include "ExpansionGroup.tpp"
#include "StdStringConversion.hpp"
#include "SyntaxParser.hpp"
#include "SyntaxRule.hpp"
#include "Tree.tpp"
#include "TypeUtils.hpp"

#include <fstream>

using namespace std;

/** Constructors and Destructors **/

TemplateTree::TemplateTree() {}

TemplateTree::TemplateTree(Tree<string>* tree)
	: tree(tree) {}

TemplateTree::TemplateTree(TemplateTree const& other)
{
	this->tree = new Tree<string>(*other.tree);
}

TemplateTree::~TemplateTree()
{
	delete tree;
}


/** Queries **/

bool TemplateTree::isOperable(TemplateNode const* templateContents) 
{
	bool isOperable = true;
	auto contentsLines = TemplateTree::getContentsLines(templateContents);
	bool hasNoDirectives = TemplateTree::filterDirectiveLines(contentsLines).empty();
	if (hasNoDirectives)
	{
		auto expandableLines = TemplateTree::filterExpandableLines(contentsLines);
		bool hasTokens = any_of(expandableLines.begin(), expandableLines.end(), [](TemplateNode const* expandableLine)
		{
			return !TemplateTree::getTokensOfType(expandableLine, nullopt).empty();
		});
		if (!hasTokens)
		{
			isOperable = false;
		}
	}
	return isOperable;
}


/** Template Syntax **/

unordered_map<string, SyntaxRule const*> const* TemplateTree::getSyntax()
{
	static const vector<SyntaxRule> syntaxRules =
	{
		// Contextual Rules of Template Name (Named)
		SyntaxRule("template_name", "(<group_spec>@)?<file_name>", false),
		SyntaxRule("file_name", "<expandable_name><extension>", false),
		SyntaxRule("expandable_name", "(<strict_text>|<token>)+", false),
		SyntaxRule("extension", ".<strict_text>", false),

		// Contextual Rules of Template Contents (Named)
		SyntaxRule("file_contents", "<contents_line>?(<new_line><contents_line>)*<new_line>?", false),
		SyntaxRule("contents_line", "<directive_line>|<expandable_line>", false),
		SyntaxRule("directive_line", "<internal_begin>|<internal_end>", false),
		SyntaxRule("expandable_line", "(<natural_text>|<token>|\\<)+", false),
		SyntaxRule("internal_begin", "#\\(expand\\) <group_spec>", false),
		SyntaxRule("internal_end", "#\\(endexpand\\)", false),

		// Contextual Rules of Simple Template Contents (Named)
		SyntaxRule("simple_template_contents", "<expandable_line>?(<new_line><expandable_line>)*<new_line>?", false),

		// Shared Contextual Rules (Named)
		SyntaxRule("group_spec", "<group_expansion>(,<group_expansion>)*", false),
		SyntaxRule("group_expansion", "<group><nested_expansion_list>?", false),
		SyntaxRule("nested_expansion_list", "\\(<nested_expansion>(,<nested_expansion>)*\\)", false),
		SyntaxRule("nested_expansion", "<property><nested_expansion_list>?", false),
		SyntaxRule("group", "<name>", false),
		SyntaxRule("token", "<property_token>|<substitute_token>", false),
		SyntaxRule("property_token", "\\<<internal_token_symbol>?(<group_ref>@)?"
			"<property>(!<token_processor_list>)?\\>", false),
		SyntaxRule("internal_token_symbol", "^", false),
		SyntaxRule("group_ref", "(<group>(:<copy_ref>)?)|<index_ref>", false),
		SyntaxRule("copy_ref", "<positive_num>", false),
		SyntaxRule("index_ref", "<positive_num>", false),
		SyntaxRule("property", "<term>(.<term>)*", false),
		SyntaxRule("term", "<field><indexer>?", false),
		SyntaxRule("field", "<name>", false),
		SyntaxRule("indexer", "\\[<index>\\]", false),
		SyntaxRule("index", "<positive_num>", false),
		SyntaxRule("substitute_token", "\\<\\<(<substitute_dict>@)?<substitute_key>\\>\\>", false),
		SyntaxRule("substitute_dict", "<name>", false),
		SyntaxRule("substitute_key", "<name>", false),
		SyntaxRule("token_processor_list", "<token_processor>(,<token_processor>)*", false),
		SyntaxRule("token_processor", "<name>", false),

		// Value Rules (Anonymous)
		SyntaxRule("natural_text", "(<readable_non_token>|<space>)+", true),
		SyntaxRule("strict_text", "<alpha_num>+", true),
		SyntaxRule("new_line", "<new_line_char>+", true),
		SyntaxRule("name", "<alpha><alpha_num>*", true),
		SyntaxRule("positive_num", "0|(<digit_19><digit_09>*)", true),
		SyntaxRule("alpha", "[A-Za-z_]", true),
		SyntaxRule("alpha_num", "[A-Za-z0-9_]", true),
		SyntaxRule("digit_09", "[0-9]", true),
		SyntaxRule("digit_19", "[1-9]", true),
		SyntaxRule("readable_non_token", "[\41-\73\75-\176]", true),
		SyntaxRule("space", "[ \t]", true),
		SyntaxRule("new_line_char", "[\n\r]", true)
	};

	static const unordered_map<string, SyntaxRule const*> syntax =
		MemoryUtils::mapItems<string, SyntaxRule>(syntaxRules,
		[](SyntaxRule const& rule) { return rule.getName(); });

	return &syntax;
}

string TemplateTree::getTemplateNameStartRule()
{
	return "template_name";
}

string TemplateTree::getTemplateContentsStartRule()
{
	return "file_contents";
}

string TemplateTree::getSimpleTemplateContentsStartRule() 
{
	return "simple_template_contents";
}

EnumStringMap<TemplateTreeTokenType> const& TemplateTree::getTokenTypeMap()
{
	static auto tokenTypeMap = EnumStringMap<TemplateTreeTokenType>
	({
		"property_token",
		"substitute_token"
	});
	return tokenTypeMap;
}


/** Parsing Methods **/

TemplateTree* TemplateTree::parseTemplateTree(string const& query, string const& startRule) 
{
	auto underlyingTree = new Tree<string>(SyntaxParser::parse(query, *getSyntax(), startRule));
	return new TemplateTree(underlyingTree);
}

TemplateTree* TemplateTree::parseTemplateName(string const& templateName)
{
	return parseTemplateTree(templateName, getTemplateNameStartRule());
}

TemplateTree* TemplateTree::parseTemplateContentsAtPath(filesystem::path const& filePath)
{
	auto ifs = ifstream(filePath);
	stringstream buffer;
	buffer << ifs.rdbuf();
	return parseTemplateContents(buffer.str());
}

TemplateTree* TemplateTree::parseTemplateContents(filesystem::path const& templateContents)
{
	return parseTemplateTree(templateContents, getTemplateContentsStartRule());
}

TemplateTree* TemplateTree::parseSimpleTemplateContents(string const& templateContents) 
{
	return parseTemplateTree(templateContents, getSimpleTemplateContentsStartRule());
}

TemplateTree* TemplateTree::parsePropertyToken(string const& propertyTokenStr) 
{
	return parseTemplateTree(propertyTokenStr, "property_token");
}

TemplateTree* TemplateTree::parseProperty(string const& propertyStr)
{
	return parseTemplateTree(propertyStr, "property");
}


/** Navigation **/

vector<TemplateNode const*> TemplateTree::getRoots(vector<shared_ptr<TemplateTree>> const& trees) 
{
	auto roots = vector<TemplateNode const*>(trees.size());
	transform(trees.begin(), trees.end(), roots.begin(),
		[](shared_ptr<TemplateTree> tree){ return tree->tree->getRoot(); });
	return roots;
}

TemplateNode* TemplateTree::getExpandableName(TemplateNode* templateName)
{
	return TypeUtils::asMutable(getExpandableName(static_cast<TemplateNode const*>(templateName)));
}

TemplateNode const* TemplateTree::getExpandableName(TemplateNode const* templateName)
{
	checkNodeOfType(templateName, "template_name");

	return templateName->getChildWithKey("file_name", 0)->getChildWithKey("expandable_name", 0);
}

vector<TemplateNode*> TemplateTree::getContentsLines(TemplateNode* fileContents)
{
	return TypeUtils::asMutable(getContentsLines(static_cast<TemplateNode const*>(fileContents)));
}

vector<TemplateNode const*> TemplateTree::getContentsLines(TemplateNode const* fileContents)
{
	checkNodeOfType(fileContents, "file_contents");

	size_t size = fileContents->getChildCountWithKey("contents_line");
	auto contentsLines = vector<TemplateNode const*>(size);
	for (size_t i = 0; i < size; i++)
		contentsLines.at(i) = fileContents->getChildWithKey("contents_line", i);
	return contentsLines;
}

vector<TemplateNode*> TemplateTree::filterChildNodes(vector<TemplateNode*> const& nodes, string const& name)
{
	return TypeUtils::asMutable(filterChildNodes(TypeUtils::asConst(nodes), name));
}

vector<TemplateNode const*> TemplateTree::filterChildNodes(vector<TemplateNode const*> const& nodes, string const& name)
{
	auto filteredNodes = vector<TemplateNode const*>();
	for (size_t i = 0; i < nodes.size(); i++)
		if (nodes.at(i)->getChild(0)->getItem() == name)
			filteredNodes.push_back(nodes.at(i)->getChild(0));
	return filteredNodes;
}

vector<TemplateNode*> TemplateTree::filterDirectiveLines(vector<TemplateNode*> const& contentsLines)
{
	return TypeUtils::asMutable(filterDirectiveLines(TypeUtils::asConst(contentsLines)));
}

vector<TemplateNode const*> TemplateTree::filterDirectiveLines(vector<TemplateNode const*> const& contentsLines)
{
	return filterChildNodes(contentsLines, "directive_line");
}

vector<TemplateNode*> TemplateTree::filterExpandableLines(vector<TemplateNode*> const& contentsLines)
{
	return TypeUtils::asMutable(filterExpandableLines(TypeUtils::asConst(contentsLines)));
}

vector<TemplateNode const*> TemplateTree::filterExpandableLines(vector<TemplateNode const*> const& contentsLines)
{
	return filterChildNodes(contentsLines, "expandable_line");
}

TemplateNode* TemplateTree::getGroupSpecifier(TemplateNode* groupSpecContainer)
{
	return TypeUtils::asMutable(getGroupSpecifier(static_cast<TemplateNode const*>(groupSpecContainer)));
}

TemplateNode const* TemplateTree::getGroupSpecifier(TemplateNode const* groupSpecContainer)
{
	checkNodeOfAnyType(groupSpecContainer, {"template_name", "internal_begin"});

	return groupSpecContainer->getChildWithKey("group_spec", 0);
}

vector<TemplateNode*> TemplateTree::getGroupExpansions(TemplateNode* groupSpec)
{
	return TypeUtils::asMutable(getGroupExpansions(static_cast<TemplateNode const*>(groupSpec)));
}

vector<TemplateNode const*> TemplateTree::getGroupExpansions(TemplateNode const* groupSpec)
{
	checkNodeOfType(groupSpec, "group_spec");

	auto groupExpansions = vector<TemplateNode const*>();
	auto groupExpansionCount = groupSpec->getChildCountWithKey("group_expansion");

	for (size_t i = 0; i < groupExpansionCount; i++)
	{
		auto groupExpansion = groupSpec->getChildWithKey("group_expansion", i);
		groupExpansions.push_back(groupExpansion);
	}
	
	return groupExpansions;
}

vector<string> TemplateTree::getGroupIds(TemplateNode const* groupSpec, ExpansionDefinition const* definition)
{
	checkNodeOfType(groupSpec, "group_spec");

	auto groups = getGroups(groupSpec, definition);
	auto groupIds = vector<string>(groups.size());
	transform(groups.begin(), groups.end(), groupIds.begin(),
		[](auto group) { return group->getId(); });
	return groupIds;
}

vector<ExpansionGroup const*> TemplateTree::getGroups(TemplateNode const* groupSpec, ExpansionDefinition const* definition)
{
	checkNodeOfType(groupSpec, "group_spec");

	auto groupExpansions = getGroupExpansions(groupSpec);
	auto groups = vector<ExpansionGroup const*>(groupExpansions.size());

	transform(groupExpansions.begin(), groupExpansions.end(), groups.begin(),
		[&definition](TemplateNode const* groupExpansion)
		{ return TemplateTree::getGroup(groupExpansion, definition); });
	
	return groups;
}

TemplateNode* TemplateTree::getGroupNode(TemplateNode* groupContainer)
{
	return TypeUtils::asMutable(getGroupNode(static_cast<TemplateNode const*>(groupContainer)));
}

TemplateNode const* TemplateTree::getGroupNode(TemplateNode const* groupContainer)
{
	checkNodeOfAnyType(groupContainer, vector<string>{"group_expansion", "group_ref"});
	
	return groupContainer->getChildWithKey("group", 0)->getChild(0);
}

ExpansionGroup const* TemplateTree::getGroup(TemplateNode const* groupContainer, ExpansionDefinition const* definition)
{
	checkNodeOfAnyType(groupContainer, vector<string>{"group_expansion", "group_ref"});

	auto groupId = getGroupNode(groupContainer)->getItem();
	return definition->getForeignGroupOfId(groupId);
}

vector<TemplateNode*> TemplateTree::getNestedExpansions(TemplateNode* expansion)
{
	return TypeUtils::asMutable(getNestedExpansions(static_cast<TemplateNode const*>(expansion)));
}

vector<TemplateNode const*> TemplateTree::getNestedExpansions(TemplateNode const* expansion)
{
	checkNodeOfAnyType(expansion, vector<string>{"group_expansion", "nested_expansion"});

	vector<TemplateNode const*> nestedExpansions;
	if (expansion->getChildCountWithKey("nested_expansion_list") > 0)
	{
		auto nestedExpansionList = expansion->getChildWithKey("nested_expansion_list", 0);
		auto nestedExpansionCount = nestedExpansionList->getChildCountWithKey("nested_expansion");
		for (size_t i = 0; i < nestedExpansionCount; i++)
		{
			auto nestedExpansion = nestedExpansionList->getChildWithKey("nested_expansion", i);
			nestedExpansions.push_back(nestedExpansion);
		}
	}
	return nestedExpansions;
}

vector<TemplateNode*> TemplateTree::getTokensOfType(TemplateNode* expandableName, optional<TemplateTreeTokenType> const& tokenType) 
{
	return TypeUtils::asMutable(getTokensOfType(static_cast<TemplateNode const*>(expandableName), tokenType));
}

vector<TemplateNode const*> TemplateTree::getTokensOfType(TemplateNode const* expandableName, optional<TemplateTreeTokenType> const& tokenType) 
{
	checkNodeOfAnyType(expandableName, vector<string>{"expandable_name", "expandable_line"});

	vector<TemplateNode const*> tokens;

	size_t childNum = 0;
	const size_t childCount = expandableName->getChildCountWithKey("token");
	
	if (childCount > 0)
	{
		while (childNum < childCount)
		{
			auto token = expandableName->getChildWithKey("token", childNum);
			string tokenName = "";
			if (tokenType.has_value())
			{
				tokenName = getTokenTypeMap().getString(tokenType.value());
				if (token->getChildCountWithKey(tokenName) > 0)
				{
					TemplateNode const* child = token->getChildWithKey(tokenName, 0);
					tokens.push_back(child);
				}
			}
			else
			{
				if (token->getChildCount() > 0)
				{
					tokens.push_back(token->getChild(0));
				}
			}
			childNum++;
		}
	}
	
	return tokens;
}

vector<TemplateNode*> TemplateTree::getPropertyTokens(TemplateNode* expandableName)
{
	return TypeUtils::asMutable(getPropertyTokens(static_cast<TemplateNode const*>(expandableName)));
}

vector<TemplateNode const*> TemplateTree::getPropertyTokens(TemplateNode const* expandableName)
{
	return getTokensOfType(expandableName, TemplateTreeTokenType::PropertyToken);
}

vector<TemplateNode*> TemplateTree::getSubstituteTokens(TemplateNode* expandableName)
{
	return TypeUtils::asMutable(getSubstituteTokens(static_cast<TemplateNode const*>(expandableName)));
}

vector<TemplateNode const*> TemplateTree::getSubstituteTokens(TemplateNode const* expandableName)
{
	return getTokensOfType(expandableName, TemplateTreeTokenType::SubstituteToken);
}

vector<TemplateNode*> TemplateTree::getPropertyTokensInSimpleTemplateContents(TemplateNode* simpleTemplateContents) 
{
	return TypeUtils::asMutable(getPropertyTokens(static_cast<TemplateNode const*>(simpleTemplateContents)));
}

vector<TemplateNode const*> TemplateTree::getPropertyTokensInSimpleTemplateContents(TemplateNode const* simpleTemplateContents) 
{
	vector<TemplateNode const*> propertyTokens;
	size_t expandableLinesCount = simpleTemplateContents->getChildCountWithKey("expandable_line");
	for (size_t i = 0; i < expandableLinesCount; i++)
	{
		auto expandableLine = simpleTemplateContents->getChildWithKey("expandable_line", i);
		auto propertyTokensInLine = getPropertyTokens(expandableLine);
		propertyTokens.insert(propertyTokens.end(), propertyTokensInLine.begin(), propertyTokensInLine.end());
	}
	return propertyTokens;
}

bool TemplateTree::isInternalPropertyToken(TemplateNode const* propertyToken)
{
	return propertyToken->getChildCountWithKey("internal_token_symbol") > 0;
}

tuple<string, size_t, size_t> TemplateTree::getGroupRef(TemplateNode const* propertyToken)
{
	checkNodeOfType(propertyToken, "property_token");

	string groupId = "";
	size_t copyNum = numeric_limits<size_t>::max();
	size_t groupIndex = numeric_limits<size_t>::max();

	if (propertyToken->getChildCountWithKey("group_ref") > 0)
	{
		auto groupRef = propertyToken->getChildWithKey("group_ref", 0);
		
		if (groupRef->getChildCountWithKey("group") > 0)
		{
			groupId = getGroupNode(groupRef)->getItem();
			if (groupRef->getChildCount() > 1)
			{
				auto copyRef = groupRef->getChildWithKey("copy_ref", 0);
				copyNum = stoull(copyRef->getChild(0)->getItem());
			}
		}
		else if (groupRef->getChildCountWithKey("index_ref") > 0)
		{
			auto indexRef = groupRef->getChildWithKey("index_ref", 0);
			groupIndex = stoull(indexRef->getChild(0)->getItem());
		}
	}
	
	return tuple(groupId, copyNum, groupIndex);
}

TemplateNode* TemplateTree::getProperty(TemplateNode* propertyContainer)
{
	return TypeUtils::asMutable(getProperty(static_cast<TemplateNode const*>(propertyContainer)));
}

TemplateNode const* TemplateTree::getProperty(TemplateNode const* propertyContainer)
{
	checkNodeOfAnyType(propertyContainer, vector<string>{"property_token", "nested_expansion"});

	return propertyContainer->getChildWithKey("property", 0);
}

vector<TemplateNode const*> TemplateTree::getProperties(vector<TemplateNode const*> propertyTokens) 
{
	auto properties = vector<TemplateNode const*>(propertyTokens.size());
	transform(propertyTokens.begin(), propertyTokens.end(), properties.begin(),
		[](TemplateNode const* propertyToken){ return getProperty(propertyToken); });
	return properties;
}

vector<TemplateNode*> TemplateTree::getPropertyTerms(TemplateNode* property)
{
	return TypeUtils::asMutable(getPropertyTerms(static_cast<TemplateNode const*>(property)));
}

vector<TemplateNode const*> TemplateTree::getPropertyTerms(TemplateNode const* property)
{
	checkNodeOfType(property, "property");

	vector<TemplateNode const*> terms;
	for (auto termIt = property->getChildBegin();
		termIt != property->getChildEnd(); termIt++)
	{
		if ((*termIt)->getItem() == "term")
		{
			terms.push_back(*termIt);
		}
	}
	return terms;
}

size_t TemplateTree::getPropertyTermCount(TemplateNode const* property) 
{
	checkNodeOfType(property, "property");
	
	return property->getChildCountWithKey("term");
}

tuple<string, size_t> TemplateTree::getFieldAndIndexer(TemplateNode const* term)
{
	checkNodeOfType(term, "term");

	auto field = term->getChildWithKey("field", 0)->getChild(0)->getItem();
	size_t index = numeric_limits<size_t>::max();
	if (term->getChildCountWithKey("indexer") > 0)
	{
		auto indexNode = term->getBranchWithKey({"indexer", "index"});
		index = stoull(indexNode->getChild(0)->getItem());
	}
	return make_tuple(field, index);
}

pair<string, string> TemplateTree::getSubstituteDictAndKey(TemplateNode const* substituteToken)
{
	pair<string, string> dictAndKey;
	if (substituteToken->getChildCountWithKey("substitute_dict") > 0)
	{
		dictAndKey.first = substituteToken->getChildWithKey("substitute_dict", 0)->getChild(0)->getItem();
	}
	dictAndKey.second = substituteToken->getChildWithKey("substitute_key", 0)->getChild(0)->getItem();
	return dictAndKey;
}

vector<TemplateNode*> TemplateTree::getTerminalChildren(TemplateNode* node)
{
	return TypeUtils::asMutable(getTerminalChildren(static_cast<TemplateNode const*>(node)));
}

vector<TemplateNode const*> TemplateTree::getTerminalChildren(TemplateNode const* node)
{
	auto children = vector<TemplateNode const*>();
	for (auto childIt = node->getChildBegin();
		childIt != node->getChildEnd(); childIt++)
	{
		if ((*childIt)->isLeaf())
			children.push_back(*childIt);
	}
	return children;
}


/** Flattening **/

string TemplateTree::flatten(TemplateNode const* root)
{
	string str;
	auto nodes = Tree<string>::traverseAll(TreeTraversalMode::DepthFirst, root);
	for (auto [node, depth] : nodes)
	{
		if (node->isLeaf())
			str += node->getItem();
	}
	return str;
}


/** Type Checking **/

bool TemplateTree::isNodeOfType(TemplateNode const* node, string const& typeName)
{
	return node != nullptr && node->getItem() == typeName && !node->isLeaf();
}

bool TemplateTree::isNodeOfAnyType(TemplateNode const* node, vector<string> const& typeNames)
{
	for (auto const& typeName : typeNames)
		if (isNodeOfType(node, typeName))
			return true;
	return true;
}

void TemplateTree::checkNodeOfType(TemplateNode const* node, string const& typeName)
{
	if (!isNodeOfType(node, typeName))
		throw invalid_argument("Node of name '" + node->getItem() + "' found when expected type '" + typeName + "'.");
}

void TemplateTree::checkNodeOfAnyType(TemplateNode const* node, vector<string> const& typeNames)
{
	if (!isNodeOfAnyType(node, typeNames))
		throw invalid_argument("Node of name '" + node->getItem() + "' found when expected one of the following: '" + toString(typeNames) + "'.");
}
