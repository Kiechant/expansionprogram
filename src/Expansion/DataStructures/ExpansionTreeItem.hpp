//
//  ExpansionTreeItem.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 27/6/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef EXPANSION_TREE_ITEM_HPP
#define EXPANSION_TREE_ITEM_HPP

#include "ComparisonOperatorMethodsDecl.hpp"
#include "ContextualEntryReference.hpp"
#include "EnumStringMapDecl.hpp"
#include "TemplateTreeDecl.hpp"

class ExpansionContext;

#include <optional>
#include <string>

class ExpansionTreeItem
{
public:
	enum class ItemType
	{
		Expansion,
		Replacement,
		Literal,
		NestedTemplate
	};

	ExpansionTreeItem();
	
	static ExpansionTreeItem makeExpansion(ExpansionContext* context);
	static ExpansionTreeItem makeReplacement(ContextualEntryReference const& entryRef, TemplateNode const* propertyToken, size_t beginResolution);
	static ExpansionTreeItem makeLiteral(std::string const& replacement);

	ItemType getItemType() const;
	static EnumStringMap<ItemType> getItemTypeStringMap();
	ExpansionContext* getContext() const;
	ContextualEntryReference const& getContextualEntryRef() const;
	std::string getReplacement() const;
	TemplateNode const* getPropertyToken() const;
	size_t getBeginResolution() const;

	friend int compare(ExpansionTreeItem const&, ExpansionTreeItem const&);
	COMPARISON_OPERATOR_METHODS_DECL(ExpansionTreeItem)

	friend std::string toString(ExpansionTreeItem const&);

	friend size_t toHash(ExpansionTreeItem const&);

private:
	ItemType itemType = ItemType::Expansion;
	ExpansionContext* context = nullptr;
	std::optional<ContextualEntryReference> entryRef;
	std::string replacement = "";
	TemplateNode const* propertyToken = nullptr;
	size_t beginResolution = 0;
};

#endif /* EXPANSION_TREE_ITEM_HPP */
