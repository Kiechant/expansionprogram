//
//  CacheMap.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 26/1/21.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef CACHE_MAP_HPP
#define CACHE_MAP_HPP

#include "CacheMapDecl.hpp"

#include <functional>
#include <map>
#include <optional>

template<typename Item, typename Key>
class CacheMap
{
public:
	CacheMap(std::function<Item(Key const&)> const& generate);

	friend void swap<>(CACHE_MAP_T&, CACHE_MAP_T&);
	CacheMap(CACHE_MAP_T const&);
	CacheMap(CACHE_MAP_T const&, std::function<Item(Key const&)> const& generate);
	CACHE_MAP_T& operator=(CACHE_MAP_T);

	Item const& get(Key const&) const;
	
private:
	std::function<Item(Key const&)> generateItemOfKey;
	mutable std::map<Key, Item> cachedItemOfKey;
};

#endif /* CACHE_MAP_HPP */
