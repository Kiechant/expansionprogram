//
//  TemplateTreeDecl.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef TEMPLATE_TREE_DECL_HPP
#define TEMPLATE_TREE_DECL_HPP

#include "TreeNodeDecl.hpp"

#include <string>
#include <variant>

enum class TemplateTreeTokenType;
using TemplateNode = TreeNode<std::string>;
class TemplateTree;

#endif /* TEMPLATE_TREE_DECL_HPP */
