//
//  TermResolver.tpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef TERM_RESOLVER_TPP
#define TERM_RESOLVER_TPP

#include "TermResolver.hpp"

#include "TemplateTree.hpp"

template<typename T>
T TermResolver<T>::resolveTerm(T const& entryRef, TemplateNode const* term) const
{
	auto [field, index] = TemplateTree::getFieldAndIndexer(term);
	return resolveTerm(entryRef, field, index);
}

#endif /* TERM_RESOLVER_TPP */
