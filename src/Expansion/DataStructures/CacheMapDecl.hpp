//
//  CacheMapDecl.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 25/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef CACHE_MAP_DECL_HPP
#define CACHE_MAP_DECL_HPP

template<typename Item, typename Key>
class CacheMap;

#define CACHE_MAP_TMPL template<typename Item, typename Key>
#define CACHE_MAP_T CacheMap<Item, Key>

CACHE_MAP_TMPL
void swap(CACHE_MAP_T&, CACHE_MAP_T&);

#endif /* CACHE_MAP_DECL_HPP */
