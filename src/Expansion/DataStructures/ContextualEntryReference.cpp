//
//  ContextualEntryReference.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 11/1/21.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "ContextualEntryReference.hpp"

#include "Comparison.hpp"
#include "ExpansionContext.hpp"
#include "ExpansionContextItem.hpp"
#include "ExpansionGroup.tpp"
#include "Hashing.hpp"
#include "MemoryUtils.hpp"
#include "TemplateTree.hpp"
#include "TreeNode.tpp"

using namespace std;

ContextualEntryReference::ContextualEntryReference(ContextReference contextRef, EntryReference entryRef) 
	: contextRef(contextRef), entryRef(entryRef) {}

ContextualEntryReference ContextualEntryReference::initFromPropertyToken(ExpansionContext* context, TemplateNode const* propertyToken) 
{
	size_t groupIndex = getIndexOfUniqueGroupReference(context->getGroups(), propertyToken);
	auto groupContextNode = context->getRoot().getContextNode()->getChild(groupIndex);
	return initFromGroupNode(context, groupContextNode);
}

ContextualEntryReference ContextualEntryReference::initFromGroupId(ExpansionContext* context, std::string const& groupId) 
{
	auto groupContextNode = context->getMutableGroupNodeOfId(groupId);
	return initFromGroupNode(context, groupContextNode);
}

ContextualEntryReference ContextualEntryReference::initFromGroupNode(ExpansionContext* context, ExpansionContextNode* groupNode) 
{
	auto groupViewNode = context->getMutableViewNodeOfGroupNode(groupNode);
	auto groupContextRef = ContextReference(groupNode, groupViewNode, false);
	return ContextualEntryReference(groupContextRef, groupNode->getItem().getEntryRef());
}

ContextReference const* ContextualEntryReference::getContextRef() const
{
	return &contextRef;
}

ContextReference* ContextualEntryReference::getContextRef()
{
	return &contextRef;
}

EntryReference const* ContextualEntryReference::getEntryRef() const
{
	return &entryRef;
}

EntryReference* ContextualEntryReference::getEntryRef() 
{
	return &entryRef;
}

int compare(ContextualEntryReference const& first, ContextualEntryReference const& second) 
{
	RETURN_IF_UNEQUAL_COMPARE(first.contextRef, second.contextRef);
	RETURN_IF_UNEQUAL_COMPARE(first.entryRef, second.entryRef);
	return 0;
}

COMPARISON_OPERATOR_METHODS_IMPL(ContextualEntryReference)

size_t toHash(ContextualEntryReference const& obj) 
{
	size_t res = initHash();
	res = combineHash(res, toHash(obj.contextRef));
	res = combineHash(res, toHash(obj.entryRef));
	return res;
}

size_t ContextualEntryReference::getIndexOfUniqueGroupReference(vector<ExpansionGroup const*> const& groups,
	TemplateNode const* propertyToken)
{
	auto indicesOfGroup = MemoryUtils::mapGroupIndexes<string, ExpansionGroup const*>(groups,
		[](auto const& group){ return group->getId(); });

	auto [groupId, copyNum, groupIndex] = TemplateTree::getGroupRef(propertyToken);
	auto terms = TemplateTree::getPropertyTerms(TemplateTree::getProperty(propertyToken));
	auto [firstField, _] = TemplateTree::getFieldAndIndexer(terms.at(0));

	bool hasGroupId = groupId != "";
	bool hasCopyNum = copyNum != numeric_limits<size_t>::max();
	bool hasGroupIndex = groupIndex != numeric_limits<size_t>::max();
	
	if (hasGroupId || hasGroupIndex)
	{
		if (hasGroupIndex)
		{
			if (groupIndex >= groups.size())
			{
				throw invalid_argument("Invalid index '" + to_string(groupIndex) + "'. Too large.");
			}
		}
		else /* hasGroupId */
		{
			auto groupIndicesIt = indicesOfGroup.find(groupId);
			if (groupIndicesIt == indicesOfGroup.end())
			{
				throw invalid_argument("Invalid group id '" + groupId + "'. Group is not present.");
			}
			auto const& groupIndices = groupIndicesIt->second;
			
			if (hasCopyNum)
			{
				if (copyNum >= groupIndices.size())
				{
					throw invalid_argument("Copy number '" + to_string(copyNum) + "' is too large. Doesn't refer to any existing duplicate.");
				}
				groupIndex = groupIndices[copyNum];
			}
			else
			{
				if (groupIndices.size() != 1)
				{
					throw invalid_argument("Ambiguous group id '" + groupId + "'. Duplicate groups in group specifier.");
				}
				groupIndex = groupIndices.front();
			}
		}
		
		if (!groups[groupIndex]->hasField(firstField))
		{
			throw invalid_argument("Referenced group of id '" + groups[groupIndex]->getId() + "' does not have an entry for '" + firstField + "', the first field in the property.");
		}
	}
	else /* No group explicitly referenced. Deduced by property. */
	{
		auto potentialGroups = ExpansionGroup::findGroupsContainingField(groups, firstField);
		if (potentialGroups.size() == 0)
		{
			 throw invalid_argument("No group has an entry for '" + firstField + "', the first field in the property.");
		}
		if (potentialGroups.size() >= 2)
		{
			throw invalid_argument("Ambiguous group. Two or more groups have an entry for '" + firstField + "', the first field in the property.");
		}
		groupIndex = indicesOfGroup[potentialGroups[0]][0];
	}

	return groupIndex;
}
