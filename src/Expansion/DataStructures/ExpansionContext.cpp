//
//  ExpansionContext.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 22/7/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "ExpansionContext.hpp"

#include "BasicTermResolver.hpp"
#include "ContextReference.hpp"
#include "ContextualEntryReference.hpp"
#include "ContextualTermResolver.hpp"
#include "ExpansionContextItem.hpp"
#include "ExpansionFieldEntry.hpp"
#include "ExpansionMember.hpp"
#include "ExpansionGroup.tpp"
#include "MiscUtils.hpp"
#include "PropertyResolver.tpp"
#include "TemplateTree.hpp"
#include "Tree.tpp"

#include <exception>

using namespace std;

using FieldFormat = ExpansionFieldEntryFormat;


/** Constructors and Destructors **/

ExpansionContext::ExpansionContext(ExpansionDefinition const* definition)
	: definition(definition)
{
	tree = new Tree<ExpansionContextItem>();
	expansionsView = new Tree<ExpansionContextNode*>(tree->getRoot());
}

ExpansionContext::~ExpansionContext()
{
	delete tree;
	delete expansionsView;
}


/** Copy and Move Semantics **/

unordered_map<uintptr_t, size_t> ExpansionContext::mapContextPointerToIndex() const
{
	// Flattens and maps the context node pointers to their indices in the tree traversal.
	auto indexOfContextNode = unordered_map<uintptr_t, size_t>();
	auto contextNodes = Tree<ExpansionContextItem>::traverseAll(TreeTraversalMode::DepthFirst, this->tree->getRoot());
	for (size_t i = 0; i < contextNodes.size(); i++)
	{
		auto contextNode = contextNodes.at(i).first;
		auto contextPtr = reinterpret_cast<uintptr_t>(contextNode);
		indexOfContextNode.emplace(contextPtr, i);
	}
	return indexOfContextNode;
}

void ExpansionContext::assignContextPointersToIndices(unordered_map<uintptr_t, size_t> const& mapping)
{
	auto newContextNodes = Tree<ExpansionContextItem>::traverseAll(TreeTraversalMode::DepthFirst, this->tree->getRoot());
	
	// Maps the expansion view.
	auto newViewNodes = Tree<ExpansionContextNode*>::traverseAll(TreeTraversalMode::DepthFirst, this->expansionsView->getRoot());
	for (size_t i = 0; i < newViewNodes.size(); i++)
	{
		auto oldContextPtr = reinterpret_cast<uintptr_t>(newViewNodes.at(i).first->getItem());
		auto newContextNode = newContextNodes.at(mapping.at(oldContextPtr)).first;
		newViewNodes.at(i).first->setItem(newContextNode);
	}
}

void swap(ExpansionContext& first, ExpansionContext& second)
{
	using std::swap;

	swap(first.definition, second.definition);
	swap(first.tree, second.tree);
	swap(first.expansionsView, second.expansionsView);
}

ExpansionContext::ExpansionContext(ExpansionContext const& other)
{
	this->definition = other.definition;

	// Copies the context and view nodes, and maps the new view nodes to the new context nodes.
	auto otherMapping = other.mapContextPointerToIndex();
	this->tree = new Tree<ExpansionContextItem>(*other.tree);
	this->expansionsView = new Tree<ExpansionContextNode*>(*other.expansionsView);
	this->assignContextPointersToIndices(otherMapping);
}

ExpansionContext::ExpansionContext(ExpansionContext&& other)
{
	swap(*this, other);
}

ExpansionContext& ExpansionContext::operator=(ExpansionContext other)
{
	swap(*this, other);
	return *this;
}


/** Basic Accessors and Modifiers **/

ContextReference ExpansionContext::getRoot() const
{
	auto contextRoot = tree->getRoot();
	auto viewRoot = expansionsView->getRoot();
	return ContextReference(contextRoot, viewRoot, false);
}


/** Creation and Editing **/

ExpansionContext* ExpansionContext::readContext(TemplateNode const* groupSpec, ExpansionDefinition const* definition)
{
	TemplateTree::checkNodeOfType(groupSpec, "group_spec");

	auto context = new ExpansionContext(definition);
	auto groupExpansions = TemplateTree::getGroupExpansions(groupSpec);
	
	for (auto* groupExpansion : groupExpansions)
	{
		// Adds context for root group in the group specifier.
		ExpansionGroup const* group = TemplateTree::getGroup(groupExpansion, definition);
		auto groupContextRef = context->addContextNodeForGroupReference(context->getRoot(), group);

		// Creates a stack to recursively parse the list of nested expansions for an expanded entry.
		auto expansionStack = stack<pair<ContextualEntryReference, queue<TemplateNode const*>>>();
		auto nestedSpecifierNodes = TemplateTree::getNestedExpansions(groupExpansion);
		expansionStack.emplace(groupContextRef, MiscUtils::makeQueue(nestedSpecifierNodes));

		// Recursively adds child context nodes for each nested member or value expansion.
		while (!expansionStack.empty())
		{
			// The contextual reference to the expanded entry, and the queue of nested expansions of that entry.
			auto currEntryRef = expansionStack.top().first;
			auto expansionQueue = &expansionStack.top().second;

			if (!expansionQueue->empty())
			{
				auto currSpecifierNode = expansionQueue->front();
				expansionQueue->pop();

				// Resolves nested member references before the final term to be expanded.
				auto terms = TemplateTree::getPropertyTerms(TemplateTree::getProperty(currSpecifierNode));
				for (size_t termNo = 0; termNo < terms.size() - 1; termNo++)
				{
					auto [field, index] = TemplateTree::getFieldAndIndexer(terms.at(termNo));
					currEntryRef = context->addContextNodeForEntryReference(currEntryRef, field, index, false);
				}

				// Validates the final term is expandable.
				auto* finalTerm = terms.back();
				auto [finalField, finalIndex] = TemplateTree::getFieldAndIndexer(finalTerm);
				FieldFormat format = BasicTermResolver(definition).getGroup(*currEntryRef.getEntryRef())->getFormat(finalField);
				if (!ExpansionFieldEntry::isExpandableFormat(format))
				{
					throw invalid_argument("Non-expandable format specified for expansion.");
				}

				// Final term must be a member if another nested expansion list is defined within it.
				nestedSpecifierNodes = TemplateTree::getNestedExpansions(currSpecifierNode);
				if (!nestedSpecifierNodes.empty())
				{
					if (!ExpansionFieldEntry::isForeignOrNestedMemberFormat(format))
					{
						throw invalid_argument("Nested argument list cannot be specified under a non-member expansion.");
					}
				}

				// Expands on the final term of group.
				currEntryRef = context->addContextNodeForEntryReference(currEntryRef, finalField, finalIndex, true);

				// Queues processing of the nested expansions of the expanded property.
				if (!nestedSpecifierNodes.empty())
				{
					expansionStack.emplace(currEntryRef, MiscUtils::makeQueue(nestedSpecifierNodes));
				}
			}
			else
			{
				expansionStack.pop();
			}
		}
	}
	
	return context;
}

ContextualEntryReference ExpansionContext::addContextNodeForGroupReference(ContextReference const& contextRef, ExpansionGroup const* group) 
{
	// Adds and references a context node for the group.
	auto contextItem = ExpansionContextItem::makeGroupReference("", group);
	auto contextNode = contextRef.getContextNode()->addChild(contextItem);
	auto viewNode = contextRef.getViewNode()->addChild(contextNode);
	auto nextContextRef = ContextReference(contextNode, viewNode, false);

	return ContextualEntryReference(nextContextRef, contextItem.getEntryRef());
}

ContextualEntryReference ExpansionContext::addContextNodeForEntryReference(ContextualEntryReference const& entryRef, string const& field, size_t index, bool doExpand)
{
	// Checks the newly created entry reference will be valid.
	BasicTermResolver::checkValidFieldAndIndexer(BasicTermResolver(definition).getGroup(*entryRef.getEntryRef()), doExpand, field, index);

	// Resolves the entry reference to the proceeding term.
	auto nextBasicRef = BasicTermResolver(definition).resolveTerm(*entryRef.getEntryRef(), field, index);
	ExpansionContextItem newContextItem = ExpansionContextItem::makeEntryReference(doExpand, nextBasicRef);

	// Creates and references a context node for the entry reference.
	auto contextNode = entryRef.getContextRef()->getContextNode()->addChild(newContextItem);
	auto viewNode = entryRef.getContextRef()->getViewNode();
	if (doExpand)
	{
		viewNode = entryRef.getContextRef()->getViewNode()->addChild(contextNode);
	}
	auto nextContextRef = ContextReference(contextNode, viewNode, false);

	return ContextualEntryReference(nextContextRef, nextBasicRef);
}


/** Group Specifier Queries **/

vector<ExpansionGroup const*> ExpansionContext::getGroups() const
{
	auto groupNodes = getGroupNodes();
	auto groups = vector<ExpansionGroup const*>(groupNodes.size());
	transform(groupNodes.begin(), groupNodes.end(), groups.begin(),
		[](ExpansionContextNode const* groupNode)
		{ return groupNode->getItem().getEntryRef().getGroup(); });
	return groups;
}

ExpansionContextNode const* ExpansionContext::getGroupNodeOfId(string const& groupId) const
{
	auto groupNodes = getGroupNodes();
	auto groupNodeIt = find_if(groupNodes.begin(), groupNodes.end(),
		[&groupId](ExpansionContextNode const* groupNode)
		{
			return groupNode->getItem().getEntryRef().getGroup()->getId() == groupId;
		});
	if (groupNodeIt == groupNodes.end())
		throw invalid_argument("Could not find group node of id '" + groupId + "' in expansion context.");
	return *groupNodeIt;
}

ExpansionContextNode* ExpansionContext::getMutableGroupNodeOfId(string const& groupId) 
{
	return MUTABLE_FROM_CONST_MEMBER_FUNC(this, getGroupNodeOfId, groupId);
}

vector<ExpansionContextNode const*> ExpansionContext::getGroupNodes() const
{
	auto groupNodes = vector<ExpansionContextNode const*>();
	size_t groupCount = tree->getRoot()->getChildCount();
	for (size_t i = 0; i < groupCount; i++)
	{
		groupNodes.push_back(tree->getRoot()->getChild(i));
	}
	return groupNodes;
}

vector<ExpansionContextNode*> ExpansionContext::getMutableGroupNodes()
{
	auto groupNodes = vector<ExpansionContextNode*>();
	size_t groupCount = tree->getRoot()->getChildCount();
	for (size_t i = 0; i < groupCount; i++)
	{
		groupNodes.push_back(tree->getRoot()->getChild(i));
	}
	return groupNodes;
}

ExpansionViewNode const* ExpansionContext::getViewNodeOfGroupNode(ExpansionContextNode const* groupNode) const
{
	return getRoot().getViewNode()->getChildWithKey(const_cast<ExpansionContextNode*>(groupNode), 0);
}

ExpansionViewNode* ExpansionContext::getMutableViewNodeOfGroupNode(ExpansionContextNode* groupNode) 
{
	return MUTABLE_FROM_CONST_MEMBER_FUNC(this, getViewNodeOfGroupNode, groupNode);
}
