//
//  ExpansionTree.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 22/07/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef EXPANSION_TREE_HPP
#define EXPANSION_TREE_HPP

#include "ExpansionSchemeDecl.hpp"
#include "TreeDecl.hpp"
#include "TemplateTreeDecl.hpp"

class ExpansionContext;
class ExpansionDefinition;
class ExpansionTreeItem;

class ExpansionTree
{
public:
	Tree<ExpansionTreeItem>* tree = nullptr;

	static ExpansionTree* initWithExpandableName(
		TemplateNode const* expandableName,
		ExpansionContext const* context,
		ExpansionDefinition const* definition
	);

	static ExpansionTree* initWithFileContents(
		TemplateNode const* fileContents,
		ExpansionContext* context,
		ExpansionDefinition const* definition,
		ExpansionSchemeElement const* schemeElement // May be null if no substitution token present.
	);

	// Extends the expansion tree by interpreting the elements in the file contents syntax tree.
	static void extendFromNodeWithFileContents(
		TreeNode<ExpansionTreeItem>* rootNode,
		TemplateNode const* fileContents,
		ExpansionDefinition const* definition,
		ExpansionSchemeElement const* schemeElement // May be null if no substitution token present.
	);

	// Extends the expansion tree by interpreting the elements in the file contents syntax tree.
	//
	// 'expansionTreePath' is the path from the root node to the current deepest internal expansion node.
	// If 'isEnclosed' is false, an error will be thrown if the extension to the expansion tree is not enclosed.
	// All #(expand) directives must balance with #(endexpand) directives to be 'enclosed'.
	// This distinction is important for substitutions, which may have unenclosed directives.
	//
	// Returns the initial file contents line type.
	static std::string extendFromNodeWithFileContents(
		std::vector<TreeNode<ExpansionTreeItem>*>* expansionTreePath,
		TemplateNode const* fileContents,
		ExpansionDefinition const* definition,
		ExpansionSchemeElement const* schemeElement, // May be null if no substitution token present.
		bool isEnclosed
	);

	~ExpansionTree();

private:
	ExpansionTree(ExpansionTreeItem const& rootItem);
};

#endif /* EXPANSION_TREE_HPP */
