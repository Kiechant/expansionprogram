//
//  ContextReference.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 11/1/21.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef CONTEXT_REFERENCE_HPP
#define CONTEXT_REFERENCE_HPP

#include "ComparisonOperatorMethodsDecl.hpp"
#include "ExpansionContextDecl.hpp"

class ContextReference
{
public:
	ContextReference(ExpansionContextNode* contextNode, ExpansionViewNode* viewNode, bool isFinalised);

	ExpansionContextNode* getContextNode() const;
	ExpansionViewNode* getViewNode() const;
	bool isFinalised() const;

	friend int compare(ContextReference const& first, ContextReference const& second);
	COMPARISON_OPERATOR_METHODS_DECL(ContextReference)
	
	friend int toHash(ContextReference const&);
	
private:
	ExpansionContextNode* contextNode;
	ExpansionViewNode* viewNode;
	bool _isFinalised;
};

#endif /* CONTEXT_REFERENCE_HPP */
