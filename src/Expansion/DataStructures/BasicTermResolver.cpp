//
//  BasicTermResolver.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 11/1/21.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "BasicTermResolver.hpp"

#include "ExpansionDefinition.hpp"
#include "ExpansionFieldEntry.hpp"
#include "ExpansionGroup.tpp"
#include "ExpansionMember.hpp"
#include "ExpansionMemberFieldMap.tpp"
#include "NestedExpansionGroup.hpp"
#include "NestedExpansionMember.hpp"
#include "PropertyResolver.tpp"
#include "TemplateTree.hpp"
#include "Tree.tpp"
#include "ValueEntry.tpp"

using namespace std;

using FieldFormat = ExpansionFieldEntryFormat;


BasicTermResolver::BasicTermResolver(ExpansionDefinition const* definition) 
	: definition(definition) {}

EntryReference BasicTermResolver::resolveTerm(EntryReference const& entryRef, string const& field, size_t index) const
{
	ExpansionGroup const* nextGroup = getGroup(entryRef);
	if (entryRef.getReferenceCategory() == EntryReference::ReferenceCategory::Member)
	{
		// Determines whether the field needs member information to resolve.
		// Functions and aliases are group-contained formats which may use member fields in subproperties.
		auto format = nextGroup->getFormat(field);
		bool isMemberRequiredField = nextGroup->isMemberContainedEntry(field) ||
			format == FieldFormat::Function || format == FieldFormat::Alias || format == FieldFormat::NestedTemplate;

		if (isMemberRequiredField)
		{
			// The field requires a reference to a specific member, and that member is available.
			return EntryReference(getMember(entryRef), field, index);
		}
	}
	// Returns a group reference if no member is required or it can't be resolved.
	return EntryReference(nextGroup, field, index);
}

size_t BasicTermResolver::getIndex(EntryReference const& entryRef) 
{
	if (entryRef.getGroup()->isListedField(entryRef.getField()))
	{
		return entryRef.getIndex();
	}
	else
	{
		return entryRef.hasIndex() ? entryRef.getIndex() : 0;
	}
}


/** Validity Checking of References **/

void BasicTermResolver::checkValidFieldAndIndexer(ExpansionGroup const* group, bool doExpand, string const& field, size_t indexer)
{
	// TODO: Relocate validity checking for expanded entries.

	bool isListed = ExpansionFieldEntry::isListedFormat(group->getFormat(field));
	size_t listSize = isListed ? group->getListSize(field) : 1;
	bool hasIndexer = indexer != numeric_limits<size_t>::max();
	
	if (hasIndexer)
	{
		// TODO: Remove this check entirely. Fields to be expanded can still be reference with an explicit index in context.
		if (doExpand)
		{
			throw invalid_argument("Indexer supplied for field to be expanded.");
		}
		else if (listSize == 1)
		{
			throw invalid_argument("Indexer supplied for field with singular entry.");
		}
		else if (indexer >= listSize)
		{
			throw invalid_argument("Index is too large for the specified field.");
		}
	}
	else if (listSize > 1)
	{
		if (!doExpand)
		{
			throw invalid_argument("No indexer supplied for field with multiple entries.");
		}
	}
}

void BasicTermResolver::checkValidTerm(ExpansionGroup const* group, bool doExpand, TemplateNode const* term)
{
	auto [field, indexer] = TemplateTree::getFieldAndIndexer(term);
	checkValidFieldAndIndexer(group, doExpand, field, indexer);
}


/** Group Dereferencing **/

ExpansionGroup const* BasicTermResolver::getGroup(EntryReference const& entryRef) const
{
	auto group = entryRef.getGroup();

	if (entryRef.hasField())
	{
		string const& field = entryRef.getField();

		// Field present. Dereferences with the group and field.
		auto format = group->getFormat(field);
		if (format == FieldFormat::ForeignMember)
		{
			string const* foreignGroupId = group->getForeignGroupId(field);
			return definition->getForeignGroupOfId(*foreignGroupId);
		}
		if (format == FieldFormat::NestedMember)
		{
			string const* nestedGroupId = group->getNestedGroupId(field);
			return definition->getNestedGroupOfId(*nestedGroupId);
		}
		if (format == FieldFormat::Owner)
		{
			string const& ownerId = dynamic_cast<NestedExpansionGroup const*>(group)->getOwnerId();
			return definition->getForeignGroupOfId(ownerId);
		}
	
		throw invalid_argument("Expected field to define a foreign or nested group.");
	}
	else
	{
		// No field present. The entry reference designates the current group.
		return group;
	}
}


/** Member Dereferencing **/

ExpansionMember const* BasicTermResolver::getDirectOrGroupContainedForeignMember(EntryReference const& entryRef) const
{
	if (!entryRef.hasIndex())
	{
		if (!entryRef.isGroupReference())
		{
			// No index present. Returns the direct member.
			return entryRef.getMember();
		}
		else
		{
			throw invalid_argument("Attempting to get a member from a group reference when no index is supplied.");
		}
	}
	else
	{
		// Index present. Returns the indexed member of the group.
		string const& memberId = entryRef.getGroup()->memberIds.at(entryRef.getIndex());
		return definition->getMemberOfId(memberId);
	}
}

ExpansionMember const* BasicTermResolver::getForeignMember(EntryReference const& entryRef) const
{
	if (entryRef.hasField())
	{
		// Field present. Dereferences with the group or member, and field.
		string const* foreignId = entryRef.getFieldMap()->getForeignMemberId(entryRef.getField(), getIndex(entryRef));
		return definition->getMemberOfId(*foreignId);
	}
	else
	{
		// No field present. The entry reference designates the current member.
		return getDirectOrGroupContainedForeignMember(entryRef);
	}
}

NestedExpansionMember const* BasicTermResolver::getNestedMember(EntryReference const& entryRef) const
{
	if (entryRef.hasField())
	{
		// Field present. Dereferences with the group and field.
		return entryRef.getFieldMap()->getNestedMember(entryRef.getField(), getIndex(entryRef));
	}
	else
	{
		// No field present. The entry reference designates the current member,
		// interpreted as a nested member.
		return static_cast<NestedExpansionMember const*>(entryRef.getMember());
	}
}

ExpansionMember const* BasicTermResolver::getOwner(NestedExpansionMember const* group) const
{
	return definition->getMemberOfId(group->getOwnerId());
}

ExpansionMember const* BasicTermResolver::getMember(EntryReference const& entryRef) const
{
	if (entryRef.hasField())
	{
		// Field present. Dereferences with the group or member, and field.
		FieldFormat format = entryRef.getGroup()->getFormat(entryRef.getField());
		if (format == FieldFormat::ForeignMember)
		{
			return getForeignMember(entryRef);
		}
		else if (format == FieldFormat::NestedMember)
		{
			return &dynamic_cast<ExpansionMember const&>(*getNestedMember(entryRef));
		}
		else if (format == FieldFormat::Owner)
		{
			return getOwner(dynamic_cast<NestedExpansionMember const*>(entryRef.getGroup()));
		}
		throw invalid_argument("Attempted to retrieve foreign or nested member from different field format.");
	}
	else
	{
		// No field present. The entry reference designates the current member.
		return getDirectOrGroupContainedForeignMember(entryRef);
	}
}


/** Value Dereferencing **/

ValueEntry BasicTermResolver::getValue(EntryReference const& entryRef) const
{
	ValueEntry repl = ValueEntry();

	vector<shared_ptr<TemplateTree>> const* args;
	vector<ValueEntry> argValues;
	vector<string> argRepls;
	shared_ptr<TemplateTree> subproperty;
	optional<EntryReference> subpropertyEntryRef;
	vector<TemplateNode const*> subproperties;

	string const& field = entryRef.getField();
	size_t index = getIndex(entryRef);

	// Selects the field map if the field is static or member-defined.
	ExpansionMemberFieldMap const* fieldMap = entryRef.getFieldMap();
	FieldFormat format = entryRef.getGroup()->getFormat(field);

	switch (format)
	{
	case FieldFormat::Identifier:
		repl.setValueWithCategory<ValueCategory::String>(entryRef.getMember()->getId());
		break;
	case FieldFormat::Name:
		repl.setValueWithCategory<ValueCategory::String>(entryRef.getMember()->getName());
		break;
	case FieldFormat::GroupIdentifier:
		repl.setValueWithCategory<ValueCategory::String>(entryRef.getGroup()->getId());
		break;
	case FieldFormat::GroupName:
		repl.setValueWithCategory<ValueCategory::String>(entryRef.getGroup()->getName());
		break;
	case FieldFormat::Value:
		repl = *fieldMap->getValue(field, index);
		break;
	case FieldFormat::ForeignMember:
	case FieldFormat::NestedMember:
		repl.setValueWithCategory<ValueCategory::String>(getMember(entryRef)->getId());
		break;
	case FieldFormat::Function:
		args = entryRef.getGroup()->getFunctionArgs(field);
		argValues = vector<ValueEntry>(args->size());
		for (size_t i = 0; i < args->size(); i++)
		{
			argValues.at(i) = PropertyResolver<EntryReference>(*this)
				.resolveFinalValue(entryRef.makeOriginEntryReference(), args->at(i)->tree->getRoot(), 0);
		}
		try
		{
			repl = entryRef.getGroup()->evaluateFunction(field, argValues);
		}
		catch (...)
		{
			throw_with_nested(runtime_error(
				"Error running function '" + field + "' in " + entryRef.getMember()->memberInfo() + "."
			));
		}
		break;
	case FieldFormat::Alias:
		subproperty = *entryRef.getGroup()->getAlias(field);
		repl = PropertyResolver<EntryReference>(*this)
			.resolveFinalValue(entryRef.makeOriginEntryReference(), subproperty->tree->getRoot(), 0);
		break;
	case FieldFormat::NestedTemplate:
		subproperties = TemplateTree::getProperties(
			TemplateTree::getPropertyTokensInSimpleTemplateContents(
				static_cast<TemplateNode const*>(fieldMap->getNestedTemplate(field)->tree->getRoot())));
		argRepls = vector<string>(subproperties.size());
		for (size_t i = 0; i < subproperties.size(); i++)
		{
			argRepls.at(i) = PropertyResolver<EntryReference>(*this)
				.resolveFinalValueString(entryRef.makeOriginEntryReference(), subproperties.at(i), 0);
		}
		repl.setValueWithCategory<ValueCategory::String>(fieldMap->evaluateNestedTemplateString(field, argRepls));
		break;
	default:
		throw invalid_argument("Unimplemented format '" + ExpansionFieldEntry::nameOfFormat(format) +
			"' for resolving value of entry reference.");
	}

	return repl;
}

string BasicTermResolver::getValueString(EntryReference const& entryRef) const
{
	return toString(getValue(entryRef));
}
