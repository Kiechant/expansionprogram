//
//  CacheMap.tpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 25/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef CACHE_MAP_TPP
#define CACHE_MAP_TPP

#include "CacheMap.hpp"

CACHE_MAP_TMPL
CACHE_MAP_T::CacheMap(std::function<Item(Key const&)> const& generate) 
	: generateItemOfKey(generate) {}

CACHE_MAP_TMPL
void swap(CACHE_MAP_T& first, CACHE_MAP_T& second)
{
	using std::swap;
	swap(first.generateItemOfKey, second.generateItemOfKey);
	swap(first.cachedItemOfKey, second.cachedItemOfKey);
}

CACHE_MAP_TMPL
CACHE_MAP_T::CacheMap(CACHE_MAP_T const& other) 
	: generateItemOfKey(other.generateItemOfKey),
	  cachedItemOfKey(other.cachedItemOfKey) {}

CACHE_MAP_TMPL
CACHE_MAP_T::CacheMap(CACHE_MAP_T const& other, std::function<Item(Key const&)> const& generate)
	: CacheMap(other)
{
	this->generateItemOfKey = generate;
}

CACHE_MAP_TMPL
CACHE_MAP_T& CACHE_MAP_T::operator=(CACHE_MAP_T other) 
{
	swap(*this, other);
	return *this;
}

CACHE_MAP_TMPL
Item const& CACHE_MAP_T::get(Key const& key) const
{
	if (cachedItemOfKey.count(key) == 0)
	{
		cachedItemOfKey.emplace(key, generateItemOfKey(key));
	}
	return cachedItemOfKey.at(key);
}

#endif /* CACHE_MAP_TPP */
