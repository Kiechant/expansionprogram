//
//  PropertyResolver.tpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef PROPERTY_RESOLVER_TPP
#define PROPERTY_RESOLVER_TPP

#include "PropertyResolver.hpp"

#include "TermResolver.tpp"
#include "ValueEntry.tpp"

template<typename RefT>
template<typename TermResolverT, typename>
PropertyResolver<RefT>::PropertyResolver(TermResolverT const& termResolver) 
{
	this->termResolver = new TermResolverT(termResolver);
}

template<typename RefT>
PropertyResolver<RefT>::~PropertyResolver() 
{
	delete termResolver;
}

template<typename RefT>
std::vector<RefT> PropertyResolver<RefT>::
resolveAllTermsAsList(RefT const& entryRef, TemplateNode const* property) const
{
	size_t termCount = TemplateTree::getPropertyTermCount(property);
	return resolveNextTermsAsList(entryRef, property, 0, termCount);
}

template<typename RefT>
RefT PropertyResolver<RefT>::
resolveTerm(RefT const& entryRef, TemplateNode const* term) const
{
	return termResolver->resolveTerm(entryRef, term);
}

template<typename RefT>
RefT PropertyResolver<RefT>::
resolveTerm(RefT const& entryRef, std::string const& field, size_t index) const
{
	return termResolver->resolveTerm(entryRef, field, index);
}

template<typename RefT>
std::optional<RefT> PropertyResolver<RefT>::
resolveNextTerm(RefT const& entryRef, TemplateNode const* property, size_t termNo) const
{
	auto terms = TemplateTree::getPropertyTerms(property);
	return termNo < terms.size() ?
		std::make_optional(resolveTerm(entryRef, terms.at(termNo))) :
		std::nullopt;
}

template<typename RefT>
std::pair<std::optional<RefT>, size_t> PropertyResolver<RefT>::
resolveNextTerms(RefT const& entryRef, TemplateNode const* property, size_t begin, size_t count) const
{
	auto entryRefs = resolveNextTermsAsList(entryRef, property, begin, count);
	std::optional<RefT> endEntryRef = !entryRefs.empty() ?
		std::make_optional(entryRefs.back()) :
		std::nullopt;
	return make_pair(endEntryRef, begin + entryRefs.size());
}

template<typename RefT>
std::vector<RefT> PropertyResolver<RefT>::
resolveNextTermsAsList(RefT const& entryRef, TemplateNode const* property, size_t begin, size_t count) const
{
	std::vector<RefT> entryRefs;
	RefT const* currEntryRef = &entryRef;
	size_t termCount = TemplateTree::getPropertyTermCount(property);
	size_t end = begin + count;
	for (size_t termNo = begin; (termNo < end) && (termNo < termCount); termNo++)
	{
		entryRefs.push_back(resolveNextTerm(*currEntryRef, property, termNo).value());
		currEntryRef = &entryRefs.back();
	}
	return entryRefs;
}

template<typename RefT>
std::optional<RefT> PropertyResolver<RefT>::
resolveFinalTerm(RefT const& entryRef, TemplateNode const* property, size_t begin) const
{
	size_t termCount = TemplateTree::getPropertyTermCount(property);
	return resolveNextTerms(entryRef, property, begin, termCount - begin).first;
}

template<typename RefT>
ValueEntry PropertyResolver<RefT>::resolveFinalValue(RefT const& entryRef, TemplateNode const* property, size_t begin) const
{
	return termResolver->getValue(resolveFinalTerm(entryRef, property, begin).value());
}

template<typename RefT>
std::string PropertyResolver<RefT>::resolveFinalValueString(RefT const& entryRef, TemplateNode const* property, size_t begin) const
{
	return termResolver->getValueString(resolveFinalTerm(entryRef, property, begin).value());
}

#endif /* PROPERTY_RESOLVER_TPP */
