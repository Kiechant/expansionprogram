//
//  ExpansionContext.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 22/7/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef EXPANSION_CONTEXT_HPP
#define EXPANSION_CONTEXT_HPP

#include "ExpansionContextDecl.hpp"

#include "GraphDecl.hpp"
#include "TemplateTreeDecl.hpp"
#include "TreeDecl.hpp"

class ContextReference;
class ContextualEntryReference;
class ExpansionContextItem;
class ExpansionDefinition;
class ExpansionGroup;

#include <map>
#include <set>
#include <tuple>
#include <unordered_map>

class ExpansionContext
{
public:
	ExpansionDefinition const* definition = nullptr;
	Tree<ExpansionContextItem>* tree = nullptr;
	Tree<ExpansionContextNode*>* expansionsView = nullptr;


	/** Constructors and Destructors **/

	ExpansionContext(ExpansionDefinition const* definition);
	~ExpansionContext();


	/** Copy and Move Semantics **/

	std::unordered_map<uintptr_t, size_t> mapContextPointerToIndex() const;
	void assignContextPointersToIndices(std::unordered_map<uintptr_t, size_t> const& mapping);
	friend void swap(ExpansionContext&, ExpansionContext&);
	ExpansionContext(ExpansionContext const&);
	ExpansionContext(ExpansionContext&&);
	ExpansionContext& operator=(ExpansionContext);


	/** Basic Accessors and Modifiers **/

	ContextReference getRoot() const;


	/** Creation and Editing **/

	static ExpansionContext* readContext(TemplateNode const* groupSpec, ExpansionDefinition const* definition);
	ContextualEntryReference addContextNodeForGroupReference(ContextReference const& contextRef, ExpansionGroup const* group);
	ContextualEntryReference addContextNodeForEntryReference(ContextualEntryReference const& entryRef, std::string const& field, size_t index, bool doExpand);


	/** Group Specifier Queries **/

	std::vector<ExpansionGroup const*> getGroups() const;
	ExpansionContextNode const* getGroupNodeOfId(std::string const& groupId) const;
	ExpansionContextNode* getMutableGroupNodeOfId(std::string const& groupId);
	std::vector<ExpansionContextNode const*> getGroupNodes() const;
	std::vector<ExpansionContextNode*> getMutableGroupNodes();
	ExpansionViewNode const* getViewNodeOfGroupNode(ExpansionContextNode const* groupNode) const;
	ExpansionViewNode* getMutableViewNodeOfGroupNode(ExpansionContextNode* groupNode);
};

#endif /* EXPANSION_CONTEXT_HPP */
