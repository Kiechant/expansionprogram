//
//  TermResolver.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 11/1/21.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef TERM_RESOLVER_HPP
#define TERM_RESOLVER_HPP

#include "TemplateTreeDecl.hpp"

class ExpansionDefinition;
class ValueEntry;

#include <optional>
#include <string>

template <typename T>
class TermResolver
{
public:
	using ReferenceType = T;

	virtual ~TermResolver() = default;

	T resolveTerm(T const& entryRef, TemplateNode const* term) const;
	virtual T resolveTerm(T const& entryRef, std::string const& field, size_t index) const = 0;
	virtual ValueEntry getValue(T const& entryRef) const = 0;
	virtual std::string getValueString(T const& entryRef) const = 0;
};

#endif /* TERM_RESOLVER_HPP */
