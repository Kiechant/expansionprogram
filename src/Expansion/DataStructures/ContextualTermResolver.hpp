//
//  ContextualTermResolver.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 11/1/21.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef CONTEXTUAL_TERM_RESOLVER_HPP
#define CONTEXTUAL_TERM_RESOLVER_HPP

#include "TermResolver.tpp"

class ContextualEntryReference;
class ExpansionContext;
class ExpansionDefinition;

class ContextualTermResolver : public TermResolver<ContextualEntryReference>
{
public:
	ExpansionDefinition const* definition;
	ExpansionContext const* context;

	ContextualTermResolver(ExpansionDefinition const* definition, ExpansionContext const* context);
	~ContextualTermResolver() = default;

	ContextualEntryReference resolveTerm(ContextualEntryReference const& entryRef, std::string const& field, size_t index) const;

	ValueEntry getValue(ContextualEntryReference const& entryRef) const;
	std::string getValueString(ContextualEntryReference const& entryRef) const;
};

#endif /* CONTEXTUAL_TERM_RESOLVER_HPP */
