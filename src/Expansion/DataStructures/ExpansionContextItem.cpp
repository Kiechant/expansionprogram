//
//  ExpansionContextItem.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 27/6/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "ExpansionContextItem.hpp"

#include "EntryReference.hpp"
#include "ExpansionMember.hpp"
#include "ExpansionGroup.tpp"
#include "GraphNode.tpp"
#include "StdComparison.hpp"
#include "StdHashing.hpp"
#include "StdStringConversion.hpp"

using namespace std;

using FieldFormat = ExpansionFieldEntryFormat;

ExpansionContextItem::ExpansionContextItem() {}

ExpansionContextItem::ExpansionContextItem(ContextType contextType, bool doExpand, string const& alias, EntryReference const& entryRef) 
	: contextType(contextType), _doExpand(doExpand), alias(alias), entryRef(entryRef) {}

ExpansionContextItem ExpansionContextItem::makeGroupSpecifier() 
{
	return ExpansionContextItem();
}

ExpansionContextItem ExpansionContextItem::makeGroupReference(string const& alias, ExpansionGroup const* group) 
{
	return ExpansionContextItem(ContextType::GroupReference, true, alias, EntryReference(group, "", numeric_limits<size_t>::max()));
}

ExpansionContextItem ExpansionContextItem::makeEntryReference(bool doExpand, EntryReference const& entryRef) 
{
	return ExpansionContextItem(ContextType::EntryReference, doExpand, "", entryRef);
}

string ExpansionContextItem::nameOfContextType(ContextType contextType)
{
	switch (contextType)
	{
	case ExpansionContextItem::ContextType::GroupSpecifier:
		return "Group Specifier";
	case ExpansionContextItem::ContextType::GroupReference:
		return "Group Reference";
	case ExpansionContextItem::ContextType::EntryReference:
		return "Entry Reference";
	}
}

ExpansionContextItem::ContextType ExpansionContextItem::getContextType() const
{
	return contextType;
}

std::string const& ExpansionContextItem::getAlias() const
{
	return alias;
}

bool ExpansionContextItem::doExpand() const
{
	return _doExpand;
}

EntryReference const& ExpansionContextItem::getEntryRef() const
{
	return entryRef.value();
}

bool ExpansionContextItem::hasEntryRef() const
{
	return entryRef.has_value();
}

size_t ExpansionContextItem::getIndex() const
{
	return entryRef.value().getIndex();
}

void ExpansionContextItem::setIndex(size_t index)
{
	entryRef.value().setIndex(index);
}

int compare(ExpansionContextItem const& first, ExpansionContextItem const& second)
{
	RETURN_IF_UNEQUAL_COMPARE((int)first.contextType, (int)second.contextType);
	RETURN_IF_UNEQUAL_COMPARE(first._doExpand, second._doExpand);
	RETURN_IF_UNEQUAL_COMPARE(first.alias, second.alias);
	RETURN_IF_UNEQUAL_COMPARE(first.entryRef, second.entryRef);
	return 0;
}

COMPARISON_OPERATOR_METHODS_IMPL(ExpansionContextItem)

string toString(ExpansionContextItem const& item)
{
	string str = "{" + ExpansionContextItem::nameOfContextType(item.contextType);
	auto entryRef = item.entryRef.value();

	if (entryRef.getGroup() != nullptr)
		str += ", group: " + entryRef.getGroup()->getId();
	
	str += ", field: " + entryRef.getField();
	str += ", index: " + toString(item.getIndex());

	str += "}";
	return str;
}

size_t toHash(ExpansionContextItem const& item)
{
	size_t res = initHash();
	res = combineHash(res, toHash((int)item.contextType));
	res = combineHash(res, toHash((int)item._doExpand));
	res = combineHash(res, toHash(item.alias));
	res = combineHash(res, toHash(item.entryRef));
	return res;
}
