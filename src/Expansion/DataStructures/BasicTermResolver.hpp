//
//  BasicTermResolver.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 11/1/21.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef BASIC_TERM_RESOLVER_HPP
#define BASIC_TERM_RESOLVER_HPP

#include "TemplateTreeDecl.hpp"
#include "TermResolver.tpp"
#include "TreeDecl.hpp"

class EntryReference;
class ExpansionGroup;
class ExpansionMember;
class NestedExpansionMember;
class ValueEntry;

class BasicTermResolver : public TermResolver<EntryReference>
{
public:
	ExpansionDefinition const* definition;

	BasicTermResolver(ExpansionDefinition const* definition);
	~BasicTermResolver() = default;

	EntryReference resolveTerm(EntryReference const& entryRef, std::string const& field, size_t index) const;

	/* Gets the entry reference of the index, interpreting an invalid index for an unlisted entry as 0. */
	static size_t getIndex(EntryReference const& entryRef);


	/** Validity Checking of References **/

	/* Throws an error if the field and indexer does not reference a distinct entry in the group.
	   Validates indexers for expanded and non-expanded entries. */
	static void checkValidFieldAndIndexer(ExpansionGroup const* group, bool doExpand, std::string const& field, size_t indexer);

	/* Validates the field and indexer of the term in the group. */
	static void checkValidTerm(ExpansionGroup const* group, bool doExpand, TemplateNode const* term);


	/** Group Dereferencing **/

	/* Returns the group of an entry reference. */
	ExpansionGroup const* getGroup(EntryReference const& entryRef) const;


	/** Member Dereferencing **/

	/* Returns the direct member of the entry reference if available, otherwise
	   the foreign member in the group at the entry reference's index. */
	ExpansionMember const* getDirectOrGroupContainedForeignMember(EntryReference const& entryRef) const;

	/* Returns the foreign member coresponding to the entry reference. */
	ExpansionMember const* getForeignMember(EntryReference const& entryRef) const;

	/* Returns the nested member coresponding to the entry reference. */
	NestedExpansionMember const* getNestedMember(EntryReference const& entryRef) const;

	/* Returns the owner of the nested member. */
	ExpansionMember const* getOwner(NestedExpansionMember const* group) const;

	/* Returns a foreign or nested member corresponding to the entry reference. */
	ExpansionMember const* getMember(EntryReference const& entryRef) const;


	/** Value Dereferencing **/

	/* Resolves the value coresponding to the entry reference. */
	ValueEntry getValue(EntryReference const& entryRef) const;

	/* Resolves the value string coresponding to the entry reference. */
	std::string getValueString(EntryReference const& entryRef) const;
};

#endif /* BASIC_TERM_RESOLVER_HPP */
