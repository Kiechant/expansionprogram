//
//  ContextualTermResolver.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 11/1/21.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "ContextualTermResolver.hpp"

#include "BasicTermResolver.hpp"
#include "ContextualEntryReference.hpp"
#include "ExpansionContextItem.hpp"
#include "TreeNode.tpp"
#include "ValueEntry.tpp"

using namespace std;

ContextualTermResolver::ContextualTermResolver(ExpansionDefinition const* definition, ExpansionContext const* context) 
	: definition(definition), context(context) {}

ContextualEntryReference ContextualTermResolver::resolveTerm(ContextualEntryReference const& entryRef, string const& field, size_t index) const
{
	optional<EntryReference> nextBasicRef;
	optional<ContextReference> nextContextRef;
	bool isFinalised = entryRef.getContextRef()->isFinalised();
	bool isFinalising = false;
	bool doExpand = false;

	auto contextNode = entryRef.getContextRef()->getContextNode();
	auto viewNode = entryRef.getContextRef()->getViewNode();

	if (!isFinalised)
	{
		auto nextContextNode = contextNode;
		auto nextViewNode = viewNode;

		// TODO: Add item-to-key function for field of context node for O(1) retrieval.
		// Searches for a context node that matches the field at the next level.
		isFinalised = true;
		for (size_t i = 0; i < contextNode->getChildCount(); i++)
		{
			auto subContextNode = contextNode->getChild(i);
			if (field == subContextNode->getItem().getEntryRef().getField())
			{
				if (subContextNode->getItem().doExpand())
				{
					if (index == numeric_limits<size_t>::max())
					{
						nextContextNode = subContextNode;
						isFinalised = false;
						break;
					}
				}
				else
				{
					if (index == subContextNode->getItem().getIndex())
					{
						nextContextNode = subContextNode;
						isFinalised = false;
						break;
					}
				}
			}
		}

		// Checks if another context node was found, otherwise the context is finalised.
		if (!isFinalised)
		{
			// If an expanded entry, retrieves the view node that corresponds to the context node.
			if (nextContextNode->getItem().doExpand())
			{
				if (viewNode->getChildCountWithKey(nextContextNode) == 0)
					throw logic_error("No expansion view node corresponding to the expanded context node.");

				nextViewNode = viewNode->getChildWithKey(nextContextNode, 0);
			}

			nextContextRef = ContextReference(nextContextNode, nextViewNode, false);
			doExpand = nextContextNode->getItem().doExpand();
		}
		isFinalising = isFinalised;
	}

	// If the context node is the last one, resolves the term as normal with the last used context node.
	if (isFinalised)
	{
		nextContextRef = ContextReference(entryRef.getContextRef()->getContextNode(), entryRef.getContextRef()->getViewNode(), true);
	}

	// An indexer should not be supplied for expanded or singular entries.
	auto basicTermResolver = BasicTermResolver(definition);
	BasicTermResolver::checkValidFieldAndIndexer(basicTermResolver.getGroup(*entryRef.getEntryRef()), doExpand, field, index);

	// Selects the index belonging to the member combination if expanding.
	size_t selectIndex = doExpand ?
		nextContextRef->getContextNode()->getItem().getIndex() :
		index;

	// Sets the entry reference to that of the parent context node if it is a group reference,
	// to capture the changed index within a member combination.
	bool isParentGroupReference = (!isFinalised || isFinalising) && contextNode->getItem().getContextType() == ExpansionContextItem::ContextType::GroupReference;
	EntryReference entryRefInContext = isParentGroupReference ?
		contextNode->getItem().getEntryRef() :
		*entryRef.getEntryRef();

	nextBasicRef = basicTermResolver.resolveTerm(entryRefInContext, field, selectIndex);

	return ContextualEntryReference(nextContextRef.value(), nextBasicRef.value());
}

ValueEntry ContextualTermResolver::getValue(ContextualEntryReference const& entryRef) const
{
	return BasicTermResolver(definition).getValue(*entryRef.getEntryRef());
}

string ContextualTermResolver::getValueString(ContextualEntryReference const& entryRef) const
{
	return BasicTermResolver(definition).getValueString(*entryRef.getEntryRef());
}
