//
//  ExpansionTreeItem.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 27/6/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "ExpansionTreeItem.hpp"

#include "EnumStringMap.tpp"
#include "ExpansionContext.hpp"
#include "ExpansionContextItem.hpp"
#include "StdComparison.hpp"
#include "StdHashing.hpp"
#include "StdStringConversion.hpp"
#include "TemplateTree.hpp"
#include "Tree.tpp"

using namespace std;

ExpansionTreeItem::ExpansionTreeItem() {}


ExpansionTreeItem ExpansionTreeItem::makeExpansion(ExpansionContext* context) 
{
	ExpansionTreeItem item;
	item.itemType = ItemType::Expansion;
	item.context = context;
	return item;
}

ExpansionTreeItem ExpansionTreeItem::makeReplacement(ContextualEntryReference const& entryRef, TemplateNode const* propertyToken, size_t beginResolution) 
{
	ExpansionTreeItem item;
	item.itemType = ItemType::Replacement;
	item.entryRef = entryRef;
	item.propertyToken = propertyToken;
	item.beginResolution = beginResolution;
	return item;
}

ExpansionTreeItem ExpansionTreeItem::makeLiteral(std::string const& replacement) 
{
	ExpansionTreeItem item;
	item.itemType = ItemType::Literal;
	item.replacement = replacement;
	return item;
}

ExpansionTreeItem::ItemType ExpansionTreeItem::getItemType() const
{
	return itemType;
}

EnumStringMap<ExpansionTreeItem::ItemType> ExpansionTreeItem::getItemTypeStringMap()
{
	static auto stringMap = EnumStringMap<ItemType>({
		"expansion",
		"replacement",
		"literal",
		"nested template"
	});
	return stringMap;
}

ExpansionContext* ExpansionTreeItem::getContext() const
{
	return context;
}

ContextualEntryReference const& ExpansionTreeItem::getContextualEntryRef() const
{
	return entryRef.value();
}

string ExpansionTreeItem::getReplacement() const
{
	return replacement;
}

TemplateNode const* ExpansionTreeItem::getPropertyToken() const
{
	return propertyToken;
}

size_t ExpansionTreeItem::getBeginResolution() const
{
	return beginResolution;
}

int compare(ExpansionTreeItem const& first, ExpansionTreeItem const& second)
{
	RETURN_IF_UNEQUAL_COMPARE((int)first.itemType, (int)second.itemType);
	RETURN_IF_UNEQUAL_COMPARE(first.context, second.context);
	RETURN_IF_UNEQUAL_COMPARE(first.entryRef, second.entryRef);
	RETURN_IF_UNEQUAL_COMPARE(first.propertyToken, second.propertyToken);
	return 0;
}

COMPARISON_OPERATOR_METHODS_IMPL(ExpansionTreeItem)

string toString(ExpansionTreeItem const& item)
{
	stringstream ss;
	ss << "{";
	ss << "itemType = " << ExpansionTreeItem::getItemTypeStringMap().getString(item.itemType) << ", ";
	if (item.context != nullptr)
		ss << "context = " << toString(item.context) << ", ";
	if (item.entryRef.has_value())
	{
		ss << "entryRef = {";
		ss << "context = " << toString(item.entryRef.value().getContextRef()->getContextNode()) << ", ";
		ss << "entry = " << toString(*item.entryRef.value().getEntryRef());
		ss << "}, ";
	}
	if (item.replacement != "")
		ss << "replacement = " << item.replacement << ", ";
	if (item.propertyToken != nullptr)
		ss << "propertyToken = " << TemplateTree::flatten(item.propertyToken) << ", ";
	ss << "beginResolution = " << item.beginResolution;
	ss << "}";
	return ss.str();
}

size_t toHash(ExpansionTreeItem const& item)
{
	size_t res = initHash();
	res = combineHash(res, toHash((int)item.itemType));
	res = combineHash(res, toHash(item.context));
	res = combineHash(res, toHash(item.entryRef));
	res = combineHash(res, toHash(item.propertyToken));
	return res;
}
