//
//  ContextualEntryReference.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 11/1/21.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef CONTEXTUAL_ENTRY_REFERENCE_HPP
#define CONTEXTUAL_ENTRY_REFERENCE_HPP

#include "ComparisonOperatorMethodsDecl.hpp"
#include "ContextReference.hpp"
#include "EntryReference.hpp"
#include "TemplateTreeDecl.hpp"

class ContextualEntryReference
{
public:
	ContextualEntryReference(ContextReference contextRef, EntryReference entryRef);

	static ContextualEntryReference initFromPropertyToken(ExpansionContext* context, TemplateNode const* propertyToken);
	static ContextualEntryReference initFromGroupId(ExpansionContext* context, std::string const& groupId);
	static ContextualEntryReference initFromGroupNode(ExpansionContext* context, ExpansionContextNode* groupNode);

	ContextReference const* getContextRef() const;
	ContextReference* getContextRef();
	EntryReference const* getEntryRef() const;
	EntryReference* getEntryRef();

	friend int compare(ContextualEntryReference const& first, ContextualEntryReference const& second);
	COMPARISON_OPERATOR_METHODS_DECL(ContextualEntryReference)

	friend size_t toHash(ContextualEntryReference const&);

	/* Gets the index of the unique group in the group specifier that the property_token
	   node references in the group_ref node.
	   Throws a descriptive error if a unique group is not referenced. */
	static size_t getIndexOfUniqueGroupReference(std::vector<ExpansionGroup const*> const& groups,
		TemplateNode const* propertyToken);
	
private:
	ContextReference contextRef;
	EntryReference entryRef;
};

#endif /* CONTEXTUAL_ENTRY_REFERENCE_HPP */
