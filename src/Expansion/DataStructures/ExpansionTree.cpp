//
//  ExpansionTree.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 22/07/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "ExpansionTree.hpp"

#include "ContextualEntryReference.hpp"
#include "ExpansionContext.hpp"
#include "ExpansionContextItem.hpp"
#include "ExpansionDefinition.hpp"
#include "ExpansionGroup.tpp"
#include "ExpansionScheme.hpp"
#include "ExpansionTreeItem.hpp"
#include "PropertyResolver.tpp"
#include "StdStringConversion.hpp"
#include "SubstitutionDictionary.hpp"
#include "TemplateTree.hpp"
#include "Tree.tpp"

using namespace std;

ExpansionTree::ExpansionTree(ExpansionTreeItem const& rootItem)
{
	tree = new Tree<ExpansionTreeItem>(rootItem);
}

ExpansionTree::~ExpansionTree()
{
	auto nodes = Tree<ExpansionTreeItem>::traverseAll(TreeTraversalMode::DepthFirst, tree->getRoot());
	for (size_t i = 1; i < nodes.size(); i++)
	{
		auto expnItem = nodes.at(i).first->getItem();
		if (expnItem.getItemType() == ExpansionTreeItem::ItemType::Expansion)
			delete expnItem.getContext();
	}
	delete tree;
}

ExpansionTree* ExpansionTree::initWithExpandableName(
	TemplateNode const* expandableName,
	ExpansionContext const* context,
	ExpansionDefinition const* definition)
{
	TemplateTree::checkNodeOfType(expandableName, "expandable_name");

	throw runtime_error("Unimplemented");
}

ExpansionTree* ExpansionTree::initWithFileContents(
	TemplateNode const* fileContents,
	ExpansionContext* context,
	ExpansionDefinition const* definition,
	ExpansionSchemeElement const* schemeElement)
{
	TemplateTree::checkNodeOfType(fileContents, "file_contents");

	// Creates an expansion tree which initiates at the nested group specifier
	// of the external expansion.
	ExpansionTreeItem rootItem = ExpansionTreeItem::makeExpansion(context);
	ExpansionTree* expnTree = new ExpansionTree(rootItem);
	
	extendFromNodeWithFileContents(
		expnTree->tree->getRoot(),
		fileContents,
		definition,
		schemeElement
	);

	return expnTree;
}

void ExpansionTree::extendFromNodeWithFileContents(
	TreeNode<ExpansionTreeItem>* rootNode,
	TemplateNode const* fileContents,
	ExpansionDefinition const* definition,
	ExpansionSchemeElement const* schemeElement)
{
	// Maintains a path from the root node to an internal expansion node.
	// The index is the depth of the path.
	auto expnTreePath = vector<TreeNode<ExpansionTreeItem>*>();
	expnTreePath.push_back(rootNode);

	extendFromNodeWithFileContents(&expnTreePath, fileContents, definition, schemeElement, true);
}

string ExpansionTree::extendFromNodeWithFileContents(
	vector<TreeNode<ExpansionTreeItem>*>* expansionTreePath,
	TemplateNode const* fileContents,
	ExpansionDefinition const* definition,
	ExpansionSchemeElement const* schemeElement,
	bool isEnclosed)
{
	// Reads the lines of the file.
	auto contentsLines = TemplateTree::getContentsLines(fileContents);
	auto newLines = TemplateTree::getTerminalChildren(fileContents);
	string initLineType = contentsLines.size() > 0 ?
		contentsLines.at(0)->getChild(0)->getItem() : "";

	// Builds a tree to expand each token according to the context of an
	// external expansion or recursive internal expansion.
	for (size_t i = 0; i < contentsLines.size(); i++)
	{
		auto contentsLine = contentsLines.at(i);
		auto contentsLineType = contentsLine->getChild(0);
		bool isDirectiveLine = contentsLineType->getItem() == "directive_line";

		if (isDirectiveLine)
		{
			auto directiveLineType = contentsLineType->getChild(0);
			if (directiveLineType->getItem() == "internal_begin")
			{
				if (expansionTreePath->size() == 2)
					throw invalid_argument("Invalid nested internal expansion detected at line " + to_string(i)
						+ ": " + TemplateTree::flatten(contentsLine));

				// Adds a new context to the expansion node based on the nested group specifier.
				auto internalGroupSpec = TemplateTree::getGroupSpecifier(directiveLineType);
				auto internalContext = ExpansionContext::readContext(internalGroupSpec, definition);

				// Creates a new internal expansion node.
				auto expnItem = ExpansionTreeItem::makeExpansion(internalContext);
				expansionTreePath->push_back(expansionTreePath->back()->addChild(expnItem));
			}
			else if (directiveLineType->getItem() == "internal_end")
			{
				if (expansionTreePath->size() == 1)
					throw invalid_argument("Invalid end internal expansion directive at line " + to_string(i)
						+ ": " + TemplateTree::flatten(contentsLine));

				// Climbs to the expansion node of the level above.
				expansionTreePath->pop_back();
			}
			else throw invalid_argument("Unrecognised directive line type.");
		}
		else if (contentsLineType->getItem() == "expandable_line")
		{
			for (size_t i = 0; i < contentsLineType->getChildCount(); i++)
			{
				auto expandableLine = contentsLineType->getChild(i);
				if (expandableLine->isLeaf())
				{
					// Creates a literal node for literal strings, stored in the field property.
					auto expnItem = ExpansionTreeItem::makeLiteral(expandableLine->getItem());
					expansionTreePath->back()->addChild(expnItem);
				}
				else if (expandableLine->getItem() == "token")
				{
					auto token = expandableLine->getChild(0);
					if (token->getItem() == "property_token")
					{
						// Identifies which expansion (i.e. external or internal) context is being referenced.
						size_t referencedDepth = TemplateTree::isInternalPropertyToken(token);
						auto referencedContext = expansionTreePath->at(referencedDepth)->getItem().getContext();

						if (referencedDepth == 0 && referencedContext->tree->getRoot()->getChildCount() == 0)
							throw invalid_argument("Property token '" + TemplateTree::flatten(token) +
								"' found in template without external group specifier.");

						// Creates a replacement node with intermediate context resolution for the property.
						auto expnItem = ExpansionTreeItem::makeReplacement(
							ContextualEntryReference::initFromPropertyToken(referencedContext, token),
							token, 0);
						expansionTreePath->back()->addChild(expnItem);
					}
					else if (token->getItem() == "substitute_token")
					{
						auto [subDictId, subKey] = TemplateTree::getSubstituteDictAndKey(token);

						if (schemeElement == nullptr)
							throw invalid_argument("No scheme element provided for template with substitution token.");

						// Does the substitution dictionary ID reference a dictionary which exists and is selected?
						if (subDictId == definition->getScheme()->getUniversalDictId())
							throw invalid_argument("Prohibited explicit use of universal dictionary '" + subDictId + "' in template.");
						if (subDictId == "")
							subDictId = definition->getScheme()->getUniversalDictId();
						auto selectedDictIds = definition->getScheme()->getSelectedDictionaryIdsForElement(*schemeElement);
						if (!selectedDictIds.contains(subDictId))
							throw invalid_argument("Dictionary '" + subDictId + "' used by " +
								"substitution token '" + TemplateTree::flatten(token) + "' but not selected in " +
								"scheme subdirectory '" + schemeElement->templateSubpath.string() + "'.");

						auto subDict = definition->getSubstitutionDictOfId(subDictId);

						// Does the field exist in the dictionary?
						if (!subDict->hasField(subKey))
							throw invalid_argument("Selected non-existent field '" + subKey + "' in substitution " +
								"dictionary '" + subDictId + "'.");

						// If the substitution is itself a template, then recursively substitute its entries.
						if (subDict->isTemplate(subKey))
						{
							string subnInitLineType = extendFromNodeWithFileContents(
								expansionTreePath,
								subDict->getReplacementTree(subKey)->tree->getRoot(),
								definition,
								schemeElement,
								false);
							isDirectiveLine = subnInitLineType == "directive_line";
							if (i == 0)
								initLineType = subnInitLineType;
						}
						// Otherwise, it will be resolved immediately as a literal.
						else
						{
							auto expnItem = ExpansionTreeItem::makeLiteral(*subDict->getReplacementString(subKey));
							expansionTreePath->back()->addChild(expnItem);
						}
					}
					else throw invalid_argument("Unrecognised node in tokenType.");
				}
				else throw invalid_argument("Unrecognised node in expandable line.");
			}
		}
		else throw invalid_argument("Unrecognised contents line type.");

		if (!isDirectiveLine && i < newLines.size())
		{
			auto expnItem = ExpansionTreeItem::makeLiteral(TemplateTree::flatten(newLines.at(i)));
			expansionTreePath->back()->addChild(expnItem);
		}
	}

	if (isEnclosed)
	{
		if (expansionTreePath->size() > 1)
		{
			throw invalid_argument("Unclosed internal expansion before end of file.");
		}
	}

	return initLineType;
}
