//
//  Cache.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 26/1/21.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef CACHE_HPP
#define CACHE_HPP

#include "CacheDecl.hpp"

#include <functional>
#include <optional>

template<typename Item>
class Cache
{
public:
	Cache(std::function<Item()> const& generate);

	friend void swap<>(CACHE_T&, CACHE_T&);
	Cache(CACHE_T const&);
	Cache(CACHE_T const&, std::function<Item()> const& generate);
	CACHE_T& operator=(CACHE_T);

	Item const& get() const;
	
private:
	std::function<Item()> generateItem;
	mutable std::optional<Item> cachedItem;
};

#endif /* CACHE_HPP */
