//
//  MemberCombination.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 22/7/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "MemberCombination.hpp"

#include "BasicTermResolver.hpp"
#include "EntryReference.hpp"
#include "ExpansionContext.hpp"
#include "ExpansionContextItem.hpp"
#include "ExpansionDefinition.hpp"
#include "ExpansionFieldEntry.hpp"
#include "ExpansionGroup.tpp"
#include "ExpansionMember.hpp"
#include "NestedExpansionMember.hpp"
#include "Tree.tpp"

using namespace std;

using FieldFormat = ExpansionFieldEntryFormat;

MemberCombination::MemberCombination(ExpansionContext* context)
	: context(context) {}

MemberCombination* MemberCombination::initFromContext(ExpansionContext* context)
{
	// Copies the member combination from the context and initialises its members.
	auto memberCombo = new MemberCombination(context);
	memberCombo->updateChildren(memberCombo->context->tree->getRoot());
	return memberCombo;
}

bool MemberCombination::getNext()
{
	if (context->tree->getRoot()->getChildCount() == 0)
		return false;

	bool isComboComplete = false;

	// Cycles the member combination in post-order. Therefore, dependent nodes are
	// cycled before the next iteration of their containing parents or grandparents.
	auto cycleTraversal = Tree<ExpansionContextNode*>::traverseInitPostOrder(context->expansionsView->getRoot());
	auto [viewNode, depth] = Tree<ExpansionContextNode*>::traverseNextPostOrder(&cycleTraversal);
	while (true)
	{
		// Cycled members of all groups. Member combination complete.
		if (viewNode == nullptr || viewNode->getItem()->getItem().getContextType() == ExpansionContextItem::ContextType::GroupSpecifier)
		{
			isComboComplete = true;
			break;
		}

		auto contextNode = viewNode->getItem();

		// Cycles the current member combo item.
		ExpansionContextItem item = contextNode->getItem();
		bool isRollover = cycleItem(&item);
		contextNode->setItem(item);

		// If the member combo item did not reset, no rollover is needed.
		if (!isRollover)
			break;
		
		tie(viewNode, depth) = Tree<ExpansionContextNode*>::traverseNextPostOrder(&cycleTraversal);
	}

	// Updates branches of all context nodes with changed values.
	updateModifiedBranches(context->expansionsView->getRoot());

	return !isComboComplete;
}

bool MemberCombination::getNext(unsigned combos)
{
	unsigned remCombos = combos;
	bool hasNext = true;
	while (remCombos > 0 && hasNext)
	{
		hasNext = getNext();
		remCombos--;
	}
	if (remCombos > 0 && !hasNext)
	{
		throw invalid_argument("More member combinations provided than can be performed.");
	}
	return hasNext;
}

void MemberCombination::updateModifiedBranches(ExpansionViewNode* root)
{
	// Recursively resets dependent entries of cycled, changed or uninitialised nodes.
	auto searchTraversal = Tree<ExpansionContextNode*>::traverseInitDepthFirst(root);
	auto [viewNode, depth] = Tree<ExpansionContextNode*>::traverseNextDepthFirst(&searchTraversal);

	while (viewNode != nullptr)
	{
		auto contextNode = viewNode->getItem();
		bool didUpdate = false;

		// Discovered node that has been set or changed. Skips node and updates its descendents.
		if (contextNode->getItem().doUpdateBranches)
		{
			contextNode->modifyItem([](ExpansionContextItem* item){ item->doUpdateBranches = false; });
			updateChildren(contextNode);
			didUpdate = true;
		}

		// Skips any updated branches from the search.
		using ViewNodePredicate = function<bool(ExpansionViewNode const*)>;
		auto skipUpdated = !didUpdate ?
			ViewNodePredicate([](auto*){ return false; }) :
			ViewNodePredicate([&viewNode = viewNode](auto* node){ return node->getParent() == viewNode; });

		tie(viewNode, depth) = Tree<ExpansionContextNode*>::traverseNextDepthFirst(&searchTraversal, skipUpdated);
	}
}

void MemberCombination::updateChildren(ExpansionContextNode* node)
{
	for (size_t childNo = 0; childNo < node->getChildCount(); childNo++)
	{
		updateBranch(node->getChild(childNo));
	}
}

void MemberCombination::updateBranch(ExpansionContextNode* branch)
{
	auto updateTraversal = Tree<ExpansionContextItem>::traverseInitDepthFirst(branch);
	auto [contextNode, _] = Tree<ExpansionContextItem>::traverseNextDepthFirst(&updateTraversal);

	while (contextNode != nullptr)
	{
		// Descendent of changed node requiring an udpate. Re-initialises the node.
		contextNode->modifyItem([](ExpansionContextItem* item){ updateItem(item); });

		tie(contextNode, _) = Tree<ExpansionContextItem>::traverseNextDepthFirst(&updateTraversal);
	}
}

void MemberCombination::updateItem(ExpansionContextItem* item)
{
	assert(item->getContextType() != ExpansionContextItem::ContextType::GroupSpecifier);
	if (item->doExpand())
	{
		item->setIndex(0);
	}
	item->doUpdateBranches = false;
}

bool MemberCombination::cycleItem(ExpansionContextItem* item)
{
	assert(item->getContextType() != ExpansionContextItem::ContextType::GroupSpecifier);
	assert(item->doExpand());

	bool isRollover = item->getIndex() == getCycleCount(item) - 1;
	item->setIndex(isRollover ? 0 : item->getIndex() + 1);
	item->doUpdateBranches = true;
	
	return isRollover;
}

size_t MemberCombination::getCycleCount(ExpansionContextItem* item) 
{
	size_t cycleCount;
	auto group = item->getEntryRef().getGroup();
	if (item->getContextType() == ExpansionContextItem::ContextType::GroupReference)
	{
		cycleCount = group->memberIds.size();
	}
	else // item->getContextType() == ExpansionContextItem::ContextType::EntryReference
	{
		cycleCount = group->getListSize(item->getEntryRef().getField());
	}
	return cycleCount;
}
