//
//  TemplateTree.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 28/7/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef TEMPLATE_TREE_HPP
#define TEMPLATE_TREE_HPP

#include "EnumStringMapDecl.hpp"
#include "TemplateTreeDecl.hpp"

#include "TreeDecl.hpp"

class ExpansionDefinition;
class ExpansionGroup;
class SyntaxRule;

#include <filesystem>
#include <unordered_map>
#include <tuple>

enum class TemplateTreeTokenType
{
	PropertyToken,
	SubstituteToken
};

class TemplateTree
{
public:
	Tree<std::string>* tree = nullptr;


	/** Constructors and Destructors **/

	TemplateTree();
	TemplateTree(Tree<std::string>* tree);
	TemplateTree(TemplateTree const&);
	~TemplateTree();


	/** Queries **/

	/* Returns whether the template contents have operable components, either
	 * regions or replaceable tokens. A non-operable template tree is interpretable as plain text. */
	static bool isOperable(TemplateNode const* templateContents);


	/** Template Syntax **/

	/* Returns the syntax to parse the name and contents of a template file. */
	static std::unordered_map<std::string, SyntaxRule const*> const* getSyntax();

	/* Returns the name of the rule to begin parsing the name of a template file. */
	static std::string getTemplateNameStartRule();

	/* Returns the name of the rule to begin parsing the contents of a template file. */
	static std::string getTemplateContentsStartRule();

	/* Returns the name of the rule to begin parsing the simple contents of a template. */
	static std::string getSimpleTemplateContentsStartRule();

	/* Returns the string map of a template tree token type. */
	static EnumStringMap<TemplateTreeTokenType> const& getTokenTypeMap();


	/** Parsing Methods **/

	/* Parses a template tree for a query and start rule */
	static TemplateTree* parseTemplateTree(std::string const& query, std::string const& startRule);

	/* Parses the name of a template file. The file need not exist. */
	static TemplateTree* parseTemplateName(std::string const& fileName);

	/* Reads and parses the contents of a template file in the file system. */
	static TemplateTree* parseTemplateContentsAtPath(std::filesystem::path const& filePath);

	/* Reads and parses the contents of a template. */
	static TemplateTree* parseTemplateContents(std::filesystem::path const& templateContents);

	/* Reads and parses the simple contents of a template string. */
	static TemplateTree* parseSimpleTemplateContents(std::string const& templateContents);

	/* Reads and parses a property token comprising a group reference, property and other entities. */
	static TemplateTree* parsePropertyToken(std::string const& propertyTokenStr);

	/* Reads and parses a property comprising a space-delimited series of terms. */
	static TemplateTree* parseProperty(std::string const& propertyStr);


	/** Navigation **/

	/* Returns the roots of a list of trees. */
	static std::vector<TemplateNode const*> getRoots(std::vector<std::shared_ptr<TemplateTree>> const&);

	/* Retrieves the expandable_name node of the template_name. This depicts the file name template. */
	static TemplateNode* getExpandableName(TemplateNode* templateName);
	static TemplateNode const* getExpandableName(TemplateNode const* templateName);

	/* Retrives the list of contents_line nodes in file_contents. */
	static std::vector<TemplateNode*> getContentsLines(TemplateNode* fileContents);
	static std::vector<TemplateNode const*> getContentsLines(TemplateNode const* fileContents);

	/* Generic filter function for the children of a list of nodes. */
	static std::vector<TemplateNode*> filterChildNodes(std::vector<TemplateNode*> const& nodes, std::string const& name);
	static std::vector<TemplateNode const*> filterChildNodes(std::vector<TemplateNode const*> const& nodes, std::string const& name);

	/* Filters directive_line nodes from the list of contents_line nodes. */
	static std::vector<TemplateNode*> filterDirectiveLines(std::vector<TemplateNode*> const& contentsLines);
	static std::vector<TemplateNode const*> filterDirectiveLines(std::vector<TemplateNode const*> const& contentsLines);

	/* Filters expandable_line nodes from the list of contents_line nodes. */
	static std::vector<TemplateNode*> filterExpandableLines(std::vector<TemplateNode*> const& contentsLines);
	static std::vector<TemplateNode const*> filterExpandableLines(std::vector<TemplateNode const*> const& contentsLines);

	/* Retrieves the group_spec node from the template_name node. */
	static TemplateNode* getGroupSpecifier(TemplateNode* groupSpecContainer);
	static TemplateNode const* getGroupSpecifier(TemplateNode const* groupSpecContainer);

	/* Retrieves the list of group_expansion nodes from the group_spec node. */
	static std::vector<TemplateNode*> getGroupExpansions(TemplateNode* groupSpec);
	static std::vector<TemplateNode const*> getGroupExpansions(TemplateNode const* groupSpec);

	/* Retrieves the list of group ids specified in the group_spec node. */
	static std::vector<std::string> getGroupIds(TemplateNode const* groupSpec, ExpansionDefinition const* definition);

	/* Retrieves a list of groups referenced by the group_spec node. */
	static std::vector<ExpansionGroup const*> getGroups(TemplateNode const* groupSpec, ExpansionDefinition const* definition);

	/* Retrieves the child of the group node from a group_expansion or group_ref node.
	   The name of the group is in the direct item of the group returned. */
	static TemplateNode* getGroupNode(TemplateNode* groupContainer);
	static TemplateNode const* getGroupNode(TemplateNode const* groupContainer);

	/* Retrieves the group referenced by a group_expansion or group_ref node. */
	static ExpansionGroup const* getGroup(TemplateNode const* groupContainer, ExpansionDefinition const* definition);

	/* Retrieves the nested expansion nodes specified under a group_expansion or nested_expansion. */
	static std::vector<TemplateNode*> getNestedExpansions(TemplateNode* expansion);
	static std::vector<TemplateNode const*> getNestedExpansions(TemplateNode const* expansion);

	/* Retrieves the property or substitute tokens in the expandable_name of the file name,
	   or the expandable_line in the file contents if the token type is provided.
	   If token type is empty, retrieves all tokens. */
	static std::vector<TemplateNode*> getTokensOfType(TemplateNode* expandableName, std::optional<TemplateTreeTokenType> const& tokenType);
	static std::vector<TemplateNode const*> getTokensOfType(TemplateNode const* expandableName, std::optional<TemplateTreeTokenType> const& tokenType);

	/* Retrieves the property tokens in the expandable_name of the file name,
	   or the expandable_line in the file contents. */
	static std::vector<TemplateNode*> getPropertyTokens(TemplateNode* expandableName);
	static std::vector<TemplateNode const*> getPropertyTokens(TemplateNode const* expandableName);

	/* Retrieves the substitute tokens in the expandable_name of the file name,
	   or the expandable_line in the file contents. */
	static std::vector<TemplateNode*> getSubstituteTokens(TemplateNode* expandableName);
	static std::vector<TemplateNode const*> getSubstituteTokens(TemplateNode const* expandableName);

	/* Retrieves the property tokens in the simple_template_contents. */
	static std::vector<TemplateNode*> getPropertyTokensInSimpleTemplateContents(TemplateNode* simpleTemplateContents);
	static std::vector<TemplateNode const*> getPropertyTokensInSimpleTemplateContents(TemplateNode const* simpleTemplateContents);

	/* True if the property token is for an internal expansion,
	   indicated by the presence of an internal_token_symbol child. */
	static bool isInternalPropertyToken(TemplateNode const* propertyToken);

	/* Retrieves the group id, copy reference and index reference in the group_ref
	   node of the property_token. An absent index is indicated by the maximum size_t value. */
	static std::tuple<std::string, size_t, size_t> getGroupRef(TemplateNode const* propertyToken);

	/* Retrives the property node in a property_token or nested_expansion. */
	static TemplateNode* getProperty(TemplateNode* propertyContainer);
	static TemplateNode const* getProperty(TemplateNode const* propertyContainer);

	/* Returns the properties of a list of property tokens. */
	static std::vector<TemplateNode const*> getProperties(std::vector<TemplateNode const*> propertyTokens);

	/* Retrieves a list of the term nodes in the property node, separated by '.' operators. */
	static std::vector<TemplateNode*> getPropertyTerms(TemplateNode* property);
	static std::vector<TemplateNode const*> getPropertyTerms(TemplateNode const* property);

	/* Returns the number of term nodes in the property node, separated by '.' operators. */
	static size_t getPropertyTermCount(TemplateNode const* property);

	/* Retrieves the field and index of the term node.
	   An absent index is indicated by the maximum size_t value. */
	static std::tuple<std::string, size_t> getFieldAndIndexer(TemplateNode const* term);

	/* Retrives the substitute dictionary and key from the substitute token.
	 * Returns an empty string for the substitute dictionary if it does not exist. */
	static std::pair<std::string, std::string> getSubstituteDictAndKey(TemplateNode const* substituteToken);

	/* Retrives a list of terminal children from the node. */
	static std::vector<TemplateNode*> getTerminalChildren(TemplateNode* node);
	static std::vector<TemplateNode const*> getTerminalChildren(TemplateNode const* node);


	/** Flattening **/

	/* Recursively flattens the strings under the subtree beginning at 'root' into a single string. */
	static std::string flatten(TemplateNode const* root);


	/** Type Checking **/

	/* Returns whether the node depicts the type name and it is not a terminal node. */
	static bool isNodeOfType(TemplateNode const* node, std::string const& typeName);

	/* Returns whether the node matches any of the types in the list. */
	static bool isNodeOfAnyType(TemplateNode const* node, std::vector<std::string> const& typeNames);

	/* Throws an error if the node does not match the type. */
	static void checkNodeOfType(TemplateNode const* node, std::string const& typeName);

	/* Throws an error if the node doesn't match any of the types in the list. */
	static void checkNodeOfAnyType(TemplateNode const* node, std::vector<std::string> const& typeNames);
};

#endif /* TEMPLATE_TREE_HPP */
