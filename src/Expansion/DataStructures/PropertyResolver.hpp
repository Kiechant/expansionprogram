//
//  PropertyResolver.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 28/7/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef PROPERTY_RESOLVER_HPP
#define PROPERTY_RESOLVER_HPP

#include "ExpansionContextDecl.hpp"
#include "TemplateTreeDecl.hpp"
#include "TermResolver.hpp"
#include "ValueEntryDecl.hpp"

class ExpansionDefinition;
class ExpansionGroup;
class ExpansionMember;
class ExpansionMemberFieldMap;

#include <optional>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

template<typename RefT>
class PropertyResolver
{
public:
	TermResolver<RefT> const* termResolver;

	template<typename TermResolverT, typename = std::enable_if_t<
		std::is_base_of_v<TermResolver<RefT>, TermResolverT> && !std::is_abstract_v<TermResolverT>,
		TermResolverT>>
	PropertyResolver(TermResolverT const& termResolver);

	~PropertyResolver();
	
	// Deleted copy and move constructors due to inability to copy term resolver.
	PropertyResolver(PropertyResolver const&) = delete;
	PropertyResolver(PropertyResolver&&) = delete;
	PropertyResolver& operator=(PropertyResolver) = delete;

	std::vector<RefT>
	resolveAllTermsAsList(RefT const& entryRef, TemplateNode const* property) const;

	RefT
	resolveTerm(RefT const& entryRef, TemplateNode const* term) const;

	RefT
	resolveTerm(RefT const& entryRef, std::string const& field, size_t index) const;

	std::optional<RefT>
	resolveNextTerm(RefT const& entryRef, TemplateNode const* property, size_t termNo) const;

	std::pair<std::optional<RefT>, size_t>
	resolveNextTerms(RefT const& entryRef, TemplateNode const* property, size_t begin, size_t count) const;

	std::vector<RefT>
	resolveNextTermsAsList(RefT const& entryRef, TemplateNode const* property, size_t begin, size_t count) const;

	std::optional<RefT>
	resolveFinalTerm(RefT const& entryRef, TemplateNode const* property, size_t begin) const;

	ValueEntry
	resolveFinalValue(RefT const& entryRef, TemplateNode const* property, size_t begin) const;

	std::string
	resolveFinalValueString(RefT const& entryRef, TemplateNode const* property, size_t begin) const;
};


/** Deduction Guide **/

template<typename TermResolverT>
PropertyResolver(TermResolverT const& termResolver) -> PropertyResolver<typename TermResolverT::ReferenceType>;

#endif /* PROPERTY_RESOLVER_HPP */
