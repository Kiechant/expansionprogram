//
//  MemberCombination.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 22/7/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef MEMBER_COMBINATION_HPP
#define MEMBER_COMBINATION_HPP

#include "ExpansionContextDecl.hpp"
#include "TreeDecl.hpp"

class ExpansionDefinition;
class ExpansionGroup;
class ExpansionMember;

#include <set>

class MemberCombination
{
public:
	ExpansionContext* context = nullptr;

	static MemberCombination* initFromContext(ExpansionContext* context);

	bool getNext();
	bool getNext(unsigned combos);

	void updateModifiedBranches(ExpansionViewNode* root);
	void updateChildren(ExpansionContextNode* node);
	void updateBranch(ExpansionContextNode* branch);

	static void updateItem(ExpansionContextItem* item);
	static bool cycleItem(ExpansionContextItem* item);
	static size_t getCycleCount(ExpansionContextItem* item);

private:
	MemberCombination(ExpansionContext* context);
};

#endif /* MEMBER_COMBINATION_HPP */
