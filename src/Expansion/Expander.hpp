//
//  Expander.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 15/7/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#ifndef EXPANDER_HPP
#define EXPANDER_HPP

#include "ExpansionContextDecl.hpp"
#include "ExpansionSchemeDecl.hpp"
#include "TemplateTreeDecl.hpp"

class ExpansionDefinition;
class ExpansionGroup;
class ExpansionTree;
class ExpansionTreeItem;
class MemberCombination;

#include <map>
#include <filesystem>
#include <tuple>
#include <unordered_map>
#include <vector>

class Expander
{
public:
	ExpansionDefinition const* definition = nullptr;

	Expander(ExpansionDefinition const* definition, std::filesystem::path const& templateDir, std::filesystem::path const& expansionDir);


	/** Single-file Expansions **/

	void expandFile(std::filesystem::path const& filePath) const;
	std::vector<std::string> expandFileNames(
		ExpansionContext* context,
		TemplateTree const* templateNameTree) const;
	std::string expandRegionAsString(
		MemberCombination const* memberCombo,
		TemplateNode const* expandable) const;
	void expandRegionIntoStream(
		std::ostream& os,
		MemberCombination const* memberCombo,
		TemplateNode const* expandable) const;
	std::vector<std::string> expandPropertyTokens(
		MemberCombination const* memberCombo,
		std::vector<TemplateNode const*> const& propertyTokens) const;
	std::string mergeReplacementsAsString(
		std::vector<std::string> const& replacements,
		TemplateNode const* expandable) const;
	void mergeReplacementsIntoStream(
		std::ostream& os,
		std::vector<std::string> const& replacements,
		TemplateNode const* expandable) const;

	void expandContentsOfFile(std::string const& filePath,
		ExpansionTree const& expnTree) const;
	void expandRegionIntoStream(
		std::ostream& os,
		TreeNode<ExpansionTreeItem>* expnNode) const;

	static bool doesFileNeedUpdate(std::filesystem::path const& filePath,
		std::filesystem::file_time_type const& sinceTime);


	/** Property Token Validation **/

	/* Throws a descriptive error if any property token does not uniquely reference an
	   expanded item in the context or expand to a valid entry outlined in the group definition.

	   If the 'asUnique' flag is set, returns true if at least one token is present for
	   each item that expands to a value marked 'unique'. Thus, a unique expansion
	   can be produced for the template comprising the property tokens passed in.
	   Principally, this is used to ensure an expanded file name does not clash with
	   other file names under the same expansion before execution. */
	void checkPropertyTokensExpand(
		ExpansionContext* context,
		std::vector<TemplateNode const*> const& propertyTokens, bool asUnique) const;


	/** Scheme Expansion **/

	void expandFilesInDirectory(std::filesystem::path const& dir) const;
	void expandWithScheme(ExpansionScheme const* scheme) const;
	std::string getNextFileInScheme(ExpansionScheme* scheme) const;

private:
	std::filesystem::path templateDir;
	std::filesystem::path expansionDir;
};

#endif /* EXPANDER_HPP */
