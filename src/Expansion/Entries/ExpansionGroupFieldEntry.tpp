//
//  ExpansionGroupFieldEntry.tpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef EXPANSION_GROUP_FIELD_ENTRY_TPP
#define EXPANSION_GROUP_FIELD_ENTRY_TPP

#include "ExpansionGroupFieldEntry.hpp"

#include "ExpansionFieldEntry.hpp"
#include "ValueEntry.tpp"

#include <cassert>

template<ExpansionFieldEntryFormat F>
ExpansionGroupFieldEntryTypeOfFormatT<F> const* ExpansionGroupFieldEntry::getEntry() const
{
	return getEntry<ExpansionGroupFieldEntryTypeOfFormatT<F>>();
}

template<ExpansionFieldEntryFormat F>
ExpansionGroupFieldEntryTypeOfFormatT<F>* ExpansionGroupFieldEntry::getMutableEntry()
{
	return getMutableEntry<ExpansionGroupFieldEntryTypeOfFormatT<F>>();
}

template<typename T>
T const* ExpansionGroupFieldEntry::getEntry() const
{
	assert(std::holds_alternative<T>(entry));
	return &std::get<T>(entry);
}

template<typename T>
T* ExpansionGroupFieldEntry::getMutableEntry()
{
	return const_cast<T*>(getEntry<T>());
}

template<ExpansionFieldEntryFormat F>
void ExpansionGroupFieldEntry::setEntryWithFormat(ExpansionGroupFieldEntryTypeOfFormatT<F> const& value)
{
	if (this->format != F)
	{
		throw "The entry does not hold the format '" +
			ExpansionFieldEntry::nameOfFormat(format) + "'.";
	}
	this->entry = value;
}

#define ENTRY_TYPE_OF_FORMAT(F, T) template<> struct \
	ExpansionGroupFieldEntryTypeOfFormat<ExpansionFieldEntryFormat::F>\
	{ using type = TypeUtils::ArgumentType<void(T)>::type; }

ENTRY_TYPE_OF_FORMAT(Identifier, 		ExpansionGroupFieldEntryType::NoEntry);
ENTRY_TYPE_OF_FORMAT(Name, 				ExpansionGroupFieldEntryType::NoEntry);
ENTRY_TYPE_OF_FORMAT(NestedTemplate, 	ExpansionGroupFieldEntryType::NoEntry);
ENTRY_TYPE_OF_FORMAT(Value, 			ExpansionGroupFieldEntryType::ValueCategorySizePair);
ENTRY_TYPE_OF_FORMAT(ForeignMember, 	ExpansionGroupFieldEntryType::StringSizePair);
ENTRY_TYPE_OF_FORMAT(NestedMember, 		ExpansionGroupFieldEntryType::StringSizePair);
ENTRY_TYPE_OF_FORMAT(Function, 			ExpansionGroupFieldEntryType::FunctionSignature);
ENTRY_TYPE_OF_FORMAT(Alias, 			ExpansionGroupFieldEntryType::Subproperty);

#undef ENTRY_TYPE_OF_FORMAT

#endif /* EXPANSION_GROUP_FIELD_ENTRY_TPP */
