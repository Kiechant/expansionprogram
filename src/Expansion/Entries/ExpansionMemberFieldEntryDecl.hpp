//
//  ExpansionMemberFieldEntryDecl.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef EXPANSION_MEMBER_FIELD_ENTRY_DECL_HPP
#define EXPANSION_MEMBER_FIELD_ENTRY_DECL_HPP

#include "ExpansionMemberFieldEntryDecl.hpp"

#include "ExpansionFieldEntryDecl.hpp"
#include "ValueEntryDecl.hpp"
#include "TemplateTreeDecl.hpp"
#include "WrappedTypeStruct.hpp"

#include <memory>
#include <string>
#include <vector>

class NestedExpansionMember;

struct ExpansionMemberFieldEntryType
{
	// Identifer, Name, Owner: id or value.
	using String = std::string;

	// Value: list of string-convertible values
	using ValueList = std::vector<ValueEntry>;

	// Foreign Member: list of foreign member ids
	using StringList = std::vector<std::string>;

	// Nested Member: list of nested members
	using NestedMemberList = std::vector<std::shared_ptr<NestedExpansionMember>>;

	// Nested Template: nested template
	using NestedTemplate = std::shared_ptr<TemplateTree>;
};

using ExpansionMemberFieldEntryVariant  = std::variant<
	ExpansionMemberFieldEntryType::String,
	ExpansionMemberFieldEntryType::ValueList,
	ExpansionMemberFieldEntryType::StringList,
	ExpansionMemberFieldEntryType::NestedMemberList,
	ExpansionMemberFieldEntryType::NestedTemplate
>;

WRAPPED_TYPE_STRUCT(ExpansionFieldEntryFormat, ExpansionMemberFieldEntryTypeOfFormat)

class ExpansionMemberFieldEntry;

#endif /* EXPANSION_MEMBER_FIELD_ENTRY_DECL_HPP */
