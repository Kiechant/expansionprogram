//
//  ValueEntry.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 21/4/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef VALUE_FORMAT_ENTRY_HPP
#define VALUE_FORMAT_ENTRY_HPP

#include "ValueEntryDecl.hpp"

#include "ComparisonOperatorMethodsDecl.hpp"
#include "WrappedTypeStruct.hpp"

#include <optional>
#include <variant>
#include <vector>

enum class ValueCategory
{
	String,
	Bool,
	Int,
	Unsigned,
	Float
};

class ValueEntry
{
public:
	/** Constructors and Destructors **/

	ValueEntry();

	template<ValueCategory C>
	static ValueEntry fromValueWithCategory(ValueTypeOfCategoryT<C> const& value);

	/** Getters and Setters **/

	template<ValueCategory C>
	ValueTypeOfCategoryT<C> const& getValue() const;

	template<ValueCategory C>
	void setValue(ValueTypeOfCategoryT<C> const& value);

	template<ValueCategory C>
	void setValueWithCategory(ValueTypeOfCategoryT<C> const& value);
	
	ValueCategory getCategory() const;

	/** Integer Bounds **/

	static int64_t getMin(ValueCategory category);
	static int64_t getMax(ValueCategory category);

	/** Casting **/

	// Attempts to cast the value to the specified type.
	std::optional<ValueEntry> tryCastCopy(ValueCategory toType) const;

	/** String Conversion **/

	friend std::string toString(ValueEntry const&);

	/** Category Referencing **/

	static std::string idOfValueCategory(ValueCategory);
	static ValueCategory valueCategoryOfId(std::string const&);

	/** Comparison **/

	friend int compare(ValueEntry const&, ValueEntry const&);
	COMPARISON_OPERATOR_METHODS_DECL(ValueEntry)

	/** Hashing **/

	friend size_t toHash(ValueEntry const&);
	
private:
	static const std::vector<std::string> _idOfValueCategory;

	ValueVariant value;
	ValueCategory category;
};

#endif /* VALUE_FORMAT_ENTRY_HPP */
