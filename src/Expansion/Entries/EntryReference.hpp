//
//  EntryReference.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 11/1/21.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef ENTRY_REFERENCE_HPP
#define ENTRY_REFERENCE_HPP

#include "ComparisonOperatorMethodsDecl.hpp"

class ExpansionMember;
class ExpansionMemberFieldMap;
class ExpansionGroup;
class ValueEntry;

#include <string>

class EntryReference
{
public:
	EntryReference(ExpansionGroup const* group, std::string const& field, size_t index);
	EntryReference(ExpansionMember const* member, std::string const& field, size_t index);

	EntryReference makeOriginEntryReference() const;
	EntryReference makeGroupReference() const;

	bool hasField() const;
	bool hasIndex() const;

	/* Returns whether the entry reference comprises a group as a container, as opposed to a member,
	   i.e. in the format (group, field, index) instead of (member, field, index). */
	bool isGroupReference() const;

	ExpansionGroup const* getGroup() const;
	ExpansionMember const* getMember() const;
	std::string const& getField() const;
	size_t getIndex() const;

	void setIndex(size_t index);

	/* Returns the field map containing the static or member-defined field. */
	ExpansionMemberFieldMap const* getFieldMap() const;

	enum class ReferenceCategory
	{
		Invalid,	// Insufficient information to unabiguously identify an entry.
		Group,		// References a group.
		Member,		// References a specific member of a group. Resolvable to a group.
		Value		// References a non-member entry resolvable to a value.
	};

	/* Returns the kind of reference this entry reference points to. */
	ReferenceCategory getReferenceCategory() const;

	/* Whether the reference is a group or member, i.e. resolvable to a group. */
	static bool isGroupOrMemberReference(ReferenceCategory refCategory);

	friend int compare(EntryReference const&, EntryReference const&);
	COMPARISON_OPERATOR_METHODS_DECL(EntryReference)

	friend size_t toHash(EntryReference const&);

	friend std::string toString(EntryReference const&);

private:
	ExpansionGroup const* group = nullptr;
	ExpansionMember const* member = nullptr;
	std::string field;
	size_t index;
};

#endif /* ENTRY_REFERENCE_HPP */
