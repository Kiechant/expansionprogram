//
//  ExpansionGroupFieldEntry.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 8/7/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#ifndef EXPANSION_GROUP_FIELD_ENTRY_HPP
#define EXPANSION_GROUP_FIELD_ENTRY_HPP

#include "ExpansionGroupFieldEntryDecl.hpp"

#include "ExpansionFieldEntry.hpp"
#include "TypeUtils.hpp"
#include "ValueEntryDecl.hpp"

class ExpansionGroup;
class NestedExpansionGroup;
class TemplateTree;

#include <tuple>
#include <variant>

class ExpansionGroupFieldEntry
{
public:
	ExpansionGroupFieldEntry(ExpansionFieldEntryFormat format, bool isStatic);
	
	ExpansionFieldEntryFormat getFormat() const;
	size_t getListSize() const;
	void setListSize(size_t size);
	bool isStatic() const;
	void isStatic(bool);
	
	template<ExpansionFieldEntryFormat F>
		ExpansionGroupFieldEntryTypeOfFormatT<F> const* getEntry() const;
	template<ExpansionFieldEntryFormat F>
		ExpansionGroupFieldEntryTypeOfFormatT<F>* getMutableEntry();
	template<typename T>
		T const* getEntry() const;
	template<typename T>
		T* getMutableEntry();
	template<ExpansionFieldEntryFormat F>
		void setEntryWithFormat(ExpansionGroupFieldEntryTypeOfFormatT<F> const& value);

private:
	ExpansionFieldEntryFormat format;
	bool _isStatic;
	ExpansionGroupFieldEntryVariant entry;
};

#endif /* EXPANSION_GROUP_FIELD_ENTRY_HPP */
