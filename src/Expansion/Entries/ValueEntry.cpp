//
//  ValueEntry.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 21/4/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "ValueEntry.tpp"

#include "StdComparison.hpp"
#include "StdHashing.hpp"
#include "StdStringConversion.hpp"

#include <algorithm>
#include <cassert>

using namespace std;

const vector<string> ValueEntry::_idOfValueCategory =
{
    "string",
    "bool",
    "int",
    "unsigned",
    "float"
};

/** Constructors and Destructors **/

ValueEntry::ValueEntry()
{
	this->setValueWithCategory<ValueCategory::Int>(0);
}

/** Getters and Setters **/

ValueCategory ValueEntry::getCategory() const
{
	return category;
}

/** Integer Bounds **/

int64_t ValueEntry::getMin(ValueCategory category)
{
	switch (category)
	{
	case ValueCategory::Int:
		return INT32_MIN;

	default:
		return 0;
	}
}

int64_t ValueEntry::getMax(ValueCategory category)
{
	switch (category)
	{
	case ValueCategory::Int:
		return INT32_MAX;

	case ValueCategory::Unsigned:
		return UINT32_MAX;

	default:
		return 0;
	}
}

/** Casting **/

optional<ValueEntry> ValueEntry::tryCastCopy(ValueCategory toType) const
{
	switch (category)
	{
		case ValueCategory::Int:
		{
			ValueType::Int underlying = getValue<ValueCategory::Int>();
			if (toType == ValueCategory::Unsigned)
			{
				if (underlying >= 0 && underlying <= ValueEntry::getMax(ValueCategory::Unsigned))
				{
					return ValueEntry::fromValueWithCategory<ValueCategory::Unsigned>(underlying);
				}
			}
			else if (toType == ValueCategory::Float)
			{
				return ValueEntry::fromValueWithCategory<ValueCategory::Float>(underlying);
			}
			break;
		}

		case ValueCategory::Unsigned:
		{
			ValueType::Unsigned underlying = getValue<ValueCategory::Unsigned>();
			if (toType == ValueCategory::Int)
			{
				if (underlying >= ValueEntry::getMin(ValueCategory::Int) && underlying <= ValueEntry::getMax(ValueCategory::Int))
				{
					return ValueEntry::fromValueWithCategory<ValueCategory::Int>(underlying);
				}
			}
			else if (toType == ValueCategory::Float)
			{
				return ValueEntry::fromValueWithCategory<ValueCategory::Float>(underlying);
			}
			break;
		}

		default:
			break;
	}
	return nullopt;
}

/** String Conversion **/

string toString(ValueEntry const& entry)
{
	using Category = ValueCategory;
	switch (entry.getCategory())
	{
	case Category::String:
		return entry.getValue<Category::String>();
	case Category::Bool:
		return toString(entry.getValue<Category::Bool>());
	case Category::Int:
		return toString(entry.getValue<Category::Int>());
	case Category::Unsigned:
		return toString(entry.getValue<Category::Unsigned>());
	case Category::Float:
		return toString(entry.getValue<Category::Float>());
	}
}

/** Category Referencing **/

string ValueEntry::idOfValueCategory(ValueCategory category)
{
    return _idOfValueCategory[size_t(category)];
}


ValueCategory ValueEntry::valueCategoryOfId(string const& id)
{
    auto it = find(_idOfValueCategory.begin(), _idOfValueCategory.end(), id);
    assert(it != _idOfValueCategory.end());
    return (ValueCategory)(it - _idOfValueCategory.begin());
}

/** Comparison **/

int compare(ValueEntry const& first, ValueEntry const& second)
{
	RETURN_IF_UNEQUAL_COMPARE((int)first.getCategory(), (int)second.getCategory());
	using Category = ValueCategory;
	switch (first.getCategory())
	{
	case Category::String:
		return Compare<string>()(first.getValue<Category::String>(), second.getValue<Category::String>() );
	case Category::Bool:
		return Compare<bool>()(first.getValue<Category::Bool>(), second.getValue<Category::Bool>());
	case Category::Int:
		return Compare<int32_t>()(first.getValue<Category::Int>(), second.getValue<Category::Int>());
	case Category::Unsigned:
		return Compare<uint32_t>()(first.getValue<Category::Unsigned>(), second.getValue<Category::Unsigned>());
	case Category::Float:
		return Compare<double>()(first.getValue<Category::Float>(), second.getValue<Category::Float>());
	}
}

COMPARISON_OPERATOR_METHODS_IMPL(ValueEntry)

/** Hashing **/

size_t toHash(ValueEntry const& entry)
{
	size_t res = initHash();
	res = combineHash(res, ToHash<int>()((int)entry.getCategory()));
	using Category = ValueCategory;
	switch (entry.getCategory())
	{
	case Category::String:
		res = combineHash(res, ToHash<string>()(entry.getValue<Category::String>()));
	case Category::Bool:
		res = combineHash(res, ToHash<bool>()(entry.getValue<Category::Bool>()));
	case Category::Int:
		res = combineHash(res, ToHash<int32_t>()(entry.getValue<Category::Int>()));
	case Category::Unsigned:
		res = combineHash(res, ToHash<uint32_t>()(entry.getValue<Category::Unsigned>()));
	case Category::Float:
		res = combineHash(res, ToHash<double>()(entry.getValue<Category::Float>()));
	}
	return res;
}