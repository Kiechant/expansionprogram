//
//  EntryReference.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 11/1/21.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "EntryReference.hpp"

#include "ExpansionFieldEntry.hpp"
#include "ExpansionMember.hpp"
#include "ExpansionGroup.tpp"
#include "NestedExpansionGroup.hpp"
#include "StdComparison.hpp"
#include "StdHashing.hpp"

using namespace std;

using FieldFormat = ExpansionFieldEntryFormat;

EntryReference::EntryReference(ExpansionGroup const* group, string const& field, size_t index) 
	: group(group), field(field), index(index) {}

EntryReference::EntryReference(ExpansionMember const* member, string const& field, size_t index) 
	: member(member), field(field), index(index) {}

EntryReference EntryReference::makeOriginEntryReference() const
{
	return isGroupReference() ?
		EntryReference(group, "", numeric_limits<size_t>::max()) :
		EntryReference(member, "", numeric_limits<size_t>::max());
}

EntryReference EntryReference::makeGroupReference() const
{
	return EntryReference(getGroup(), field, index);
}

bool EntryReference::hasField() const
{
	return field != "";
}

bool EntryReference::hasIndex() const
{
	return index != numeric_limits<size_t>::max();
}

bool EntryReference::isGroupReference() const
{
	return group != nullptr;
}

ExpansionGroup const* EntryReference::getGroup() const
{
	return isGroupReference() ? group : member->getGroup();
}

ExpansionMember const* EntryReference::getMember() const
{
	if (isGroupReference())
	{
		throw invalid_argument("Attempting to get a direct member from a group-type entry reference.");
	}
	return member;
}

string const& EntryReference::getField() const
{
	return field;
}

size_t EntryReference::getIndex() const
{
	return index;
}

void EntryReference::setIndex(size_t index)
{
	this->index = index;
}

ExpansionMemberFieldMap const* EntryReference::getFieldMap() const
{
	return getGroup()->isMemberContainedEntry(field) ?
		getMember()->getFieldMap() :
		getGroup()->getStaticFieldMap();
}

EntryReference::ReferenceCategory EntryReference::getReferenceCategory() const
{
	if (!hasField())
	{
		if (isGroupReference())
		{
			if (hasIndex())
			{
				if (dynamic_cast<NestedExpansionGroup const*>(getGroup()) != nullptr)
				{
					// A group reference to a member of a specific index is not possible for nested groups.
					return ReferenceCategory::Invalid;
				}
				else
				{
					// References a specific member of the group by index.
					return ReferenceCategory::Member;
				}
			}
			else
			{
				// References a group, with no index to select a specific member.
				return ReferenceCategory::Group;
			}
		}
		else
		{
			if (!hasIndex())
			{
				// A member with no field or index.
				return ReferenceCategory::Member;
			}
			else
			{
				// Redundant index in member entry. Reference is ambiguous.
				return ReferenceCategory::Invalid;
			}
		}
	}

	if (hasIndex() != getGroup()->isListedField(getField()))
	{
		// Either an index for a non-listed format or no index for a listed format.
		return ReferenceCategory::Invalid;
	}
	
	FieldFormat format = getGroup()->getFormat(getField());
	bool isGroupContained = getGroup()->isGroupContainedEntry(getField());
	bool isMemberFormat = ExpansionFieldEntry::isForeignOrNestedMemberFormat(format);

	if (isGroupContained || !isGroupReference())
	{
		// Either a member reference to a member-contained entry, or a
		// group or member reference to a group-contained entry.
		return isMemberFormat ? ReferenceCategory::Member : ReferenceCategory::Value;
	}
	else // isGroupReference() && !isGroupContained
	{
		if (isMemberFormat)
		{
			// A field and index which selects an instance member entry when the member is not present.
			// Only the group is known.
			return ReferenceCategory::Group;
		}
		else
		{
			// A group reference to a member-contained non-member format is insufficient to
			// resolve further.
			return ReferenceCategory::Invalid;
		}
	}
}

bool EntryReference::isGroupOrMemberReference(ReferenceCategory refCategory) 
{
	return refCategory == ReferenceCategory::Group || refCategory == ReferenceCategory::Member;
}

int compare(EntryReference const& first, EntryReference const& second) 
{
	auto compareGroup = CompareNullableWithKey<ExpansionGroup const*, string const&>(mem_fn(&ExpansionGroup::getId));
	auto compareMember = CompareNullableWithKey<ExpansionMember const*, string const&>(mem_fn(&ExpansionMember::getId));
	RETURN_IF_UNEQUAL(first.group, second.group, compareGroup);
	RETURN_IF_UNEQUAL(first.member, second.member, compareMember);
	RETURN_IF_UNEQUAL_COMPARE(first.field, second.field);
	RETURN_IF_UNEQUAL_COMPARE(first.index, second.index);
	return 0;
}

COMPARISON_OPERATOR_METHODS_IMPL(EntryReference)

size_t toHash(EntryReference const& entryRef) 
{
	size_t res = initHash();
	if (entryRef.group != nullptr)
	{
		res = combineHash(res, toHash(entryRef.getGroup()->getId()));
	}
	if (entryRef.member != nullptr)
	{
		res = combineHash(res, toHash(entryRef.getMember()->getId()));
	}
	res = combineHash(res, toHash(entryRef.getField()));
	res = combineHash(res, toHash(entryRef.index));
	return res;
}

string toString(EntryReference const& entryRef) 
{
	string out = "(";
	
	if (entryRef.isGroupReference())
	{
		out += entryRef.getGroup()->getId() + ", ";
	}
	else
	{
		out += entryRef.getMember()->getId() + ", ";
	}

	if (entryRef.hasField())
	{
		out += entryRef.getField() + ", ";
	}
	else
	{
		out += "-, ";
	}
	
	if (entryRef.hasIndex())
	{
		out += to_string(entryRef.getIndex());
	}
	else
	{
		out += "-";
	}
	
	out += ")";
	return out;
}
