//
//  ExpansionGroupFieldEntry.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 8/7/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#include "ExpansionGroupFieldEntry.tpp"

#include <iostream>
#include <utility>

using namespace std;
using FieldFormat = ExpansionFieldEntryFormat;

ExpansionGroupFieldEntry::ExpansionGroupFieldEntry(
	ExpansionFieldEntryFormat format, bool isStatic) : format(format), _isStatic(isStatic)
{
	if (isStatic && !ExpansionFieldEntry::isMemberDefinedFormat(format))
	{
		throw invalid_argument("No static variant of non-member-defined field.");
	}

	switch (format)
	{
	case FieldFormat::Value:
		entry = ExpansionGroupFieldEntryType::ValueCategorySizePair();
		break;
	case FieldFormat::ForeignMember:
	case FieldFormat::NestedMember:
		entry = ExpansionGroupFieldEntryType::StringSizePair();
		break;
	case FieldFormat::Function:
		entry = ExpansionGroupFieldEntryType::FunctionSignature();
		break;
	case FieldFormat::Alias:
		entry = ExpansionGroupFieldEntryType::Subproperty();
		break;
	default:
		break;
	}
}

FieldFormat ExpansionGroupFieldEntry::getFormat() const
{
	return format;
}

size_t ExpansionGroupFieldEntry::getListSize() const
{
	switch (format)
	{
	case FieldFormat::Value:
		return get<ExpansionGroupFieldEntryType::ValueCategorySizePair>(entry).second;
	case FieldFormat::ForeignMember:
	case FieldFormat::NestedMember:
		return get<ExpansionGroupFieldEntryType::StringSizePair>(entry).second;
	default:
		throw invalid_argument("No list for format.");
	}
}

void ExpansionGroupFieldEntry::setListSize(size_t size)
{
	switch (format)
	{
	case FieldFormat::Value:
		get<ExpansionGroupFieldEntryType::ValueCategorySizePair>(entry).second = size;
		break;
	case FieldFormat::ForeignMember:
	case FieldFormat::NestedMember:
		get<ExpansionGroupFieldEntryType::StringSizePair>(entry).second = size;
		break;
	default:
		throw invalid_argument("No list for format.");
	}
}

bool ExpansionGroupFieldEntry::isStatic() const
{
	if (!ExpansionFieldEntry::isMemberDefinedFormat(format))
		throw invalid_argument("No static variant for non-member-defined format.");
	return _isStatic;
}

void ExpansionGroupFieldEntry::isStatic(bool _isStatic)
{
	if (!ExpansionFieldEntry::isMemberDefinedFormat(format))
		throw invalid_argument("No static variant for non-member-defined format.");
	this->_isStatic = _isStatic;
}
