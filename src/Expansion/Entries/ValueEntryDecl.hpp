//
//  ValueEntryDecl.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef VALUE_ENTRY_DECL_HPP
#define VALUE_ENTRY_DECL_HPP

#include "WrappedTypeStruct.hpp"

#include <cstdint>
#include <string>
#include <variant>

enum class ValueCategory;

struct ValueType
{
	using String = std::string;
	using Bool = bool;
	using Int = int32_t;
	using Unsigned = uint32_t;
	using Float = double;
};

using ValueVariant = std::variant<
	ValueType::String,
	ValueType::Bool,
	ValueType::Int,
	ValueType::Unsigned,
	ValueType::Float
>;

WRAPPED_TYPE_STRUCT(ValueCategory, ValueTypeOfCategory)

class ValueEntry;

#endif /* VALUE_ENTRY_DECL_HPP */
