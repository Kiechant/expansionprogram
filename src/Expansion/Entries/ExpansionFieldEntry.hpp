//
//  ExpansionFieldEntry.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 8/7/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#ifndef EXPANSION_FIELD_ENTRY_HPP
#define EXPANSION_FIELD_ENTRY_HPP

#include "ExpansionFieldEntryDecl.hpp"

#include <vector>
#include <unordered_map>

enum class ExpansionFieldEntryFormat
{
	Identifier,
	Name,
	GroupIdentifier,
	GroupName,
	Owner,
	Value,
	ForeignMember,
	NestedMember,
	NestedTemplate,
	Function,
	Alias
};

class ExpansionFieldEntry
{
public:
	static ExpansionFieldEntryFormat formatOfId(std::string const& id);
	static std::string idOfFormat(ExpansionFieldEntryFormat format);
	static std::string nameOfFormat(ExpansionFieldEntryFormat format);
	static std::string nameOfId(std::string const& id);

	static bool isStaticId(std::string const& id);
	static std::string memberDefinedIdOfStaticId(std::string const& staticId);
	static bool isGroupId(std::string const& id);
	static std::string memberDefinedIdOfGroupId(std::string const& groupId);

	static bool isForeignOrNestedMemberFormat(ExpansionFieldEntryFormat format);
	static bool isLiteralFormat(ExpansionFieldEntryFormat format);
	static bool isExpandableFormat(ExpansionFieldEntryFormat format);
	static bool isGroupDefinedFormat(ExpansionFieldEntryFormat format);
	static bool isMemberDefinedFormat(ExpansionFieldEntryFormat format);
	static bool isDefaultMemberContainedFormat(ExpansionFieldEntryFormat format);
	static bool isListedFormat(ExpansionFieldEntryFormat format);
	static bool isDefaultUniqueFormat(ExpansionFieldEntryFormat format);
	static bool isUniqueSupportingFormat(ExpansionFieldEntryFormat format);
	static bool isInterdependentFormat(ExpansionFieldEntryFormat format);

private:
	static std::vector<std::string> const& getIdOfFormatMap();
	static std::unordered_map<std::string, size_t> const& getFormatIndexOfIdMap();
	static std::string const& getStaticFormatPrefix();
	static std::string const& getGroupFormatPrefix();

	static std::string memberDefinedIdOfPrefixedId(std::string const& prefixedId, std::string const& prefix);
};

#endif /* EXPANSION_FIELD_ENTRY_HPP */
