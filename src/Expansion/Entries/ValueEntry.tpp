//
//  ValueEntry.tpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef VALUE_FORMAT_ENTRY_TPP
#define VALUE_FORMAT_ENTRY_TPP

#include "ValueEntry.hpp"

#define MAP_VALUE_TYPE(C, V) template<> struct \
	ValueTypeOfCategory<ValueCategory::C> { using type = \
	ValueType::V; };

MAP_VALUE_TYPE(String, String)
MAP_VALUE_TYPE(Bool, Bool)
MAP_VALUE_TYPE(Int, Int)
MAP_VALUE_TYPE(Unsigned, Unsigned)
MAP_VALUE_TYPE(Float, Float)

#undef MAP_VALUE_TYPE

/** Constructors and Destructors **/

template<ValueCategory C>
ValueEntry ValueEntry::fromValueWithCategory(ValueTypeOfCategoryT<C> const& value)
{
	ValueEntry entry;
	entry.setValueWithCategory<C>(value);
	return entry;
}

/** Getters and Setters **/

template<ValueCategory C>
ValueTypeOfCategoryT<C> const& ValueEntry::getValue() const
{
	if (!std::holds_alternative<ValueTypeOfCategoryT<C>>(value))
		throw "Value doesn't hold type corresponding to value category.";
	return std::get<ValueTypeOfCategoryT<C>>(value);
}

template<ValueCategory C>
void ValueEntry::setValue(ValueTypeOfCategoryT<C> const& value)
{
	assert(std::holds_alternative<ValueTypeOfCategoryT<C>>(value));
	this->value = value;
}

template<ValueCategory C>
void ValueEntry::setValueWithCategory(ValueTypeOfCategoryT<C> const& value)
{
	this->value = value;
	category = C;
}

#endif /* VALUE_FORMAT_ENTRY_TPP */
