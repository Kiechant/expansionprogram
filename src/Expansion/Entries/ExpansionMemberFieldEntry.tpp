//
//  ExpansionMemberFieldEntry.tpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef EXPANSION_MEMBER_FIELD_ENTRY_TPP
#define EXPANSION_MEMBER_FIELD_ENTRY_TPP

#include "ExpansionMemberFieldEntry.hpp"

#include "ExpansionFieldEntry.hpp"
#include "ValueEntry.tpp"

template<ExpansionFieldEntryFormat F>
ExpansionMemberFieldEntry ExpansionMemberFieldEntry::makeFromFormat(bool isStatic)
{
	ExpansionMemberFieldEntry newEntry;
	newEntry.format = F;
	newEntry._isStatic = isStatic;
	newEntry.entry = ExpansionMemberFieldEntryTypeOfFormatT<F>();
	return newEntry;
}

template<ExpansionFieldEntryFormat F>
ExpansionMemberFieldEntryTypeOfFormatT<F> const* ExpansionMemberFieldEntry::getEntry() const
{
	return getEntry<ExpansionMemberFieldEntryTypeOfFormatT<F>>();
}

template<ExpansionFieldEntryFormat F>
ExpansionMemberFieldEntryTypeOfFormatT<F>* ExpansionMemberFieldEntry::getMutableEntry()
{
	return getMutableEntry<ExpansionMemberFieldEntryTypeOfFormatT<F>>();
}

template<typename T>
T const* ExpansionMemberFieldEntry::getEntry() const
{
	assert(std::holds_alternative<T>(entry));
	return &std::get<T>(entry);
}

template<typename T>
T* ExpansionMemberFieldEntry::getMutableEntry()
{
	return const_cast<T*>(getEntry<T>());
}

template<ExpansionFieldEntryFormat F>
void ExpansionMemberFieldEntry::setEntry(ExpansionMemberFieldEntryTypeOfFormatT<F> const& value)
{
	this->entry = value;
}

#define ENTRY_TYPE_OF_FORMAT(F, T) template<> struct \
	ExpansionMemberFieldEntryTypeOfFormat<ExpansionFieldEntryFormat::F>{ using type = T; }

ENTRY_TYPE_OF_FORMAT(Identifier, 		ExpansionMemberFieldEntryType::String);
ENTRY_TYPE_OF_FORMAT(Name, 				ExpansionMemberFieldEntryType::String);
ENTRY_TYPE_OF_FORMAT(Owner, 			ExpansionMemberFieldEntryType::String);
ENTRY_TYPE_OF_FORMAT(Value, 			ExpansionMemberFieldEntryType::ValueList);
ENTRY_TYPE_OF_FORMAT(ForeignMember, 	ExpansionMemberFieldEntryType::StringList);
ENTRY_TYPE_OF_FORMAT(NestedMember, 		ExpansionMemberFieldEntryType::NestedMemberList);
ENTRY_TYPE_OF_FORMAT(NestedTemplate, 	ExpansionMemberFieldEntryType::NestedTemplate);

#undef ENTRY_TYPE_OF_FORMAT

#endif /* EXPANSION_MEMBER_FIELD_ENTRY_TPP */
