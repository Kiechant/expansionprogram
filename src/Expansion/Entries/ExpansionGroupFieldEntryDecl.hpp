//
//  ExpansionGroupFieldEntryDecl.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef EXPANSION_GROUP_FIELD_ENTRY_DECL_HPP
#define EXPANSION_GROUP_FIELD_ENTRY_DECL_HPP

#include "ExpansionFieldEntryDecl.hpp"
#include "TemplateTreeDecl.hpp"
#include "ValueEntryDecl.hpp"
#include "WrappedTypeStruct.hpp"

#include <memory>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

struct ExpansionGroupFieldEntryType
{
	// Identifier, Name, NestedTemplate, StaticNestedTemplate: No entry.
	using NoEntry = std::monostate;

	// Value, StaticValue: value type, list size
	using ValueCategorySizePair = std::pair<ValueCategory, size_t>;

	// ForeignMember, StaticForeignMember: foreign group id, list size
	// NestedMember, StaticNestedMember: nested group id, list size
	using StringSizePair = std::pair<std::string, size_t>;

	// Function: function name, return type, [function args...]
	using FunctionSignature = std::tuple<std::string, ValueCategory, std::vector<std::shared_ptr<TemplateTree>>>;
	
	// Alias: sub-property
	using Subproperty = std::shared_ptr<TemplateTree>;
};

using ExpansionGroupFieldEntryVariant = std::variant<
	ExpansionGroupFieldEntryType::NoEntry,
	ExpansionGroupFieldEntryType::ValueCategorySizePair,
	ExpansionGroupFieldEntryType::StringSizePair,
	ExpansionGroupFieldEntryType::FunctionSignature,
	ExpansionGroupFieldEntryType::Subproperty
>;

WRAPPED_TYPE_STRUCT(ExpansionFieldEntryFormat, ExpansionGroupFieldEntryTypeOfFormat)

class ExpansionGroupFieldEntry;

#endif /* EXPANSION_GROUP_FIELD_ENTRY_DECL_HPP */
