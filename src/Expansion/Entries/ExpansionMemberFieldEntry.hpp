//
//  ExpansionFieldEntry.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 7/7/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#ifndef EXPANSION_MEMBER_FIELD_ENTRY_HPP
#define EXPANSION_MEMBER_FIELD_ENTRY_HPP

#include "ExpansionMemberFieldEntryDecl.hpp"

#include "ExpansionFieldEntryDecl.hpp"
#include "TemplateTreeDecl.hpp"
#include "ValueEntryDecl.hpp"

class NestedExpansionMember;

#include <cassert>
#include <iostream>
#include <memory>
#include <variant>
#include <vector>

class ExpansionMemberFieldEntry
{
public:
	static ExpansionMemberFieldEntry makeFromFormat(ExpansionFieldEntryFormat format, bool isStatic);
	template<ExpansionFieldEntryFormat F>
	static ExpansionMemberFieldEntry makeFromFormat(bool isStatic);

	static unsigned char getDivisionCount(ExpansionFieldEntryFormat format);

	ExpansionFieldEntryFormat getFormat() const;
	size_t getListSize() const;
	void setListSize(size_t size);
	bool isStatic() const;
	void isStatic(bool);
	
	template<ExpansionFieldEntryFormat F>
		ExpansionMemberFieldEntryTypeOfFormatT<F>* getMutableEntry();
	template<ExpansionFieldEntryFormat F>
		ExpansionMemberFieldEntryTypeOfFormatT<F> const* getEntry() const;
	template<typename T>
		T* getMutableEntry();
	template<typename T>
		T const* getEntry() const;
	template<ExpansionFieldEntryFormat F>
		void setEntry(ExpansionMemberFieldEntryTypeOfFormatT<F> const& value);
	
private:
	ExpansionFieldEntryFormat format;
	bool _isStatic;
	ExpansionMemberFieldEntryVariant entry;
};

#endif /* EXPANSION_MEMBER_FIELD_ENTRY_HPP */
