//
//  ExpansionFieldEntry.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 7/7/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#include "ExpansionMemberFieldEntry.tpp"

#include "TypeUtils.hpp"

using namespace std;
using FieldFormat = ExpansionFieldEntryFormat;

ExpansionMemberFieldEntry ExpansionMemberFieldEntry::makeFromFormat(FieldFormat format, bool isStatic)
{
	if (isStatic && !ExpansionFieldEntry::isMemberDefinedFormat(format))
	{
		throw invalid_argument("No static variant for identifier and name.");
	}

	switch (format)
	{
	case FieldFormat::Identifier:
		return makeFromFormat<FieldFormat::Identifier>(isStatic);
	case FieldFormat::Name:
		return makeFromFormat<FieldFormat::Name>(isStatic);
	case FieldFormat::Value:
		return makeFromFormat<FieldFormat::Value>(isStatic);
	case FieldFormat::ForeignMember:
		return makeFromFormat<FieldFormat::ForeignMember>(isStatic);
	case FieldFormat::NestedMember:
		return makeFromFormat<FieldFormat::NestedMember>(isStatic);
	case FieldFormat::NestedTemplate:
		return makeFromFormat<FieldFormat::NestedTemplate>(isStatic);
	default:
		throw invalid_argument("Cannot make member entry from format.");
	}
}

unsigned char ExpansionMemberFieldEntry::getDivisionCount(FieldFormat format)
{
	switch (format)
	{
	case FieldFormat::Identifier:
	case FieldFormat::Name:
	case FieldFormat::Value:
	case FieldFormat::ForeignMember:
	case FieldFormat::NestedMember:
	case FieldFormat::NestedTemplate:
		return 1;
	default:
		throw invalid_argument("Division count only applies to member-defined field.");
	}
}

FieldFormat ExpansionMemberFieldEntry::getFormat() const
{
	return format;
}

size_t ExpansionMemberFieldEntry::getListSize() const
{
	switch (format)
	{
	case FieldFormat::Value:
		return get<ExpansionMemberFieldEntryType::ValueList>(entry).size();
	case FieldFormat::ForeignMember:
		return get<ExpansionMemberFieldEntryType::StringList>(entry).size();
	case FieldFormat::NestedMember:
		return get<ExpansionMemberFieldEntryType::NestedMemberList>(entry).size();
	default:
		throw invalid_argument("No list for format.");
	}
}

void ExpansionMemberFieldEntry::setListSize(size_t size)
{
	switch (format)
	{
	case FieldFormat::Value:
		get<ExpansionMemberFieldEntryType::ValueList>(entry).resize(size);
		break;
	case FieldFormat::ForeignMember:
		get<ExpansionMemberFieldEntryType::StringList>(entry).resize(size);
		break;
	case FieldFormat::NestedMember:
		get<ExpansionMemberFieldEntryType::NestedMemberList>(entry).resize(size);
		break;
	default:
		throw invalid_argument("No list for format.");
	}
}

bool ExpansionMemberFieldEntry::isStatic() const
{
	return _isStatic;
}

void ExpansionMemberFieldEntry::isStatic(bool _isStatic)
{
	this->_isStatic = _isStatic;
}
