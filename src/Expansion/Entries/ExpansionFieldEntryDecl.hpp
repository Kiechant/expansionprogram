//
//  ExpansionFieldEntryDecl.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef EXPANSION_FIELD_ENTRY_DECL_HPP
#define EXPANSION_FIELD_ENTRY_DECL_HPP

enum class ExpansionFieldEntryFormat;
class ExpansionFieldEntry;

#endif /* EXPANSION_FIELD_ENTRY_DECL_HPP */
