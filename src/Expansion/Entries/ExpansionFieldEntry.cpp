//
//  ExpansionFieldEntry.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 8/7/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#include "ExpansionFieldEntry.hpp"

#include "MemoryUtils.hpp"

#include <iostream>
#include <sstream>

using namespace std;

vector<string> const& ExpansionFieldEntry::getIdOfFormatMap() 
{
	static auto idOfFormatMap = vector<string>
	{
		"identifer",
		"name",
		"group_identifier",
		"group_name",
		"owner",
		"value",
		"foreign_member",
		"nested_member",
		"nested_template",
		"function",
		"alias",
		"group_nested_template",
		"static_value",
		"static_foreign_member",
		"static_nested_member"
	};
	return idOfFormatMap;
}


unordered_map<string, size_t> const& ExpansionFieldEntry::getFormatIndexOfIdMap() 
{
	static auto formatIndexOfIdMap = MemoryUtils::mapItemIndexes<string, string>(
		ExpansionFieldEntry::getIdOfFormatMap(),
		[](string const& item){ return item; });
	return formatIndexOfIdMap;
}

string const& ExpansionFieldEntry::getStaticFormatPrefix() 
{
	static string staticFormatPrefix = "static_";
	return staticFormatPrefix;
}

string const& ExpansionFieldEntry::getGroupFormatPrefix() 
{
	static string staticFormatPrefix = "group_";
	return staticFormatPrefix;
}

ExpansionFieldEntryFormat ExpansionFieldEntry::formatOfId(string const& id)
{
	string searchId = id;
	string memberDefinedId = memberDefinedIdOfStaticId(id);
	if (memberDefinedId != "")
		searchId = memberDefinedId;
	memberDefinedId = memberDefinedIdOfGroupId(id);
	if (memberDefinedId != "")
		searchId = memberDefinedId;
	return (ExpansionFieldEntryFormat)(getFormatIndexOfIdMap().at(searchId));
}

string ExpansionFieldEntry::idOfFormat(ExpansionFieldEntryFormat format)
{
	return getIdOfFormatMap()[size_t(format)];
}

string ExpansionFieldEntry::nameOfFormat(ExpansionFieldEntryFormat format)
{
	return nameOfId(idOfFormat(format));
}

string ExpansionFieldEntry::nameOfId(string const& id)
{
	stringstream ss;
	bool newWord = true;
	constexpr int lowerToUpper = 'A' - 'a';
	for (auto& ch : id)
	{
		if (ch == '_')
		{
			newWord = true;
			ss << " ";
		}
		else if (newWord)
		{
			ss << char(ch + lowerToUpper);
			newWord = false;
		}
		else
		{
			ss << ch;
		}
	}
	return ss.str();
}

bool ExpansionFieldEntry::isStaticId(string const& id)
{
	string memberDefinedId = memberDefinedIdOfStaticId(id);
	return memberDefinedId != "";
}

string ExpansionFieldEntry::memberDefinedIdOfStaticId(string const& staticId)
{
	return memberDefinedIdOfPrefixedId(staticId, getStaticFormatPrefix());
}


bool ExpansionFieldEntry::isGroupId(string const& id) 
{
	string memberDefinedId = memberDefinedIdOfGroupId(id);
	return memberDefinedId != "";
}

string ExpansionFieldEntry::memberDefinedIdOfGroupId(string const& groupId) 
{
	return memberDefinedIdOfPrefixedId(groupId, getGroupFormatPrefix());
}

bool ExpansionFieldEntry::isExpandableFormat(ExpansionFieldEntryFormat format)
{
	return ((int)format >= (int)ExpansionFieldEntryFormat::Value && (int)format <= (int)ExpansionFieldEntryFormat::NestedTemplate);
}

bool ExpansionFieldEntry::isForeignOrNestedMemberFormat(ExpansionFieldEntryFormat format)
{
	return format == ExpansionFieldEntryFormat::ForeignMember || format == ExpansionFieldEntryFormat::NestedMember;
}

bool ExpansionFieldEntry::isLiteralFormat(ExpansionFieldEntryFormat format)
{
	return ((int)format >= (int)ExpansionFieldEntryFormat::Identifier) || ((int)format <= (int) ExpansionFieldEntryFormat::Value);
}

bool ExpansionFieldEntry::isGroupDefinedFormat(ExpansionFieldEntryFormat format)
{
	return format == ExpansionFieldEntryFormat::Function || format == ExpansionFieldEntryFormat::Alias;
}

bool ExpansionFieldEntry::isMemberDefinedFormat(ExpansionFieldEntryFormat format)
{
	return ((int)format >= (int)ExpansionFieldEntryFormat::Value && (int)format <= (int)ExpansionFieldEntryFormat::NestedTemplate);
}

bool ExpansionFieldEntry::isDefaultMemberContainedFormat(ExpansionFieldEntryFormat format) 
{
	return format == ExpansionFieldEntryFormat::Identifier || format == ExpansionFieldEntryFormat::Name || format == ExpansionFieldEntryFormat::Owner;
}

bool ExpansionFieldEntry::isListedFormat(ExpansionFieldEntryFormat format)
{
	return isMemberDefinedFormat(format) && format != ExpansionFieldEntryFormat::NestedTemplate;
}
bool ExpansionFieldEntry::isDefaultUniqueFormat(ExpansionFieldEntryFormat format)
{
	return format == ExpansionFieldEntryFormat::Identifier || format == ExpansionFieldEntryFormat::Name;
}

bool ExpansionFieldEntry::isUniqueSupportingFormat(ExpansionFieldEntryFormat format)
{
	return isDefaultUniqueFormat(format) && format == ExpansionFieldEntryFormat::Value;
}

bool ExpansionFieldEntry::isInterdependentFormat(ExpansionFieldEntryFormat format)
{
	return format == ExpansionFieldEntryFormat::NestedTemplate || format == ExpansionFieldEntryFormat::Function || format == ExpansionFieldEntryFormat::Alias;
}

string ExpansionFieldEntry::memberDefinedIdOfPrefixedId(string const& prefixedId, string const& prefix) 
{
	if (getFormatIndexOfIdMap().count(prefixedId) == 0)
	{
		throw invalid_argument("Unrecognised format id '" + prefixedId + "'.");
	}
	string memberDefinedId = "";
	if (prefixedId.compare(0, prefix.size(), prefix) == 0)
	{
		memberDefinedId = prefixedId.substr(prefix.size());
		if (getFormatIndexOfIdMap().count(memberDefinedId) == 0)
		{
			throw logic_error("Member defined variant of '" + prefix + "' prefixed id does not exist.");
		}
	}
	return memberDefinedId;
}
