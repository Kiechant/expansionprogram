//
//  Expander.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 15/7/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#include "Expander.hpp"

#include "BasicTermResolver.hpp"
#include "ContextualTermResolver.hpp"
#include "ExpanderOptions.hpp"
#include "ExpansionContext.hpp"
#include "ExpansionContextItem.hpp"
#include "ExpansionDefinition.hpp"
#include "ExpansionGroup.tpp"

// TODO: Separate out
#include "ExpansionFieldEntry.hpp"

#include "ExpansionScheme.hpp"
#include "ExpansionTree.hpp"
#include "ExpansionTreeItem.hpp"
#include "MemberCombination.hpp"
#include "PropertyResolver.tpp"
#include "StdStringConversion.hpp"
#include "TemplateTree.hpp"
#include "Tree.tpp"

#include <algorithm>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <optional>
#include <sstream>

using namespace std;

using FieldFormat = ExpansionFieldEntryFormat;

Expander::Expander(ExpansionDefinition const* definition, filesystem::path const& templateDir, filesystem::path const& expansionDir)
	: definition(definition), templateDir(templateDir), expansionDir(expansionDir)
{
	if (!filesystem::is_directory(templateDir))
		throw invalid_argument("Template directory '" + templateDir.string() + "' is not a directory.");
	if (!filesystem::is_directory(expansionDir))
		throw invalid_argument("Expansion directory '" + expansionDir.string() + "' is not a directory.");
}


/** Single-file Expansions **/

void Expander::expandFile(filesystem::path const& filePath) const
{
	auto tmplPath = filesystem::path(templateDir / filePath);
	if (!filesystem::is_regular_file(tmplPath))
		throw invalid_argument("Template file with subpath '" + string(filePath) + "' is not a file in the template directory.");
	auto tmplModTime = filesystem::last_write_time(tmplPath);

	// Parses template name into syntax tree. Comprises group specifier and expandable name.
	TemplateTree const* tmplNameTree = TemplateTree::parseTemplateName(tmplPath.filename());
	TemplateNode const* tmplNameRoot = tmplNameTree->tree->getRoot();

	// Reads group specifier into a context tree.
	auto groupSpec = TemplateTree::getGroupSpecifier(tmplNameRoot);
	auto expandableName = TemplateTree::getExpandableName(tmplNameRoot);
	auto propertyTokens = TemplateTree::getPropertyTokens(expandableName);

	if (groupSpec == nullptr)
	{
		// Ensures there are no property tokens, since the template name is not expanded.
		if (!propertyTokens.empty())
		{
			throw invalid_argument("Property tokens found in template name when no group specifier has been provided.");
		}
	}

	ExpansionContext* context = nullptr;
	auto expnFiles = vector<pair<unsigned, string>>();
	auto editedFiles = vector<pair<unsigned, string>>();

	auto expanderMode = definition->getOptions()->mode;
	bool doClean = expanderMode == ExpanderOptions::Mode::Clean || expanderMode == ExpanderOptions::Mode::CleanThenExpand;

	if (groupSpec != nullptr)
	{
		// A group specifier is present. Performs an external expansion.

		context = ExpansionContext::readContext(groupSpec, definition);
		
		try
		{
			// Validates the property tokens of the template name expand to unique combinations.
			checkPropertyTokensExpand(context, propertyTokens, true);
		}
		catch (...)
		{
			throw_with_nested(invalid_argument("The template name '" + tmplPath.filename().string() + "' does not expand."));
		}

		// Expands the template file name to the list of names of expanded files.
		auto groups = TemplateTree::getGroups(groupSpec, definition);
		auto expnFileNames = expandFileNames(context, tmplNameTree);
		for (unsigned permNo = 0; permNo < expnFileNames.size(); permNo++)
		{
			expnFiles.push_back(make_pair(permNo, expnFileNames.at(permNo)));
		}
	}
	else
	{
		// No group specifier. Performs a memberless expansion with the same file name.

		context = new ExpansionContext(definition);
		expnFiles.push_back(make_pair(0, TemplateTree::flatten(expandableName) + "." + definition->getScheme()->expansionExtension));
	}

	// Only expands ands writes new contents for each non-updated expansion files.
	// Validates expansion file can be written to that location.
	for (size_t i = 0; i < expnFiles.size(); i++)
	{
		auto expnFilePath = expansionDir / filePath.parent_path() / expnFiles.at(i).second;
		if (doClean || doesFileNeedUpdate(expnFilePath, tmplModTime))
		{
			if (filesystem::exists(expnFilePath) && !filesystem::is_regular_file(expnFilePath))
				throw invalid_argument("Expansion file at '" + string(expnFilePath) + "' exists but is not a regular file, and hence can't be replaced.");
			editedFiles.push_back(expnFiles.at(i));
		}
	}

	if (!editedFiles.empty())
	{
		// Creates the directories required for the expansion file to be stored.
		auto expnFileDir = expansionDir / filePath.parent_path();
		filesystem::create_directories(expnFileDir);

		// Parses template contents into syntax tree.
		TemplateTree const* tmplContentsTree = TemplateTree::parseTemplateContentsAtPath(tmplPath);
		TemplateNode const* tmplContentsRoot = tmplContentsTree->tree->getRoot();

		// TODO: Validate cycles here.

		// Cycles through the member combinations and expands the contents of each non-updated expansion file.
		auto memberCombo = MemberCombination::initFromContext(context);
		auto schemeElement = definition->getScheme()->getElementOfSubdirectory(filePath.parent_path());

		ExpansionTree* expnTree;
		try
		{
			expnTree = ExpansionTree::initWithFileContents(tmplContentsRoot, memberCombo->context, definition, &schemeElement);
		}
		catch (...)
		{
			throw_with_nested(runtime_error("Error in generating expansion tree for template at: " + filePath.string()));
		}

		unsigned permNo = 0;
		
		for (size_t nextInd = 0; nextInd < editedFiles.size(); nextInd++)
		{
			// Cycles the member combination until the next file requiring an update.
			unsigned nextPerm = editedFiles.at(nextInd).first;
			while (permNo != nextPerm)
			{
				memberCombo->getNext();
				permNo++;
			}
			
			// Expands this member combination into a new file.
			string expnFilePath = expnFileDir / editedFiles.at(nextInd).second;
			expandContentsOfFile(expnFilePath, *expnTree);
		}

		delete expnTree;
		delete memberCombo;
		delete tmplContentsTree;
	}

	delete context;
	delete tmplNameTree;
}

vector<string> Expander::expandFileNames(
	ExpansionContext* context,
	TemplateTree const* tmplNameTree) const
{
	auto expnFileNames = vector<string>();

	auto memberCombo = MemberCombination::initFromContext(context);
	do
	{
		auto expandableName = TemplateTree::getExpandableName(tmplNameTree->tree->getRoot());
		auto expn = expandRegionAsString(memberCombo, expandableName);
		expnFileNames.push_back(expn + "." + definition->getScheme()->expansionExtension);
	}
	while (memberCombo->getNext());

	delete memberCombo;

	return expnFileNames;
}

string Expander::expandRegionAsString(
	MemberCombination const* memberCombo,
	TemplateNode const* expandable) const
{
	stringstream ss;
	expandRegionIntoStream(ss, memberCombo, expandable);
	return ss.str();
}

void Expander::expandRegionIntoStream(
	ostream& os,
	MemberCombination const* memberCombo,
	TemplateNode const* expandable) const
{
	auto tokens = TemplateTree::getPropertyTokens(expandable);
	auto replacements = expandPropertyTokens(memberCombo, tokens);
	mergeReplacementsIntoStream(os, replacements, expandable);
}

vector<string> Expander::expandPropertyTokens(
	MemberCombination const* memberCombo,
	vector<TemplateNode const*> const& propertyTokens) const
{
	vector<string> replacements;
	for (auto propertyToken : propertyTokens)
	{
		string repl = PropertyResolver(ContextualTermResolver(definition, memberCombo->context))
			.resolveFinalValueString(ContextualEntryReference::initFromPropertyToken(memberCombo->context, propertyToken),
			TemplateTree::getProperty(propertyToken), 0);
		replacements.push_back(repl);
	}
	return replacements;
}

string Expander::mergeReplacementsAsString(
	vector<string> const& replacements,
	TemplateNode const* expandable) const
{
	stringstream ss;
	mergeReplacementsIntoStream(ss, replacements, expandable);
	return ss.str();
}

void Expander::mergeReplacementsIntoStream(
	ostream& os,
	vector<string> const& replacements,
	TemplateNode const* expandable) const
{
	size_t tokenNo = 0;
	for (size_t i = 0; i < expandable->getChildCount(); i++)
	{
		TemplateNode const* node = expandable->getChild(i);
		if (node->isLeaf())
		{
			os << node->getItem();
		}
		else if (node->getItem() == "token")
		{
			os << replacements.at(tokenNo);
			tokenNo++;
		}
	}
}

void Expander::expandContentsOfFile(string const& filePath,
	ExpansionTree const& expnTree) const
{
	auto ofs = ofstream(filesystem::path(filePath));
	expandRegionIntoStream(ofs, expnTree.tree->getRoot());
}

void Expander::expandRegionIntoStream(
	ostream& os,
	TreeNode<ExpansionTreeItem>* rootNode) const
{
	size_t expansionDepth = 0;
	stack<stack<Tree<ExpansionTreeItem>::DepthNode>> traversals;
	stack<MemberCombination*> memberCombos;
	stack<TreeNode<ExpansionTreeItem>*> traversalRoots;
	traversals.push(Tree<ExpansionTreeItem>::traverseInitDepthFirst(rootNode));
	bool doSkipNext = true;

	auto [node, depth] = Tree<ExpansionTreeItem>::traverseNextDepthFirst(&traversals.top());

	while (!traversals.empty())
	{
		while (node != nullptr)
		{
			if (!doSkipNext)
			{
				auto const& expnItem = node->getItem();
				auto itemType = expnItem.getItemType();
				if (itemType == ExpansionTreeItem::ItemType::Expansion)
				{
					// Enters an internal expansion. Initialises a nested traversal.
					expansionDepth++;
					traversals.push(Tree<ExpansionTreeItem>::traverseInitDepthFirst(node));
					traversalRoots.push(node);
					memberCombos.push(MemberCombination::initFromContext(node->getItem().getContext()));
					doSkipNext = true;
				}
				else if (itemType == ExpansionTreeItem::ItemType::Replacement)
				{
					// Encounters a replacement. Resolves the value beginning from the first unresolved term.
					os << PropertyResolver<ContextualEntryReference>(ContextualTermResolver(definition, expnItem.getContext()))
						.resolveFinalValueString(expnItem.getContextualEntryRef(),
							TemplateTree::getProperty(expnItem.getPropertyToken()),
							expnItem.getBeginResolution());
				}
				else if (itemType == ExpansionTreeItem::ItemType::Literal)
				{
					os << expnItem.getReplacement();
				}
				else
				{
					throw invalid_argument("Unimplemented substitution or nested template.");
				}
			}
			else doSkipNext = false;
			tie(node, depth) = Tree<ExpansionTreeItem>::traverseNextDepthFirst(&traversals.top());
		}

		// Cycles the member combination while combinations exist for an internal expansion.
		// Otherwise, climbs to the outer traversal.
		bool hasNextMemberCombo = false;
		if (expansionDepth > 0)
		{
			hasNextMemberCombo = memberCombos.top()->getNext();
		}

		if (!hasNextMemberCombo)
		{
			traversals.pop();
			if (expansionDepth > 0)
			{
				auto traversalRoot = traversalRoots.top();
				auto skipChildrenOfInternalExpansion = [traversalRoot](TreeNode<ExpansionTreeItem> const* node)
				{
					return node->getParent() == traversalRoot;
				};
				tie(node, depth) = Tree<ExpansionTreeItem>::traverseNextDepthFirst(&traversals.top(), skipChildrenOfInternalExpansion);
				traversalRoots.pop();
				delete memberCombos.top();
				memberCombos.pop();
			}
			expansionDepth--;
		}
		else
		{
			traversals.top() = Tree<ExpansionTreeItem>::traverseInitDepthFirst(traversalRoots.top());
			tie(node, depth) = Tree<ExpansionTreeItem>::traverseNextDepthFirst(&traversals.top());
			doSkipNext = true;
		}
	}
}

bool Expander::doesFileNeedUpdate(std::filesystem::path const& filePath,
	std::filesystem::file_time_type const& sinceTime)
{
	if (filesystem::is_regular_file(filePath))
	{
		auto lastWriteTime = filesystem::last_write_time(filePath);
		return sinceTime > lastWriteTime;
	}
	return true;
}


/** Property Token Validation **/

void Expander::checkPropertyTokensExpand(
	ExpansionContext* context,
	vector<TemplateNode const*> const& propertyTokens, bool asUnique) const
{
	auto groups = context->getGroups();
	auto entryDepthNodes = Tree<ExpansionContextNode*>::traverseAll(TreeTraversalMode::DepthFirst, context->expansionsView->getRoot());

	auto indicesOfEntries = MemoryUtils::mapItemIndexes<
		ExpansionContextNode*, Tree<ExpansionContextNode*>::DepthNode>(
			entryDepthNodes, [](auto const& node){ return node.first->getItem(); });

	auto tokensOfEntry = vector<vector<TemplateNode const*>>(entryDepthNodes.size());
	auto doesExpandAsUnique = vector<bool>(entryDepthNodes.size());

	// Maps each property token to the expansion it references.
	// Checks whether the property token expands to a unique value.
	for_each(propertyTokens.begin(), propertyTokens.end(),
		[&, this](auto propertyToken)
		{
			auto property = TemplateTree::getProperty(propertyToken);

			optional<ContextualEntryReference> entryRef;
			try
			{
				// Dereferences the group to a unique index in the specifier, and the associated context node.
				entryRef = ContextualEntryReference::initFromPropertyToken(context, propertyToken);
			}
			catch (...)
			{
				throw_with_nested(invalid_argument("Cannot identify which group '" + TemplateTree::flatten(propertyToken) + "' refers to."));
			}

			// Resolves the expansion context node which is the nearest ancestor to the property.
			auto resolvedRef = PropertyResolver<ContextualEntryReference>(ContextualTermResolver(definition, context))
				.resolveFinalTerm(entryRef.value(), property, 0).value();
			size_t entryIndex = indicesOfEntries.at(resolvedRef.getContextRef()->getContextNode());
			tokensOfEntry.at(entryIndex).push_back(propertyToken);

			// Determines whether the resolved entry references a unique field.
			if (asUnique && resolvedRef.getEntryRef()->getGroup()->isUnique(resolvedRef.getEntryRef()->getField()))
				doesExpandAsUnique.at(entryIndex) = true;
		});
		
	// Throws error if entry is not referenced or does not produce unique expansions.
	// Ignores group specifier root at the zeroth index.
	for (size_t i = 1; i < entryDepthNodes.size(); i++)
	{
		// TODO: Reverse entry node to diagnostic string.
		// e.g. nested expansion x(a.b) in x(a.b(c),d.e)
		auto const& entryItem = entryDepthNodes.at(i).first->getItem()->getItem();
		string errorPrefix = "The entry for " + entryItem.getEntryRef().getGroup()->groupInfo() + " for field '" +
			entryItem.getEntryRef().getField() + "' at index " + to_string(i);

		if (tokensOfEntry.at(i).empty())
		{
			throw errorPrefix + " is not referenced by any property token.";
		}
		else if (asUnique && !doesExpandAsUnique.at(i))
		{
			// Produces helpful error message for no unique expansions
			// for a group that is uniquely referenced.
			string errMsg = errorPrefix + " does not have a unique expansion.\n";
			errMsg += "Candidates are:\n";
			for_each(tokensOfEntry.at(i).begin(), tokensOfEntry.at(i).end(),
				[&errMsg](TemplateNode const* token)
				{
					errMsg += "\t" + TemplateTree::flatten(token) + "\n";
				});
			throw errMsg;
		}
	}
}


/** Scheme Expansion **/

void Expander::expandFilesInDirectory(filesystem::path const& dir) const
{
	for (auto const& dirEntry : filesystem::directory_iterator(templateDir / dir))
	{
		if (filesystem::is_regular_file(dirEntry) && dirEntry.path().extension() == "." + definition->getScheme()->templateExtension)
		{
			expandFile(dir / dirEntry.path().filename());
		}
	}
}

void Expander::expandWithScheme(ExpansionScheme const* scheme) const
{
	size_t elCount = scheme->getElementCount();
	for (size_t i = 0; i < elCount; i++)
	{
		auto const& el = scheme->getElement(i);
		filesystem::path templateSubpath = el.templateSubpath != "" ? el.templateSubpath : ".";
		if (filesystem::is_directory(templateDir / templateSubpath))
			expandFilesInDirectory(templateSubpath);
		else
			cerr << "Subdirectory '" << templateSubpath.string() << "' specified in scheme " <<
				"does not exist in template directory '" << templateDir.string() << "'." << endl;
	}
}

string Expander::getNextFileInScheme(ExpansionScheme* scheme) const
{
	throw "Unimplemented";
}
