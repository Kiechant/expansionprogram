//
//  UserFunction.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef USER_FUNCTION_HPP
#define USER_FUNCTION_HPP

#include "ValueEntryDecl.hpp"

#include <cstdlib>
#include <string>
#include <vector>

using EpValue = ValueEntry;
using EpValueCategory = ValueCategory;

template<EpValueCategory C>
EpValue makeEpValue(ValueTypeOfCategoryT<C> const& value);

class EpArgumentList
{
public:
	EpArgumentList(std::vector<EpValue> const& arguments);

	template<EpValueCategory C>
	ValueTypeOfCategoryT<C> getNext();

private:
	std::vector<EpValue> arguments;
	size_t nextIndex = 0;
};

#endif // USER_FUNCTION_HPP
