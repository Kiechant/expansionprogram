//
//  ExpanderOptions.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/4/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef EXPANDER_OPTIONS_HPP
#define EXPANDER_OPTIONS_HPP

#include <cstdint>
#include <filesystem>
#include <string>
#include <vector>

class ExpanderOptions
{
public:
	ExpanderOptions();

	enum class Mode
	{
		Clean,
		CleanThenExpand,
		Expand
	};

	enum class PathCategory
	{
		Groups,
		Members,
		Functions,
		Substitutions
	};

	Mode mode = Mode::Expand;
	uint8_t verbosity = 0;
	bool doLogFailedOperation = false;
	bool doContinueAfterFailedOperation = false;
	bool doRegenerateExpansions = false;

	std::string schemePath = "scheme.json";
	std::string universalDictName = "universal";

	// Returns a copy of the list of paths of the path category.
	std::vector<std::filesystem::path> getPaths(PathCategory pathCategory) const;

	// Adds a path to the list of paths of the path category.
	// If this is the first time a path is added, the default path/s is replaced.
	void addPath(PathCategory pathCategory, std::filesystem::path const& path);

private:
	std::vector<std::filesystem::path> groupsPaths;
	std::vector<std::filesystem::path> membersPaths;
	std::vector<std::filesystem::path> functionsPaths;
	std::vector<std::filesystem::path> substitutionsPaths;

	// Whether the path list is default. Initialised to true.
	// Enum position corresponds to path list.
	std::vector<bool> _isPathListDefault;

	// Returns a pointer to the list of paths of the path category.
	std::vector<std::filesystem::path>* getMutPointerToPathList(PathCategory pathCategory);
	std::vector<std::filesystem::path> const* getPointerToPathList(PathCategory pathCategory) const;

	// Returns whether the list of paths of the path category is default or custom.
	// A path list is custom once a path is added.
	bool isPathListDefault(PathCategory pathCategory);
};

#endif /* EXPANDER_OPTIONS_HPP */
