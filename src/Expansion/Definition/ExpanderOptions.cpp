//
//  ExpanderOptions.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/4/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "ExpanderOptions.hpp"

#include "TypeUtils.hpp"

using namespace std;
namespace fs = filesystem;

ExpanderOptions::ExpanderOptions()
{
	groupsPaths.push_back("groups");
	membersPaths.push_back("members");
	functionsPaths.push_back("functions");
	substitutionsPaths.push_back("substitutions");

	_isPathListDefault = vector<bool>(4, true);
}

vector<fs::path> ExpanderOptions::getPaths(PathCategory pathCategory) const
{
	return *getPointerToPathList(pathCategory);
}

void ExpanderOptions::addPath(PathCategory pathCategory, fs::path const& path)
{
	vector<fs::path>* pathList = getMutPointerToPathList(pathCategory);
	if (isPathListDefault(pathCategory))
	{
		pathList->clear();
		_isPathListDefault.at((int)pathCategory) = false;
	}
	pathList->push_back(path);
}

vector<fs::path>* ExpanderOptions::getMutPointerToPathList(PathCategory pathCategory)
{
	return MUTABLE_FROM_CONST_MEMBER_FUNC(this, getPointerToPathList, pathCategory);
}

vector<fs::path> const* ExpanderOptions::getPointerToPathList(PathCategory pathCategory) const
{
	switch (pathCategory)
	{
	case PathCategory::Groups:
		return &groupsPaths;
	case PathCategory::Members:
		return &membersPaths;
	case PathCategory::Functions:
		return &functionsPaths;
	case PathCategory::Substitutions:
		return &substitutionsPaths;
	}
}

bool ExpanderOptions::isPathListDefault(PathCategory pathCategory)
{
	return _isPathListDefault.at((int)pathCategory);
}
