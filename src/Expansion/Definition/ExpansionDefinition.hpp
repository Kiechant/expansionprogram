//
//  ExpansionDefinition.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/4/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef EXPANSION_DEFINITION_HPP
#define EXPANSION_DEFINITION_HPP

#include "EntryReference.hpp"
#include "GraphDecl.hpp"
#include "HashingDecl.hpp"
#include "TemplateTreeDecl.hpp"

class ExpanderOptions;
class ExpansionCategory;
class ExpansionGroup;
class ExpansionMember;
class ExpansionMemberFieldMap;
class ExpansionScheme;
class NestedExpansionGroup;
class SubstitutionDictionary;
class SubstitutionReference;

#include <filesystem>
#include <functional>
#include <map>
#include <memory>
#include <optional>
#include <set>
#include <vector>

#include <nlohmann/json.hpp>

class ExpansionDefinition
{
public:
	/** Subclasses **/

	enum class ReservedField
	{
		GroupId,
		GroupName,
		Categories,
		Id,
		Name,
		GroupOwner,
		Arguments,
		Owner,
		Index,
		UniqueFields
	};

	struct GroupContext
	{
		ExpansionGroup* group = nullptr;
		std::string field;
		nlohmann::json::const_iterator entryBegin;
		nlohmann::json::const_iterator entryEnd;

		GroupContext();
		GroupContext(ExpansionGroup* group,
			std::string field,
			nlohmann::json::const_iterator entryBegin,
			nlohmann::json::const_iterator entryEnd) :
			group(group), field(field), entryBegin(entryBegin), entryEnd(entryEnd) {}

		size_t entrySize() const
		{
			return entryEnd - entryBegin;
		}
	};

	struct MemberContext
	{
		ExpansionMemberFieldMap* fieldMap = nullptr;
		std::string field;
		nlohmann::json::const_iterator entryBegin;
		nlohmann::json::const_iterator entryEnd;

		MemberContext();
		MemberContext(ExpansionMemberFieldMap* fieldMap,
			std::string field,
			nlohmann::json::const_iterator entryBegin,
			nlohmann::json::const_iterator entryEnd) :
			fieldMap(fieldMap), field(field), entryBegin(entryBegin), entryEnd(entryEnd) {}

		static MemberContext staticContextFromGroupContext(GroupContext const& cxt, size_t offset, bool isListed);

		size_t entrySize() const
		{
			return entryEnd - entryBegin;
		}
	};

	using GroupMap 				= std::map<std::string, ExpansionGroup*>;
	using NestedGroupMap 		= std::map<std::string, NestedExpansionGroup*>;
	using MemberMap 			= std::map<std::string, ExpansionMember*>;
	using CategoryMap 			= std::map<std::string, ExpansionCategory*>;
	using SubstitutionDictMap 	= std::map<std::string, SubstitutionDictionary*>;


	/** Constructors and Destructors **/

	ExpansionDefinition(std::filesystem::path const& location, ExpanderOptions const* options);
	ExpansionDefinition(ExpansionDefinition const&);
	~ExpansionDefinition();
	ExpansionDefinition& operator=(ExpansionDefinition const&);


	/** Reserved Field Queries **/

	static std::string idOfReservedField(ReservedField field);
	static std::optional<ReservedField> reservedFieldOfId(std::string const& id);
	static bool isGroupIdOrNameField(ReservedField field);
	static bool isIdOrNameField(ReservedField field);
	static bool isGroupField(ReservedField field);
	static bool isNestedGroupField(ReservedField field);
	static bool isMemberField(ReservedField field);
	static bool isNestedMemberField(ReservedField field);


	/** Basic Accessors **/

	std::filesystem::path const& getDefinitionPath() const;
	std::filesystem::path getFullPath(std::filesystem::path const& subfolder);
	std::vector<std::filesystem::path> getFullPaths(std::vector<std::filesystem::path> const& subfolders);

	ExpanderOptions const* getOptions() const;
	ExpansionScheme const* getScheme() const;

	std::vector<ExpansionGroup const*> getAllGroups() const;
	ExpansionGroup const* getAnyGroupOfId(std::string const& id) const;
	ExpansionGroup const* getForeignGroupOfId(std::string const& id) const;
	NestedExpansionGroup const* getNestedGroupOfId(std::string const& id) const;
	bool isAnyGroup(std::string const& id) const;
	bool isForeignGroup(std::string const& id) const;
	bool isNestedGroup(std::string const& id) const;

	ExpansionMember const* getMemberOfId(std::string const& id) const;
	ExpansionCategory const* getCategoryOfId(std::string const& id) const;
	SubstitutionDictionary const* getSubstitutionDictOfId(std::string const& id) const;

protected:
	// Protected for testing purposes.
	ExpansionDefinition();


	/** Subdirectory Reading **/

	// Made an instance method to pass temporary variables, including 'argumentsOfEntry'.
	std::tuple<GroupMap, CategoryMap, NestedGroupMap> readGroupsAndCategories(
		std::filesystem::path const& definitionPath,
		std::vector<std::filesystem::path> const& groupsLocation
	);

	static MemberMap readMembersOfGroup(
		std::filesystem::path const& definitionPath,
		std::vector<std::filesystem::path> const& membersLocation,
		ExpansionGroup* group
	);
	static void readFunctionsOfGroup(
		std::filesystem::path const& definitionPath,
		std::vector<std::filesystem::path> const& functionsLocation,
		ExpansionGroup* group
	);
	static SubstitutionDictMap readSubstitutionDictionaries(
		std::filesystem::path const& definitionPath,
		std::vector<std::filesystem::path> const& substitutionsLocation,
		std::string const& universalDictName,
		std::set<std::string> const& includeNames
	);


	/** Single Json Reading **/

	// Made an instance method to pass temporary variables, including 'argumentsOfEntry'.
	void readGroupAndCategory(GroupMap* groupOfId, CategoryMap* categoryOfId,  NestedGroupMap* nestedGroupOfId,
		std::string const& groupTitle, nlohmann::json const& defn);

	static void readMemberOfGroup(MemberMap* memberOfId, ExpansionGroup* group,
		std::string const& memberTitle, nlohmann::json const& defn);


	/** Group Reading Helpers **/

	static void readGroupValue(GroupContext const& context);
	static void readGroupForeignMember(GroupContext const& context);
	static void readGroupNestedMember(GroupContext const& context);
	static void readGroupNestedTemplate(GroupContext const& context);
	static void readFunction(GroupContext const& context);
	static void readAlias(GroupContext const& context);
	static void readStaticValue(GroupContext const& context);
	static void readStaticForeignMember(GroupContext const& context);
	static std::vector<std::vector<std::vector<ValueEntry>>> readStaticNestedMember(GroupContext const& context);
	static void readStaticNestedTemplate(GroupContext const& context);


	/** Member Reading Helpers **/

	static void readMemberValue(MemberContext const& context);
	static void readMemberForeignMember(MemberContext const& context);
	static void readMemberNestedMember(MemberContext const& context);
	static void readMemberNestedTemplate(MemberContext const& context);


	/** Nested Group Reading Helpers **/

	static void readNestedGroupId(GroupContext const& context);
	static void readNestedGroupName(GroupContext const& context);


	/** Validation Methods **/

	// Validates no circular dependencies exist between entries in the definition,
	// including function arguments, aliases and nested template parameters.
	void validateNoCyclesInEntries();

	// Validates substitution dictionaries contain valid references to substitution dictionaries
	// loaded by the particular scheme, and that there are no circular depedencies between them.
	void validateSubstitutionDicts();

	// Validates no circular dependencies exist between substitution dictionaries loaded
	// into the definition by the particular scheme.
	void validateNoCyclesInSubstitutionDicts();

	static void validateUniqueFields(ExpansionGroup const& group, MemberMap const& memberOfId);
	static void validateUniqueField(ExpansionGroup const& group, std::string const& field, MemberMap const& memberOfId);

	// Validates that a scheme makes reference to existing substitution dictionaries in the definition.
	static std::vector<std::string> validateSchemeReferencesExistingSubstitutionDictionaries(
		ExpansionScheme const& scheme,
		SubstitutionDictMap const& dictMap);


	/** Misc **/

	// Gets any item in the definition which is stored in a map from the ID to the item.
	// Throws an error if the item doesn't exist.
	template<typename T>
	T const* getItemOfId(
		std::map<std::string, T*> const& itemOfId,
		std::string const& id,
		std::string const& errItemName
	) const;

	// Formats the error when no item is found.
	static std::out_of_range noItemInMapError(std::string const& itemName, std::string const& id);

	// Throws an error if the number of JSON elements in an entry is not equal to the number
	// expected by that format. For instance, it is expected to have two elements in the 'value'
	// format for a group, the format and type name: ['value', 'string']
	static void checkFormatArgsSizeEquals(size_t entrySize, size_t expectedFormatArgsSize);

	// Maps the nested groups onto their owner groups. This way, the nested group can be accessed
	// directly from the group without using the expansion definition.
	void mapNestedGroupsToOwner();

	// Gets either the interdependent group or member fields from the expansion group.
	static std::vector<std::string> getInterdependentFields(ExpansionGroup const* group, bool doMemberFields);

	// Gets either the interdependent group or member entry references from the expansion group,
	// which includes the combination of each index.
	std::vector<EntryReference> getInterdependentEntries(ExpansionGroup const* group, bool doMemberFields);

	// Gets all the interdependent group or member entries, which includes the combination of each index.
	std::vector<EntryReference> getAllInterdependentEntries(bool doMemberFields);

	// Gets the subproperties of an interdependent entry.
	static std::vector<TemplateNode const*> getSubproperties(EntryReference const& entryRef);

	// Reads a new entry reference into the dependency tree by adding the dependencies of its subproperties.
	void addEntryDependenciesToGraph(EntryReference const& entryRef);

	// Resolves the subproperty and adds an edge from the source entry to the target entry.
	// If the source entry is a member reference, joins it to an existing group reference.
	void addSubpropertyDependenciesToGraph(EntryReference const& entryRef, TemplateNode const* subproperty);

	// Adds a dependency between a member reference and a group reference.
	// If the entry is member-contained, the group entry is dependent on the member entry.
	// Otherwise, the member entry is dependent on the group entry.
	void addMemberGroupDependency(EntryReference const& memberRef);

public:
	// TODO: Public for testing purposes. Hide from public interface.
	static void mapNestedGroupsToOwner(GroupMap* groupOfId, NestedGroupMap* nestedGroupOfId);

private:
	static std::vector<std::string> const& getIdOfReservedFieldMap();

	std::filesystem::path definitionPath;

	ExpanderOptions const* options = nullptr;
	ExpansionScheme* scheme = nullptr;
	GroupMap foreignGroupOfId;
	NestedGroupMap nestedGroupOfId;
	MemberMap memberOfId;
	CategoryMap categoryOfId;
	SubstitutionDictMap substitutionDictOfId;

	std::shared_ptr<Graph<EntryReference>> dependencyGraph;
	std::shared_ptr<Graph<SubstitutionReference>> substitutionDependencyGraph;

	// Used to persist the arguments of a static nested member before the nested group has been read.
	// The arguments are used later to instantiate the member inside the group to which the reference belongs.
	std::shared_ptr<std::unordered_map<
		EntryReference,
		std::shared_ptr<
			std::vector<std::vector<std::vector<ValueEntry>>>>, ToHash<EntryReference>>>
	argumentsOfEntry;
};

template<typename T>
T const* ExpansionDefinition::getItemOfId(std::map<std::string, T*> const& itemOfId,
	std::string const& id, std::string const& errItemName) const
{
	auto it = itemOfId.find(id);
	if (it == itemOfId.end())
		throw noItemInMapError(errItemName, id);
	return &dynamic_cast<T const&>(*it->second);
}

#endif /* EXPANSION_DEFINITION_HPP */
