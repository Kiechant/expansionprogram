//
//  ExpansionSchemeDecl.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 25/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef EXPANSION_SCHEME_DECL_HPP
#define EXPANSION_SCHEME_DECL_HPP

#include <filesystem>
#include <unordered_map>
#include <vector>

enum class ExpansionOperation;
struct ExpansionSchemeElement;
struct ExpansionSchemeDictionarySet;
struct ExpansionPipeline;

using ExpansionSchemeStringToPipeline = std::unordered_map<std::string, ExpansionPipeline>;
using ExpansionSchemeStringToDictionarySet = std::unordered_map<std::string, ExpansionSchemeDictionarySet>;

class ExpansionScheme;

#endif /* EXPANSION_SCHEME_DECL_HPP */
