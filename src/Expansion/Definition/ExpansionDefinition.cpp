//
//  ExpansionDefinition.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/4/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "ExpansionDefinition.hpp"

#include "BasicTermResolver.hpp"
#include "ExpanderOptions.hpp"
#include "ExpansionCategory.hpp"
#include "ExpansionFieldEntry.hpp"
#include "ExpansionGroup.tpp"
#include "ExpansionMember.hpp"
#include "ExpansionMemberFieldMap.tpp"
#include "ExpansionScheme.hpp"
#include "Graph.tpp"
#include "FileUtils.hpp"
#include "MemoryUtils.hpp"
#include "NestedExpansionMember.hpp"
#include "NestedExpansionGroup.hpp"
#include "PropertyResolver.tpp"
#include "SubstitutionDictionary.hpp"
#include "SubstitutionReference.hpp"
#include "TemplateTree.hpp"
#include "Tree.tpp"
#include "ValueEntry.tpp"

#include <dlfcn.h>
#include <exception>
#include <fstream>
#include <map>

#include <set>

using namespace std;
namespace fs = filesystem;
using json_t = nlohmann::json;
using Entry = ExpansionFieldEntry;
using Format = ExpansionFieldEntryFormat;

ExpansionDefinition::MemberContext ExpansionDefinition::MemberContext::staticContextFromGroupContext(GroupContext const& cxt, size_t offset, bool isListed)
{
	// Assures only a single array.
	assert(cxt.entrySize() - offset == 1);
	json_t::const_iterator entriesBegin, entriesEnd;
	if (isListed)
	{
		tie(entriesBegin, entriesEnd) = FileUtils::rangeFromJsonArray(*(cxt.entryBegin + offset));
	}
	else
	{
		entriesBegin = cxt.entryBegin + offset;
		entriesEnd = cxt.entryEnd;
	}
	return MemberContext(cxt.group->getMutableStaticFieldMap(), cxt.field, entriesBegin, entriesEnd);
}

vector<string> const& ExpansionDefinition::getIdOfReservedFieldMap()
{
	static auto idOfReservedFieldMap = vector<string>
	{
		"group_id",
		"group_name",
		"categories",
		"id",
		"name",
		"group_owner",
		"arguments",
		"owner",
		"index",
		"unique_fields"
	};
	return idOfReservedFieldMap;
}


/** Reserved Field Queries **/

string ExpansionDefinition::idOfReservedField(ReservedField field)
{
	return getIdOfReservedFieldMap()[(size_t)field];
}

optional<ExpansionDefinition::ReservedField> ExpansionDefinition::reservedFieldOfId(string const& id)
{
	auto reservedIt = find(getIdOfReservedFieldMap().begin(), getIdOfReservedFieldMap().end(), id);
	if (reservedIt == getIdOfReservedFieldMap().end())
		return nullopt;
	return make_optional((ReservedField)(reservedIt - getIdOfReservedFieldMap().begin()));
}

bool ExpansionDefinition::isGroupIdOrNameField(ExpansionDefinition::ReservedField field)
{
	return field == ReservedField::GroupId || field == ReservedField::GroupName;
}

bool ExpansionDefinition::isIdOrNameField(ExpansionDefinition::ReservedField field)
{
	return field == ReservedField::Id || field == ReservedField::Name;
}

bool ExpansionDefinition::isGroupField(ExpansionDefinition::ReservedField field)
{
	return isGroupIdOrNameField(field) ||
		field == ReservedField::Categories || field == ReservedField::UniqueFields;
}

bool ExpansionDefinition::isNestedGroupField(ExpansionDefinition::ReservedField field)
{
	return isGroupIdOrNameField(field) ||
		field == ReservedField::GroupOwner || field == ReservedField::Arguments;
}

bool ExpansionDefinition::isMemberField(ExpansionDefinition::ReservedField field)
{
	return isIdOrNameField(field);
}

bool ExpansionDefinition::isNestedMemberField(ExpansionDefinition::ReservedField field)
{
	return isIdOrNameField(field) ||
		field == ReservedField::Owner || field == ReservedField::Index;
}


/** Constructors and Destructors **/

ExpansionDefinition::ExpansionDefinition()
{
	argumentsOfEntry = make_shared<unordered_map<EntryReference,
		shared_ptr<vector<vector<vector<ValueEntry>>>>,
			ToHash<EntryReference>>>();
	dependencyGraph = make_shared<Graph<EntryReference>>();
	substitutionDependencyGraph = make_shared<Graph<SubstitutionReference>>();
}

ExpansionDefinition::ExpansionDefinition(fs::path const& location, ExpanderOptions const* options)
	: definitionPath(location), options(options), scheme()
{
	argumentsOfEntry = make_shared<unordered_map<EntryReference,
		shared_ptr<vector<vector<vector<ValueEntry>>>>,
			ToHash<EntryReference>>>();
	dependencyGraph = make_shared<Graph<EntryReference>>();
	substitutionDependencyGraph = make_shared<Graph<SubstitutionReference>>();

	this->scheme = new ExpansionScheme(fs::path(location) / options->schemePath, options->universalDictName);

	auto groupsAndCats = readGroupsAndCategories(definitionPath, options->getPaths(ExpanderOptions::PathCategory::Groups));
	foreignGroupOfId = move(get<0>(groupsAndCats));
	categoryOfId = move(get<1>(groupsAndCats));
	nestedGroupOfId = move(get<2>(groupsAndCats));

	// Maps the nested groups onto the containing groups.
	mapNestedGroupsToOwner();

	// TODO: Refactor so group reading, and static field instantiation are separate passes across the files.
	// Instantiates the static nested members for each group.
	for (ExpansionGroup const* group : getAllGroups())
	{
		for (string const& field : group->getFields())
		{
			if (group->getFormat(field) == Format::NestedMember && group->isStatic(field))
			{
				auto entryRef = EntryReference(group, field, numeric_limits<size_t>::max());
				auto args = argumentsOfEntry->at(entryRef);
				for (size_t i = 0; i < args->size(); i++)
				{
					auto* staticFieldMap = const_cast<ExpansionGroup*>(group)->getMutableStaticFieldMap();
					staticFieldMap->createNestedMember(field, args->at(i), i);
					staticFieldMap->getMutableNestedMember(field, i)->getMutableFieldMap()->tryCastAllValuesToRequestedType();

					try
					{
						staticFieldMap->getNestedMember(field, i)->getFieldMap()->validateAllValuesHaveRequestedType();
					}
					catch (...)
					{
						throw_with_nested(invalid_argument(
							"Error initialising value of static nested member '" + field + "' of " + group->groupInfo() + "."
						));
					}
				}
			}
		}
	}

	// Reads the members and functions of each group.
	for (auto& item : foreignGroupOfId)
	{
		ExpansionGroup* group = item.second;
		auto memberOfIdInGroup = readMembersOfGroup(definitionPath, options->getPaths(ExpanderOptions::PathCategory::Members), group);
		memberOfId.insert(memberOfIdInGroup.begin(), memberOfIdInGroup.end());
		readFunctionsOfGroup(definitionPath, options->getPaths(ExpanderOptions::PathCategory::Functions), group);
	}

	validateNoCyclesInEntries();

	substitutionDictOfId = readSubstitutionDictionaries(
		definitionPath,
		options->getPaths(ExpanderOptions::PathCategory::Substitutions),
		options->universalDictName,
		scheme->getAllDictionaryIds());

	validateSubstitutionDicts();

	auto validityMsgs = validateSchemeReferencesExistingSubstitutionDictionaries(*scheme, substitutionDictOfId);
	if (!validityMsgs.empty())
	{
		stringstream ss;
		ss << "Error in scheme:\n";
		for (string const& msg : validityMsgs)
		{
			ss << "\t" << msg << "\n";
		}
		throw invalid_argument(ss.str());
	}
}

ExpansionDefinition::ExpansionDefinition(ExpansionDefinition const& other)
{
	*this = other;
}

ExpansionDefinition::~ExpansionDefinition()
{
	delete scheme;

	MemoryUtils::deepDeleteMap(foreignGroupOfId);
	MemoryUtils::deepDeleteMap(nestedGroupOfId);
	MemoryUtils::deepDeleteMap(memberOfId);
	MemoryUtils::deepDeleteMap(categoryOfId);
	MemoryUtils::deepDeleteMap(substitutionDictOfId);
}

ExpansionDefinition& ExpansionDefinition::operator=(ExpansionDefinition const& other)
{
	this->options = other.options;
	this->scheme = new ExpansionScheme(*other.scheme);

	this->foreignGroupOfId 		= MemoryUtils::deepCopyMap(other.foreignGroupOfId);
	this->nestedGroupOfId 		= MemoryUtils::deepCopyMap(other.nestedGroupOfId);
	mapNestedGroupsToOwner(&this->foreignGroupOfId, &this->nestedGroupOfId);

	this->memberOfId 			= MemoryUtils::deepCopyMap(other.memberOfId);
	this->categoryOfId 			= MemoryUtils::deepCopyMap(other.categoryOfId);
	this->substitutionDictOfId 	= MemoryUtils::deepCopyMap(other.substitutionDictOfId);

	return *this;
}


/** Basic Accessors **/

fs::path const& ExpansionDefinition::getDefinitionPath() const
{
	return definitionPath;
}

fs::path ExpansionDefinition::getFullPath(fs::path const& subfolder)
{
	return definitionPath / subfolder;
}

vector<fs::path> ExpansionDefinition::getFullPaths(vector<fs::path> const& subfolders)
{
	auto fullPaths = vector<fs::path>(subfolders);
	transform(subfolders.begin(), subfolders.end(), fullPaths.begin(),
		[this](fs::path const& subfolder){ return this->getFullPath(subfolder); });
	return fullPaths;
}

ExpanderOptions const* ExpansionDefinition::getOptions() const
{
	return options;
}

ExpansionScheme const* ExpansionDefinition::getScheme() const
{
	return scheme;
}

vector<ExpansionGroup const*> ExpansionDefinition::getAllGroups() const
{
	vector<ExpansionGroup const*> groups;
	for (auto [groupId, group] : foreignGroupOfId)
	{
		groups.push_back(group);
	}
	for (auto [groupId, group] : nestedGroupOfId)
	{
		groups.push_back(group);
	}
	return groups;
}

ExpansionGroup const* ExpansionDefinition::getAnyGroupOfId(string const& id) const
{
	if (isForeignGroup(id))
		return foreignGroupOfId.at(id);
	if (isNestedGroup(id))
		return nestedGroupOfId.at(id);
	throw noItemInMapError("Group", id);
}

ExpansionGroup const* ExpansionDefinition::getForeignGroupOfId(string const& id) const
{
	return getItemOfId<ExpansionGroup>(foreignGroupOfId, id, "Foreign group");
}

NestedExpansionGroup const* ExpansionDefinition::getNestedGroupOfId(string const& id) const
{
	return getItemOfId<NestedExpansionGroup>(nestedGroupOfId, id, "Nested group");
}

bool ExpansionDefinition::isAnyGroup(string const& id) const
{
	return isForeignGroup(id) || isNestedGroup(id);
}

bool ExpansionDefinition::isForeignGroup(string const& id) const
{
	return foreignGroupOfId.count(id) > 0;
}

bool ExpansionDefinition::isNestedGroup(string const& id) const
{
	return nestedGroupOfId.count(id) > 0;
}

ExpansionMember const* ExpansionDefinition::getMemberOfId(string const& id) const
{
	return getItemOfId<ExpansionMember>(memberOfId, id, "Member");
}

ExpansionCategory const* ExpansionDefinition::getCategoryOfId(string const& id) const
{
	return getItemOfId<ExpansionCategory>(categoryOfId, id, "Category");
}

SubstitutionDictionary const* ExpansionDefinition::getSubstitutionDictOfId(string const& id) const
{
	return getItemOfId<SubstitutionDictionary>(substitutionDictOfId, id, "Substitution dictionary");
}


/** Subdirectory Reading **/

tuple<ExpansionDefinition::GroupMap, ExpansionDefinition::CategoryMap, ExpansionDefinition::NestedGroupMap>
	ExpansionDefinition::readGroupsAndCategories(
		fs::path const& definitionPath, vector<fs::path> const& groupsLocations)
{
	GroupMap groupOfId;
	CategoryMap categoryOfId;
	NestedGroupMap nestedGroupOfId;

	const auto onFileFound = [&](fs::path const& groupPath)
	{
		if (FileUtils::isJsonExtension(groupPath.extension()) && groupPath.stem().extension() == ".eg")
		{
			fs::path fullGroupPath = definitionPath / groupPath;
			json_t defn = FileUtils::readJsonFromFile(fullGroupPath);
			if (!defn.is_object())
				throw invalid_argument("Expected top-level dictionary in group at path: " + groupPath.string());

			try
			{
				readGroupAndCategory(&groupOfId, &categoryOfId, &nestedGroupOfId, fullGroupPath.stem().stem(), defn);
			}
			catch (...)
			{
				throw_with_nested(invalid_argument("Error reading group at '" + groupPath.string() + "' in definition."));
			}
		}
	};

	const auto onDirNotFound = [](fs::path const& groupPath)
	{
		cerr << "Group path '" << groupPath.string() << "' not found in definition." << endl;
	};

	FileUtils::forEachFileInDirectoryList(groupsLocations, definitionPath, onFileFound, onDirNotFound);

	return make_tuple(groupOfId, categoryOfId, nestedGroupOfId);
}

ExpansionDefinition::MemberMap ExpansionDefinition::readMembersOfGroup(
	fs::path const& definitionPath, vector<fs::path> const& membersLocations, ExpansionGroup* group)
{
	MemberMap memberOfId;

	for (fs::path const& membersLocation : membersLocations)
	{
		fs::path fullMembersLocation = definitionPath / membersLocation;
		if (!fs::is_directory(fullMembersLocation))
		{
			cerr << "Member path '" << membersLocation.string() << "' not found in definition." << endl;
		}

		fs::path groupMembersLocation = fullMembersLocation / group->getId();

		if (fs::is_directory(groupMembersLocation))
		{
			for (fs::path memberPath : fs::directory_iterator(groupMembersLocation))
			{
				if (FileUtils::isJsonExtension(memberPath.extension()) && memberPath.stem().extension() == ".em")
				{
					json_t defn = FileUtils::readJsonFromFile(memberPath);
					try
					{
						readMemberOfGroup(&memberOfId, group, memberPath.stem().stem(), defn);
					}
					catch (...)
					{
						throw_with_nested(invalid_argument("Error reading member at '" + fs::relative(memberPath.string(), definitionPath).string() + "' in definition."));
					}
				}
			}
		}
	}

	// Sorts the member and category ids of the group based using the existing sorted set.
	group->memberIds = MiscUtils::sortVectorOfIndexMap(&group->indexOfMemberId);
	group->categoryIds = MiscUtils::sortVectorOfIndexMap(&group->indexOfCategoryId);

	validateUniqueFields(*group, memberOfId);

	return memberOfId;
}

void ExpansionDefinition::readFunctionsOfGroup(
	fs::path const& definitionPath, vector<fs::path> const& functionsLocations, ExpansionGroup* group)
{
	vector<string> funcFields;
	vector<string> funcNames;

	// Lists the function names specified in the group definition.
	for (auto const& field : group->getFields())
	{
		if (group->getFormat(field) == Format::Function)
		{
			funcFields.push_back(field);
			funcNames.push_back(*group->getFunctionName(field));
		}
	}

	bool didFindLib = false;

	if (!funcFields.empty())
	{
		string libFile = "lib" + group->getId() + ".so";

		for (fs::path const& functionsLocation : functionsLocations)
		{
			fs::path fullFunctionsLocation = definitionPath / functionsLocation;
			string libPath = string(fullFunctionsLocation / libFile);

			if (fs::is_regular_file(libPath))
			{
				didFindLib = true;

				// Loads the library. Throws error on failure.
				void* library = dlopen(libPath.c_str(), RTLD_NOW);
				if (library == nullptr)
				{
					throw invalid_argument("Failed to load library file associated with group '" + group->getId() +
						"' at path: " + libPath);
				}

				// Loads the function names in the group's definition and attaches
				// the function pointer to the group object.
				for (auto const& funcName : funcNames)
				{
					void* funcUntyped = dlsym(library, funcName.c_str());
					if (funcUntyped == nullptr)
					{
						throw invalid_argument("No symbol associated with function name: " + funcName);
					}

					auto func = reinterpret_cast<ExpansionGroup::FunctionPtrType>(funcUntyped);
					group->setFunctionOfName(funcName, func);
				}

				break;
			}
			else
			{
				string srcFile = group->getId() + ".mf.cpp";

				if (fs::is_regular_file(fullFunctionsLocation / srcFile))
				{
					throw invalid_argument("Source file '" + (functionsLocation / srcFile).string() + "' " +
						"exists but hasn't been compiled into a library.");
				}
			}
		}

		if (!didFindLib)
		{
			throw invalid_argument("No source or library file found for group '" + group->getId() + "'.");
		}
	}
}

ExpansionDefinition::SubstitutionDictMap ExpansionDefinition::readSubstitutionDictionaries(
	fs::path const& definitionPath,
	vector<fs::path> const& substitutionsLocations,
	string const& universalDictName,
	set<string> const& includeNames)
{
	bool didFindUniversalDict = false;
	auto substitutionDictsOfId = SubstitutionDictMap();
	fs::path universalDictFileName = universalDictName + ".json";

	for (fs::path substitutionsLocation : substitutionsLocations)
	{
		fs::path fullSubnLocation = definitionPath / substitutionsLocation;
		if (fs::is_directory(fullSubnLocation))
		{
			// Checks whether the universal dictionary exists in one of the substitutions dictionaries.
			fs::path universalDictPath = fullSubnLocation / universalDictFileName;
			bool hasUniversalDict = fs::exists(universalDictPath);
			if (didFindUniversalDict && hasUniversalDict)
			{
				throw invalid_argument("Duplicate universal dictionary of name '" + universalDictFileName.string() +
					"' found in substitutions directory '" + substitutionsLocation.string() + "'.");
			}
			didFindUniversalDict = hasUniversalDict || didFindUniversalDict;

			if (fs::is_directory(fullSubnLocation))
			{
				for (fs::path dictPath : fs::directory_iterator(fullSubnLocation))
				{
					if (includeNames.count(dictPath.stem()) > 0 && FileUtils::isJsonExtension(dictPath.extension()))
					{
						SubstitutionDictionary* dict = new SubstitutionDictionary(dictPath);
						substitutionDictsOfId.emplace(dict->getId(), dict);
					}
				}
			}
		}

		else
		{
			cerr << "Substitution dictionary path '" << substitutionsLocation.string() << "' not found in definition." << endl;
		}
	}

	if (!didFindUniversalDict)
	{
		throw invalid_argument("Universal dictionary of name '" + universalDictFileName.string() +
			"' was not found in substitutions directories.");
	}

	return substitutionDictsOfId;
}


/** Single Json Reading **/

void ExpansionDefinition::readGroupAndCategory(GroupMap* groupOfId, CategoryMap* categoryOfId, NestedGroupMap* nestedGroupOfId,
	string const& groupTitle, json_t const& defn)
{
	// Infers if group is nested from whether an owner has been defined.
	string groupOwnerField = idOfReservedField(ReservedField::GroupOwner);
	string groupOwner = FileUtils::getIfJsonObjectHasStringOrDefault(defn, groupOwnerField, "");
	bool isNested = (groupOwner != "");

	// Infers group id from file name and whether an explicit id entry is present.
	string groupIdField = idOfReservedField(ReservedField::GroupId);
	string groupId = FileUtils::getIfJsonObjectHasStringOrDefault(defn, groupIdField, groupTitle);

	// Checks group hasn't already been created.
	if (groupOfId->count(groupId) > 0 || nestedGroupOfId->count(groupId) > 0)
	{
		throw invalid_argument("Duplicate group");
	}

	// Infers group name from id and whether an explicit name entry is present.
	string groupNameField = idOfReservedField(ReservedField::GroupName);
	string groupName = FileUtils::getIfJsonObjectHasStringOrDefault(defn, groupNameField, groupId);

	// Creates group with unique id and non-unique display name.
	ExpansionGroup* group = (!isNested) ?
		new ExpansionGroup(groupId, groupName) :
		new NestedExpansionGroup(groupId, groupName, groupOwner);

	// Reads categories of group.
	string categoriesField = idOfReservedField(ReservedField::Categories);
	if (!isNested && defn.count(categoriesField) == 1)
	{
		json_t catEntries = defn[categoriesField];
		assert(catEntries.is_object());
		for (auto const& [catIdObj, memberIdsObj] : catEntries.items())
		{
			string catId = FileUtils::getIfJsonString(catIdObj);

			// Checks if category already exists.
			if (categoryOfId->count(catId) > 0)
			{
				throw invalid_argument("Duplicate category");
			}

			auto [memberIdsBegin, memberidsEnd] = FileUtils::rangeFromJsonArray(memberIdsObj);
			vector<string> memberIds = FileUtils::stringVectorFromJsonRange(memberIdsBegin, memberidsEnd);

			(*categoryOfId)[catId] = new ExpansionCategory(catId, groupId, memberIds);
			group->categoryIds.push_back(catId);
			group->indexOfCategoryId.emplace(catId, group->categoryIds.size() - 1);
		}
	}
	else if (isNested && defn.count(categoriesField) == 1)
	{
		throw invalid_argument("Nested group cannot specify categories.");
	}

	// Reads arguments of nested group.
	string argsField = idOfReservedField(ReservedField::Arguments);
	vector<string> nestedArgs;
	vector<bool> didDefineNestedArg;
	if (isNested && defn.count(argsField) == 1)
	{
		json_t argsEntry = defn[argsField];
		auto [argsBegin, argsEnd] = FileUtils::rangeFromJsonArray(argsEntry);
		nestedArgs = FileUtils::stringVectorFromJsonRange(argsBegin, argsEnd);

		// Confirms no repitition of arguments in arguments list.
		auto sortedArgs = nestedArgs;
		sort(sortedArgs.begin(), sortedArgs.end());
		auto matchIt = adjacent_find(sortedArgs.begin(), sortedArgs.end());
		if (matchIt != sortedArgs.end())
			throw invalid_argument("Duplicate argument for nested group: " + *matchIt);

		// Checks fields are id, name or unreserved. Ensures ordering of id and name fields.
		bool didDefineId = false;
		bool didDefineName = false;
		for (size_t i = 0; i < nestedArgs.size(); i++)
		{
			auto const& arg = nestedArgs[i];

			auto reservedField = reservedFieldOfId(arg);

			if (reservedField.has_value() && !isIdOrNameField(reservedField.value()))
				throw invalid_argument("Invalid reserved field used for nested group argument: " + arg);

			if (reservedField.has_value())
			{
				if (reservedField.value() == ReservedField::Id)
				{
					if (i != 0)
						throw invalid_argument("Must specify id at the beginning.");
					didDefineId = true;
				}
				else if (reservedField.value() == ReservedField::Name)
				{
					if ((!didDefineId && i > 0) || (didDefineId && i > 1))
						throw invalid_argument("Must specify name at beginning or immediately after id.");
					didDefineName = true;
				}
			}

			static_cast<NestedExpansionGroup*>(group)->addArgument(arg);
		}

		// Id and name fields are implicitly defined, others must be manually defined.
		didDefineNestedArg = vector<bool>(nestedArgs.size());
		size_t idAndNameOffset = didDefineId + didDefineName;
		fill(didDefineNestedArg.begin(), didDefineNestedArg.begin() + idAndNameOffset, true);
	}
	else if (isNested)
	{
		throw invalid_argument("Nested group must specify arguments.");
	}
	else if (defn.count(argsField) == 1)
	{
		throw invalid_argument("Standard group cannot specify arguments.");
	}

	for (auto [field, entry] : defn.items())
	{
		// Checks whether the field is in nested arguments.
		bool isInNestedArgs = false;
		size_t nestedArgNo = 0;
		if (isNested)
		{
			auto nestedArgIt = find(nestedArgs.begin(), nestedArgs.end(), field);
			nestedArgNo = nestedArgIt - nestedArgs.begin();
			isInNestedArgs = nestedArgIt != nestedArgs.end();
		}

		auto reservedField = reservedFieldOfId(field);
		if (!reservedField.has_value())
		{
			if (!entry.is_array() || entry.empty())
				throw invalid_argument("Expected entry to be non-empty array");

			string formatId = entry[0];

			Format format;
			try
			{
				format = Entry::formatOfId(formatId);
			}
			catch (...)
			{
				throw_with_nested(invalid_argument("Error parsing field '" + field + "'."));
			}

			// Whether the entry is fully resolved in the group (static), or defined in the group
			// but may need reference to a member for resolution (group-defined).
			bool isStatic =  ExpansionFieldEntry::isStaticId(formatId);
			bool isGroupDefinedVariant = ExpansionFieldEntry::isGroupId(formatId);
			bool isGroupDefined = ExpansionFieldEntry::isGroupDefinedFormat(format) || isGroupDefinedVariant;
			bool isGroupContained = isStatic || isGroupDefined;

			// Checks only value arguments have been added to nested member.
			// Logs defined values.
			if (isNested)
			{
				if (!isGroupContained && !isInNestedArgs)
					throw invalid_argument("Instance entry defined in nested group without being an argument for: " + field);
				else if (((format != Format::Value && format != Format::ForeignMember) || isGroupContained) && isInNestedArgs)
					throw invalid_argument("Unsupported entry type or group-contained entry defined as argument in nested group for: " + field);
				didDefineNestedArg[nestedArgNo] = true;
			}

			group->initField(field, format, isStatic || isGroupDefinedVariant);
			GroupContext context = GroupContext(group, field, entry.begin() + 1, entry.end());

			try
			{
				switch (format)
				{
				case Format::Identifier:
				case Format::Name:
				case Format::GroupIdentifier:
				case Format::GroupName:
					throw logic_error("Id and name should be unreachable in the group definition.");
				case Format::Value:
					if (!isStatic)
						readGroupValue(context);
					else
						readStaticValue(context);
					break;
				case Format::ForeignMember:
					if (!isStatic)
						readGroupForeignMember(context);
					else
						readStaticForeignMember(context);
					break;
				case Format::NestedMember:
					if (!isStatic)
						readGroupNestedMember(context);
					else
						this->argumentsOfEntry->emplace(
							EntryReference(group, field, numeric_limits<size_t>::max()),
							make_shared<vector<vector<vector<ValueEntry>>>>(readStaticNestedMember(context)));
					break;
				case Format::NestedTemplate:
					if (!isGroupDefinedVariant)
						readGroupNestedTemplate(context);
					else
						readStaticNestedTemplate(context);
					break;
				case Format::Function:
					readFunction(context);
					break;
				case Format::Alias:
					readAlias(context);
					break;
				case Format::Owner:
					throw invalid_argument("Reserved field encountered during generic field reading.");
				}
			}
			catch (...)
			{
				throw_with_nested(invalid_argument("Error parsing field '" + field + "' of " + group->groupInfo() + "."));
			}
		}
		else
		{
			// Checks reserved field belongs to the type of group being processed.
			if ((!isNested && !isGroupField(reservedField.value())) ||
				(isNested && !isNestedGroupField(reservedField.value())))
				throw invalid_argument("Reserved field error for: " + field);
		}
	}

	group->getMutableStaticFieldMap()->tryCastAllValuesToRequestedType();
	group->getStaticFieldMap()->validateAllValuesHaveRequestedType();

	// Checks all arguments have been defined for a nested group.
	if (isNested)
	{
		auto firstUndefined = find(didDefineNestedArg.begin(), didDefineNestedArg.end(), false);
		if (firstUndefined != didDefineNestedArg.end())
		{
			size_t offset = firstUndefined - didDefineNestedArg.begin();
			throw invalid_argument("Argument not defined for nested group: " + nestedArgs[offset]);
		}
	}

	// Reads unique fields if specified.
	string uniqueFieldsField = idOfReservedField(ReservedField::UniqueFields);
	if (defn.contains(uniqueFieldsField))
	{
		auto [uniqueFieldsBegin, uniqueFieldsEnd] = FileUtils::rangeFromJsonArray(defn[uniqueFieldsField]);
		auto uniqueFields = FileUtils::stringVectorFromJsonRange(uniqueFieldsBegin, uniqueFieldsEnd);
		for (string const& uniqueField : uniqueFields)
		{
			group->setUnique(uniqueField);
		}
	}

	if (!isNested)
	{
		groupOfId->emplace(groupId, group);
	}
	else
	{
		nestedGroupOfId->emplace(groupId, static_cast<NestedExpansionGroup*>(group));
	}
}

void ExpansionDefinition::readMemberOfGroup(MemberMap* memberOfId, ExpansionGroup* group,
	string const& memberTitle, json_t const& defn)
{
	string memberIdField = idOfReservedField(ReservedField::Id);
	string memberId = FileUtils::getIfJsonObjectHasStringOrDefault(defn, memberIdField, memberTitle);
	if (memberOfId->count(memberId) > 0)
	{
		throw invalid_argument("Duplicate member id. Must be universally unique.");
	}

	string memberNameField = idOfReservedField(ReservedField::Name);
	string memberName = FileUtils::getIfJsonObjectHasStringOrDefault(defn, memberNameField, memberId);

	string groupIdField = idOfReservedField(ReservedField::GroupId);
	string groupId;
	try
	{
		groupId = FileUtils::getIfJsonObjectHasString(defn, groupIdField);
	}
	catch (...)
	{
		throw invalid_argument("Group ID field '" + groupIdField + "' expected in member.");
	}
	if (groupId != group->getId())
	{
		throw invalid_argument("Group id of member does not match.");
	}

	ExpansionMember* member = new ExpansionMember(memberId, memberName, group);

	for (auto [field, entry] : defn.items())
	{
		auto reservedField = reservedFieldOfId(field);
		if (!reservedField.has_value())
		{
			if (!entry.is_array())
				throw invalid_argument("Field '" + field + "' is not an array.");

			auto format = group->getFormat(field);

			auto context = MemberContext(member->getMutableFieldMap(), field, entry.begin(), entry.end());

			try
			{
				switch (format)
				{
				case Format::Identifier:
				case Format::Name:
					// 'id' and 'name' fields are reserved, so this should only be reached for invalid feilds.
					throw logic_error("Id and name are reserved. Should never reach.");
				case Format::Value:
					readMemberValue(context);
					break;
				case Format::ForeignMember:
					readMemberForeignMember(context);
					break;
				case Format::NestedMember:
					readMemberNestedMember(context);
					break;
				case Format::NestedTemplate:
					readMemberNestedTemplate(context);
					break;
				default:
					throw invalid_argument("Unexpected field in member!");
				}
			}
			catch (...)
			{
				throw_with_nested(invalid_argument("Error parsing field '" + field + "' of " + member->memberInfo() + "."));
			}
		}
		else
		{
			// Checks reserved field belongs to member.
			if (!isMemberField(reservedField.value()) &&
				reservedField.value() != ReservedField::GroupId)
				throw invalid_argument("Reserved field error for: " + field);
		}
	}

	member->getMutableFieldMap()->tryCastAllValuesToRequestedType();
	member->getFieldMap()->validateAllValuesHaveRequestedType();

	memberOfId->emplace(memberId, member);
	group->memberIds.push_back(memberId);
	group->indexOfMemberId.emplace(memberId, group->memberIds.size() - 1);
}


/** Misc **/

out_of_range ExpansionDefinition::noItemInMapError(string const& itemName, string const& id)
{
	return out_of_range(itemName + " of id '" + id + "' does not exist in definition.");
}

void ExpansionDefinition::checkFormatArgsSizeEquals(size_t entrySize, size_t expectedFormatArgsSize)
{
	if (entrySize != expectedFormatArgsSize)
	{
		throw invalid_argument("Format args size doesn't match.");
	}
}

void ExpansionDefinition::mapNestedGroupsToOwner()
{
	mapNestedGroupsToOwner(&foreignGroupOfId, &nestedGroupOfId);
}

void ExpansionDefinition::mapNestedGroupsToOwner(GroupMap* groupOfId, NestedGroupMap* nestedGroupOfId)
{
	// Maps the nested groups onto the containing groups.
	for (auto const& [nestedGroupId, nestedGroup] : *nestedGroupOfId)
	{
		string ownerGroupId = nestedGroup->getOwnerId();
		if (groupOfId->count(ownerGroupId) == 0)
			throw invalid_argument("Owner of nested group '" + nestedGroup->getId() +
				"' defined with non-existent group of id '" + ownerGroupId + "'.");
		ExpansionGroup* ownerGroup = groupOfId->at(ownerGroupId);
		ownerGroup->setNestedGroupOfId(nestedGroupId, nestedGroup);
	}
}

vector<string> ExpansionDefinition::getInterdependentFields(ExpansionGroup const* group, bool doMemberFields)
{
	auto fields = group->getFields();
	vector<string> interdependentFields;
	for (string const& field : fields)
	{
		Format format = group->getFormat(field);
		if (ExpansionFieldEntry::isInterdependentFormat(format))
		{
			bool isGroupField = group->isGroupContainedEntry(field);
			if (doMemberFields != isGroupField)
			{
				interdependentFields.push_back(field);
			}
		}
	};
	return interdependentFields;
}

vector<EntryReference> ExpansionDefinition::getInterdependentEntries(ExpansionGroup const* group, bool doMemberFields)
{
	vector<EntryReference> entryRefs;
	vector<string> fields = getInterdependentFields(group, doMemberFields);
	for (string const& field : fields)
	{
		auto format = group->getFormat(field);
		size_t listSize = ExpansionFieldEntry::isListedFormat(format) ? group->getListSize(field) : 0;
		for (size_t index = 0; index <= listSize; index++)
		{
			size_t selectIndex = listSize > 1 ? index : numeric_limits<size_t>::max();
			if (!doMemberFields)
			{
				entryRefs.push_back(EntryReference(group, field, selectIndex));
			}
			else
			{
				for (auto memberId : group->memberIds)
				{
					auto member = memberOfId.at(memberId);
					entryRefs.push_back(EntryReference(member, field, selectIndex));
				}
			}
		}
	}
	return entryRefs;
}

vector<EntryReference> ExpansionDefinition::getAllInterdependentEntries(bool doMemberFields)
{
	vector<EntryReference> entryRefs;
	for (auto group : getAllGroups())
	{
		auto groupEntryRefs = getInterdependentEntries(group, doMemberFields);
		entryRefs.insert(entryRefs.end(), groupEntryRefs.begin(), groupEntryRefs.end());
	}
	return entryRefs;
}

vector<TemplateNode const*> ExpansionDefinition::getSubproperties(EntryReference const& entryRef)
{
	vector<TemplateNode const*> subproperties;

	shared_ptr<TemplateTree> templateTree;
	vector<shared_ptr<TemplateTree>> subpropertyTrees;
	vector<TemplateNode const*> subpropertyTokens;

	auto format = entryRef.getGroup()->getFormat(entryRef.getField());
	switch (format)
	{
	case Format::NestedTemplate:
		templateTree = entryRef.getFieldMap()->getNestedTemplate(entryRef.getField());
		subpropertyTokens = TemplateTree::getPropertyTokensInSimpleTemplateContents(
			static_cast<TemplateNode const*>(templateTree->tree->getRoot()));
		subproperties = TemplateTree::getProperties(subpropertyTokens);
		break;
	case Format::Function:
		subpropertyTrees = *entryRef.getGroup()->getFunctionArgs(entryRef.getField());
		subproperties = TemplateTree::getRoots(subpropertyTrees);
		break;
	case Format::Alias:
		templateTree = *entryRef.getGroup()->getAlias(entryRef.getField());
		subproperties.push_back(templateTree->tree->getRoot());
		break;
	default:
		throw invalid_argument("Cannot get subproperties for unsupported format '" + ExpansionFieldEntry::idOfFormat(format) + "'.");
	}

	return subproperties;
}

void ExpansionDefinition::addEntryDependenciesToGraph(EntryReference const& entryRef)
{
	// Adds the entry to the dependency graph.
	dependencyGraph->getOrAddVertexWithKey(entryRef);

	// The initial reference is a member reference which has a dependency with its group, if it exists.
	if (!entryRef.isGroupReference())
	{
		addMemberGroupDependency(entryRef);
	}

	for (auto subproperty : getSubproperties(entryRef))
	{
		addSubpropertyDependenciesToGraph(entryRef, subproperty);
	}
}

void ExpansionDefinition::addSubpropertyDependenciesToGraph(EntryReference const& entryRef, TemplateNode const* subproperty)
{
	auto containerEntryRef = entryRef.makeOriginEntryReference();

	// Resolves the property to a group or member entry.
	auto resolvedRef = PropertyResolver<EntryReference>(BasicTermResolver(this))
		.resolveFinalTerm(containerEntryRef, subproperty, 0).value();

	// Adds a dependency from the initial entry to the final entry if interdependent.
	auto resolvedFormat = resolvedRef.getGroup()->getFormat(resolvedRef.getField());
	if (ExpansionFieldEntry::isInterdependentFormat(resolvedFormat))
	{
		auto sourceNode = dependencyGraph->getMutableVertexWithKey(entryRef);
		auto resolvedNode = dependencyGraph->getOrAddVertexWithKey(resolvedRef);
		dependencyGraph->addIfNoEdgeWithKey(sourceNode, resolvedNode);

		// The resolved reference is a member reference which has a dependency with its group.
		if (!resolvedRef.isGroupReference())
		{
			addMemberGroupDependency(resolvedRef);
		}
	}
}

void ExpansionDefinition::addMemberGroupDependency(EntryReference const& memberRef)
{
	auto groupRef = memberRef.makeGroupReference();
	auto memberNode = dependencyGraph->getMutableVertexWithKey(memberRef);
	auto groupNode = dependencyGraph->getOrAddVertexWithKey(groupRef);

	if (memberRef.getGroup()->isMemberContainedEntry(memberRef.getField()))
	{
		dependencyGraph->addIfNoEdgeWithKey(groupNode, memberNode);
	}
	else
	{
		dependencyGraph->addIfNoEdgeWithKey(memberNode, groupNode);
	}
}


/** Group Reading Helpers **/

void ExpansionDefinition::readGroupValue(ExpansionDefinition::GroupContext const& cxt)
{
	checkFormatArgsSizeEquals(cxt.entrySize(), 2);
	cxt.group->setValueType(cxt.field, cxt.entryBegin->get<string>());
	cxt.group->setListSize(cxt.field, (cxt.entryBegin + 1)->get<size_t>());
}

void ExpansionDefinition::readGroupForeignMember(ExpansionDefinition::GroupContext const& cxt)
{
	checkFormatArgsSizeEquals(cxt.entrySize(), 2);
	cxt.group->setForeignGroupId(cxt.field, cxt.entryBegin->get<string>());
	cxt.group->setListSize(cxt.field, (cxt.entryBegin + 1)->get<size_t>());
}

void ExpansionDefinition::readGroupNestedMember(ExpansionDefinition::GroupContext const& cxt)
{
	checkFormatArgsSizeEquals(cxt.entrySize(), 2);
	cxt.group->setNestedGroupId(cxt.field, cxt.entryBegin->get<string>());
	cxt.group->setListSize(cxt.field, (cxt.entryBegin + 1)->get<size_t>());
}

void ExpansionDefinition::readGroupNestedTemplate(ExpansionDefinition::GroupContext const& cxt)
{
	checkFormatArgsSizeEquals(cxt.entrySize(), 0);
}

void ExpansionDefinition::readFunction(ExpansionDefinition::GroupContext const& cxt)
{
	checkFormatArgsSizeEquals(cxt.entrySize(), 3);
	cxt.group->setFunctionName(cxt.field, cxt.entryBegin->get<string>());
	cxt.group->setFunctionReturnType(cxt.field, (cxt.entryBegin + 1)->get<string>());

	auto argVector = vector<string>();
	assert((cxt.entryBegin + 2)->is_array());
	for (auto const& arg : *(cxt.entryBegin + 2))
	{
		assert(arg.is_string());
		argVector.push_back(arg);
	}
	cxt.group->setFunctionArgs(cxt.field, argVector.begin(), argVector.end());
}

void ExpansionDefinition::readAlias(ExpansionDefinition::GroupContext const& cxt)
{
	checkFormatArgsSizeEquals(cxt.entrySize(), 1);
	cxt.group->setAlias(cxt.field, cxt.entryBegin->get<string>());
}

void ExpansionDefinition::readStaticValue(ExpansionDefinition::GroupContext const& cxt)
{
	checkFormatArgsSizeEquals(cxt.entrySize(), 2);
	cxt.group->setValueType(cxt.field, cxt.entryBegin->get<string>());
	cxt.group->setListSize(cxt.field, FileUtils::sizeOfJsonArray(*(cxt.entryBegin + 1)));
	readMemberValue(MemberContext::staticContextFromGroupContext(cxt, 1, true));
}

void ExpansionDefinition::readStaticForeignMember(ExpansionDefinition::GroupContext const& cxt)
{
	checkFormatArgsSizeEquals(cxt.entrySize(), 2);
	cxt.group->setForeignGroupId(cxt.field, cxt.entryBegin->get<string>());
	cxt.group->setListSize(cxt.field, FileUtils::sizeOfJsonArray(*(cxt.entryBegin + 1)));
	readMemberForeignMember(MemberContext::staticContextFromGroupContext(cxt, 1, true));
}

vector<vector<vector<ValueEntry>>> ExpansionDefinition::readStaticNestedMember(ExpansionDefinition::GroupContext const& cxt)
{
	checkFormatArgsSizeEquals(cxt.entrySize(), 2);
	cxt.group->setNestedGroupId(cxt.field, cxt.entryBegin->get<string>());
	cxt.group->setListSize(cxt.field, FileUtils::sizeOfJsonArray(*(cxt.entryBegin + 1)));

	MemberContext memberCxt = MemberContext::staticContextFromGroupContext(cxt, 1, true);
	return FileUtils::argumentVectorFromJsonRange(memberCxt.entryBegin, memberCxt.entryEnd);
}

void ExpansionDefinition::readStaticNestedTemplate(ExpansionDefinition::GroupContext const& cxt)
{
	checkFormatArgsSizeEquals(cxt.entryEnd - cxt.entryBegin, 1);
	readMemberNestedTemplate(MemberContext::staticContextFromGroupContext(cxt, 0, false));
}


/** Member Reading Helpers **/

void ExpansionDefinition::readMemberValue(ExpansionDefinition::MemberContext const& cxt)
{
	// checkFormatArgsSizeEquals(cxt.entrySize(), 1);
	vector<ValueEntry> values = FileUtils::valueVectorFromJsonRange(cxt.entryBegin, cxt.entryEnd);
	for (size_t i = 0; i < values.size(); i++)
	{
		cxt.fieldMap->setValue(cxt.field, values[i], i);
	}
}

void ExpansionDefinition::readMemberForeignMember(ExpansionDefinition::MemberContext const& cxt)
{
	// checkFormatArgsSizeEquals(cxt.entrySize(), 1);
	vector<string> foreignMemberIds = FileUtils::stringVectorFromJsonRange(cxt.entryBegin, cxt.entryEnd);
	for (size_t i = 0; i < foreignMemberIds.size(); i++)
		cxt.fieldMap->setForeignMemberId(cxt.field, foreignMemberIds[i], i);
}

void ExpansionDefinition::readMemberNestedMember(ExpansionDefinition::MemberContext const& cxt)
{
	// checkFormatArgsSizeEquals(cxt.entrySize(), 1);
	vector<vector<vector<ValueEntry>>> nestedArgs = FileUtils::argumentVectorFromJsonRange(cxt.entryBegin, cxt.entryEnd);
	for (size_t i = 0; i < nestedArgs.size(); i++)
	{
		cxt.fieldMap->createNestedMember(cxt.field, nestedArgs[i], i);
		cxt.fieldMap->getMutableNestedMember(cxt.field, i)->getMutableFieldMap()->tryCastAllValuesToRequestedType();

		try
		{
			cxt.fieldMap->getNestedMember(cxt.field, i)->getFieldMap()->validateAllValuesHaveRequestedType();
		}
		catch (...)
		{
			throw_with_nested(invalid_argument(
				"Error instantiating nested member '" + cxt.field + "' in " + cxt.fieldMap->getMember()->memberInfo() + "."
			));
		}
	}
}

void ExpansionDefinition::readMemberNestedTemplate(ExpansionDefinition::MemberContext const& cxt)
{
	checkFormatArgsSizeEquals(cxt.entrySize(), 1);
	cxt.fieldMap->parseNestedTemplate(cxt.field, *cxt.entryBegin);
}


/** Nested Group Reading Helpers **/

void ExpansionDefinition::readNestedGroupId(ExpansionDefinition::GroupContext const& cxt)
{
	checkFormatArgsSizeEquals(cxt.entrySize(), 0);
}

void ExpansionDefinition::readNestedGroupName(ExpansionDefinition::GroupContext const& cxt)
{
	checkFormatArgsSizeEquals(cxt.entrySize(), 0);
}


/** Validation Methods **/

void ExpansionDefinition::validateNoCyclesInEntries()
{
	dependencyGraph->setKeyFunction([](EntryReference const& entryRef){ return entryRef; });

	// Adds dependencies for all group entries.
	for (auto groupEntryRef : getAllInterdependentEntries(false))
	{
		addEntryDependenciesToGraph(groupEntryRef);
	}

	// Adds dependencies for all member entries.
	for (auto memberEntryRef : getAllInterdependentEntries(true))
	{
		addEntryDependenciesToGraph(memberEntryRef);
	}

	// Validates no cycles in the dependencies between entries.
	auto cycle = dependencyGraph->getCycle();
	if (cycle.has_value())
	{
		string message = "Expansion context has cycle with the following items:";
		for (auto dependencyNode : cycle.value())
		{
			auto const& entryRef = dependencyNode->getValue();
			string formatName = ExpansionFieldEntry::nameOfFormat(entryRef.getGroup()->getFormat(entryRef.getField()));

			string cycleElem;
			if (entryRef.isGroupReference())
			{
				cycleElem = "\n  group '" + entryRef.getGroup()->getName() + "', " + formatName + " field '" + entryRef.getField() + "'";
			}
			else
			{
				cycleElem = "\n  member '" + entryRef.getMember()->getName() + "', " + formatName + " field '" + entryRef.getField() + "'";
			}

			message += cycleElem;
		}
		throw runtime_error(message);
	}
}

void ExpansionDefinition::validateSubstitutionDicts()
{
	substitutionDependencyGraph->setKeyFunction([](SubstitutionReference const& subRef){ return subRef; });

	for (auto const& [subDictId, subDict] : substitutionDictOfId)
	{
		for (string subKey : subDict->getFields())
		{
			if (subDict->isTemplate(subKey))
			{
				TemplateTree const* entryTree = subDict->getReplacementTree(subKey);
				TemplateNode const* entryRoot = entryTree->tree->getRoot();
				auto expandableLines = TemplateTree::filterExpandableLines(TemplateTree::getContentsLines(entryRoot));
				for (auto const* expandableLine : expandableLines)
				{
					auto subTokens = TemplateTree::getSubstituteTokens(expandableLine);
					for (auto const* subToken : subTokens)
					{
						auto [refDictId, refKey] = TemplateTree::getSubstituteDictAndKey(subToken);
						if (refDictId == "")
						{
							refDictId = options->universalDictName;
						}

						SubstitutionDictionary const* refDict;
						try
						{
							refDict = getSubstitutionDictOfId(refDictId);
						}
						catch (...)
						{
							throw_with_nested(invalid_argument("Reference to non-existent substitution dictionary in " +
								TemplateTree::flatten(subToken) +
								" within dictionary '" + subDictId + "' and key '" + subKey + "'."));
						}

						if (!refDict->hasField(refKey))
						{
							throw invalid_argument("Reference to non-existent substitution key in " +
								TemplateTree::flatten(subToken) +
								" within dictionary '" + subDictId + "' and key '" + subKey + "'.");
						}
						else if (refDict->isTemplate(refKey))
						{
							auto* source = substitutionDependencyGraph->getOrAddVertexWithKey(SubstitutionReference(subDict, subKey));
							auto* target = substitutionDependencyGraph->getOrAddVertexWithKey(SubstitutionReference(refDict, refKey));
							substitutionDependencyGraph->addIfNoEdgeWithKey(source, target);
						}
					}
				}
			}
		}
	}

	auto cycle = substitutionDependencyGraph->getCycle();
	if (cycle.has_value())
	{
		string msg = "Susbtitution dictionaries have cycle with the following items:";
		for (auto const* dependencyNode : cycle.value())
		{
			auto const& subRef = dependencyNode->getValue();
			msg += "\n  dict '" + subRef.getDictionary()->getId() + "', key '" + subRef.getField() + "'";
		}
		throw runtime_error(msg);
	}
}

void ExpansionDefinition::validateUniqueFields(ExpansionGroup const& group, ExpansionDefinition::MemberMap const& memberOfId)
{
	validateUniqueField(group, ExpansionFieldEntry::idOfFormat(ExpansionFieldEntryFormat::Name), memberOfId);
	for (string const& uniqueField : group.getUniqueFields())
	{
		validateUniqueField(group, uniqueField, memberOfId);
	}
}

void ExpansionDefinition::validateUniqueField(ExpansionGroup const& group, string const& field, ExpansionDefinition::MemberMap const& memberOfId)
{
	unordered_map<string, ExpansionMember*> encounteredValues;
	for (string const& memberId : group.memberIds)
	{
		auto member = memberOfId.find(memberId)->second;
		string value = BasicTermResolver(nullptr).getValueString(EntryReference(member, field, 0));
		if (encounteredValues.count(value) >= 1)
			throw invalid_argument("Duplicate value '" + value + "' found in expansion members " +
				"'" + encounteredValues.find(value)->second->getId() + "' and '" + member->getId() + "' " +
				"for unique field '" + field + "'.");
		encounteredValues.emplace(value, member);
	}
}

vector<string> ExpansionDefinition::validateSchemeReferencesExistingSubstitutionDictionaries(
	ExpansionScheme const& scheme,
	SubstitutionDictMap const& dictMap)
{
	auto validityMsgs = vector<string>();

	size_t dictSetCount = scheme.getDictionarySetCount();
	for (size_t i = 0; i < dictSetCount; i++)
	{
		auto const& dictSet = scheme.getDictionarySet(i);
		for (string const& dictId : dictSet.dictionaryIds)
		{
			if (dictMap.find(dictId) == dictMap.end())
			{
				validityMsgs.push_back(
					"No dictionary matches id '" + dictId + "' in " +
					"dictionary set '" + dictSet.id + "' of scheme.");
			}
		}
	}

	size_t elementCount = scheme.getElementCount();
	for (size_t i = 0; i < elementCount; i++)
	{
		auto const& element = scheme.getElement(i);
		for (string const& dictSelector : element.dictionarySelectors)
		{
			if (!ExpansionScheme::isSetSelector(dictSelector))
			{
				if (dictMap.find(dictSelector) == dictMap.end())
				{
					validityMsgs.push_back(
						"No dictionary matches id '" + dictSelector + "' in " +
						"template subdirectory '" + element.templateSubpath.string() + "' of scheme.");
				}
			}
		}
	}

	return validityMsgs;
}
