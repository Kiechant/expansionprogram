//
//  ExpansionCategory.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/4/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "ExpansionCategory.hpp"

using namespace std;

ExpansionCategory::ExpansionCategory(string const& id, string const& groupId, vector<string> const& memberIds)
    : id(id), groupId(groupId), memberIds(memberIds) {}

string const* ExpansionCategory::getId() const
{
    return &id;
}

string const* ExpansionCategory::getGroupId() const
{
    return &groupId;
}

vector<string> const* ExpansionCategory::getMemberIds() const
{
    return &memberIds;
}