//
//  ExpansionCategory.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/4/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef EXPANSION_CATEGORY_HPP
#define EXPANSION_CATEGORY_HPP

#include <string>
#include <vector>

class ExpansionCategory
{
public:
    ExpansionCategory() = default;
    ExpansionCategory(std::string const& id, std::string const& groupId, std::vector<std::string> const& memberIds);

    std::string const* getId() const;
    std::string const* getGroupId() const;
    std::vector<std::string> const* getMemberIds() const;

private:
    std::string id;
    std::string groupId;
    std::vector<std::string> memberIds;
};

#endif /* EXPANSION_CATEGORY_HPP */
