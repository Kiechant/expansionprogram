//
//  ExpansionScheme.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/4/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef EXPANSION_SCHEME_HPP
#define EXPANSION_SCHEME_HPP

#include "ExpansionSchemeDecl.hpp"

#include "EnumStringMapDecl.hpp"

#include <set>

#include <nlohmann/json.hpp>

class SubstitutionDictionary;

enum class ExpansionOperation
{
	ExpandExternal,
	ExpandInternal,
	Substitute
};

struct ExpansionSchemeElement
{
	ExpansionSchemeElement(std::filesystem::path const& templateSubpath, std::string const& pipelineId, std::vector<std::string> const& dictionarySelectors);

	std::filesystem::path templateSubpath;
	std::string pipelineId;
	std::vector<std::string> dictionarySelectors;
};

struct ExpansionSchemeDictionarySet
{
	ExpansionSchemeDictionarySet(std::string const& id, std::vector<std::string> const& dictionaryIds);

	std::string id;
	std::vector<std::string> dictionaryIds;
};

struct ExpansionPipeline
{
	ExpansionPipeline(std::string const& id, std::vector<ExpansionOperation> const& operations);

	std::string id;
	std::vector<ExpansionOperation> operations;
};

class ExpansionScheme
{
public:
	static EnumStringMap<ExpansionOperation> const& operationIdMap();

	std::string templateExtension;
	std::string expansionExtension;

	ExpansionScheme(std::filesystem::path const& schemePath,
		std::string const& universalDictId,
		std::string const& templateExtension = "template",
		std::string const& expansionExtension = "expansion");


	/** Basic Accessors **/

	std::string const& getUniversalDictId() const;


	/** Element Access **/

	ExpansionSchemeElement const& getElement(size_t index) const;
	size_t getElementCount() const;
	ExpansionSchemeElement const& getElementOfSubdirectory(std::string const& subdir) const;


	/** Pipeline Access **/

	ExpansionPipeline const& getPipeline(size_t index) const;
	size_t getPipelineCount() const;
	ExpansionPipeline const& getPipelineOfId(std::string const& id) const;


	/** Dictionary Set Access **/

	ExpansionSchemeDictionarySet const& getDictionarySet(size_t index) const;
	size_t getDictionarySetCount() const;
	ExpansionSchemeDictionarySet const& getDictionarySetOfId(std::string const& id) const;

	/* Amalgamates all included dictionaries from the template subdirectories and
	   dictionary sets into one mutually-exclusive set, sorted in alphabetical order. */
	std::set<std::string> getAllDictionaryIds() const;

	/* Amalgamates all selected dictionaries for a particular subdirectory into one mutually
	   exclusive set, sorted in alphabetical oorder. */
	std::set<std::string> getSelectedDictionaryIdsForElement(ExpansionSchemeElement const& element) const;

	static std::string const& getDefaultDictionarySetId();


	/** External Validation Methods **/

	/* Validates whether the subdirectories listed in this scheme exist in the parent path.
	   Note that a scheme's subfolders can be applied to any root folder for flexibility. */
	void validateSubdirectoriesInParentExist(std::filesystem::path const& parentPath) const;


	/** Selector Methods **/

	static std::string makeSetSelector(std::string const& setId);
	static bool isSetSelector(std::string const& selector);
	static std::string getIfSetId(std::string const& setSelector);

protected:
	std::filesystem::path schemePath;
	std::string universalDictId;
	static const std::string defaultDictSetId;
	std::vector<ExpansionSchemeElement> elements;
	std::vector<ExpansionPipeline*> pipelines;
	ExpansionSchemeStringToPipeline pipelineOfId;
	std::vector<ExpansionSchemeDictionarySet*> dictionarySets;
	ExpansionSchemeStringToDictionarySet dictionarySetOfId;


	/** Reading Methods **/

	/* Reads the expansion scheme, including the subdirectories, pipelines and dictionary sets, from a JSON scheme file.*/
	static std::tuple<std::vector<ExpansionSchemeElement>, ExpansionSchemeStringToPipeline, ExpansionSchemeStringToDictionarySet, std::vector<std::string>>
	readScheme(std::filesystem::path const& schemePath, std::string const& universalDictId);

	/* Reads a list of expansion scheme elements from the corresponding subdirectories section of the JSON scheme file. */
	static std::pair<std::vector<ExpansionSchemeElement>, std::vector<std::string>>
	readElements(
		nlohmann::json const& subdirectoriesJson,
		ExpansionSchemeStringToPipeline const& pipelineOfId,
		ExpansionSchemeStringToDictionarySet const& dictSetOfId,
		std::string const& universalDictId);

	/* Reads a list of pipelines from the corresponding section of the JSON scheme file. */
	static std::pair<ExpansionSchemeStringToPipeline, std::vector<std::string>>
	readPipelines(nlohmann::json const& pipelinesJson);

	/* Reads a list of dictionary sets from the corresponding section of the JSON scheme file. */
	static std::pair<ExpansionSchemeStringToDictionarySet, std::vector<std::string>>
	readDictionarySets(nlohmann::json const& dictionarySetsJson, std::string const& universalDictId);


	/** Validation Methods **/

	/* Validates an element of the expansion scheme, including the subdirectory, pipeline and dictionary sets. */
	static std::vector<std::string> validateElement(
		ExpansionSchemeElement const& element,
		ExpansionSchemeStringToPipeline const& pipelineOfId,
		ExpansionSchemeStringToDictionarySet const& dictionarySetOfId,
		std::string const& universalDictId);

	/* Validates the subdirectory path (not the expansion pipeline ID or dictionary sets). */
	static std::string validateSubdirectory(std::filesystem::path const& path);

	/* Validates a component of a subdirectory path, i.e. a single directory or file within the full path. */
	static std::string validateSubdirectoryComponent(std::string const& path, size_t begin, size_t end);

	/* Validates the pipeline id exists. */
	static std::string validatePipelineId(std::string const& pipelineId, ExpansionSchemeStringToPipeline const& pipelineOfId);

	/* Validates the dictionary selectors of the element for:
	     - Repitition of dictionaries or sets.
	     - Malformed strings.
	     - Existence of dictionary sets in the current scheme.
	     - Universal dictionary explicitly named.
	   Ignores existence of dictionaries themselves. This must be validated in relation
	   to dictionaries loaded in expansion definiton. */
	static std::vector<std::string> validateDictionarySelectorsInElement(
		std::vector<std::string> const& dictionarySelectors,
		ExpansionSchemeStringToDictionarySet const& dictSetOfId,
		std::string const& universalDictId);
};

#endif /* EXPANSION_SCHEME_HPP */
