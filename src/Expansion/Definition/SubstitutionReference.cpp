//
//  SubstitutionReference.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 26/4/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "StdComparison.hpp"
#include "StdHashing.hpp"
#include "SubstitutionDictionary.hpp"
#include "SubstitutionReference.hpp"

using namespace std;

SubstitutionReference::SubstitutionReference(SubstitutionDictionary const* dictionary, std::string const& field) 
	: dictionary(dictionary), field(field) {}

SubstitutionDictionary const* SubstitutionReference::getDictionary() const
{
	return dictionary;
}

std::string const& SubstitutionReference::getField() const
{
	return field;
}

int compare(SubstitutionReference const& first, SubstitutionReference const& second)
{
	RETURN_IF_UNEQUAL(first.dictionary, second.dictionary, compareNullStatus);
	if (first.dictionary != nullptr && second.dictionary != nullptr)
		RETURN_IF_UNEQUAL_COMPARE(first.dictionary->getId(), second.dictionary->getId());
	RETURN_IF_UNEQUAL_COMPARE(first.field, second.field);
	return 0;
}

COMPARISON_OPERATOR_METHODS_IMPL(SubstitutionReference)

size_t toHash(SubstitutionReference const& ref)
{
	size_t res = initHash();
	res = combineHash(res, toHash(ref.dictionary));
	res = combineHash(res, toHash(ref.field));
	return res;
}

string toString(SubstitutionReference const& ref)
{
	string str = "(";
	if (ref.dictionary != nullptr)
	{
		str += ref.dictionary->getId() + ", ";
	}
	else
	{
		str += ", ";
	}
	str += ref.field;
	str += ")";
	return str;
}
