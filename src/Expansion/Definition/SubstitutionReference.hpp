//
//  SubstitutionReference.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 26/4/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef SUBSTITUTION_REFERENCE_HPP
#define SUBSTITUTION_REFERENCE_HPP

#include "ComparisonOperatorMethodsDecl.hpp"
#include "HashingDecl.hpp"
#include "StringConversionDecl.hpp"

#include <string>

class SubstitutionDictionary;

class SubstitutionReference
{
public:
	SubstitutionReference(SubstitutionDictionary const* dictionary, std::string const& field);

	SubstitutionDictionary const* getDictionary() const;
	std::string const& getField() const;

	friend int compare(SubstitutionReference const&, SubstitutionReference const&);
	COMPARISON_OPERATOR_METHODS_DECL(SubstitutionReference)

	friend size_t toHash(SubstitutionReference const&);

	friend std::string toString(SubstitutionReference const&);

private:
	SubstitutionDictionary const* dictionary;
	std::string field;
};

#endif /* SUBSTITUTION_REFERENCE_HPP */
