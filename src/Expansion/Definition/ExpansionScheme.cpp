//
//  ExpansionScheme.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/4/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "ExpansionScheme.hpp"

#include "EnumStringMap.tpp"
#include "FileUtils.hpp"
#include "MiscUtils.hpp"
#include "StdHashing.hpp"
#include "SubstitutionDictionary.hpp"
#include "SyntaxRule.hpp"
#include "TemplateTree.hpp"

#include <iostream>
#include <fstream>
#include <regex>
#include <set>
#include <sstream>

using namespace std;
namespace fs = filesystem;
using json_t = nlohmann::json;

ExpansionSchemeElement::ExpansionSchemeElement(fs::path const& templateSubpath, string const& pipelineId, vector<string> const& dictionarySelectors)
{
	this->templateSubpath = templateSubpath;
	this->pipelineId = pipelineId;
	this->dictionarySelectors = dictionarySelectors;
}

ExpansionSchemeDictionarySet::ExpansionSchemeDictionarySet(string const& id, vector<string> const& dictionaryIds)
{
	this->id = id;
	this->dictionaryIds = dictionaryIds;
}

ExpansionPipeline::ExpansionPipeline(string const& id, vector<ExpansionOperation> const& operations)
{
	this->id = id;
	this->operations = operations;
}

EnumStringMap<ExpansionOperation> const& ExpansionScheme::operationIdMap()
{
	static auto _operationIdMap = EnumStringMap<ExpansionOperation>(vector<string>{
		"expand_external",
		"expand_internal",
		"substitute"
	});
	return _operationIdMap;
}

const string ExpansionScheme::defaultDictSetId = "default";

ExpansionScheme::ExpansionScheme(
	fs::path const& schemePath,
	string const& universalDictId,
	string const& templateExtension,
	string const& expansionExtension)
	: templateExtension(templateExtension),
	  expansionExtension(expansionExtension),
	  schemePath(schemePath),
	  universalDictId(universalDictId)
{
	auto [elements, pipelineOfId, dictionarySetOfId, validityMessages] = readScheme(schemePath, universalDictId);

	// Throws an error if any of the paths aren't valid.
	stringstream ss;
	for (size_t i = 0; i < validityMessages.size(); i++)
	{
		if (validityMessages[i] != "")
		{
			ss << "  " << validityMessages[i] << endl;
		}
	}
	if (ss.str() != "")
	{
		throw invalid_argument("Error in reading scheme at '" + schemePath.string() + "':\n" + ss.str());
	}

	for (size_t i = 0; i < elements.size(); i++)
	{
		// Converts empty path to current directory.
		if (elements[i].templateSubpath == "")
			elements[i].templateSubpath = ".";
		this->elements.push_back(elements[i]);
	}

	this->pipelineOfId = move(pipelineOfId);

	/* Allows retrieving the pipelines by index in alphabetical order. */
	this->pipelines = MiscUtils::sortAndIndexMappedItems<string, ExpansionPipeline>(
		&this->pipelineOfId,
		[](ExpansionPipeline const* first, ExpansionPipeline const* second)
		{ return first->id < second->id; });

	this->dictionarySetOfId = move(dictionarySetOfId);

	/* Allows retrieving the dictionaries by index in alphabetical order. */
	this->dictionarySets = MiscUtils::sortAndIndexMappedItems<string, ExpansionSchemeDictionarySet>(
		&this->dictionarySetOfId,
		[](ExpansionSchemeDictionarySet const* first, ExpansionSchemeDictionarySet const* second)
		{ return first->id < second->id; });
}


/** Basic Accessors **/

string const& ExpansionScheme::getUniversalDictId() const
{
	return universalDictId;
}


/** Element Access **/

ExpansionSchemeElement const& ExpansionScheme::getElement(size_t index) const
{
	if (index < getElementCount())
	{
		return elements.at(index);
	}
	else
	{
		throw out_of_range("Element index out of range.");
	}
}

size_t ExpansionScheme::getElementCount() const
{
	return elements.size();
}

ExpansionSchemeElement const& ExpansionScheme::getElementOfSubdirectory(string const& subdir) const
{
	string const& adjustedSubdir = (subdir != "") ? subdir : ".";
	auto elIt = find_if(elements.begin(), elements.end(),
		[&adjustedSubdir](ExpansionSchemeElement const& el)
		{
			return adjustedSubdir == el.templateSubpath.string();
		});
	if (elIt == elements.end())
		throw invalid_argument("Subdirectory '" + adjustedSubdir + "' is unspecified in scheme.");
	return *elIt;
}


/** Pipeline Access **/

ExpansionPipeline const& ExpansionScheme::getPipeline(size_t index) const
{
	if (index < getPipelineCount())
	{
		return *(pipelines[index]);
	}
	else
	{
		throw out_of_range("Pipeline index out of range.");
	}
}

size_t ExpansionScheme::getPipelineCount() const
{
	return pipelineOfId.size();
}

ExpansionPipeline const& ExpansionScheme::getPipelineOfId(string const& id) const
{
	return pipelineOfId.at(id);
}


/** Dictionary Set Access **/

ExpansionSchemeDictionarySet const& ExpansionScheme::getDictionarySet(size_t index) const
{
	if (index < getDictionarySetCount())
	{
		return *(dictionarySets[index]);
	}
	else
	{
		throw out_of_range("Dictionary set index out of range.");
	}
}

size_t ExpansionScheme::getDictionarySetCount() const
{
	return dictionarySetOfId.size();
}

ExpansionSchemeDictionarySet const& ExpansionScheme::getDictionarySetOfId(string const& id) const
{
	return dictionarySetOfId.at(id);
}

set<string> ExpansionScheme::getAllDictionaryIds() const
{
	set<string> dictIds;
	for (auto const* dictSet : dictionarySets)
	{
		for (auto const& dictId : dictSet->dictionaryIds)
		{
			dictIds.insert(dictId);
		}
	}
	for (auto const& elem : elements)
	{
		for (auto const& dictSelector : elem.dictionarySelectors)
		{
			if (!isSetSelector(dictSelector))
			{
				dictIds.insert(dictSelector);
			}
		}
	}
	return dictIds;
}

set<string> ExpansionScheme::getSelectedDictionaryIdsForElement(ExpansionSchemeElement const& element) const
{
	set<string> dictIds;
	for (auto const& dictSelector : element.dictionarySelectors)
	{
		string setId = getIfSetId(dictSelector);
		if (setId != "")
		{
			for (auto const& dictId : getDictionarySetOfId(setId).dictionaryIds)
			{
				dictIds.insert(dictId);
			}
		}
		else dictIds.insert(dictSelector);
	}
	return dictIds;
}


string const& ExpansionScheme::getDefaultDictionarySetId()
{
	return defaultDictSetId;
}


/** External Validation Methods **/

void ExpansionScheme::validateSubdirectoriesInParentExist(fs::path const& parentPath) const
{
	stringstream ss;
	for (auto const& el : elements)
	{
		if (!fs::is_directory(parentPath / el.templateSubpath))
		{
			ss << "  " << el.templateSubpath.string() << endl;
		}
	}
	if (ss.str() != "")
	{
		throw invalid_argument("Subdirectories do not exist in scheme in parent path '" + parentPath.string() + "':\n" + ss.str());
	}
}


/** Selector Methods **/

string ExpansionScheme::makeSetSelector(string const& setId)
{
	return "#" + setId;
}

bool ExpansionScheme::isSetSelector(string const& setSelector)
{
	return setSelector.length() > 0 && setSelector[0] == '#';
}

string ExpansionScheme::getIfSetId(string const& setSelector)
{
	if (isSetSelector(setSelector))
	{
		return string(setSelector.begin() + 1, setSelector.end());
	}
	return "";
}


/** Reading Methods **/

tuple<vector<ExpansionSchemeElement>, ExpansionSchemeStringToPipeline, ExpansionSchemeStringToDictionarySet, vector<string>>
ExpansionScheme::readScheme(fs::path const& schemePath, string const& universalDictId)
{
	if (!fs::is_regular_file(schemePath))
		throw invalid_argument("No scheme exists at path: " + string(schemePath));
	json_t scheme = FileUtils::readJsonFromFile(schemePath);
	if (!scheme.is_object())
		throw invalid_argument("Expected top-level dictionary in scheme at path: " + schemePath.string());

	vector<string> validityMessages;

	vector<ExpansionPipeline*> pipelines;
	ExpansionSchemeStringToPipeline pipelineOfId;
	json_t jsonPipelines = FileUtils::getIfJsonObjectHasObject(scheme, "pipelines");
	tie(pipelineOfId, validityMessages) = readPipelines(jsonPipelines);

	vector<ExpansionSchemeDictionarySet*> dictionarySets;
	ExpansionSchemeStringToDictionarySet dictionarySetOfId;
	string dictSetKey = "dictionary_sets";

	/* Processes the user's dictionary sets if specified. */
	if (scheme.count(dictSetKey) && scheme[dictSetKey].is_object())
	{
		json_t dictionarySetEntries = FileUtils::getIfJsonObjectHasObject(scheme, dictSetKey);
		tie(dictionarySetOfId, validityMessages) = readDictionarySets(dictionarySetEntries, universalDictId);
	}

	/* The user can omit specifying dictionary sets, in which case, the default dictionary set
	   will be created with the universal dictionary. */
	else
	{
		dictionarySetOfId.emplace(defaultDictSetId, ExpansionSchemeDictionarySet(defaultDictSetId, vector<string>{universalDictId}));
	}

	vector<ExpansionSchemeElement> elements;
	json_t subdirEntries = FileUtils::getIfJsonObjectHasArray(scheme, "subdirectories");
	tie(elements, validityMessages) = readElements(subdirEntries, pipelineOfId, dictionarySetOfId, universalDictId);

	return make_tuple(move(elements), move(pipelineOfId), move(dictionarySetOfId), move(validityMessages));
}

pair<vector<ExpansionSchemeElement>, vector<string>> ExpansionScheme::readElements(
	nlohmann::json const& subdirectoriesJson,
	ExpansionSchemeStringToPipeline const& pipelineOfId,
	ExpansionSchemeStringToDictionarySet const& dictSetOfId,
	string const& universalDictId)
{
	vector<ExpansionSchemeElement> elements;
	vector<string> validityMessages;
	// TODO: Implement subdirectory searching.
	for (json_t const& schemeElem : subdirectoriesJson)
	{
		size_t elemSize = FileUtils::sizeOfJsonArray(schemeElem);
		if (elemSize < 2 || elemSize > 3)
			throw invalid_argument("Scheme element must contain 2 to 3 values: "
				"subdirectory, pipeline and optional dictionary list.");

		string subdir = FileUtils::getIfJsonString(schemeElem[0]);
		string pipelineId = FileUtils::getIfJsonString(schemeElem[1]);

		// TODO: Return and validate dictionary IDs from scheme element.

		// TODO: Standardise multi-dimensional array access.

		vector<string> dictionarySelectors;
		bool hasDictionarySelectors = elemSize >= 3;

		/* If the dictionary IDs are supplied, reads them into the list. */
		if (hasDictionarySelectors)
		{
			auto [begin, end] = FileUtils::rangeFromJsonArray(schemeElem[2]);
			dictionarySelectors = FileUtils::stringVectorFromJsonRange(begin, end);
		}

		auto element = ExpansionSchemeElement(subdir, pipelineId, dictionarySelectors);
		vector<string> elemValidityMsgs = validateElement(element, pipelineOfId, dictSetOfId, universalDictId);

		if (!hasDictionarySelectors)
		{
			/* Adds the default dictionary set if no dictionary selectors specified.
			Must be after validation, which complains if the user supplied explicit
			inclusions of the default dictionary set. */
			element.dictionarySelectors.insert(element.dictionarySelectors.begin(), makeSetSelector(ExpansionScheme::defaultDictSetId));
		}

		validityMessages.insert(validityMessages.end(), elemValidityMsgs.begin(), elemValidityMsgs.end());

		elements.push_back(element);
	}
	return make_tuple(move(elements), move(validityMessages));
}

pair<ExpansionSchemeStringToPipeline, vector<string>> ExpansionScheme::readPipelines(nlohmann::json const& pipelinesJson)
{
	ExpansionSchemeStringToPipeline pipelineOfId;
	for (auto const& [pipelineId, pipelineObj] : pipelinesJson.items())
	{
		auto [pipelineBegin, pipelineEnd] = FileUtils::rangeFromJsonArray(pipelineObj);
		auto opIds = FileUtils::stringVectorFromJsonRange(pipelineBegin, pipelineEnd);
		auto ops = vector<ExpansionOperation>();
		for (auto const& opId : opIds)
		{
			ops.push_back(operationIdMap().getEnum(opId));
		}
		pipelineOfId.emplace(pipelineId, ExpansionPipeline(pipelineId, move(ops)));
	}
	return make_tuple(pipelineOfId, vector<string>());
}

pair<ExpansionSchemeStringToDictionarySet, vector<string>> ExpansionScheme::readDictionarySets(
	nlohmann::json const& dictionarySetsJson, string const& universalDictId)
{
	ExpansionSchemeStringToDictionarySet dictionarySetOfId;
	bool didFindDefault = false;
	for (auto const& [setName, setDictionaries] : dictionarySetsJson.items())
	{
		if (!setDictionaries.is_array())
			throw invalid_argument("Dictionary set '" + setName + "' initialised without an array.");
		if (setName == defaultDictSetId)
			didFindDefault = true;

		// Validates dictionary set id pattern.
		if (setName == "")
			throw invalid_argument("Empty dictionary set id.");
		auto idPattern = regex(SubstitutionDictionary::getIdPattern());
		if (!regex_match(setName, idPattern))
			throw invalid_argument("Dictionary set id '" + setName + "' is invalid.");

		auto dictionaryIds = vector<string>();
		dictionaryIds.push_back(universalDictId);
		for (auto const& dictionaryId : setDictionaries)
		{
			if (!dictionaryId.is_string())
				throw invalid_argument("Dictionary set '" + setName + "' has a non-string entry.");

			string dictId = FileUtils::getIfJsonString(dictionaryId);

			if (dictId == universalDictId)
				throw invalid_argument("Universal dictionary '" + dictId + "' named in dictionary set '" + setName + "'. " +
					"This is implicitly included in each dictionary set.");
			
			// Validates dictionary id pattern.
			if (dictId == "")
				throw invalid_argument("Empty dictionary id in dictionary set '" + setName + "'.");
			auto idPattern = regex(SubstitutionDictionary::getIdPattern());
			if (!regex_match(dictId, idPattern))
			{
				if (ExpansionScheme::isSetSelector(dictId) && regex_match(dictId.begin() + 1, dictId.end(), idPattern))
					throw invalid_argument("Prohibited use of dictionary set selector '" + dictId + "' in dictionary set '" + setName + "'.");
				else
					throw invalid_argument("Dictionary id '" + dictId + "' is invalid in dictionary set '" + setName + "'.");
			}

			dictionaryIds.push_back(dictId);
		}
		dictionarySetOfId.emplace(setName, ExpansionSchemeDictionarySet(setName, dictionaryIds));
	}
	if (!didFindDefault)
		dictionarySetOfId.emplace(defaultDictSetId, ExpansionSchemeDictionarySet(defaultDictSetId, vector<string>{universalDictId}));
	return make_tuple(dictionarySetOfId, vector<string>());
}


/** Validation Methods **/

vector<string> ExpansionScheme::validateElement(
	ExpansionSchemeElement const& element,
	ExpansionSchemeStringToPipeline const& pipelineOfId,
	ExpansionSchemeStringToDictionarySet const& dictionarySetOfId,
	string const& universalDictId)
{
	vector<string> validityMessages;
	string subdirMsg = validateSubdirectory(element.templateSubpath);
	if (subdirMsg != "")
		validityMessages.push_back(subdirMsg);
	string pipelineIdMsg = validatePipelineId(element.pipelineId, pipelineOfId);
	if (pipelineIdMsg != "")
		validityMessages.push_back(pipelineIdMsg);
	vector<string> dictIdMsgs = validateDictionarySelectorsInElement(element.dictionarySelectors, dictionarySetOfId, universalDictId);
	if (!dictIdMsgs.empty())
		validityMessages.insert(validityMessages.end(), dictIdMsgs.begin(), dictIdMsgs.end());
	return validityMessages;
}

string ExpansionScheme::validateSubdirectory(fs::path const& path)
{
	string pathStr = path.string();

	if (pathStr == "")
	{
		return "";
	}

	if (pathStr.size() > 1)
	{
		if (pathStr[0] == '/')
		{
			return "Path '" + path.string() + "' begins with slash '/'.";
		}
	}

	size_t last = 0;
	size_t curr = 0;

	while (curr < pathStr.size())
	{
		if (pathStr[curr] == '/')
		{
			string msg = validateSubdirectoryComponent(pathStr, last, curr);
			if (msg != "") return msg;
			last = curr + 1;
		}

		curr++;
	}

	if (last < pathStr.size())
	{
		string msg = validateSubdirectoryComponent(pathStr, last, curr);
		if (msg != "") return msg;
	}
	else
	{
		// A final backslash has been provided.
		return "Path '" + pathStr + "' ends with slash '/'.";
	}

	return "";
}

string ExpansionScheme::validateSubdirectoryComponent(string const& path, size_t begin, size_t end)
{
	string component = path.substr(begin, end - begin);
	if (component == "." ||
		component == ".." ||
		component == "")
	{
		return "Path '" + path + "' contains a special character '.' or '..', or a double slash -'//'.";
	}
	return "";
}

string ExpansionScheme::validatePipelineId(string const& pipelineId, ExpansionSchemeStringToPipeline const& pipelineOfId)
{
	if (pipelineOfId.count(pipelineId) == 0)
		return "Undefined pipeline id specified for subdirectory.";
	return "";
}

vector<string> ExpansionScheme::validateDictionarySelectorsInElement(
	vector<string> const& dictionarySelectors,
	ExpansionSchemeStringToDictionarySet const& dictSetOfId,
	string const& universalDictId)
{
	auto validityMessages = vector<string>();

	// Stores dictionary id counts in the format:
	// (dictId, isSet) : count
	unordered_map<pair<string, bool>, size_t, ToHash<pair<string, bool>>> dictIdCounts;

	auto getIdOrSetMsg = [](bool isSet) -> string { return string(!isSet ? "id" : "set id"); };

	// Validates the format of the dictionary ids and sets.
	for (size_t i = 0; i < dictionarySelectors.size(); i++)
	{
		string const& dictSelector = dictionarySelectors.at(i);
		string dictId;

		// Determines whether the selector specifies a dictionary set.
		bool isSet = false;
		if (isSetSelector(dictSelector))
		{
			dictId = getIfSetId(dictSelector);
			isSet = true;
		}
		else
		{
			dictId = dictSelector;
		}

		// Validates dictionary id or set is not empty.
		if (dictId.size() == 0)
		{
			validityMessages.push_back("Empty dictionary " + getIdOrSetMsg(isSet) + " at position " + to_string(i) + ".");
		}
		else
		{
			// Validates the id pattern of the remaining part.
			// Dictionary ids and sets share the same id pattern.
			auto idRegex = regex(SubstitutionDictionary::getIdPattern());
			if (!regex_match(dictId, idRegex))
			{
				validityMessages.push_back(string("Dictionary " + getIdOrSetMsg(isSet) + " '" + dictId + "' is of an invalid name."));
			}
			else
			{
				// Dictionary id or set id is of a valid form.
				// Adds it to the set for additional validation.
				dictIdCounts[make_pair(dictId, isSet)]++;
			}
		}
	}

	for (auto const& [dictIdDesc, dictIdCount] : dictIdCounts)
	{
		string const& dictId = get<0>(dictIdDesc);
		bool isSet = get<1>(dictIdDesc);

		// Validates that the default dictionary set is not selected explicitly.
		if (dictId == universalDictId)
		{
			if (!isSet)
			{
				validityMessages.push_back(string("Universal dictionary '" + dictId + "' selected. "
					"This is implicitly included as part of the default dictionary set."));
			}
			else
			{
				validityMessages.push_back(string("Dictionary set of name '" + dictId + "' selected. "
					"This name is reserved for the universal dictionary."));
			}
		}

		// Validates that the universal dictionary is not selected explicitly.
		else if (dictId == ExpansionScheme::defaultDictSetId)
		{
			if (!isSet)
			{
				validityMessages.push_back(string("Dictionary of name '" + dictId + "' selected. "
					"This name is reserved for the default dictionary set."));
			}
		}

		// Validates that a dictionary set exists within this scheme.
		else if (isSet && dictSetOfId.count(dictId) == 0)
		{
			validityMessages.push_back(
				"Dictionary set '" + dictId + "' does not exist.");
		}

		// Validates that no duplicate ids or sets are listed.
		else if (dictIdCount > 1)
		{
			validityMessages.push_back(
				"Duplicate dictionary " + getIdOrSetMsg(isSet) + "s '" + dictId + "'. " +
				to_string(dictIdCount) + " instances present.");
		}
	}

	return validityMessages;
}
