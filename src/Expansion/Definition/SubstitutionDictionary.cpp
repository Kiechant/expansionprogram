//
//  SubstitutionDictionary.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/4/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "SubstitutionDictionary.hpp"

#include "FileUtils.hpp"
#include "TemplateTree.hpp"
#include "Tree.tpp"

using namespace std;
namespace fs = filesystem;
using json_t = nlohmann::json;

SubstitutionDictionary::SubstitutionDictionary(fs::path const& dictionaryPath)
{
	SubstitutionDictionary dict = readSubstitutionDictionary(dictionaryPath);
	this->id = dict.id;
	this->dictionary = dict.dictionary;
}

SubstitutionDictionary::SubstitutionDictionary(string const& id, SubstitutionMap const& dictionary)
	: id(id), dictionary(dictionary) {}

string const& SubstitutionDictionary::getId() const
{
	return id;
}

bool SubstitutionDictionary::hasField(string const& field) const
{
	return dictionary.find(field) != dictionary.end();
}

bool SubstitutionDictionary::isTemplate(string const& field) const
{
	return dictionary.at(field).isTemplate;
}

TemplateTree const* SubstitutionDictionary::getReplacementTree(string const& field) const
{
	Entry const& entry = dictionary.at(field);
	if (!entry.isTemplate)
		throw invalid_argument("The replacement for '" + field + "' is not a tree.");
	return get<shared_ptr<TemplateTree>>(entry.replacement).get();
}

string const* SubstitutionDictionary::getReplacementString(string const& field) const
{
	Entry const& entry = dictionary.at(field);
	if (entry.isTemplate)
		throw invalid_argument("The replacement for '" + field + "' is not a string.");
	return &get<string>(entry.replacement);
}

vector<string> SubstitutionDictionary::getFields() const
{
	vector<string> entryIds;
	for (auto const& [entryId, entry] : dictionary)
	{
		entryIds.push_back(entryId);
	}
	return entryIds;
}

vector<SubstitutionDictionary::Entry const*> SubstitutionDictionary::getEntries() const
{
	vector<SubstitutionDictionary::Entry const*> entries;
	for (auto const& [entryId, entry] : dictionary)
	{
		entries.push_back(&entry);
	}
	return entries;
}

std::string SubstitutionDictionary::getIdPattern()
{
	return "[a-zA-Z_][a-zA-Z0-9_]*";
}

SubstitutionDictionary SubstitutionDictionary::readSubstitutionDictionary(fs::path const& dictionaryPath)
{
	json_t defn = FileUtils::readJsonFromFile(dictionaryPath);
	
	string id = dictionaryPath.stem();
	SubstitutionMap dict;

	for (auto const& [field, replObj] : defn.items())
	{
		string replStr = FileUtils::getIfJsonString(replObj);
		dict.emplace(field, generateReplacementFromString(replStr));
	}

	return SubstitutionDictionary(move(id), move(dict));
}

SubstitutionDictionary::Entry SubstitutionDictionary::generateReplacementFromString(string const& replacementString)
{
	TemplateTree* tree = TemplateTree::parseTemplateContents(replacementString);
	Entry entry;
	if (tree != nullptr && TemplateTree::isOperable(tree->tree->getRoot()))
	{
		entry = Entry{Replacement(shared_ptr<TemplateTree>(tree)), true};
	}
	else
	{
		entry = Entry{replacementString, false};
	}
	return entry;
}
