//
//  SubstitutionDictionary.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/4/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef SUBSTITUTION_DICTIONARY_HPP
#define SUBSTITUTION_DICTIONARY_HPP

#include "TemplateTreeDecl.hpp"

#include <filesystem>
#include <unordered_map>
#include <variant>
#include <vector>

class SubstitutionDictionary
{
public:
	using Replacement = std::variant<std::string, std::shared_ptr<TemplateTree>>;

	struct Entry
	{
		Replacement replacement;
		bool isTemplate = false;
	};

	using SubstitutionMap = std::unordered_map<std::string, Entry>;

	SubstitutionDictionary(std::filesystem::path const& dictionaryPath);
	SubstitutionDictionary(std::string const& id, SubstitutionMap const& dictionary);

	std::string const& getId() const;
	bool hasField(std::string const& field) const;
	bool isTemplate(std::string const& field) const;
	TemplateTree const* getReplacementTree(std::string const& field) const;
	std::string const* getReplacementString(std::string const& field) const;
	std::vector<std::string> getFields() const;
	std::vector<Entry const*> getEntries() const;

	static std::string getIdPattern();

protected:
	std::string id;
	SubstitutionMap dictionary;

	static SubstitutionDictionary readSubstitutionDictionary(std::filesystem::path const& dictionaryPath);
	static Entry generateReplacementFromString(std::string const& replacementString);
};

#endif /* SUBSTITUTION_DICTIONARY_HPP */
