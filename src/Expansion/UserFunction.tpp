//
//  UserFunction.tpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef USER_FUNCTION_TPP
#define USER_FUNCTION_TPP

#include "UserFunction.hpp"

#include "ValueEntry.tpp"

template<EpValueCategory C>
EpValue makeEpValue(ValueTypeOfCategoryT<C> const& value)
{
	return ValueEntry::fromValueWithCategory<C>(value);
}

template<EpValueCategory C>
ValueTypeOfCategoryT<C> EpArgumentList::getNext()
{
	if (nextIndex >= arguments.size())
	{
		throw std::invalid_argument("Attempted access argument of index " + std::to_string(nextIndex) +
			". Argument list is of size " + std::to_string(arguments.size()) + ".");
	}
	else if (arguments[nextIndex].getCategory() != C)
	{
		throw std::invalid_argument("Attempted accessing argument using category " +
			ValueEntry::idOfValueCategory(C) +
			". Argument " + std::to_string(nextIndex) + " is of category " +
			ValueEntry::idOfValueCategory(arguments[nextIndex].getCategory()) + ".");
	}
	return arguments[nextIndex++].getValue<C>();
};

#endif /* USER_FUNCTION_TPP */
