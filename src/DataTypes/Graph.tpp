//
//  Graph.tpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 19/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef GRAPH_TPP
#define GRAPH_TPP

#include "Graph.hpp"

#include "GraphNode.tpp"
#include "MemoryUtils.hpp"
#include "MiscUtils.hpp"
#include "StdStringConversion.hpp"

#include <exception>

GRAPH_TMPL
void swap(GRAPH_T& first, GRAPH_T& second)
{
	using std::swap;

	swap(first.vertices, second.vertices);
	swap(first.sources, second.sources);
	swap(first.sinks, second.sinks);

	// Swaps the graphs which the vertices belong to.
	std::for_each(first.vertices.begin(), first.vertices.end(),
		[&first](GraphNode<T>* node){ node->setGraph(&first); });
	std::for_each(second.vertices.begin(), second.vertices.end(),
		[&second](GraphNode<T>* node){ node->setGraph(&second); });
	
	swap(first._hasCycles, second._hasCycles);
	swap(first._didUpdateCycleStatus, second._didUpdateCycleStatus);
}

GRAPH_TMPL
GRAPH_T::Graph() {}

GRAPH_TMPL
GRAPH_T::Graph(GRAPH_T const& other)
{
	// Creates the new vertices with a copy of the value without the edges.
	std::function<GraphNode<T>*(GraphNode<T>* const&)> createNewVertex =
		[this](GraphNode<T>* const& node)
		{ return new GraphNode<T>(this, node->getValue()); };
	this->vertices = MiscUtils::transformCopy(other.vertices, createNewVertex);

	// Maps the addresses of the old vertices to the addresses of the new vertices.
	auto indexOfVertex = MemoryUtils::mapItemIndexes(other.vertices, MiscUtils::offsetOfPtrRef<GraphNode<T>>);
	std::function<GraphNode<T>*(GraphNode<T>* const&)> mapToNewVertex =
		[this, &indexOfVertex](GraphNode<T>* const& node)
		{
			size_t offset = MiscUtils::offsetOfPtrRef<GraphNode<T>>(node);
			return this->vertices[indexOfVertex[offset]];
		};
	
	// Maps the sources and sinks to the new vertices.
	this->sources = MiscUtils::transformCopy(other.sources, mapToNewVertex);
	this->sinks = MiscUtils::transformCopy(other.sinks, mapToNewVertex);
	
	// Maps the edges of the vertices to the new vertices.
	for (size_t v = 0; v < other.vertices.size(); v++)
	{
		this->vertices[v]->outgoingEdges = MiscUtils::transformCopy(other.vertices[v]->outgoingEdges, mapToNewVertex);
		this->vertices[v]->incomingEdges = MiscUtils::transformCopy(other.vertices[v]->incomingEdges, mapToNewVertex);
	}
	
	this->_hasCycles = other._hasCycles;
	this->_didUpdateCycleStatus = other._didUpdateCycleStatus;
}

GRAPH_TMPL
GRAPH_T::Graph(GRAPH_T&& other)
{
	swap(*this, other);
}

GRAPH_TMPL
GRAPH_T& GRAPH_T::operator=(GRAPH_T other)
{
	swap(*this, other);
	return this;
}

GRAPH_TMPL
GRAPH_NODE_T* GRAPH_T::addVertex(T const& value)
{
	GRAPH_NODE_T* newVertex = new GRAPH_NODE_T(this, value);

	vertices.push_back(newVertex);
	sources.push_back(newVertex);
	sinks.push_back(newVertex);

	if (keyFunction.has_value())
	{
		auto key = keyFunction.value()(value);
		verticesOfKey.value()[key].push_back(newVertex);
		newVertex->incomingEdgesOfKey = std::unordered_map<Key, std::vector<GRAPH_NODE_T*>, ToHash<Key>>();
		newVertex->outgoingEdgesOfKey = std::unordered_map<Key, std::vector<GRAPH_NODE_T*>, ToHash<Key>>();
	}

	return newVertex;
}

GRAPH_TMPL
void GRAPH_T::removeVertex(GRAPH_NODE_T* vertex)
{
	if (!MiscUtils::containsElement(vertices, vertex))
	{
		throw std::out_of_range("No vertex of " + toString(vertex) + " in graph.");
	}

	// TODO: Algorithm to remove edges is O(n^2) (sloooow). May need to improve.
	// Problem is the O(n) search for vertices in each list.

	auto outVertices = vertex->outgoingEdges;
	for (auto outVertex : outVertices)
	{
		removeEdge(vertex, outVertex);
	}

	auto inVertices = vertex->incomingEdges;
	for (auto inVertex : inVertices)
	{
		removeEdge(inVertex, vertex);
	}

	MiscUtils::eraseElement(&vertices, vertex);
	MiscUtils::eraseIfElement(&sources, vertex);
	MiscUtils::eraseIfElement(&sinks, vertex);

	if (keyFunction.has_value())
	{
		auto key = keyFunction.value()(vertex->value);
		MiscUtils::eraseElementInMappedList(&verticesOfKey.value(), key, vertex);
	}
}

GRAPH_TMPL
size_t GRAPH_T::getVertexCount() const
{
	return vertices.size();
}

GRAPH_TMPL
GRAPH_NODE_T const* GRAPH_T::getVertex(size_t index) const
{
	if (index >= getVertexCount())
	{
		throw std::out_of_range("Vertex does not exist at index " + std::to_string(index) + " in graph.");
	}
	return vertices.at(index);
}

GRAPH_TMPL
GRAPH_NODE_T* GRAPH_T::getMutableVertex(size_t index)
{
	return MUTABLE_FROM_CONST_MEMBER_FUNC(this, getVertex, index);
}

GRAPH_TMPL
void GRAPH_T::setKeyFunction(std::function<Key(T const&)> keyFunction)
{
	if (!vertices.empty())
	{
		throw std::invalid_argument("Cannot set key function for graph with existing vertices.");
	}
	this->keyFunction = keyFunction;
	this->verticesOfKey = std::unordered_map<Key, std::vector<GRAPH_NODE_T*>, ToHash<Key>>();
}

GRAPH_TMPL
GRAPH_NODE_T const* GRAPH_T::getVertexWithKey(Key const& key, size_t index) const
{
	if (!keyFunction.has_value())
	{
		throw std::invalid_argument("Cannot get vertex when key function is not set.");
	}
	auto vertexIt = verticesOfKey.value().find(key);
	if (vertexIt == verticesOfKey.value().end() || index >= vertexIt->second.size())
	{
		throw std::invalid_argument("No vertex exists in the graph with the key and index.");
	}
	return vertexIt->second.at(index);
}

GRAPH_TMPL
GRAPH_NODE_T* GRAPH_T::getMutableVertexWithKey(Key const& key, size_t index)
{
	return MUTABLE_FROM_CONST_MEMBER_FUNC(this, getVertexWithKey, key, index);
}

GRAPH_TMPL
size_t GRAPH_T::getVertexCountWithKey(Key const& key) const
{
	if (!keyFunction.has_value())
	{
		throw std::invalid_argument("Cannot get vertex count when key function is not set.");
	}
	return verticesOfKey.value().count(key) != 0 ?
		verticesOfKey.value().at(key).size() :
		0;
}

GRAPH_TMPL
GRAPH_NODE_T* GRAPH_T::getOrAddVertexWithKey(T const& value) 
{
	if (!keyFunction.has_value())
	{
		throw std::invalid_argument("Cannot get or add vertex count when key function is not set.");
	}
	auto key = keyFunction.value()(value);
	return (getVertexCountWithKey(key) == 0) ?
		addVertex(value) :
		getMutableVertexWithKey(key);
}

GRAPH_TMPL
size_t GRAPH_T::getSourceCount() const
{
	return sources.size();
}

GRAPH_TMPL
GRAPH_NODE_T const* GRAPH_T::getSource(size_t index) const
{
	if (index >= getSourceCount())
	{
		throw std::out_of_range("Source does not exist at index " + std::to_string(index) + " in graph.");
	}
	return sources.at(index);
}

GRAPH_TMPL
GRAPH_NODE_T* GRAPH_T::getMutableSource(size_t index)
{
	return MUTABLE_FROM_CONST_MEMBER_FUNC(this, getSource, index);
}

GRAPH_TMPL
size_t GRAPH_T::getSinkCount() const
{
	return sinks.size();
}

GRAPH_TMPL
GRAPH_NODE_T const* GRAPH_T::getSink(size_t index) const
{
	if (index >= getSinkCount())
	{
		throw std::out_of_range("Sink does not exist at index " + std::to_string(index) + " in graph.");
	}
	return sinks.at(index);
}

GRAPH_TMPL
GRAPH_NODE_T* GRAPH_T::getMutableSink(size_t index)
{
	return MUTABLE_FROM_CONST_MEMBER_FUNC(this, getSink, index);
}

GRAPH_TMPL
void GRAPH_T::addEdge(GRAPH_NODE_T* source, GRAPH_NODE_T* target)
{
	checkAllNodesInGraph({source, target});

	if (hasEdge(source, target))
	{
		std::string fromToMsg = "from vertex " + toString(source) + " to vertex " + toString(target);
		throw std::out_of_range("Attempting to add edge which already exists " + fromToMsg + ".");
	}

	if (!_hasCycles)
	{
		_didUpdateCycleStatus = false;
	}
	
	bool wasGraphSink = source->getOutdegree() == 0;
	source->addOutgoingNode(target);
	if (wasGraphSink)
	{
		MiscUtils::eraseElement(&sinks, source);
	}

	bool wasGraphSource = target->getIndegree() == 0;
	target->addIncomingNode(source);
	if (wasGraphSource)
	{
		MiscUtils::eraseElement(&sources, target);
	}
}

GRAPH_TMPL
void GRAPH_T::removeEdge(GRAPH_NODE_T* source, GRAPH_NODE_T* target)
{
	checkAllNodesInGraph({source, target});

	source->removeOutgoingNode(target);
	bool isGraphSink = source->getOutdegree() == 0;
	if (isGraphSink)
	{
		sinks.push_back(source);
	}

	target->removeIncomingNode(source);
	bool isGraphSource = target->getIndegree() == 0;
	if (isGraphSource)
	{
		sources.push_back(target);
	}

	if (_hasCycles)
	{
		_didUpdateCycleStatus = false;
	}
}

GRAPH_TMPL
bool GRAPH_T::hasEdge(GRAPH_NODE_T const* source, GRAPH_NODE_T const* target) const
{
	auto hasOutgoing = MiscUtils::containsElement(source->outgoingEdges, target);
	auto hasIncoming = MiscUtils::containsElement(target->incomingEdges, source);

	std::string fromToMsg = "from vertex " + toString(source) + " to vertex " + toString(target);
	validateEdge(hasOutgoing, hasIncoming, fromToMsg);

	return hasOutgoing && hasIncoming;
}

GRAPH_TMPL
bool GRAPH_T::hasEdgeWithKey(GRAPH_NODE_T const* source, GRAPH_NODE_T const* target) const
{
	if (!keyFunction.has_value())
	{
		throw std::invalid_argument("Cannot query edge if key function is not set.");
	}

	auto outgoingNode = source->getIfOutgoingNodeWithKeyOfValue(target->getValue());
	auto incomingNode = target->getIfIncomingNodeWithKeyOfValue(source->getValue());
	
	std::string fromToMsg = "from vertex " + toString(source) + " to vertex " + toString(target);
	validateEdge(outgoingNode != nullptr, incomingNode != nullptr, fromToMsg);

	return outgoingNode != nullptr && incomingNode != nullptr;
}

GRAPH_TMPL
void GRAPH_T::validateEdge(bool hasOutgoing, bool hasIncoming, std::string const& fromToMsg) const
{
	if (hasOutgoing != hasIncoming)
	{
		if (hasOutgoing)
		{
			throw std::logic_error("Unreflected outgoing edge " + fromToMsg + ".");
		}
		else /* hasIncoming */
		{
			throw std::logic_error("Unreflected incoming edge " + fromToMsg + ".");
		}
	}
}

GRAPH_TMPL
void GRAPH_T::addIfNoEdgeWithKey(GRAPH_NODE_T* source, GRAPH_NODE_T* target) 
{
	checkAllNodesInGraph({source, target});

	if (!keyFunction.has_value())
	{
		throw std::invalid_argument("Cannot get or add edge count when key function is not set.");
	}

	if (!hasEdgeWithKey(source, target))
	{
		addEdge(source, target);
	}
}

GRAPH_TMPL
bool GRAPH_T::checkGraphForCycles()
{
	return checkGraphForCyclesInternal().has_value();
}

GRAPH_TMPL
std::optional<std::vector<GRAPH_NODE_T const*>> GRAPH_T::getCycle()
{
	auto optNodeAndCycle = checkGraphForCyclesInternal();
	if (optNodeAndCycle.has_value())
	{
		auto [node, searchPath] = optNodeAndCycle.value();
		auto cycleBegin = find(searchPath.begin(), searchPath.end(), node);
		return std::vector<GRAPH_NODE_T const*>(cycleBegin, searchPath.end());
	}
	return std::nullopt;
}

GRAPH_TMPL
std::optional<std::pair<GRAPH_NODE_T const*, std::vector<GRAPH_NODE_T const*>>> GRAPH_T::checkGraphForCyclesInternal()
{
	GRAPH_NODE_T const* repeatedNode = nullptr;
	auto path = std::vector<GRAPH_NODE_T const*>();
	if (!didUpdateCycleStatus())
	{
		_hasCycles = false;
		auto visitedNodes = std::unordered_set<GRAPH_NODE_T const*>();
		for (size_t curr = 0; curr < getVertexCount(); curr++)
		{
			auto vertex = getVertex(curr);
			if (visitedNodes.count(vertex) != 1)
			{
				auto nodesInPath = std::unordered_set<GRAPH_NODE_T const*>();
				if (checkNodeForCycles(vertex, &repeatedNode, &path, &nodesInPath, &visitedNodes))
				{
					_hasCycles = true;
					break;
				}
			}
		}
		_didUpdateCycleStatus = true;
	}
	return _hasCycles ? std::make_optional(std::make_pair(repeatedNode, path)) : std::nullopt;
}

GRAPH_TMPL
bool GRAPH_T::checkNodeForCycles(GRAPH_NODE_T const* node, GRAPH_NODE_T const** repeatedNode, std::vector<GRAPH_NODE_T const*>* path, std::unordered_set<GRAPH_NODE_T const*>* nodesInPath, std::unordered_set<GRAPH_NODE_T const*>* visitedNodes)
{
	if (nodesInPath->count(node) == 1)
	{
		*repeatedNode = node;
		return true;
	}
	else if (visitedNodes->count(node) != 1)
	{
		visitedNodes->insert(node);
		path->push_back(node);
		nodesInPath->insert(node);
		for (size_t curr = 0; curr < node->getOutdegree(); curr++)
		{
			if (checkNodeForCycles(node->getOutgoingNode(curr), repeatedNode, path, nodesInPath, visitedNodes))
			{
				return true;
			}
		}
		path->pop_back();
		nodesInPath->erase(node);
	}
	return false;
}

GRAPH_TMPL
bool GRAPH_T::hasCycles() const
{
	return _hasCycles;
}

GRAPH_TMPL
bool GRAPH_T::didUpdateCycleStatus() const
{
	return _didUpdateCycleStatus;
}

GRAPH_TMPL
void GRAPH_T::checkNodeInGraph(GRAPH_NODE_T const* node) const
{
	if (node->getGraph() != this)
	{
		throw std::invalid_argument("Attempted to edit node " + toString(node) + " which belongs to a different graph " +
			toString(node->getGraph()) + ". Expected graph " + toString(this) + ".");
	}
}

GRAPH_TMPL
void GRAPH_T::checkAllNodesInGraph(std::vector<GRAPH_NODE_T const*> const& nodes) const
{
	std::for_each(nodes.begin(), nodes.end(), [this](GRAPH_NODE_T const* node)
	{ return this->checkNodeInGraph(node); });
}

GRAPH_TMPL
std::function<Key(T const&)> const& GRAPH_T::getKeyFunction() const
{
	if (!keyFunction.has_value())
	{
		throw std::invalid_argument("Cannot get key function when the key function is not set.");
	}
	return keyFunction.value();
}

GRAPH_TMPL
std::string toString(GRAPH_T const& graph) 
{
	std::string out;
	for (size_t v = 0; v < graph.vertices.size(); v++)
	{
		auto vertex = graph.vertices.at(v);
		out += ToString<GRAPH_NODE_T>()(*vertex) + "\n";
		size_t outdegree = vertex->getOutdegree();
		for (size_t e = 0; e < outdegree; e++)
		{
			out += " -> " + ToString<GRAPH_NODE_T>()(*vertex->getOutgoingNode(e)) + "\n";
		}
	}
	return out;
}

#endif /* GRAPH_TPP */
