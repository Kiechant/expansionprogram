//
//  Tree.tpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 19/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef TREE_TPP
#define TREE_TPP

#include "Tree.hpp"

#include "MemoryUtils.hpp"
#include "StdComparison.hpp"
#include "StringConversion.hpp"
#include "TreeNode.tpp"
#include "TypeUtils.hpp"

#include <cassert>
#include <sstream>


TREE_TMPL
typename TREE_T::NodePredicate TREE_T::dontKillBranch = [](TREE_NODE_T const*){ return false; };


/** Constructors and Destructors **/

TREE_TMPL
TREE_T::Tree()
{
	root = new TREE_NODE_T();
}

TREE_TMPL
TREE_T::Tree(TREE_NODE_T* root)
{
	if (root == nullptr)
		throw std::invalid_argument("Cannot construct tree with null root.");
	this->root = root;
}

TREE_TMPL
TREE_T::Tree(Item const& rootValue)
	: root(new TREE_NODE_T(rootValue)) {}

TREE_TMPL
TREE_T::~Tree()
{
	deepDelete(root);
}


/** Copy and Move Semantics **/

TREE_TMPL
void swap(TREE_T& first, TREE_T& second)
{
	using std::swap;
	swap(first.root, second.root);
}

TREE_TMPL
TREE_T::Tree(TREE_T const& other)
{
	this->root = deepCopy(other.root);
}

TREE_TMPL
TREE_T::Tree(TREE_T&& other) : TREE_T::TREE_T()
{
	swap(*this, other);
}

TREE_TMPL
TREE_T& TREE_T::operator=(TREE_T other)
{
	swap(*this, other);
	return *this;
}


/** Comparison **/

TREE_TMPL
int compare(TREE_T const& first, TREE_T const& second)
{
	auto firstUpcoming = TREE_T::traverseInitDepthFirst(first.root);
	auto secondUpcoming = TREE_T::traverseInitDepthFirst(second.root);

	while (!firstUpcoming.empty() && !secondUpcoming.empty())
	{
		auto [firstNode, firstDepth] = TREE_T::traverseNextDepthFirst(&firstUpcoming);
		auto [secondNode, secondDepth] = TREE_T::traverseNextDepthFirst(&secondUpcoming);

		RETURN_IF_UNEQUAL_COMPARE(firstDepth, secondDepth);
		RETURN_IF_UNEQUAL_COMPARE(*firstNode, *secondNode);
	}

	RETURN_IF_UNEQUAL_COMPARE(!firstUpcoming.empty(), !secondUpcoming.empty());
	return 0;
}

COMPARISON_OPERATOR_METHODS_TMPL_IMPL(TREE_TMPL, TREE_T)


/** String Conversion **/

TREE_TMPL
TREE_T::operator std::string() const
{
	std::stringstream ss;
	auto depthNodes = TREE_T::traverseAll(TreeTraversalMode::DepthFirst, this->getRoot());
	for (auto [node, depth] : depthNodes)
	{
		ss << std::string(depth, '\t') << ToString<decltype(node)>()(node) << "\n";
	}
	return ss.str();
}

/** Accessors and Mutators **/

TREE_TMPL
TREE_NODE_T* TREE_T::getRoot() const
{
	return root;
}


/** Traversal Methods **/

TREE_TMPL
std::vector<typename TREE_T::ConstDepthNode> TREE_T::traverseAll(
	TreeTraversalMode mode, TREE_NODE_T const* root,
	NodePredicate const& killBranchIf)
{
	switch (mode)
	{
	case TreeTraversalMode::DepthFirst:
		return traverseAllDepthFirst(root, killBranchIf);
	default:
		throw "Unrecognised const traversal mode.";
	}
}

TREE_TMPL
std::vector<typename TREE_T::DepthNode> TREE_T::traverseAll(
	TreeTraversalMode mode, TREE_NODE_T* root,
	NodePredicate const& killBranchIf)
{
	switch (mode)
	{
	case TreeTraversalMode::DepthFirst:
		return traverseAllDepthFirst(root, killBranchIf);
	case TreeTraversalMode::BreadthFirst:
		return traverseAllBreadthFirst(root, killBranchIf);
	case TreeTraversalMode::PostOrder:
		return traverseAllPostOrder(root, killBranchIf);
	default:
		throw "Unrecognised traversal mode.";
	}
}

TREE_TMPL
std::vector<typename TREE_T::ConstDepthNode> TREE_T::traverseAllDepthFirst(TREE_NODE_T const* root,
	NodePredicate const& killBranchIf)
{
	std::vector<TREE_T::ConstDepthNode> visited;
	auto upcoming = traverseInitDepthFirst(root);
	while (!upcoming.empty())
	{
		if (!killBranchIf(upcoming.top().first))
		{
			visited.push_back(traverseNextDepthFirst(&upcoming));
		}
		else upcoming.pop();
	}
	return visited;
}

TREE_TMPL
std::vector<typename TREE_T::DepthNode> TREE_T::traverseAllDepthFirst(TREE_NODE_T* root,
	NodePredicate const& killBranchIf)
{
	std::vector<TREE_T::DepthNode> visited;
	auto upcoming = traverseInitDepthFirst(root);
	while (!upcoming.empty())
	{
		if (!killBranchIf(upcoming.top().first))
		{
			visited.push_back(traverseNextDepthFirst(&upcoming));
		}
		else upcoming.pop();
	}
	return visited;
}

TREE_TMPL
std::stack<typename TREE_T::ConstDepthNode> TREE_T::traverseInitDepthFirst(TREE_NODE_T const* root)
{
	assert(root != nullptr);

	std::stack<TREE_T::ConstDepthNode> upcoming;
	upcoming.push({root, 0});

	return upcoming;
}

TREE_TMPL
std::stack<typename TREE_T::DepthNode> TREE_T::traverseInitDepthFirst(TREE_NODE_T* root)
{
	assert(root != nullptr);

	std::stack<TREE_T::DepthNode> upcoming;
	upcoming.push({root, 0});

	return upcoming;
}

TREE_TMPL
typename TREE_T::ConstDepthNode TREE_T::traverseNextDepthFirst(std::stack<ConstDepthNode>* upcoming,
	NodePredicate const& killBranchIf)
{
	while (!upcoming->empty())
	{
		auto [nextNode, nextDepth] = upcoming->top();
		upcoming->pop();
		if (!killBranchIf(nextNode))
		{
			if (nextNode->getChildCount() > 0)
			{
				for (size_t c = nextNode->getChildCount() - 1; ; c--)
				{
					upcoming->push({nextNode->getChild(c), nextDepth + 1});
					if (c == 0) break;
				}
			}
			return {nextNode, nextDepth};
		}
	}
	return {nullptr, 0};
}

TREE_TMPL
typename TREE_T::DepthNode TREE_T::traverseNextDepthFirst(std::stack<DepthNode>* upcoming,
	NodePredicate const& killBranchIf)
{
	while (!upcoming->empty())
	{
		auto [nextNode, nextDepth] = upcoming->top();
		upcoming->pop();
		if (!killBranchIf(nextNode))
		{
			if (nextNode->getChildCount() > 0)
			{
				for (size_t c = nextNode->getChildCount() - 1; ; c--)
				{
					upcoming->push({nextNode->getChild(c), nextDepth + 1});
					if (c == 0) break;
				}
			}
			return {nextNode, nextDepth};
		}
	}
	return {nullptr, 0};
}

TREE_TMPL
std::vector<typename TREE_T::DepthNode> TREE_T::traverseAllBreadthFirst(TREE_NODE_T* root,
	NodePredicate const& killBranchIf)
{
	std::vector<TREE_T::DepthNode> visited;
	auto upcoming = traverseInitBreadthFirst(root);
	while (!upcoming.empty())
	{
		if (!killBranchIf(upcoming.front().first))
		{
			visited.push_back(traverseNextBreadthFirst(&upcoming));
		}
		else upcoming.pop();
	}
	return visited;
}

TREE_TMPL
std::queue<typename TREE_T::DepthNode> TREE_T::traverseInitBreadthFirst(TREE_NODE_T* root)
{
	assert(root != nullptr);

	std::queue<TREE_T::DepthNode> upcoming;
	upcoming.push({root, 0});

	return upcoming;
}

TREE_TMPL
typename TREE_T::DepthNode TREE_T::traverseNextBreadthFirst(std::queue<DepthNode>* upcoming)
{
	if (!upcoming->empty())
	{
		auto [nextNode, nextDepth] = upcoming->front();
		upcoming->pop();
		for (size_t c = 0; c < nextNode->getChildCount(); c++)
		{
			upcoming->push({nextNode->getChild(c), nextDepth + 1});
		}
		return {nextNode, nextDepth};
	}
	return {nullptr, 0};
}

TREE_TMPL
std::vector<typename TREE_T::DepthNode> TREE_T::traverseAllPostOrder(TREE_NODE_T* root,
	NodePredicate const& killBranchIf)
{
	std::vector<TREE_T::DepthNode> visited;
	auto upcoming = traverseInitPostOrder(root);
	while (!upcoming.empty())
	{
		visited.push_back(traverseNextPostOrder(&upcoming, killBranchIf));
	}
	return visited;
}

TREE_TMPL
std::stack<typename TREE_T::MarkedDepthNode> TREE_T::traverseInitPostOrder(TREE_NODE_T* root)
{
	assert(root != nullptr);

	std::stack<TREE_T::MarkedDepthNode> upcoming;
	upcoming.push({root, 0, false});

	return upcoming;
}

TREE_TMPL
typename TREE_T::DepthNode TREE_T::traverseNextPostOrder(std::stack<MarkedDepthNode>* upcoming,
	NodePredicate const& killBranchIf)
{
	if (!upcoming->empty())
	{
		auto [nextNode, nextDepth, areBranchesExplored] = upcoming->top();
		if (!areBranchesExplored)
		{
			// Marks the current node as explored and recursively explores its branches.
			std::get<2>(upcoming->top()) = true;
			while (!areBranchesExplored)
			{
				if (!killBranchIf(nextNode))
				{
					if (nextNode->getChildCount() > 0)
					{
						for (size_t c = nextNode->getChildCount() - 1; ; c--)
						{
							upcoming->push({nextNode->getChild(c), nextDepth + 1, false});
							if (c == 0) break;
						}
					}
				}
				else
				{
					upcoming->pop();
				}
				std::tie(nextNode, nextDepth, areBranchesExplored) = upcoming->top();
				std::get<2>(upcoming->top()) = true;
			}
		}
		upcoming->pop();
		return {nextNode, nextDepth};
	}
	return {nullptr, 0};
}

#endif /* TREE_TPP */
