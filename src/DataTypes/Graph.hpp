//
//  Graph.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 8/8/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef GRAPH_HPP
#define GRAPH_HPP

#include "GraphDecl.hpp"

#include "GraphNodeDecl.hpp"
#include "HashingDecl.hpp"

#include <stack>
#include <unordered_map>
#include <unordered_set>
#include <vector>

/* Forward declares friend methods. */
GRAPH_TMPL class Graph;
GRAPH_TMPL void swap(GRAPH_T&, GRAPH_T&);
GRAPH_TMPL std::string toString(GRAPH_T const&);

template <typename T, typename Key>
class Graph
{
public:
	Graph();

	friend void swap<>(GRAPH_T&, GRAPH_T&);
	Graph(GRAPH_T const&);
	Graph(GRAPH_T&&);
	GRAPH_T& operator=(GRAPH_T);

	GRAPH_NODE_T* addVertex(T const& value);
	void removeVertex(GRAPH_NODE_T* vertex);

	size_t getVertexCount() const;
	GRAPH_NODE_T const* getVertex(size_t index) const;
	GRAPH_NODE_T* getMutableVertex(size_t index);

	void setKeyFunction(std::function<Key(T const&)> keyFunction);
	GRAPH_NODE_T const* getVertexWithKey(Key const& key, size_t index = 0) const;
	GRAPH_NODE_T* getMutableVertexWithKey(Key const& key, size_t index = 0);
	size_t getVertexCountWithKey(Key const& key) const;

	GRAPH_NODE_T* getOrAddVertexWithKey(T const& value);

	size_t getSourceCount() const;
	GRAPH_NODE_T const* getSource(size_t index) const;
	GRAPH_NODE_T* getMutableSource(size_t index);
	size_t getSinkCount() const;
	GRAPH_NODE_T const* getSink(size_t index) const;
	GRAPH_NODE_T* getMutableSink(size_t index);

	void addEdge(GRAPH_NODE_T* source, GRAPH_NODE_T* target);
	void removeEdge(GRAPH_NODE_T* source, GRAPH_NODE_T* target);

	bool hasEdge(GRAPH_NODE_T const* source, GRAPH_NODE_T const* target) const;
	bool hasEdgeWithKey(GRAPH_NODE_T const* source, GRAPH_NODE_T const* target) const;
	void validateEdge(bool hasOutgoing, bool hasIncoming, std::string const& fromToMsg) const;

	void addIfNoEdgeWithKey(GRAPH_NODE_T* source, GRAPH_NODE_T* target);

	bool checkGraphForCycles();
	std::optional<std::vector<GRAPH_NODE_T const*>> getCycle();

	bool hasCycles() const;
	bool didUpdateCycleStatus() const;

	std::function<Key(T const&)> const& getKeyFunction() const;

	friend std::string toString<>(GRAPH_T const&);
	
private:
	std::vector<GRAPH_NODE_T*> vertices;
	std::vector<GRAPH_NODE_T*> sources; // Vertices with indegree 0.
	std::vector<GRAPH_NODE_T*> sinks;   // Vertices with outdegree 0.

	std::optional<std::function<Key(T const&)>> keyFunction;
	std::optional<std::unordered_map<Key, std::vector<GRAPH_NODE_T*>, ToHash<Key>>> verticesOfKey;

	bool _hasCycles = false;
	bool _didUpdateCycleStatus = true;

	std::optional<std::pair<GRAPH_NODE_T const*, std::vector<GRAPH_NODE_T const*>>> checkGraphForCyclesInternal();
	bool checkNodeForCycles(GRAPH_NODE_T const* node, GRAPH_NODE_T const** repeatedNode, std::vector<GRAPH_NODE_T const*>* path, std::unordered_set<GRAPH_NODE_T const*>* nodesInPath, std::unordered_set<GRAPH_NODE_T const*>* visitedNodes);

	void checkAllNodesInGraph(std::vector<GRAPH_NODE_T const*> const& nodes) const;
	void checkNodeInGraph(GRAPH_NODE_T const* node) const;
};

#endif /* GRAPH_HPP */
