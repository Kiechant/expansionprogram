//
//  EnumStringMapDecl.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 21/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef ENUM_STRING_MAP_DECL_HPP
#define ENUM_STRING_MAP_DECL_HPP

#define ENUM_STRING_MAP_TMPL template<typename E>
#define ENUM_STRING_MAP_T EnumStringMap<E>

template<typename E>
class EnumStringMap;

#endif /* ENUM_STRING_MAP_DECL_HPP */
