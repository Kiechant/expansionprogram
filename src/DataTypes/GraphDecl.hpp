//
//  GraphDecl.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef GRAPH_DECL_HPP
#define GRAPH_DECL_HPP

template<typename T, typename Key>
class Graph;

template<typename T, typename Key = T>
class Graph;

#define GRAPH_TMPL template<typename T, typename Key>
#define GRAPH_T Graph<T, Key>

#endif /* GRAPH_DECL_HPP */
