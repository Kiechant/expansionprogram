//
//  EnumStringMap.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 21/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef ENUM_STRING_MAP_HPP
#define ENUM_STRING_MAP_HPP

#include "EnumStringMapDecl.hpp"

#include <string>
#include <unordered_map>
#include <vector>

template<typename E>
class EnumStringMap
{
	static_assert(std::is_enum_v<E>, "EnumStringMap must be instantiated with an enum type.");

public:
	EnumStringMap(std::vector<std::string> const& enumToStrList);

	std::string const& getString(E enumValue) const;
	E getEnum(std::string const& str) const;

private:
	std::vector<std::string> enumToStrList;
	std::unordered_map<std::string, E> strToEnumMap;
};

#endif /* ENUM_STRING_MAP_HPP */
