//
//  GraphNodeDecl.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef GRAPH_NODE_DECL_HPP
#define GRAPH_NODE_DECL_HPP

template<typename T, typename Key>
class GraphNode;

#define GRAPH_NODE_TMPL template<typename T, typename Key>
#define GRAPH_NODE_T GraphNode<T, Key>

#endif /* GRAPH_NODE_DECL_HPP */
