//
//  TreeDecl.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef TREE_DECL_HPP
#define TREE_DECL_HPP

template<typename Item, typename Key>
class Tree;

template<typename Item, typename Key = Item>
class Tree;

#define TREE_TMPL template<typename Item, typename Key>
#define TREE_T Tree<Item, Key>

#endif /* TREE_DECL_HPP */
