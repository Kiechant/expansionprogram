//
//  Tree.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 29/1/20.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#ifndef TREE_HPP
#define TREE_HPP

#include "TreeDecl.hpp"

#include "ComparisonOperatorMethodsDecl.hpp"
#include "TreeNodeDecl.hpp"

#include <functional>
#include <queue>
#include <stack>
#include <vector>

/* Forward declares friend methods. */
TREE_TMPL class Tree;
TREE_TMPL void swap(TREE_T&, TREE_T&);
TREE_TMPL int compare(TREE_T const&, TREE_T const&);

/* Defines the way in which a subtree will be searched. */
enum class TreeTraversalMode
{
	DepthFirst,
	BreadthFirst,
	PostOrder
};

/* A tree data structure comprised of nodes with a variable number of children,
   and a parent. Comprised of a single root node, which internally stores the
   network of subnodes.
   
   The Tree is respronsible tree-wide operations, including memory management,
   traversal and comparison. */
template<typename Item, typename Key>
class Tree
{
public:
	/** Constructors and Destructors **/
	
	Tree();
	Tree(TREE_NODE_T* root);
	Tree(Item const& rootValue);
	~Tree();
	

	/** Copy and Move Semantics **/
	
	friend void swap<>(TREE_T&, TREE_T&);
	Tree(const Tree&);
	Tree(Tree&&);
	Tree& operator=(Tree);


	/** Comparison **/

	friend int compare<>(TREE_T const&, TREE_T const&);
	COMPARISON_OPERATOR_METHODS_DECL(Tree)

	
	/** String Conversion **/

	operator std::string() const;
	

	/** Accessors and Mutators **/
	
	TREE_NODE_T* getRoot() const;
	

	/** Traversal Methods **/

	/* The first element is the node, the second is its depth from the root. */
	using DepthNode = std::pair<TREE_NODE_T*, size_t>;
	using ConstDepthNode = std::pair<TREE_NODE_T const*, size_t>;
	using MarkedDepthNode = std::tuple<TREE_NODE_T*, size_t, bool>;

	using NodePredicate = std::function<bool(TREE_NODE_T const*)>;
	static NodePredicate dontKillBranch;
	
	/* Generates a vector of all subnodes to the root visited in the sequence of
	   the traversal mode. */
	static std::vector<DepthNode> traverseAll(TreeTraversalMode mode, TREE_NODE_T* root,
		NodePredicate const& killBranchIf = dontKillBranch);
	static std::vector<ConstDepthNode> traverseAll(TreeTraversalMode mode, TREE_NODE_T const* root,
		NodePredicate const& killBranchIf = dontKillBranch);

	/* Traverses all subnodes from the root using depth-first search. */
	static std::vector<DepthNode> traverseAllDepthFirst(TREE_NODE_T* root,
		NodePredicate const& killBranchIf = dontKillBranch);
	static std::vector<ConstDepthNode> traverseAllDepthFirst(TREE_NODE_T const* root,
		NodePredicate const& killBranchIf = dontKillBranch);

	/* Initialises a depth-first search at the root. */
	static std::stack<DepthNode> traverseInitDepthFirst(TREE_NODE_T* root);
	static std::stack<ConstDepthNode> traverseInitDepthFirst(TREE_NODE_T const* root);
	
	/* Obtains the next node in the sequence for a depth-first search. */
	static DepthNode traverseNextDepthFirst(std::stack<DepthNode>* upcoming,
		NodePredicate const& killBranchIf = dontKillBranch);
	static ConstDepthNode traverseNextDepthFirst(std::stack<ConstDepthNode>* upcoming,
		NodePredicate const& killBranchIf = dontKillBranch);

	/* Traverses all subnodes from the root using breadth-first search. */
	static std::vector<DepthNode> traverseAllBreadthFirst(TREE_NODE_T* root,
		NodePredicate const& killBranchIf = dontKillBranch);

	/* Initialises a breadth-first search at the root. */
	static std::queue<DepthNode> traverseInitBreadthFirst(TREE_NODE_T* root);

	/* Obtains the next node in the sequence for a breadth-first search. */
	static DepthNode traverseNextBreadthFirst(std::queue<DepthNode>* upcoming);

	/* Traverses all subnodes from the root using post-order search. */
	static std::vector<DepthNode> traverseAllPostOrder(TREE_NODE_T* root,
		NodePredicate const& killBranchIf = dontKillBranch);

	/* Initialises a post-order search at the root. */
	static std::stack<MarkedDepthNode> traverseInitPostOrder(TREE_NODE_T* root);

	/* Obtains the next node in the sequence for a post-order search. */
	static DepthNode traverseNextPostOrder(std::stack<MarkedDepthNode>* upcoming,
		NodePredicate const& killBranchIf = dontKillBranch);

protected:
	TREE_NODE_T* root = nullptr;
};

#endif
