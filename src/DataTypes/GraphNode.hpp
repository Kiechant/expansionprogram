//
//  GraphNode.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 8/8/20.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef GRAPH_NODE_HPP
#define GRAPH_NODE_HPP

#include "HashingDecl.hpp"
#include "GraphDecl.hpp"

#include <functional>
#include <unordered_map>
#include <vector>

/* Forward declares friend methods. */
GRAPH_NODE_TMPL class GraphNode;
GRAPH_NODE_TMPL void swap(GRAPH_NODE_T&, GRAPH_NODE_T&);

template <typename T, typename Key = T>
class GraphNode
{
friend Graph<T, Key>;

public:
	friend void swap<>(GRAPH_NODE_T&, GRAPH_NODE_T&);
	GraphNode(GRAPH_NODE_T const&);
	GraphNode(GRAPH_NODE_T&&);
	GRAPH_NODE_T& operator=(GRAPH_NODE_T);

	Graph<T, Key> const* getGraph() const;

	T const& getValue() const;
	void setValue(T const& value);
	void modifyValue(std::function<void(T*)> modifyFunc);

	size_t getDegree() const;
	size_t getIndegree() const;
	size_t getOutdegree() const;
	GRAPH_NODE_T const* getIncomingNode(size_t index) const;
	GRAPH_NODE_T const* getOutgoingNode(size_t index) const;

	GRAPH_NODE_T const* getIncomingNodeWithKey(Key const& key, size_t index = 0) const;
	GRAPH_NODE_T const* getIncomingNodeWithKeyOfValue(T const& value) const;
	GRAPH_NODE_T const* getIfIncomingNodeWithKeyOfValue(T const& value) const;
	size_t getIncomingNodeCountWithKey(Key const& key) const;

	GRAPH_NODE_T const* getOutgoingNodeWithKey(Key const& key, size_t index = 0) const;
	GRAPH_NODE_T const* getOutgoingNodeWithKeyOfValue(T const& value) const;
	GRAPH_NODE_T const* getIfOutgoingNodeWithKeyOfValue(T const& value) const;
	size_t getOutgoingNodeCountWithKey(Key const& key) const;

	operator std::string() const;

private:
	GraphNode(Graph<T, Key> const* graph, T const& value);

	Graph<T, Key> const* graph = nullptr;
	T value;

	std::vector<GRAPH_NODE_T*> incomingEdges;
	std::vector<GRAPH_NODE_T*> outgoingEdges;

	// Initialised by graph when a vertex is added and the key function is set.
	std::optional<std::unordered_map<Key, std::vector<GRAPH_NODE_T*>, ToHash<Key>>> incomingEdgesOfKey;
	std::optional<std::unordered_map<Key, std::vector<GRAPH_NODE_T*>, ToHash<Key>>> outgoingEdgesOfKey;
	
	void setGraph(Graph<T, Key> const*);

	GRAPH_NODE_T* getMutableIncomingNode(size_t index);
	GRAPH_NODE_T* getMutableOutgoingNode(size_t index);

	void addIncomingNode(GRAPH_NODE_T* node);
	void removeIncomingNode(GRAPH_NODE_T* node);
	void addOutgoingNode(GRAPH_NODE_T* node);
	void removeOutgoingNode(GRAPH_NODE_T* node);
};

#endif /* GRAPH_NODE_HPP */
