//
//  TreeNode.tpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 19/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef TREE_NODE_TPP
#define TREE_NODE_TPP

#include "TreeNode.hpp"

#include "MemoryUtils.hpp"
#include "StdComparison.hpp"
#include "StdHashing.hpp"

#include <algorithm>
#include <exception>

/** Constructors and Destructors **/

TREE_NODE_TMPL
TREE_NODE_T::TreeNode()
	: keyOfItem(defaultKeyOfItem())
{
	static_assert(std::is_constructible_v<Key, Item>, "TreeNode constructor invoked "
		"with default item-to-key function when no default exists. Must pass "
		"explicit item-to-key function to a specialised TreeNode constructor.");
}

TREE_NODE_TMPL
TREE_NODE_T::TreeNode(Item const& item)
	: TreeNode()
{
	this->item = item;
}

TREE_NODE_TMPL
TREE_NODE_T::TreeNode(
	std::function<Key(Item const&)> const& keyOfItem)
		: keyOfItem(keyOfItem) {}

TREE_NODE_TMPL
TREE_NODE_T::TreeNode(
	Item const& item, std::function<Key(Item const&)> const& keyOfItem)
		: item(item), keyOfItem(keyOfItem) {}


/** Comparison **/

TREE_NODE_TMPL
int compare(TREE_NODE_T const& first, TREE_NODE_T const& second)
{
	return compare(first.item, second.item);
}

COMPARISON_OPERATOR_METHODS_TMPL_IMPL(TREE_NODE_TMPL, TREE_NODE_T)


/** String Conversion **/

TREE_NODE_TMPL
TREE_NODE_T::operator std::string() const
{
	return ToString<Item>()(item);
}


/** Deep Copy and Deletion **/

TREE_NODE_TMPL
TREE_NODE_T* deepCopy(TREE_NODE_T const* other)
{
	auto _new = new TREE_NODE_T(*other);
	
	transform(other->children.begin(), other->children.end(), _new->children.begin(),
		[_new](TREE_NODE_T* const& child)
		{
			TREE_NODE_T* newChild = deepCopy(child);
			newChild->parent = _new;
			return newChild;
		});
	
	return _new;
}

TREE_NODE_TMPL
void deepDelete(TREE_NODE_T const* node)
{
	const_cast<TREE_NODE_T*>(node)->deepDeleteChildren();
	delete node;
}


/** Accessors and Mutators **/

TREE_NODE_TMPL
TREE_NODE_T* TREE_NODE_T::getParent() const
{
	return parent;
}

TREE_NODE_TMPL
bool TREE_NODE_T::isRoot() const
{
	return parent == nullptr;
}

TREE_NODE_TMPL
bool TREE_NODE_T::isLeaf() const
{
	return children.size() == 0;
}

TREE_NODE_TMPL
Item const& TREE_NODE_T::getItem() const
{
	return item;
}

TREE_NODE_TMPL
void TREE_NODE_T::setItem(Item const& newItem)
{
	if (this->item != newItem)
	{
		if (parent != nullptr)
		{
			Key oldKey = parent->keyOfItem(this->item);
			this->item = newItem;
			parent->rekeyChild(this, oldKey, parent->keyOfItem(newItem));
		}
		else
		{
			this->item = newItem;
		}
	}
}

TREE_NODE_TMPL
void TREE_NODE_T::modifyItem(std::function<void(Item* item)> modifyFunc)
{
	if (parent != nullptr)
	{
		Key oldKey = parent->keyOfItem(item);
		modifyFunc(&item);
		parent->rekeyChild(this, oldKey, parent->keyOfItem(item));
	}
	else modifyFunc(&item);
}


/** Accessing and Iterating over Children **/

TREE_NODE_TMPL
size_t TREE_NODE_T::getChildCount() const
{
	return children.size();
}

TREE_NODE_TMPL
typename TREE_NODE_T::TreeNodeIter TREE_NODE_T::getChildBegin() const
{
	return children.begin();
}

TREE_NODE_TMPL
typename TREE_NODE_T::TreeNodeIter TREE_NODE_T::getChildEnd() const
{
	return children.end();
}

TREE_NODE_TMPL
TREE_NODE_T* TREE_NODE_T::getChild(size_t at) const
{
	if (at < children.size())
	{
		return children[at];
	}
	return nullptr;
}

TREE_NODE_TMPL
TREE_NODE_T* TREE_NODE_T::getBranch(std::vector<size_t> const& indices) const
{
	if (indices.empty())
		throw std::invalid_argument("Indices list cannot be empty.");
	TREE_NODE_T* curr = const_cast<TREE_NODE_T*>(this); 
	for (size_t index : indices)
		curr = curr->getChild(index);
	return curr;
}


/** Indexing Children by Key **/

TREE_NODE_TMPL
size_t TREE_NODE_T::getChildCountWithKey(Key const& key) const
{
	auto indicesIt = childrenOfKey.find(key);
	return (indicesIt != childrenOfKey.end()) ? indicesIt->second.size() : 0; 
}

TREE_NODE_TMPL
TREE_NODE_T* TREE_NODE_T::getChildWithKey(Key const& key, size_t keyIndex) const
{
	auto indicesIt = childrenOfKey.find(key);
	if (indicesIt != childrenOfKey.end())
	{
		if (keyIndex < indicesIt->second.size())
		{
			size_t index = indicesIt->second[keyIndex];
			return children[index];
		}
	}
	return nullptr;
}

TREE_NODE_TMPL
TREE_NODE_T* TREE_NODE_T::getBranchWithKey(std::vector<Key> const& keys) const
{
	if (keys.empty())
		throw std::invalid_argument("Keys list cannot be empty.");
	TREE_NODE_T* curr = const_cast<TREE_NODE_T*>(this);
	for (Key const& key : keys)
		curr = curr->getChildWithKey(key, 0);
	return curr;
}


/** Finding Children by Key **/

TREE_NODE_TMPL
typename TREE_NODE_T::TreeNodeIter TREE_NODE_T::findChild(Key const& key) const
{
	return findChild(key, children.begin());
}

TREE_NODE_TMPL
typename TREE_NODE_T::TreeNodeIter TREE_NODE_T::findChild(
	Key const& key, TREE_NODE_T::TreeNodeIter begin) const
{
	return std::find_if(begin, children.end(), [this, &key](TREE_NODE_T* const& child)
		{
			return keyOfItem(child->item) == key;
		});
}

TREE_NODE_TMPL
size_t TREE_NODE_T::findChild(Key const& key, size_t begin) const
{
	return findChild(key, children.begin() + begin) - children.begin();
}


/** Adding Children **/

TREE_NODE_TMPL
TREE_NODE_T* TREE_NODE_T::addChild(TREE_NODE_T* child)
{
	children.push_back(child);
	addChildIndexToKey(children.size() - 1, keyOfItem(child->item));
	child->parent = this;
	return child;
}

TREE_NODE_TMPL
TREE_NODE_T* TREE_NODE_T::addChild(Item const& item)
{
	TREE_NODE_T* child = new TREE_NODE_T(item, keyOfItem);
	return addChild(child);
}

TREE_NODE_TMPL
void TREE_NODE_T::addChildren(std::vector<Item> const& items)
{
	addChildren(createNodesFromItems(items));
}

TREE_NODE_TMPL
void TREE_NODE_T::addChildren(std::vector<TreeNode*> const& nodes)
{
	addChildren(nodes.begin(), nodes.end());
}

TREE_NODE_TMPL
template<typename It>
std::enable_if_t<TREE_NODE_T::template IsTreeNodeInputIterator<It>::value, void>
TREE_NODE_T::addChildren(It begin, It end)
{
	for (It curr = begin; curr != end; curr++)
		addChild(*curr);
}

TREE_NODE_TMPL
TREE_NODE_T* TREE_NODE_T::addBranch(std::vector<Item> const& items)
{
	return addBranch(createNodesFromItems(items));
}

TREE_NODE_TMPL
TREE_NODE_T* TREE_NODE_T::addBranch(std::vector<TREE_NODE_T*> const& nodes)
{
	return addBranch(nodes.begin(), nodes.end());
}

TREE_NODE_TMPL
template<typename It>
std::enable_if_t<TREE_NODE_T::template IsTreeNodeInputIterator<It>::value, TREE_NODE_T*>
TREE_NODE_T::addBranch(It begin, It end)
{
	TREE_NODE_T* curr = this;
	for (auto it = begin; it != end; it++)
		curr = curr->addChild(*it);
	return curr;
}


/** Removing and Deleting Children **/

TREE_NODE_TMPL
void TREE_NODE_T::clearChildren()
{
	children = std::vector<TREE_NODE_T*>();
}

TREE_NODE_TMPL
void TREE_NODE_T::deepDeleteChildren()
{
	for (auto child : children)
	{
		deepDelete(child);
	}
	children = std::vector<TREE_NODE_T*>();
}


/** Key Management **/

TREE_NODE_TMPL
std::function<Key(Item const&)> TREE_NODE_T::defaultKeyOfItem()
{
	return [](Item const& item){ return Key(item); };
}

TREE_NODE_TMPL
void TREE_NODE_T::addChildIndexToKey(size_t childIndex, Key const& key)
{
	auto* indices = &childrenOfKey[key];
	indices->push_back(childIndex);
	children[childIndex]->keyIndex = indices->size() - 1;
}

TREE_NODE_TMPL
void TREE_NODE_T::removeKeyIndex(size_t keyIndex, Key const& key)
{
	auto* indices = &childrenOfKey.at(key);

	if (indices->size() == 1)
	{
		// Removes key from map if only one exists.
		childrenOfKey.erase(key);
	}
	else if (indices->size() == 2)
	{
		// Removes child index from list.
		// Swaps the child indices at the key index and the last position.
		// Updates the key index of the last child.
		std::swap(indices->at(keyIndex), indices->back());
		indices->pop_back();
		children[indices->at(keyIndex)]->keyIndex = keyIndex;
	}
}

TREE_NODE_TMPL
void TREE_NODE_T::rekeyChild(TREE_NODE_T* child, Key const& oldKey, Key const& newKey)
{
	if (oldKey != newKey)
	{
		auto const& indices = childrenOfKey.at(oldKey);
		for (size_t keyIndex = 0; keyIndex < indices.size(); keyIndex++)
		{
			size_t childIndex = indices[keyIndex];
			if (children[childIndex] == child)
			{
				removeKeyIndex(keyIndex, oldKey);
				addChildIndexToKey(childIndex, newKey);
				break;
			}
		}
	}
}


/** Helper Methods **/

TREE_NODE_TMPL
std::vector<TREE_NODE_T*> TREE_NODE_T::createNodesFromItems(std::vector<Item> const& items)
{
	auto nodes = std::vector<TREE_NODE_T*>(items.size(), nullptr);
	std::transform(items.begin(), items.end(), nodes.begin(),
		[](Item const& item){ return new TREE_NODE_T(item); });
	return nodes;
}

#endif /* TREE_NODE_TPP */
