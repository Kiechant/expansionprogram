//
//  EnumStringMap.tpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 21/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef ENUM_STRING_MAP_TPP
#define ENUM_STRING_MAP_TPP

#include "EnumStringMap.hpp"

#include "MemoryUtils.hpp"

#include <algorithm>
#include <limits>

ENUM_STRING_MAP_TMPL
ENUM_STRING_MAP_T::EnumStringMap(std::vector<std::string> const& enumToStrList) 
{
	if (enumToStrList.size() >= std::numeric_limits<int>::max())
	{
		throw std::invalid_argument("Enum-to-string list too large in enum map instantiation.");
	}
	this->enumToStrList = enumToStrList;
	for (int i = 0; i < (int)enumToStrList.size(); i++)
	{
		this->strToEnumMap.emplace(enumToStrList.at(i), (E)i);
	}
}

ENUM_STRING_MAP_TMPL
std::string const& ENUM_STRING_MAP_T::getString(E enumValue) const
{
	if ((size_t)enumValue >= enumToStrList.size() || (int)enumValue < 0)
	{
		throw std::out_of_range("Enum of value '" + std::to_string((int)enumValue) + "' is outside the enum map.");
	}
	return enumToStrList.at((size_t)enumValue);
}

ENUM_STRING_MAP_TMPL
E ENUM_STRING_MAP_T::getEnum(std::string const& str) const
{
	auto enumIt = strToEnumMap.find(str);
	if (enumIt == strToEnumMap.end())
	{
		throw std::out_of_range("String of value '" + str + "' is outside the enum map.");
	}
	return enumIt->second;
}

#endif /* ENUM_STRING_MAP_TPP */
