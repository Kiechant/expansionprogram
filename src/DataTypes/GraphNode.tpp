//
//  GraphNode.tpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 19/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef GRAPH_NODE_TPP
#define GRAPH_NODE_TPP

#include "GraphNode.hpp"

#include "MiscUtils.hpp"
#include "StdHashing.hpp"
#include "StdStringConversion.hpp"
#include "TypeUtils.hpp"

#include <algorithm>

GRAPH_NODE_TMPL
GRAPH_NODE_T::GraphNode(Graph<T, Key> const* graph, T const& value)
	: graph(graph), value(value) {}

GRAPH_NODE_TMPL
void swap(GRAPH_NODE_T& first, GRAPH_NODE_T& second)
{
	using std::swap;

	swap(first.graph, second.graph);
	swap(first.value, second.value);
	swap(first.incomingEdges, second.incomingEdges);
	swap(first.outgoingEdges, second.outgoingEdges);
}

GRAPH_NODE_TMPL
GRAPH_NODE_T::GraphNode(GRAPH_NODE_T const& other)
{
	this->graph = other.graph;
	this->value = other.value;
	this->incomingEdges = other.incomingEdges;
	this->outgoingEdges = other.outgoingEdges;
}

GRAPH_NODE_TMPL
GRAPH_NODE_T::GraphNode(GRAPH_NODE_T&& other)
{
	swap(*this, other);
}

GRAPH_NODE_TMPL
GRAPH_NODE_T& GRAPH_NODE_T::operator=(GRAPH_NODE_T other)
{
	swap(*this, other);
	return this;
}

GRAPH_NODE_TMPL
Graph<T, Key> const* GRAPH_NODE_T::getGraph() const
{
	return graph;
}

GRAPH_NODE_TMPL
void GRAPH_NODE_T::setGraph(Graph<T, Key> const* graph)
{
	this->graph = graph;
}

GRAPH_NODE_TMPL
T const& GRAPH_NODE_T::getValue() const
{
	return value;
}

GRAPH_NODE_TMPL
void GRAPH_NODE_T::setValue(T const& value)
{
	this->value = value;
}

GRAPH_NODE_TMPL
void GRAPH_NODE_T::modifyValue(std::function<void(T*)> modifyFunc)
{
	modifyFunc(&this->value);
}

GRAPH_NODE_TMPL
size_t GRAPH_NODE_T::getDegree() const
{
	return getIndegree() + getOutdegree();
}

GRAPH_NODE_TMPL
size_t GRAPH_NODE_T::getIndegree() const
{
	return incomingEdges.size();
}

GRAPH_NODE_TMPL
size_t GRAPH_NODE_T::getOutdegree() const
{
	return outgoingEdges.size();
}

GRAPH_NODE_TMPL
GRAPH_NODE_T const* GRAPH_NODE_T::getIncomingNode(size_t index) const
{
	if (index >= getIndegree())
	{
		throw std::out_of_range("Incoming edge does not exist at index " + std::to_string(index) + " in graph node.");
	}
	return incomingEdges.at(index);
}

GRAPH_NODE_TMPL
GRAPH_NODE_T const* GRAPH_NODE_T::getOutgoingNode(size_t index) const
{
	if (index >= getOutdegree())
	{
		throw std::out_of_range("Outgoing edge does not exist at index " + std::to_string(index) + " in graph node.");
	}
	return outgoingEdges.at(index);
}

GRAPH_NODE_TMPL
GRAPH_NODE_T const* GRAPH_NODE_T::getIncomingNodeWithKey(Key const& key, size_t index) const
{
	if (!incomingEdgesOfKey.has_value())
	{
		throw std::invalid_argument("Cannot get incoming node when map is not initialised.");
	}
	auto edgeIt = incomingEdgesOfKey.value().find(key);
	if (edgeIt == incomingEdgesOfKey.value().end() || index >= edgeIt->second.size())
	{
		throw std::invalid_argument("Incoming node does not exist with key and index.");
	}
	return edgeIt->second.at(index);
}

GRAPH_NODE_TMPL
GRAPH_NODE_T const* GRAPH_NODE_T::getIncomingNodeWithKeyOfValue(T const& value) const
{
	auto node = getIfIncomingNodeWithKeyOfValue(value);
	if (node == nullptr)
	{
		throw std::invalid_argument("Incoming node of value '" + ToString<T>(value) + "' does not exist.");
	}
	return node;
}

GRAPH_NODE_TMPL
GRAPH_NODE_T const* GRAPH_NODE_T::getIfIncomingNodeWithKeyOfValue(T const& value) const
{
	auto key = graph->getKeyFunction()(value);
	if (incomingEdgesOfKey.value().count(key) > 0)
	{
		auto candidateEdges = &incomingEdgesOfKey.value().at(key);
		auto matchedEdgeIt = find_if(candidateEdges->begin(), candidateEdges->end(),
			[&value](GRAPH_NODE_T* target)
			{
				return target->getValue() == value;
			});
		return (matchedEdgeIt != candidateEdges->end()) ?
			*matchedEdgeIt :
			nullptr;
	}
	return nullptr;
}

GRAPH_NODE_TMPL
size_t GRAPH_NODE_T::getIncomingNodeCountWithKey(Key const& key) const
{
	if (!incomingEdgesOfKey.has_value())
	{
		throw std::invalid_argument("Cannot get incoming node count when map is not initialised.");
	}
	return incomingEdgesOfKey.value().count(key) != 0 ?
		incomingEdgesOfKey.value().at(key).size() :
		0;
}

GRAPH_NODE_TMPL
GRAPH_NODE_T const* GRAPH_NODE_T::getOutgoingNodeWithKey(Key const& key, size_t index) const
{
	if (!outgoingEdgesOfKey.has_value())
	{
		throw std::invalid_argument("Cannot get outgoing node when map is not initialised.");
	}
	auto edgeIt = outgoingEdgesOfKey.value().find(key);
	if (edgeIt == outgoingEdgesOfKey.value().end() || index >= edgeIt->second.size())
	{
		throw std::invalid_argument("Outgoing node does not exist with key and index.");
	}
	return edgeIt->second.at(index);
}

GRAPH_NODE_TMPL
GRAPH_NODE_T const* GRAPH_NODE_T::getOutgoingNodeWithKeyOfValue(T const& value) const
{
	auto node = getIfOutgoingNodeWithKeyOfValue(value);
	if (node == nullptr)
	{
		throw std::invalid_argument("Outgoing node of value '" + ToString<T>(value) + "' does not exist.");
	}
	return node;
}

GRAPH_NODE_TMPL
GRAPH_NODE_T const* GRAPH_NODE_T::getIfOutgoingNodeWithKeyOfValue(T const& value) const
{
	auto key = graph->getKeyFunction()(value);
	if (outgoingEdgesOfKey.value().count(key) > 0)
	{
		auto candidateEdges = &outgoingEdgesOfKey.value().at(key);
		auto matchedEdgeIt = find_if(candidateEdges->begin(), candidateEdges->end(),
			[&value](GRAPH_NODE_T* target)
			{
				return target->getValue() == value;
			});
		return (matchedEdgeIt != candidateEdges->end()) ?
			*matchedEdgeIt :
			nullptr;
	}
	return nullptr;
}

GRAPH_NODE_TMPL
size_t GRAPH_NODE_T::getOutgoingNodeCountWithKey(Key const& key) const
{
	if (!outgoingEdgesOfKey.has_value())
	{
		throw std::invalid_argument("Cannot get incoming node count when map is not initialised.");
	}
	return outgoingEdgesOfKey.value().count(key) != 0 ?
		outgoingEdgesOfKey.value().at(key).size() :
		0;
}

GRAPH_NODE_TMPL
GRAPH_NODE_T::operator std::string() const
{
	return ToString<T>()(value);
}

GRAPH_NODE_TMPL
GRAPH_NODE_T* GRAPH_NODE_T::getMutableIncomingNode(size_t index)
{
	return MUTABLE_FROM_CONST_MEMBER_FUNC(this, getIncomingNode, index);
}

GRAPH_NODE_TMPL
GRAPH_NODE_T* GRAPH_NODE_T::getMutableOutgoingNode(size_t index)
{
	return MUTABLE_FROM_CONST_MEMBER_FUNC(this, getOutgoingNode, index);
}

GRAPH_NODE_TMPL
void GRAPH_NODE_T::addIncomingNode(GRAPH_NODE_T* node)
{
	incomingEdges.push_back(node);

	if (incomingEdgesOfKey.has_value())
	{
		auto key = graph->getKeyFunction()(node->value);
		incomingEdgesOfKey.value()[key].push_back(node);
	}
}

GRAPH_NODE_TMPL
void GRAPH_NODE_T::removeIncomingNode(GRAPH_NODE_T* node)
{
	if (!MiscUtils::eraseIfElement(&incomingEdges, node))
	{
		throw std::out_of_range("No incoming edge with source node of " + toString(node) + " in graph node.");
	}

	if (incomingEdgesOfKey.has_value())
	{
		auto key = graph->getKeyFunction()(node->value);
		MiscUtils::eraseElementInMappedList(&incomingEdgesOfKey.value(), key, node);
	}
}

GRAPH_NODE_TMPL
void GRAPH_NODE_T::addOutgoingNode(GRAPH_NODE_T* node)
{
	outgoingEdges.push_back(node);

	if (outgoingEdgesOfKey.has_value())
	{
		auto key = graph->getKeyFunction()(node->value);
		outgoingEdgesOfKey.value()[key].push_back(node);
	}
}

GRAPH_NODE_TMPL
void GRAPH_NODE_T::removeOutgoingNode(GRAPH_NODE_T* node)
{
	if (!MiscUtils::eraseIfElement(&outgoingEdges, node))
	{
		throw std::out_of_range("No outgoing edge with target node of " + toString(node) + " in graph node.");
	}

	if (outgoingEdgesOfKey.has_value())
	{
		auto key = graph->getKeyFunction()(node->value);
		MiscUtils::eraseElementInMappedList(&outgoingEdgesOfKey.value(), key, node);
	}
}

#endif /* GRAPH_NODE_TPP */
