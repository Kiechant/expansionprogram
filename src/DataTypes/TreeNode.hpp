//
//  TreeNode.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 4/6/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#ifndef TREE_NODE_HPP
#define TREE_NODE_HPP

#include "TreeNodeDecl.hpp"

#include "ComparisonDecl.hpp"
#include "ComparisonOperatorMethodsDecl.hpp"
#include "HashingDecl.hpp"
#include "StringConversionDecl.hpp"

#include <exception>
#include <functional>
#include <iterator>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

/* Forward declares friend methods. */
TREE_NODE_TMPL class TreeNode;
TREE_NODE_TMPL int compare(TREE_NODE_T const&, TREE_NODE_T const&);
TREE_NODE_TMPL TREE_NODE_T* deepCopy(TREE_NODE_T const*);
TREE_NODE_TMPL void deepDelete(TREE_NODE_T const*);

/* A node for a tree data structure.
   
   Contains an item of type 'Item'. Links to a parent node
   and a list of child nodes. Nodes are hashed to a key of type 'Key'.
   The key function can be specially set. It defaults to the item itself.

   The TreeNode class is only responsible for its own
   memory and performs operations local to the node with the exception
   of the deep copy and delete functions. */
template<typename Item, typename Key>
class TreeNode
{
public:
	static_assert(std::is_default_constructible_v<Item>,
		"TreeNode must be instantiated with a default constructible item.");
	static_assert(IsComparableV<Item>,
		"TreeNode must be instantiated with a comparable item.");
	static_assert(IsHashableV<Key> || std::is_pointer_v<Key>,
		"TreeNode must be instantiated with a hashable key.");


	/** Constructors and Destructors **/

	TreeNode();
	TreeNode(Item const& item);
	TreeNode(std::function<Key(Item const&)> const&);
	TreeNode(Item const& item, std::function<Key(Item const&)> const&);

	virtual ~TreeNode() = default;


	/** Comparison **/

	friend int compare<>(TREE_NODE_T const&, TREE_NODE_T const&);
	COMPARISON_OPERATOR_METHODS_DECL(TreeNode)


	/** String Conversion **/

	operator std::string() const;


	/** Deep Copy and Deletion **/
	
	/* The TreeNode is not responsible for the management of its internal memory.
	   These methods are conveniences for copying and deleting nodes in the
	   subtree from the current node. */
	
	/* Copies this node and recursively copies its children. */
	friend TREE_NODE_T* deepCopy<>(TREE_NODE_T const*);

	/* Deletes this node and recursively deletes its children. */
	friend void deepDelete<>(TREE_NODE_T const*);
	

	/** Accessors and Mutators **/
	
	TreeNode* getParent() const;
	bool isRoot() const;
	bool isLeaf() const;
	Item const& getItem() const;

	/* Sets the item and updates the keys of the parent. */
	void setItem(Item const& item);

	/* Modifies the item with a function without copying the item.
	   Updates the keys of the parent. */
	void modifyItem(std::function<void(Item* item)> modifyFunc);


	/** Accessing and Iterating over Children **/
	
	using TreeNodeIter = typename std::vector<TreeNode*>::const_iterator;

	size_t getChildCount() const;
	TreeNodeIter getChildBegin() const;
	TreeNodeIter getChildEnd() const;
	TreeNode* getChild(size_t at) const;

	/* Returns the child at the end of a branch denoted by a list of recursive indices. */
	TreeNode* getBranch(std::vector<size_t> const& indices) const;


	/** Indexing Children by Key **/
	
	/* Returns the child at 'keyIndex' in the list of children matching the key. */
	TreeNode* getChildWithKey(Key const&, size_t keyIndex) const;

	/* Returns the number of children which match the key. */
	size_t getChildCountWithKey(Key const&) const;

	/* Returns the child at the end of a branch denoted by a recursive list of keys.
	   The first child to match the key is taken at each level. */
	TreeNode* getBranchWithKey(std::vector<Key> const& keys) const;


	/** Finding Children by Key **/

	/* Calls findChild() from the first of the node's children. */
	TreeNodeIter findChild(Key const& key) const;

	/* Iterates over the node's children from the 'begin' iterator inclusive and
	   returns the first child which matches the key. */
	TreeNodeIter findChild(Key const& key, TreeNodeIter begin) const;
	
	/* Iterates over the node's children from the 'begin' index inclusive and
	   returns the first child which matches the key. */
	size_t findChild(Key const& key, size_t begin) const;


	/** Adding Children **/
	
	TreeNode* addChild(TreeNode* child);
	TreeNode* addChild(Item const& item);

	/* Type constraint. 'value' evaluates to true if type 'T' is an
	   input iterator for a 'TreeNode*' type. */
	template <typename T>
	struct IsTreeNodeInputIterator
	{
		static constexpr bool value =
			std::is_convertible_v<
				typename std::iterator_traits<T>::iterator_category,
				std::input_iterator_tag> &&
			std::is_same_v<
				typename std::iterator_traits<T>::value_type,
				TreeNode*>;
	};

	/* Adds all children from the beginning to the end of the iterators. */
	void addChildren(std::vector<Item> const& items);
	void addChildren(std::vector<TreeNode*> const& nodes);
	template <typename It>
	std::enable_if_t<IsTreeNodeInputIterator<It>::value, void>
	addChildren(It begin, It end);

	/* Adds a recursive branch of nodes. Returns the end of the branch. */
	TreeNode* addBranch(std::vector<Item> const& items);
	TreeNode* addBranch(std::vector<TreeNode*> const& nodes);
	template <typename It>
	std::enable_if_t<IsTreeNodeInputIterator<It>::value, TreeNode*>
	addBranch(It begin, It end);

	/** Removing and Deleting Children **/

	void clearChildren();
	void deepDeleteChildren();

protected:
	/** Properties **/

	Item item = Item();
	TreeNode* parent = nullptr;
	std::vector<TreeNode*> children;
	
	std::function<Key(Item const&)> keyOfItem;
	std::unordered_map<Key, std::vector<size_t>, ToHash<Key>> childrenOfKey;

	/* The number of the child in the parent's key list. */
	size_t keyIndex = 0;


	/** Key Management **/

	/* Returns the default hash function for the current item if none is specified. */
	static std::function<Key(Item const&)> defaultKeyOfItem();

	void addChildIndexToKey(size_t childIndex, Key const& key);
	void removeKeyIndex(size_t keyIndex, Key const& key);
	void rekeyChild(TreeNode* child, Key const& oldKey, Key const& newKey);


	/** Helper Methods **/

	static std::vector<TreeNode*> createNodesFromItems(std::vector<Item> const& items);
};

#endif /* TREE_NODE_HPP */
