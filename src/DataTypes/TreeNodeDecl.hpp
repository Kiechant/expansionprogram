//
//  TreeNodeDecl.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef TREE_NODE_DECL_HPP
#define TREE_NODE_DECL_HPP

template<typename Item, typename Key>
class TreeNode;

template<typename Item, typename Key = Item>
class TreeNode;

#define TREE_NODE_TMPL template<typename Item, typename Key>
#define TREE_NODE_T TreeNode<Item, Key>

#endif /* TREE_NODE_DECL_HPP */
