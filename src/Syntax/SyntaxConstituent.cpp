//
//  SyntaxConstituent.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 2/7/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#include "SyntaxConstituent.tpp"

#include "MemoryUtils.hpp"
#include "MiscUtils.hpp"
#include "StdComparison.hpp"

#include <iostream>
#include <sstream>

using namespace std;

SyntaxConstituent::SyntaxConstituent()
{
	setValue<SyntaxConstituentFormat::Null>((void*)0);
}

SyntaxConstituent::~SyntaxConstituent()
{
	if (format == SyntaxConstituentFormat::Group)
	{
		MemoryUtils::deepDelete(getValue<SyntaxConstituentFormat::Group>());
	}
	else if (format == SyntaxConstituentFormat::Alternative)
	{
		auto alts = getValue<SyntaxConstituentFormat::Alternative>();
		for (auto alt : *alts)
		{
			MemoryUtils::deepDelete(alt);
		}
		delete alts;
	}
	else if (isPostfixOperator(format))
	{
		delete getValue<SyntaxConstituentValueType::Constituent>();
	}
}

void swap(SyntaxConstituent& first, SyntaxConstituent& second)
{
	using std::swap;
	swap(first.format, second.format);
	swap(first.value, second.value);
}

SyntaxConstituent::SyntaxConstituent(SyntaxConstituent const& other)
{
	format = other.format;
	
	if (format == SyntaxConstituentFormat::Group)
	{
		value = MemoryUtils::deepCopy(other.getValue<SyntaxConstituentFormat::Group>());
	}
	else if (format == SyntaxConstituentFormat::Alternative)
	{
		auto otherAlts = other.getValue<SyntaxConstituentFormat::Alternative>();
		auto thisAlts = new vector<vector<SyntaxConstituent const*> const*>();
		for (auto otherAlt : *otherAlts)
		{
			thisAlts->push_back(MemoryUtils::deepCopy(otherAlt));
		}
		value = thisAlts;
	}
	else if (isPostfixOperator(format))
	{
		auto otherCon = other.getValue<SyntaxConstituentValueType::Constituent>();
		auto thisCon = new SyntaxConstituent(*otherCon);
		setValue<SyntaxConstituentValueType::Constituent>(thisCon);
	}
	else if (format == SyntaxConstituentFormat::String || format == SyntaxConstituentFormat::Rule)
	{
		setValue<SyntaxConstituentValueType::String>(other.getValue<SyntaxConstituentValueType::String>());
	}
	else if (format == SyntaxConstituentFormat::Set)
	{
		setValue<SyntaxConstituentFormat::Set>(other.getValue<SyntaxConstituentFormat::Set>());
	}
}

SyntaxConstituent::SyntaxConstituent(SyntaxConstituent&& other)
{
	swap(*this, other);
}

SyntaxConstituent& SyntaxConstituent::operator=(SyntaxConstituent other)
{
	swap(*this, other);
	return *this;
}

int compare(SyntaxConstituent const& first, SyntaxConstituent const& second)
{
	using SyntaxConstituentFormat = SyntaxConstituentFormat;
	using SyntaxConstituentValueType = SyntaxConstituentValueType;

	RETURN_IF_UNEQUAL_COMPARE((int)first.format, (int)second.format);
	SyntaxConstituentFormat format = first.format;

	if (format == SyntaxConstituentFormat::Group)
	{
		auto firstConList = first.getValue<SyntaxConstituentFormat::Group>();
		auto secondConList = second.getValue<SyntaxConstituentFormat::Group>();
		RETURN_IF_UNEQUAL(*firstConList, *secondConList, MemoryUtils::deepCompare);
	}
	else if (format == SyntaxConstituentFormat::Alternative)
	{
		auto firstAltList = first.getValue<SyntaxConstituentFormat::Alternative>();
		auto secondAltList = second.getValue<SyntaxConstituentFormat::Alternative>();
		
		RETURN_IF_UNEQUAL(*firstAltList, *secondAltList,
			[](auto const& first, auto const& second) -> int
			{
				return MemoryUtils::deepCompareBy(first, second,
					MemoryUtils::deepCompare<remove_pointer_t<SyntaxConstituentValueType::List>>);
			});
	}
	else if (SyntaxConstituent::isPostfixOperator(format))
	{
		auto firstCon = first.getValue<SyntaxConstituentValueType::Constituent>();
		auto secondCon = second.getValue<SyntaxConstituentValueType::Constituent>();
		RETURN_IF_UNEQUAL_COMPARE(*firstCon, *secondCon);
	}
	else if (format == SyntaxConstituentFormat::String || format == SyntaxConstituentFormat::Rule)
	{
		RETURN_IF_UNEQUAL_COMPARE(first.getValue<SyntaxConstituentValueType::String>(),
			second.getValue<SyntaxConstituentValueType::String>());
	}
	else if (format == SyntaxConstituentFormat::Set)
	{
		RETURN_IF_UNEQUAL_COMPARE(first.getValue<SyntaxConstituentValueType::Set>(),
			second.getValue<SyntaxConstituentValueType::Set>());
	}

	return 0;
}

SyntaxConstituent::operator string() const
{
	stringstream ss;
	ss << "SyntaxConstituent {" << nameOfFormat(format) << ", ";
	if (format == SyntaxConstituentFormat::Group)
	{
		ss << MiscUtils::stringifyVector<SyntaxConstituent const>(*getValue<SyntaxConstituentFormat::Group>());
	}
	else if (format == SyntaxConstituentFormat::Alternative)
	{
		ss << "{";
		auto alts = getValue<SyntaxConstituentFormat::Alternative>();
		if (!alts->empty())
		{
			for (size_t i = 0; i < alts->size() - 1; i++)
			{
				ss << MiscUtils::stringifyVector<SyntaxConstituent const>(*alts->at(i)) << ",";
			}
			ss << MiscUtils::stringifyVector(*alts->back());
		}
		ss << "}";
	}
	else if (isPostfixOperator(format))
	{
		ss << string(*getValue<SyntaxConstituentValueType::Constituent>());
	}
	else if (format == SyntaxConstituentFormat::String || format == SyntaxConstituentFormat::Rule)
	{
		ss << "\"" << getValue<SyntaxConstituentValueType::String>() << "\"";
	}
	else if (format == SyntaxConstituentFormat::Set)
	{
		ss << string(getValue<SyntaxConstituentFormat::Set>());
	}
	ss << "}";
	return ss.str();
}

string SyntaxConstituent::nameOfFormat(SyntaxConstituentFormat format)
{
	switch (format)
	{
	case SyntaxConstituentFormat::Null:
		return "Null";
	case SyntaxConstituentFormat::String:
		return "String";
	case SyntaxConstituentFormat::Rule:
		return "Rule";
	case SyntaxConstituentFormat::Group:
		return "Group";
	case SyntaxConstituentFormat::Set:
		return "Set";
	case SyntaxConstituentFormat::Alternative:
		return "Alternative";
	case SyntaxConstituentFormat::Optional:
		return "Optional";
	case SyntaxConstituentFormat::List:
		return "List";
	case SyntaxConstituentFormat::OptionalList:
		return "OptionalList";
	}
}

SyntaxConstituentFormat SyntaxConstituent::getFormat() const
{
	return format;
}

// TODO: Move into a more logical location.
bool SyntaxConstituent::isEnclosure(SyntaxConstituentFormat format)
{
	return format == SyntaxConstituentFormat::Rule ||
		format == SyntaxConstituentFormat::Group ||
		format == SyntaxConstituentFormat::Set;
}

// TODO: Move into a more logical location.
bool SyntaxConstituent::isPostfixOperator(SyntaxConstituentFormat format)
{
	return format == SyntaxConstituentFormat::Optional ||
		format == SyntaxConstituentFormat::List ||
		format == SyntaxConstituentFormat::OptionalList;
}
