//
//  SyntaxConstituent.tpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef SYNTAX_CONSTITUENT_TPP
#define SYNTAX_CONSTITUENT_TPP

#include "SyntaxConstituent.hpp"

template<SyntaxConstituentFormat F>
SyntaxConstituent* SyntaxConstituent::create(SyntaxConstituentValueTypeOfT<F> const& value)
{
	SyntaxConstituent* obj = new SyntaxConstituent();
	obj->setValueAndFormat<F>(value);
	return obj;
}

template<typename V>
SyntaxConstituent* SyntaxConstituent::create(SyntaxConstituentFormat format, AnySyntaxConstituentValueTypeT<V> const& value)
{
	SyntaxConstituent* obj = new SyntaxConstituent();
	obj->setValueAndFormat<V>(format, value);
	return obj;
}

template<typename V, SyntaxConstituentFormat F>
bool SyntaxConstituent::isValueTypeOfFormat()
{
	return std::is_same<V, SyntaxConstituentValueTypeOfT<F>>::value;
}

template<typename V>
bool SyntaxConstituent::isValueTypeOfFormat(SyntaxConstituentFormat format)
{
	switch (format)
	{
	case SyntaxConstituentFormat::Null:
		return isValueTypeOfFormat<V, SyntaxConstituentFormat::Null>();
	case SyntaxConstituentFormat::String:
		return isValueTypeOfFormat<V, SyntaxConstituentFormat::String>();
	case SyntaxConstituentFormat::Rule:
		return isValueTypeOfFormat<V, SyntaxConstituentFormat::Rule>();
	case SyntaxConstituentFormat::Group:
		return isValueTypeOfFormat<V, SyntaxConstituentFormat::Group>();
	case SyntaxConstituentFormat::Set:
		return isValueTypeOfFormat<V, SyntaxConstituentFormat::Set>();
	case SyntaxConstituentFormat::Alternative:
		return isValueTypeOfFormat<V, SyntaxConstituentFormat::Alternative>();
	case SyntaxConstituentFormat::Optional:
		return isValueTypeOfFormat<V, SyntaxConstituentFormat::Optional>();
	case SyntaxConstituentFormat::List:
		return isValueTypeOfFormat<V, SyntaxConstituentFormat::List>();
	case SyntaxConstituentFormat::OptionalList:
		return isValueTypeOfFormat<V, SyntaxConstituentFormat::OptionalList>();
	}
}

template<SyntaxConstituentFormat F>
SyntaxConstituentValueTypeOfT<F> const& SyntaxConstituent::getValue() const
{
	return getValue<SyntaxConstituentValueTypeOfT<F>>();
}

template<SyntaxConstituentFormat F>
std::add_pointer_t<SyntaxConstituentValueTypeOfT<F>> SyntaxConstituent::getMutableValue()
{
	return getMutableValue<SyntaxConstituentValueTypeOfT<F>>();
}

template<typename V>
AnySyntaxConstituentValueTypeT<V> const& SyntaxConstituent::getValue() const
{
	if (std::holds_alternative<V>(value))
	{
		return std::get<V>(value);
	}
	else
	{
		throw "SyntaxConstituent does not store the value type requested.";
	}
}

template<typename V>
std::add_pointer_t<AnySyntaxConstituentValueTypeT<V>> SyntaxConstituent::getMutableValue()
{
	return &const_cast<AnySyntaxConstituentValueTypeT<V>&>(getValue<V>());
}

template<SyntaxConstituentFormat F>
void SyntaxConstituent::setValue(SyntaxConstituentValueTypeOfT<F> const& value)
{
	setValue<SyntaxConstituentValueTypeOfT<F>>(value);
}

template<typename V>
void SyntaxConstituent::setValue(AnySyntaxConstituentValueTypeT<V> const& value)
{
	this->value = value;
}

template<SyntaxConstituentFormat F>
void SyntaxConstituent::setValueAndFormat(SyntaxConstituentValueTypeOfT<F> const& value)
{
	setValueAndFormat<SyntaxConstituentValueTypeOfT<F>>(F, value);
}

template<typename V>
void SyntaxConstituent::setValueAndFormat(SyntaxConstituentFormat format, AnySyntaxConstituentValueTypeT<V> const& value)
{
	if (isValueTypeOfFormat<V>(format))
	{
		this->format = format;
		setValue<V>(value);
	}
	else
	{
		throw "Setting a constituent value to a type with an unmatching format.";
	}
}

#define VALUE_TYPE_OF_FORMAT(F, T) template<> struct \
	SyntaxConstituentValueTypeOf<SyntaxConstituentFormat::F> { using type = \
	SyntaxConstituentValueType::T; };

VALUE_TYPE_OF_FORMAT(Null, Void)
VALUE_TYPE_OF_FORMAT(String, String)
VALUE_TYPE_OF_FORMAT(Rule, String)
VALUE_TYPE_OF_FORMAT(Group, List)
VALUE_TYPE_OF_FORMAT(Set, Set)
VALUE_TYPE_OF_FORMAT(Alternative, AltList)
VALUE_TYPE_OF_FORMAT(Optional, Constituent)
VALUE_TYPE_OF_FORMAT(List, Constituent)
VALUE_TYPE_OF_FORMAT(OptionalList, Constituent)

#undef VALUE_TYPE_OF_FORMAT

#endif /* SYNTAX_CONSTITUENT_TPP */
