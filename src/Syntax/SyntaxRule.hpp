//
//  SyntaxRule.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 22/6/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#ifndef SYNTAX_RULE_HPP
#define SYNTAX_RULE_HPP

#include "ComparisonOperatorMethodsDecl.hpp"
#include "SyntaxConstituentDecl.hpp"

#include <exception>
#include <memory>
#include <optional>
#include <variant>
#include <vector>

class SyntaxRule
{
public:
	using BracketParseFunc = std::function<SyntaxConstituent*(std::string const&, size_t*)>;

	// An enclosure specifying sub-constituents or a sub-rule
	// which are treated as a collective. Implemented with open/close brackets.
	struct Enclosure
	{
		std::string name;	// The name of the enclosure.
		char open;			// The open bracket e.g.  '(' for '()'.
		char close;			// The close bracket e.g. ')' for '()'.

		// The parsing function called when the open bracket is detected.
		BracketParseFunc parseFunc;

		// The format of constituent to wrap around the sub-constituent/s or sub-rule.
		SyntaxConstituentFormat constituentType;
	};

	// An operator modifies one or more constituents.
	struct Operator
	{
		std::string name;	// The name of the operator.
		char opr;			// The operator symbol to be parsed e.g. '?' for optional.

		// The format of constituent to wrap around the modified subconstituent/s.
		SyntaxConstituentFormat constituentType;
	};

	// Constructors and destructors.
	SyntaxRule(std::string const& name, std::string const& schema, bool isAnonymous);
	~SyntaxRule();

	// Copy and move semantics.
	friend void swap(SyntaxRule&, SyntaxRule&);
	SyntaxRule(SyntaxRule const&);
	SyntaxRule(SyntaxRule&&);
	SyntaxRule& operator=(SyntaxRule);

	// Comparison.
	friend int compare(SyntaxRule const&, SyntaxRule const&);
	COMPARISON_OPERATOR_METHODS_DECL(SyntaxRule)

	// Getters and setters.
	std::string const& getName() const;
	std::string const& getPattern() const;
	std::vector<SyntaxConstituent const*>::const_iterator getConstituentsBegin() const;
	std::vector<SyntaxConstituent const*>::const_iterator getConstituentsEnd() const;
	bool isAnonymous() const;

protected:
	class StaticInfo
	{
	public:
		StaticInfo();

		// Bracket sets for a rule, group and set.
		Enclosure ruleBrackets;
		Enclosure groupBrackets;
		Enclosure setBrackets;
		std::vector<Enclosure const*> bracketsList;

		// Infix operator for alternative.
		Operator altOpr;

		// Unary postfix operators for optional, list and optional list.
		Operator optOpr;
		Operator listOpr;
		Operator optListOpr;
		std::vector<Operator const*> postfixOperators;

		// Union of instruction characters.
		std::string instructionChars;

	private:
		std::string compileInstructions();
	};

	// Contextual range parsing variables.
	struct SetContext
	{
		size_t curr = 0;
		std::optional<char> prevCh;
		std::optional<char> lowCh;
		std::optional<char> highCh;
	};

	SyntaxRule();

	void isAnonymous(bool value);

	static std::vector<SyntaxConstituent const*>* generateFromSchema(std::string const& schema);

	static void mustBe(std::string const& schema, size_t* index, char equals, bool doAdvance = true,
		std::string const& errorMsg = "invalid character '{}' detected in mustBe()");

	static std::vector<SyntaxConstituent*> parseString(std::string const& schema, size_t* begin, char endCh = '\0');
	static void appendStringIfPresent(std::vector<SyntaxConstituent*>* constituents, std::string* str);
	static SyntaxConstituent* parseRule(std::string const& schema, size_t* begin);
	static SyntaxConstituent* parseGroup(std::string const& schema, size_t* begin);
	static SyntaxConstituent* parseSet(std::string const& schema, size_t* begin);
	static void addEqualOrRangeToSet(SyntaxSet* members, SetContext* context);
	static void addRangeToSet(SyntaxSet* members, SetContext* context);
	static std::vector<SyntaxConstituent*> parseAlternative(std::string const& schema, size_t* begin, char endCh = '\0');
	static SyntaxConstituent* parsePostfixOperator(Operator const&, std::string const& schema, size_t* begin, SyntaxConstituent* left);

	static std::invalid_argument parsingError(int errPos, std::string const& errMsg);

	static bool isRuleChar(char);
	static bool isTextChar(char);
	static bool isInstructionChar(char);

	static std::shared_ptr<const StaticInfo> staticInfo();

	std::string name;
	std::string pattern;
	std::vector<SyntaxConstituent const*>* constituents = nullptr;
	bool _isAnonymous;
};

#endif /* SYNTAX_RULE_HPP */
