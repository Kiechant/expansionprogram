//
//  SyntaxSet.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef SYNTAX_SET_HPP
#define SYNTAX_SET_HPP

#include "ComparisonOperatorMethodsDecl.hpp"

#include <string>
#include <vector>

// Subclass which determines if a character is in a set.
class SyntaxSet
{
public:
	// The ranges and single characters in the set.
	std::vector<std::pair<char, char>> ranges;
	std::string equals = "";

	// Returns true if the character is in the set.
	bool contains(char) const;

	// Comparison.
	friend int compare(SyntaxSet const&, SyntaxSet const&);
	COMPARISON_OPERATOR_METHODS_DECL(SyntaxSet)
	
	operator std::string() const;
};

#endif /* SYNTAX_SET_HPP */
