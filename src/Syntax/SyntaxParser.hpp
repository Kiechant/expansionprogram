//
//  SyntaxParser.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 4/6/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#ifndef SYNTAX_PARSER_HPP
#define SYNTAX_PARSER_HPP

#include "ComparisonOperatorMethodsDecl.hpp"
#include "StringConversionDecl.hpp"
#include "SyntaxConstituentDecl.hpp"
#include "SyntaxParserDiagnostic.hpp"
#include "TreeNodeDecl.hpp"
#include "TreeDecl.hpp"

class SyntaxRule;
class SyntaxSet;

#include <functional>
#include <memory>
#include <optional>
#include <unordered_map>
#include <variant>

/* Tool for building a syntax tree by evaluating a query string
   with a custom syntax. */
class SyntaxParser
{
public:
	/* Constructors and Destructors */

	SyntaxParser(std::string const& query, std::unordered_map<std::string, SyntaxRule const*> const& syntax, std::string const& startRule);
	~SyntaxParser();

	/* Copy and Move Semantics */

	friend void swap(SyntaxParser&, SyntaxParser&);
	SyntaxParser(SyntaxParser const&);
	SyntaxParser(SyntaxParser&&);
	SyntaxParser& operator=(SyntaxParser);

	/* Parsing Methods */

	/* Front-facing member parse function. Analyses query string
	   and generates a syntax tree according to the defined syntax. */
	Tree<std::string> parse();

	/* Wraps parse functionality with the construction of the parser object. */
	static Tree<std::string> parse(std::string const& query,
		std::unordered_map<std::string, SyntaxRule const*> const& syntax,
		std::string const& startRule);

protected:
	std::string query;
	std::unordered_map<std::string, SyntaxRule const*> syntax;
	std::string startRule;

	size_t curr;
	char currCh;

	/* Indexing Methods */

	/* Advances to the next character marker. */
	void advance();

	/* Moves the character marker to index. */
	void seek(size_t index);

	/* Asserts the current character is equal to 'equals'.
	   If the character matches, the character marker is advanced if doAdvance
	   is true. Otherwise, throws an error. */
	void mustBe(char equals, bool doAdvance = true);

	/* Parsing Helper Methods */

	/* Commits a terminal value, with optional corresponding node, to a list of
	   processed siblings 'nextNodes'. If the rightmost sibling is a terminal node,
	   its value is combined with 'terminal', and 'terminalNode', if provided,
	   is deleted. If 'terminal' is the empty string, returns false,
	   indicating parsing failed for the expected terminal. */
	static bool commitTerminal(std::string const& terminal,
		std::vector<TreeNode<std::string>*>* nextNodes, TreeNode<std::string>* terminalNode = nullptr);

	/* Commits a list of non-terminal nodes 'parsedNodes' to a list of
	   siblings 'nextNodes'. If the leftmost parsed node is terminal, then
	   that node will be separately processed with a call to 'parseTerminal'. */
	static bool commitNonTerminal(std::vector<TreeNode<std::string>*> const& parsedNodes,
		std::vector<TreeNode<std::string>*>* nextNodes);

	/* Categorical Parsing Methods */

	/* Matches characters to an exact pattern. Returns the pattern if
	   the match succeeded, otherwise returns the empty string.*/
	std::string parseString(std::string const& pattern,
		std::optional<SyntaxParserDiagnosticNode>* diagNode);

	/* Matches characters to a rule. Returns a new node with the rule name
	   or, if the rule is anonymous and has a single terminal child, the child.
	   If parsing failed, returns a null pointer. */
	TreeNode<std::string>* parseRule(std::string const& rule,
		std::optional<SyntaxParserDiagnosticNode>* diagNode);

	/* Parses a group of constituents. Returns the list of nodes matched
	   for each constituent in sequence. Returns an empty list if matching failed. */
	std::vector<TreeNode<std::string>*> parseGroup(
		std::vector<SyntaxConstituent const*>::const_iterator beginCon,
		std::vector<SyntaxConstituent const*>::const_iterator endCon,
		std::optional<SyntaxParserDiagnosticNode>* diagNode);

	/* Matches the current character to the set function. Returns a
	   string containing the matched character if matching was successful,
	   otherwise returns an empty string. */
	std::string parseSet(SyntaxConstituentValueType::Set const& set,
		std::optional<SyntaxParserDiagnosticNode>* diagNode);

	/* Matches characters to one of the groups in the pair, with the left
	   having priority. Returns the parsed list of nodes if matching succeeding,
	   otherwise returns the empty list. */
	std::vector<TreeNode<std::string>*> parseAlternative(
		SyntaxConstituentValueType::AltList const&,
		std::optional<SyntaxParserDiagnosticNode>* diagNode);

	/* Optionally matches characters to the contained group and returns
	   the parsed group. If matching fails, the character marker is rewound
	   and an empty list is returned. */
	std::vector<TreeNode<std::string>*> parseOptional(
		SyntaxConstituentValueType::Constituent const&,
		std::optional<SyntaxParserDiagnosticNode>* diagNode);
	std::vector<TreeNode<std::string>*> parseOptional(
		std::vector<SyntaxConstituent const*> const&,
		std::optional<SyntaxParserDiagnosticNode>* diagNode);

	/* Matches characters to a list of one or more groups.
	   Returns the joined list of parsed nodes for each group.
	   If no groups are matched returns an empty list. */
	std::vector<TreeNode<std::string>*> parseList(
		SyntaxConstituentValueType::Constituent const&,
		std::optional<SyntaxParserDiagnosticNode>* diagNode);
	std::vector<TreeNode<std::string>*> parseList(std::vector<
		SyntaxConstituent const*> const&,
		std::optional<SyntaxParserDiagnosticNode>* diagNode);

	/* Matches characters to a list of zero or more groups.
	   Returns the joined list of parsed nodes for each group.
	   Rewinds the character marker and returns an empty list if no groups are matched. */
	std::vector<TreeNode<std::string>*> parseOptionalList(
		SyntaxConstituentValueType::Constituent const&,
		std::optional<SyntaxParserDiagnosticNode>* diagNode);
	std::vector<TreeNode<std::string>*> parseOptionalList(
		std::vector<SyntaxConstituent const*> const&,
		std::optional<SyntaxParserDiagnosticNode>* diagNode);

	/* Error Handling */

	/* Returns a string describing the current character
	   and index in the query string. */
	std::string charInfo();

	std::invalid_argument parsingError(size_t errPos, std::string errRule);
};

#endif /* SYNTAX_PARSER_HPP */
