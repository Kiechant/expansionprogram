//
//  SyntaxParserDiagnostic.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef SYNTAX_PARSER_DIAGNOSTIC_HPP
#define SYNTAX_PARSER_DIAGNOSTIC_HPP

#include "ComparisonOperatorMethodsDecl.hpp"
#include "TreeNodeDecl.hpp"
#include "TreeDecl.hpp"

class SyntaxSet;
class SyntaxRule;

#include <variant>

struct SyntaxParserDiagnostic;

using SyntaxParserDiagnosticNode = TreeNode<SyntaxParserDiagnostic, size_t>;
using SyntaxParserDiagnosticTree = Tree<SyntaxParserDiagnostic, size_t>;

struct SyntaxParserDiagnostic
{
	enum class Format
	{
		Null,
		TrailingChars,
		Alternative,
		StringNoMatch,
		StringEnd,
		SetNoMatch,
		SetEnd
	};

	SyntaxParserDiagnostic();
	SyntaxParserDiagnostic(SyntaxRule const* rule, size_t queryPos, Format format,
		std::variant<std::monostate, char, SyntaxSet const*> expected);

	SyntaxRule const* rule = nullptr;
	size_t queryPos = 0;
	Format format = Format::Null;
	std::variant<std::monostate, char, SyntaxSet const*> expected;

	std::string makeMessage(std::string const& query) const;
	std::string expectedMessage() const;

	friend int compare(SyntaxParserDiagnostic const&, SyntaxParserDiagnostic const&);
	COMPARISON_OPERATOR_METHODS_DECL(SyntaxParserDiagnostic)

	operator std::string() const;

	static size_t toKey(SyntaxParserDiagnostic const&);
};

#endif /* SYNTAX_PARSER_DIAGNOSTIC_HPP */
