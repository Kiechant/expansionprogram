//
//  SyntaxConstituentDecl.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef SYNTAX_CONSTITUENT_DECL_HPP
#define SYNTAX_CONSTITUENT_DECL_HPP

#include "WrappedTypeStruct.hpp"

class SyntaxSet;

#include <string>
#include <variant>
#include <vector>

class SyntaxConstituent;
enum class SyntaxConstituentFormat;

// The different types of values the constituent can have.
struct SyntaxConstituentValueType
{
	// Null: Initialised to a null pointer.
	using Void = void*;
	
	// String: A terminal sequence of characters.
	// Rule: The name of another syntax rule.
	using String = std::string;
	
	// Group: The grouped series of subconstituents.
	using List = std::vector<SyntaxConstituent const*> const*;
	
	// Set: Matches to a character or range in the set.
	using Set = SyntaxSet;
	
	// Alternative: A list of alternative subconstituents.
	using AltList = std::vector<List> const*;
	
	// Optional, List, OptionalList: The left constituent to modify.
	using Constituent = SyntaxConstituent const*;
};

// The value is different depending on the constituent type.
using SyntaxConstituentValueVariant = std::variant<
	SyntaxConstituentValueType::Void,
	SyntaxConstituentValueType::String,
	SyntaxConstituentValueType::List,
	SyntaxConstituentValueType::Set,
	SyntaxConstituentValueType::AltList,
	SyntaxConstituentValueType::Constituent
>;

WRAPPED_TYPE_STRUCT(SyntaxConstituentFormat, SyntaxConstituentValueTypeOf)

template<typename V>
static constexpr bool IsAnySyntaxConstituentValueTypeV =
	std::is_same<V, SyntaxConstituentValueType::Void>::value ||
	std::is_same<V, SyntaxConstituentValueType::String>::value ||
	std::is_same<V, SyntaxConstituentValueType::List>::value ||
	std::is_same<V, SyntaxConstituentValueType::Set>::value ||
	std::is_same<V, SyntaxConstituentValueType::AltList>::value ||
	std::is_same<V, SyntaxConstituentValueType::Constituent>::value;

template<typename V>
using AnySyntaxConstituentValueTypeT = std::enable_if_t<IsAnySyntaxConstituentValueTypeV<V>, V>;

#endif /* SYNTAX_CONSTITUENT_DECL_HPP */
