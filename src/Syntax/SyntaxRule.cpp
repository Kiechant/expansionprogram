//
//  SyntaxRule.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 22/6/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#include "SyntaxRule.hpp"

#include "SyntaxConstituent.tpp"
#include "MemoryUtils.hpp"
#include "MiscUtils.hpp"
#include "StdComparison.hpp"

#include <algorithm>
#include <cctype>
#include <iostream>
#include <functional>
#include <sstream>
#include <utility>

using namespace std;
using Enclosure = SyntaxRule::Enclosure;
using Operator = SyntaxRule::Operator;
using FieldFormat = SyntaxConstituentFormat;
using ValueType = SyntaxConstituentValueType;

SyntaxRule::StaticInfo::StaticInfo()
{
	// Bracket sets for a rule, group and set.
	ruleBrackets = {"rule", '<', '>', parseRule, FieldFormat::Rule};
	groupBrackets = {"group", '(', ')', parseGroup, FieldFormat::Group};
	setBrackets = {"set", '[', ']', parseSet, FieldFormat::Set};
	bracketsList = {&ruleBrackets, &groupBrackets, &setBrackets};

	// Infix operator for alternative.
	altOpr = {"alternative", '|', FieldFormat::Alternative};

	// Unary postfix operators for optional, list and optional list.
	optOpr = {"optional", '?', FieldFormat::Optional};
	listOpr = {"list", '+', FieldFormat::List};
	optListOpr = {"optional list", '*', FieldFormat::OptionalList};
	postfixOperators = {&optOpr, &listOpr, &optListOpr};

	// Union of instruction characters.
	instructionChars = compileInstructions();
}

string SyntaxRule::StaticInfo::compileInstructions()
{
	string instr = "";
	for (auto brackets : bracketsList)
	{
		instr.push_back(brackets->open);
		instr.push_back(brackets->close);
	}
	instr.push_back(altOpr.opr);
	for (auto opr : postfixOperators)
	{
		instr.push_back(opr->opr);
	}
	return instr;
}

bool SyntaxRule::isAnonymous() const
{
	return _isAnonymous;
}

void SyntaxRule::isAnonymous(bool value)
{
	_isAnonymous = value;
}

SyntaxRule::SyntaxRule(string const& name, string const& schema, bool isAnonymous)
	: name(name), pattern(schema), _isAnonymous(isAnonymous)
{
	constituents = generateFromSchema(schema);
}

SyntaxRule::SyntaxRule() {}

SyntaxRule::~SyntaxRule()
{
	MemoryUtils::deepDelete(constituents);
}

void swap(SyntaxRule& first, SyntaxRule& second)
{
	using std::swap;
	swap(first.name, second.name);
	swap(first.constituents, second.constituents);
	swap(first._isAnonymous, second._isAnonymous);
}

SyntaxRule::SyntaxRule(SyntaxRule const& other)
{
	name = other.name;
	constituents = MemoryUtils::deepCopy(other.constituents);
	_isAnonymous = other._isAnonymous;
}

SyntaxRule::SyntaxRule(SyntaxRule&& other)
{
	swap(*this, other);
}

SyntaxRule& SyntaxRule::operator=(SyntaxRule other)
{
	swap(*this, other);
	return *this;
}

int compare(SyntaxRule const& first, SyntaxRule const& second)
{
	RETURN_IF_UNEQUAL_COMPARE(first.name, second.name);
	RETURN_IF_UNEQUAL_COMPARE(first._isAnonymous, second._isAnonymous);
	RETURN_IF_UNEQUAL(*first.constituents, *second.constituents, MemoryUtils::deepCompare);
	return 0;
}

COMPARISON_OPERATOR_METHODS_IMPL(SyntaxRule)

string const& SyntaxRule::getName() const
{
	return name;
}

string const& SyntaxRule::getPattern() const
{
	return pattern;
}

vector<SyntaxConstituent const*>::const_iterator SyntaxRule::getConstituentsBegin() const
{
	return constituents->cbegin();
}

vector<SyntaxConstituent const*>::const_iterator SyntaxRule::getConstituentsEnd() const
{
	return constituents->cend();
}

vector<SyntaxConstituent const*>* SyntaxRule::generateFromSchema(string const& schema)
{
	size_t begin = 0;
	vector<SyntaxConstituent*> cons = parseAlternative(schema, &begin);
	return MemoryUtils::createNewConstVector<SyntaxConstituent>(cons.begin(), cons.end());
}

vector<SyntaxConstituent*> SyntaxRule::parseString(string const& schema, size_t* begin, char endCh)
{
	assertMsg(endCh == '\0' || endCh == ')', "End character must be null or a close group bracket.");

	auto cons = vector<SyntaxConstituent*>();
	bool isEscaped = false;
	string substr = "";

	size_t curr;

	// Loops through the string until the final character.
	for (curr = *begin; curr < schema.length(); curr++)
	{
		char currCh = schema[curr];

		// End of group encountered.
		if (endCh != '\0' && currCh == endCh)
		{
			if (!cons.empty() || curr > *begin)
			{
				appendStringIfPresent(&cons, &substr);
				*begin = curr;
				return cons;
			}
			else
			{
				throw parsingError(curr, "empty group");
			}
		}

		// The current character is an unescaped escape symbol.
		if (currCh == '\\' && !isEscaped)
		{
			isEscaped = true;
		}
		// The current character is an unescaped instruction symbol.
		else if (isInstructionChar(currCh) && !isEscaped)
		{
			appendStringIfPresent(&cons, &substr);

			bool didParseInstr = false;

			// Checks if the character matches one of the bracket sets
			// for a rule, group or set.
			for (auto brackets : staticInfo()->bracketsList)
			{
				// The current character is an open bracket.
				if (currCh == brackets->open)
				{
					cons.push_back(brackets->parseFunc(schema, &curr));
					didParseInstr = true;
					break;
				}
				// The current character is an unexpected close bracket.
				else if (currCh == brackets->close)
				{
					throw parsingError(curr, brackets->name + " close '" + brackets->close +
						"' before " + brackets->name + " open '" + brackets->open + "'");
				}
			}

			// The current character is an infix alternative operator.
			if (!didParseInstr && currCh == staticInfo()->altOpr.opr)
			{
				appendStringIfPresent(&cons, &substr);
				*begin = curr;
				return cons;
			}

			if (!didParseInstr)
			{
				// Checks if the current characters matches one of the postfix operators
				// for an optional, list, or optional list.
				for (auto opr : staticInfo()->postfixOperators)
				{
					// The current character is a postfix operator.
					if (currCh == opr->opr)
					{
						SyntaxConstituent* left;

						if (cons.empty())
						{
							throw parsingError(curr, opr->name + " '" + string(1, opr->opr) +
								"' missing left operand");
						}

						FieldFormat prevType = cons.back()->getFormat();

						// The last sequence is plain text.
						if (prevType == FieldFormat::String)
						{
							// Multiple characters: Pops off the final character as a separate constituent.
							if (cons.back()->getValue<FieldFormat::String>().size() > 1)
							{
								string* str = cons.back()->getMutableValue<FieldFormat::String>();
								char prevCh = str->back();
								str->pop_back();
								left = SyntaxConstituent::create<FieldFormat::String>(string(prevCh, 1));
							}
							// Single character: uses the last sequence and wraps the enclosure.
							else
							{
								left = cons.back();
								cons.pop_back();
							}
						}
						// The last sequence is an enclosure.
						// Operator wraps the enclosure.
						else if (SyntaxConstituent::isEnclosure(prevType))
						{
							left = cons.back();
							cons.pop_back();
						}
						else
						{
							throw parsingError(curr, opr->name + " '" + string(1, opr->opr) +
								"' missing left operand");
						}

						cons.push_back(parsePostfixOperator(*opr, schema, &curr, left));
						break;
					}
				}
			}
		}
		// The current character is plain text, a non-null control chraracter,
		// or an escaped instruction character.
		else
		{
			substr.push_back(currCh);
			isEscaped = false;
		}
	}

	// The end of the string has been reached.
	if (endCh == '\0')
	{
		if (curr == 0)
		{
			throw parsingError(0, "empty string");
		}

		appendStringIfPresent(&cons, &substr);
		*begin = schema.size();
		return cons;
	}
	else // endCh == ')'
	{
		throw parsingError(curr, "end encountered before group close ')'");
	}
}

void SyntaxRule::appendStringIfPresent(vector<SyntaxConstituent*>* constituents, string* str)
{
	// Creates a constituent for the trailing substring.
	if (!str->empty())
	{
		auto constituent = SyntaxConstituent::create<FieldFormat::String>(move(*str));
		constituents->push_back(constituent);
		*str = "";
	}
}

void SyntaxRule::mustBe(string const& schema, size_t* index, char equals, bool doAdvance, string const& errorMsg)
{
	if (schema[*index] != equals)
	{
		throw parsingError(*index, MiscUtils::subChar(errorMsg, schema[*index]));
	}
	if (doAdvance)
	{
		*index += 1;
	}
}

SyntaxConstituent* SyntaxRule::parseRule(string const& schema, size_t* begin)
{
	mustBe(schema, begin, staticInfo()->ruleBrackets.open);

	size_t tail = *begin;
	size_t curr = *begin;
	while (isRuleChar(schema[curr]))
	{
		curr++;
	}

	char currCh = schema[curr];
	if (currCh == staticInfo()->ruleBrackets.open)
	{
		throw parsingError(curr, "rule open '<' before rule close '>'");
	}
	else if (currCh != staticInfo()->ruleBrackets.close)
	{
		if (curr == schema.length())
		{
			throw parsingError(curr, "end encountered before rule close '>'");
		}
		else
		{
			throw parsingError(curr, MiscUtils::subChar("invalid character '{}' in rule", currCh));
		}
	}
	else if (curr == *begin)
	{
		throw parsingError(curr, "empty rule");
	}

	*begin = curr;
	return SyntaxConstituent::create<FieldFormat::Rule>(schema.substr(tail, curr - tail));
}

SyntaxConstituent* SyntaxRule::parseGroup(string const& schema, size_t* begin)
{
	mustBe(schema, begin,staticInfo()-> groupBrackets.open);

	vector<SyntaxConstituent*> subCons = parseAlternative(schema, begin, staticInfo()->groupBrackets.close);
	auto subConsConst = MemoryUtils::createNewConstVector<SyntaxConstituent>(subCons.begin(), subCons.end());

	return SyntaxConstituent::create<FieldFormat::Group>(subConsConst);
}

SyntaxConstituent* SyntaxRule::parseSet(string const& schema, size_t* begin)
{
	mustBe(schema, begin, staticInfo()->setBrackets.open);

	ValueType::Set set;
	SetContext context;

	context.curr = *begin;
	char currCh = schema[context.curr];
	bool isEscaped = false;

	// Loops while the current character is valid for a set,
	// or if a set bracket appears.
	while (context.curr < schema.length())
	{
		// Unescaped escape symbol. Escapes next character.
		if (currCh == '\\' && !isEscaped)
		{
			isEscaped = true;
		}
		// Unescaped open set bracket.
		else if (currCh == staticInfo()->setBrackets.open && !isEscaped)
		{
			throw parsingError(context.curr, "set open '[' before set close ']'");
		}
		// Unescaped close set bracket.
		else if (currCh == staticInfo()->setBrackets.close && !isEscaped)
		{
			// Empty set.
			if (!context.prevCh.has_value())
			{
				throw parsingError(context.curr, "empty set");
			}
			// Incomplete range. Missing upper value.
			else if (context.lowCh.has_value() && !context.highCh.has_value())
			{
				throw parsingError(context.curr, "range specifier '-' missing right operand");
			}
			// Set is complete.
			else
			{
				addEqualOrRangeToSet(&set, &context);
				*begin = context.curr;
				return SyntaxConstituent::create<FieldFormat::Set>(set);
			}
		}
		// Unescaped range symbol implies new range.
		else if (currCh == '-' && !isEscaped)
		{
			// Duplicate range symbol.
			if (context.lowCh.has_value() && !context.highCh.has_value())
			{
				throw parsingError(context.curr, "range specifier '-' repeated");
			}
			// No previous character or the previous character is already the end of a range.
			else if (!context.prevCh.has_value() || context.highCh.has_value())
			{
				throw parsingError(context.curr, "range specifier '-' missing left operand");
			}
			// Prepares the beginning of a range.
			else
			{
				context.lowCh = context.prevCh.value();
			}
		}
		// Plain text or escaped character.
		else
		{
			// The current character is the upper value of the current range.
			if (context.lowCh.has_value() && !context.highCh.has_value())
			{
				context.highCh = currCh;
			}
			// Commits the previous character or range.
			else
			{
				addEqualOrRangeToSet(&set, &context);
			}
			isEscaped = false;
		}

		// Commits the previous character if it's not the escape symbol.
		if (!isEscaped)
		{
			context.prevCh = currCh;
		}

		currCh = schema[++context.curr];
	}

	// Unexpected character or end of string encountered before set close.
	if (context.curr == schema.length())
	{
		throw parsingError(context.curr, "end encountered before set close ']'");
	}
	else
	{
		throw parsingError(context.curr, "unexpected character '" + string(1, currCh) + "' in set");
	}
}

void SyntaxRule::addEqualOrRangeToSet(SyntaxSet* set, SyntaxRule::SetContext* context)
{
	// The previous character is the end of a range. Add range.
	if (context->highCh.has_value())
	{
		addRangeToSet(set, context);
	}
	// The previous character is isolated. Commits an equal check.
	else if (context->prevCh.has_value())
	{
		set->equals.push_back(context->prevCh.value());
	}
}

void SyntaxRule::addRangeToSet(SyntaxSet* set, SyntaxRule::SetContext* context)
{
	// Checks the range is strictly ascending.
	if (context->highCh.value() <= context->lowCh.value())
	{
		throw parsingError(context->curr - 1, "range must be from a lower to higher character");
	}
	// Commits the range.
	else
	{
		set->ranges.push_back({context->lowCh.value(), context->highCh.value()});
		context->lowCh = nullopt;
		context->highCh = nullopt;
	}
}

vector<SyntaxConstituent*> SyntaxRule::parseAlternative(string const& schema, size_t* begin, char endCh)
{
	vector<vector<SyntaxConstituent*>> alternatives;

	vector<SyntaxConstituent*> alt = parseString(schema, begin, endCh);

	if (alt.empty())
	{
		throw parsingError(*begin, "alternative '|' missing left operand");
	}

	while (true)
	{
		alternatives.push_back(move(alt));

		if (schema[*begin] == endCh) break;

		mustBe(schema, begin, '|');
		alt = parseString(schema, begin, endCh);

		if (alt.empty())
		{
			throw parsingError(*begin, "alternative '|' missing right operand");
		}
	}

	if (alternatives.size() == 1)
	{
		return alternatives[0];
	}
	else
	{
		auto altsConst = new vector<ValueType::List>();
		for (auto const& alt : alternatives)
		{
			altsConst->push_back(MemoryUtils::createNewConstVector<SyntaxConstituent>(
				alt.begin(), alt.end()));
		}
		auto altsCon = SyntaxConstituent::create<ValueType::AltList>(staticInfo()->altOpr.constituentType, altsConst);
		return vector<SyntaxConstituent*>(1, altsCon);
	}
}

SyntaxConstituent* SyntaxRule::parsePostfixOperator(Operator const& opr, string const& schema, size_t* begin, SyntaxConstituent* left)
{
	mustBe(schema, begin, opr.opr, false);

	return SyntaxConstituent::create<ValueType::Constituent>(opr.constituentType, left);
}

invalid_argument SyntaxRule::parsingError(int errPos, string const& errMsg)
{
	string fullMsg = "Error in parsing syntax rule at character " +
		to_string(errPos) + ": " + errMsg + ".";
	return invalid_argument(fullMsg);
}

bool SyntaxRule::isRuleChar(char ch)
{
	return isalnum(ch) || ch == '_';
}

bool SyntaxRule::isTextChar(char ch)
{
	return !(iscntrl(ch) || isInstructionChar(ch));
}

bool SyntaxRule::isInstructionChar(char ch)
{
	return staticInfo()->instructionChars.find(ch) != string::npos;
}

shared_ptr<const SyntaxRule::StaticInfo> SyntaxRule::staticInfo()
{
	static auto _staticInfo = make_shared<const StaticInfo>();
	return _staticInfo;
}
