//
//  SyntaxSet.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "SyntaxSet.hpp"

using namespace std;

#include "StdComparison.hpp"

bool SyntaxSet::contains(char ch) const
{
	// Checks if the character is between one of the ranges.
	for (auto r : ranges)
	{
		if (ch >= r.first && ch <= r.second)
		{
			return true;
		}
	}
	// Checks if the character is equal to one of the equals values.
	for (auto e : equals)
	{
		if (ch == e)
		{
			return true;
		}
	}
	return false;
}

int compare(SyntaxSet const& first, SyntaxSet const& second)
{
	RETURN_IF_UNEQUAL_COMPARE(first.ranges, second.ranges);
	RETURN_IF_UNEQUAL_COMPARE(first.equals, second.equals);
	return 0;
}

COMPARISON_OPERATOR_METHODS_IMPL(SyntaxSet)

SyntaxSet::operator string() const
{
	string str;
	str += "[" + equals;
	for (auto r : ranges)
	{
		str += string(1, r.first) + "-" + string(1, r.second);
	}
	str += "]";
	return str;
}
