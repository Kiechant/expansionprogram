//
//  SyntaxParserError.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef SYNTAX_PARSER_ERROR_HPP
#define SYNTAX_PARSER_ERROR_HPP

#include "SyntaxParserDiagnostic.hpp"

#include <exception>
#include <memory>
#include <string>

class SyntaxParserError : public std::exception
{
public:
	SyntaxParserError(std::string const& query, std::shared_ptr<SyntaxParserDiagnosticTree> const& diagTree);
	const char* what() const throw();

protected:
	std::string message;
	std::string query;
	std::shared_ptr<SyntaxParserDiagnosticTree> diagnosticTree;

	static std::string makeMessage(std::string const& query, SyntaxParserDiagnosticTree const& diagTree);
};

#endif /* SYNTAX_PARSER_ERROR_HPP */
