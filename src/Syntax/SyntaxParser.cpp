//
//  SyntaxParser.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 4/6/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#include "SyntaxParser.hpp"

#include "StdStringConversion.hpp"
#include "SyntaxConstituent.tpp"
#include "SyntaxParserDiagnostic.hpp"
#include "SyntaxParserError.hpp"
#include "SyntaxRule.hpp"
#include "MiscUtils.hpp"
#include "TreeNode.tpp"
#include "Tree.tpp"

#include <algorithm>
#include <cctype>
#include <iostream>
#include <sstream>
#include <string>

using namespace std;

using ConFormat = SyntaxConstituentFormat;
using ConType = SyntaxConstituentValueType;

/* Constructors and Destructors */

SyntaxParser::SyntaxParser(string const& query, unordered_map<string, SyntaxRule const*> const& syntax, string const& startRule)
	: query(query), syntax(syntax), startRule(startRule) {}

SyntaxParser::~SyntaxParser() {}

/* Copy and Move Semantics */

void swap(SyntaxParser& first, SyntaxParser& second)
{
	using std::swap;
	swap(first.query, second.query);
	swap(first.syntax, second.syntax);
}

SyntaxParser::SyntaxParser(SyntaxParser const& other)
{
	query = other.query;
	syntax = other.syntax;
}

SyntaxParser::SyntaxParser(SyntaxParser&& other)
{
	swap(*this, other);
}

SyntaxParser& SyntaxParser::operator=(SyntaxParser other)
{
	swap(*this, other);
	return *this;
}

/* Parsing Methods */

Tree<string> SyntaxParser::parse()
{
	seek(0);
	optional<SyntaxParserDiagnosticNode> diagnosticRoot;
	TreeNode<string>* node = parseRule(startRule, &diagnosticRoot);

	// No parsing error was detected, but parsing completed before the end of
	// the query. Prepares a diagnostic for unexpected trailing characters.
	// 'node' may have been created if parsing was otherwise successful.
	if (!diagnosticRoot.has_value() && curr < query.length())
	{
		if (node != nullptr) deepDelete(node);
		auto diag = SyntaxParserDiagnostic(nullptr, curr, SyntaxParserDiagnostic::Format::TrailingChars, monostate());
		diagnosticRoot = SyntaxParserDiagnosticNode(diag, SyntaxParserDiagnostic::toKey);
	}
	
	// Throws parsing error if a diagnostic root is set.
	// This indicates an error. 'node' should be uncreated and set to nullptr.
	if (diagnosticRoot.has_value())
	{
		throw SyntaxParserError(query, make_shared<SyntaxParserDiagnosticTree>(new SyntaxParserDiagnosticNode(move(diagnosticRoot.value()))));
	}

	// An empty string succeeded in parsing due to a rule with all optional subconstituents.
	if (node == nullptr)
	{
		node = new TreeNode<string>("");
	}

	return Tree<string>(node);
}

Tree<string> SyntaxParser::parse(string const& query,
	unordered_map<string, SyntaxRule const*> const& syntax,
	string const& startRule)
{
	SyntaxParser parser(query, syntax, startRule);
	return parser.parse();
}

/* Indexing Methods */

void SyntaxParser::advance()
{
	currCh = query[++curr];
}

void SyntaxParser::seek(size_t index)
{
	curr = index;
	currCh = query[curr];
}

void SyntaxParser::mustBe(char equals, bool doAdvance)
{
	if (currCh != equals)
	{
		stringstream ss;
		ss << "Found " << charInfo() << ". Expected '" << equals << "'.";
		throw ss.str();
	}
	else if (doAdvance)
	{
		advance();
	}
}

/* Parsing Helper Methods */

bool SyntaxParser::commitTerminal(string const& terminal,
	vector<TreeNode<string>*>* nextNodes, TreeNode<string>* terminalNode)
{
	assert(!terminalNode || terminalNode->getItem() == terminal);
	
	if (!terminal.empty())
	{
		if (!nextNodes->empty() &&
			nextNodes->back()->isLeaf())
		{
			// Merges two terminal values into a single node.
			nextNodes->back()->modifyItem([&terminal](string* backValue)
			{
				backValue->insert(backValue->end(), terminal.begin(), terminal.end());
			});
			
			if (terminalNode)
			{
				delete terminalNode;
			}
		}
		else
		{
			TreeNode<string>* nextNode = terminalNode ?
				terminalNode : new TreeNode<string>(terminal);
			nextNodes->push_back(nextNode);
		}
		return true;
	}
	return false;
}

bool SyntaxParser::commitNonTerminal(vector<TreeNode<string>*> const& parsedNodes,
	vector<TreeNode<string>*>* nextNodes)
{
	if (!parsedNodes.empty())
	{
		if (parsedNodes.front()->isLeaf())
		{
			// Attempts merging two terminal siblings into a single node.
			string terminal = parsedNodes.front()->getItem();
			commitTerminal(terminal, nextNodes, parsedNodes.front());
			nextNodes->insert(nextNodes->end(), parsedNodes.begin() + 1,
				parsedNodes.end());
		}
		else
		{
			nextNodes->insert(nextNodes->end(), parsedNodes.begin(),
				parsedNodes.end());
		}
		return true;
	}
	return false;
}

/* Categorical Parsing Methods */

string SyntaxParser::parseString(string const& pattern, optional<SyntaxParserDiagnosticNode>* diagNode)
{
	string matched = "";
	size_t minLen = min(query.length(), pattern.length());
	size_t start = curr;
	
	while (curr - start < minLen)
	{
		// The query string doesn't match the pattern.
		if (currCh != pattern[curr - start]) break;

		advance();
	}
	
	if (curr - start == pattern.length())
	{
		// The query string matched the full pattern.
		matched = pattern;
	}
	else if (matched == "" && diagNode != nullptr)
	{
		// The query string doesn't match the pattern in full. Creates diagnostic.
		auto diagFormat = (curr < query.length()) ?
			SyntaxParserDiagnostic::Format::StringNoMatch : SyntaxParserDiagnostic::Format::StringEnd;
		*diagNode = SyntaxParserDiagnosticNode(SyntaxParserDiagnostic(nullptr, curr, diagFormat, pattern[curr - start]),
			SyntaxParserDiagnostic::toKey);
	}

	return matched;
}

TreeNode<string>* SyntaxParser::parseRule(string const& ruleName, optional<SyntaxParserDiagnosticNode>* diagNode)
{
	auto ruleEntry = syntax.find(ruleName);
	if (ruleEntry == syntax.end())
	{
		throw invalid_argument("No rule found of the name '" + ruleName + "'.");
	}

	SyntaxRule const* rule = ruleEntry->second;
	
	auto parsedNodes = parseGroup(rule->getConstituentsBegin(),
		rule->getConstituentsEnd(), diagNode);
	
	if (!parsedNodes.empty())
	{
		if (rule->isAnonymous() &&
			parsedNodes.size() == 1 &&
			parsedNodes.front()->isLeaf())
		{
			// An anonymous rule with a single terminal child
			// adopts the child's value.
			return parsedNodes.front();
		}
		else
		{
			// Adds parsed children underneath a new rule node.
			TreeNode<string>* nextNode = new TreeNode<string>(ruleName);
			nextNode->addChildren(parsedNodes.begin(), parsedNodes.end());
			return nextNode;
		}
	}
	else if (diagNode != nullptr && diagNode->has_value())
	{
		// Sets the rule of the passed diagnostic and any unassigned subconstituents if parsing failed.
		auto childDiagNodes = SyntaxParserDiagnosticTree::traverseAll(TreeTraversalMode::DepthFirst, &diagNode->value(),
			[](SyntaxParserDiagnosticNode const* childDiagNode){ return childDiagNode->getItem().rule != nullptr; });
		
		for (auto [childDiagNode, childDepth] : childDiagNodes)
		{
			childDiagNode->modifyItem([this, &ruleName](SyntaxParserDiagnostic* diag)
			{
				diag->rule = this->syntax.find(ruleName)->second;
			});
		}	
	}
	return nullptr;
}

vector<TreeNode<string>*> SyntaxParser::parseGroup(
	vector<SyntaxConstituent const*>::const_iterator beginCon,
	vector<SyntaxConstituent const*>::const_iterator endCon,
	optional<SyntaxParserDiagnosticNode>* diagNode)
{
	vector<TreeNode<string>*> nextNodes;
	
	for (auto currConIt = beginCon; currConIt != endCon; currConIt++)
	{
		SyntaxConstituent const* currCon = *currConIt;
		ConFormat format = currCon->getFormat();
		bool _didParse = false;
		
		// Temporary parsing variables.
		string terminal;
		TreeNode<string>* nextNode;
		ConType::List subCons;
		auto parsedNodes = vector<TreeNode<string>*>();
		
		// Parses chararcters depending on the type of the current constituent.
		switch (format)
		{
		case ConFormat::Null:
			throw "Uninitiated constituent discovered.";
			
		case ConFormat::String:
			terminal = parseString(currCon->getValue<ConFormat::String>(), diagNode);
			_didParse = commitTerminal(terminal, &nextNodes);
			break;
			
		case ConFormat::Rule:
			nextNode = parseRule(currCon->getValue<ConFormat::Rule>(), diagNode);
			if (nextNode)
			{
				parsedNodes.push_back(nextNode);
			}
			_didParse = commitNonTerminal(parsedNodes, &nextNodes);
			break;
			
		case ConFormat::Group:
			subCons = currCon->getValue<ConFormat::Group>();
			parsedNodes = parseGroup(subCons->begin(), subCons->end(), diagNode);
			_didParse = commitNonTerminal(parsedNodes, &nextNodes);
			break;
			
		case ConFormat::Set:
			terminal = parseSet(currCon->getValue<ConFormat::Set>(), diagNode);
			_didParse = commitTerminal(terminal, &nextNodes);
			break;
			
		case ConFormat::Alternative:
			parsedNodes = parseAlternative(currCon->getValue<ConFormat::Alternative>(), diagNode);
			_didParse = commitNonTerminal(parsedNodes, &nextNodes);
			break;
			
		case ConFormat::Optional:
			parsedNodes = parseOptional(currCon->getValue<ConFormat::Optional>(), nullptr);
			commitNonTerminal(parsedNodes, &nextNodes);
			_didParse = true;
			break;
			
		case ConFormat::List:
			parsedNodes = parseList(currCon->getValue<ConFormat::List>(), diagNode);
			_didParse = commitNonTerminal(parsedNodes, &nextNodes);
			break;
			
		case ConFormat::OptionalList:
			parsedNodes = parseOptionalList(currCon->getValue<ConFormat::OptionalList>(), nullptr);
			commitNonTerminal(parsedNodes, &nextNodes);
			_didParse = true;
			break;
		}
		
		if (!_didParse)
		{
			// Discards unused nodes when parsing fades and returns an empty list.
			MemoryUtils::deepDelete(nextNodes,
				[](TreeNode<string> const* node){ deepDelete(node); });
			return vector<TreeNode<string>*>();
		}
	}
	
	return nextNodes;
}

string SyntaxParser::parseSet(ConType::Set const& set, optional<SyntaxParserDiagnosticNode>* diagNode)
{
	// Returns the current character if it is within the set.
	string match = "";
	if (curr < query.length() && set.contains(currCh))
	{
		match.push_back(currCh);
		advance();
	}
	else if (diagNode != nullptr)
	{
		// Character did not match the set. Creates diagnostic.
		auto diagFormat = (curr < query.length()) ?
			SyntaxParserDiagnostic::Format::SetNoMatch : SyntaxParserDiagnostic::Format::SetEnd;
		*diagNode = SyntaxParserDiagnosticNode(SyntaxParserDiagnostic(nullptr, curr, diagFormat, &set),
			SyntaxParserDiagnostic::toKey);
	}
	
	return match;
}

vector<TreeNode<string>*> SyntaxParser::parseAlternative(ConType::AltList const& altList,
	optional<SyntaxParserDiagnosticNode>* diagNode)
{
	vector<TreeNode<string>*> nodeList;
	vector<SyntaxParserDiagnosticNode> childDiagNodes;
	optional<SyntaxParserDiagnosticNode> childDiagNode;

	// Attempts parsing alternatives before the last as optional.
	for (size_t currAlt = 0; currAlt < altList->size() - 1; currAlt++)
	{
		nodeList = parseOptional(*altList->at(currAlt), &childDiagNode);

		if (nodeList.empty())
		{
			// Parsing the optional failed. Adds a temporary diagnostic node.
			if (diagNode != nullptr && childDiagNode.has_value())
			{
				childDiagNodes.push_back(childDiagNode.value());
				childDiagNode = nullopt;
			}
		}
		else break;
	}
	
	size_t prev = curr;
	if (nodeList.empty())
	{
		// No match for previous alternatives.
		// Attempts parsing last alternative as a requirement.
		auto alt = altList->back();
		nodeList = parseGroup(alt->begin(), alt->end(), &childDiagNode);
	}

	if (diagNode != nullptr)
	{
		if (nodeList.empty())
		{
			// Parsing the required alternative failed. Builds the returned diagnostic subtree.
			if (childDiagNode.has_value())
			{
				childDiagNodes.push_back(childDiagNode.value());
			}

			*diagNode = SyntaxParserDiagnosticNode(
				SyntaxParserDiagnostic(nullptr, prev, SyntaxParserDiagnostic::Format::Alternative, query[prev]),
				SyntaxParserDiagnostic::toKey);
			for (auto const& child : childDiagNodes)
			{
				diagNode->value().addChild(new SyntaxParserDiagnosticNode(move(child)));
			}
		}
		else
		{
			// Parsing the required alternative succeeded. Recursively deletes redundant diagnostics.
			for (auto& child : childDiagNodes)
			{
				child.deepDeleteChildren();
			}
		}
		
	}
	
	return nodeList;
}

vector<TreeNode<string>*> SyntaxParser::parseOptional(
	ConType::Constituent const& constituent, optional<SyntaxParserDiagnosticNode>* diagNode)
{
	return parseOptional(vector<SyntaxConstituent const*>{constituent}, diagNode);
}

vector<TreeNode<string>*> SyntaxParser::parseOptional(
	vector<SyntaxConstituent const*> const& constituents, optional<SyntaxParserDiagnosticNode>* diagNode)
{
	size_t prev = curr;
	auto nodeList = parseGroup(constituents.begin(), constituents.end(), diagNode);
	
	if (nodeList.empty())
	{
		seek(prev);
	}
	
	return nodeList;
}

vector<TreeNode<string>*> SyntaxParser::parseList(
	ConType::Constituent const& constituent, optional<SyntaxParserDiagnosticNode>* diagNode)
{
	return parseList(vector<SyntaxConstituent const*>{constituent}, diagNode);
}

vector<TreeNode<string>*> SyntaxParser::parseList(
	vector<SyntaxConstituent const*> const& constituents, optional<SyntaxParserDiagnosticNode>* diagNode)
{
	auto nodeList = parseGroup(constituents.begin(), constituents.end(), diagNode);
	
	if (!nodeList.empty())
	{
		auto subList = parseOptionalList(constituents, nullptr);
		commitNonTerminal(subList, &nodeList);
	}
	
	return nodeList;
}

vector<TreeNode<string>*> SyntaxParser::parseOptionalList(
	ConType::Constituent const& constituent, optional<SyntaxParserDiagnosticNode>* diagNode)
{
	return parseOptionalList(vector<SyntaxConstituent const*>{constituent}, diagNode);
}

vector<TreeNode<string>*> SyntaxParser::parseOptionalList(
	vector<SyntaxConstituent const*> const& constituents, optional<SyntaxParserDiagnosticNode>* diagNode)
{
	vector<TreeNode<string>*> nodeList;
	vector<TreeNode<string>*> subList;
	
	do
	{
		subList = parseOptional(constituents, diagNode);
		commitNonTerminal(subList, &nodeList);
	}
	while (!subList.empty());
	
	return nodeList;
}

/* Error Handling */

string SyntaxParser::charInfo()
{
	stringstream ss;
	ss << "character '" << currCh << "' at position " << curr;
	return ss.str();
}

invalid_argument SyntaxParser::parsingError(size_t errPos, string errRule)
{
	return invalid_argument("Error parsing query string at character " +
		to_string(errPos) + ". Expected character to match rule '" + errRule + "'.");
}
