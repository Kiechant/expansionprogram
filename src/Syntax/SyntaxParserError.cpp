//
//  SyntaxParserError.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "SyntaxParserError.hpp"

using namespace std;

#include "MiscUtils.hpp"
#include "Tree.tpp"

SyntaxParserError::SyntaxParserError(string const& query, shared_ptr<SyntaxParserDiagnosticTree> const& diagTree)
	: message(makeMessage(query, *diagTree)), query(query), diagnosticTree(diagTree) {}

string SyntaxParserError::makeMessage(string const& query, SyntaxParserDiagnosticTree const& diagTree)
{
	string msg = "Failed parsing query string with provided syntax and start rule:\n";

	auto diagDepthNodes =  SyntaxParserDiagnosticTree::traverseAll(
		TreeTraversalMode::DepthFirst, diagTree.getRoot());
	for (auto [node, depth] : diagDepthNodes)
	{
		msg += string(depth + 1, '\t') + node->getItem().makeMessage(query) + "\n";
	}

	return MiscUtils::printableString(msg);
}

const char* SyntaxParserError::what() const throw()
{
	return message.c_str();
}

