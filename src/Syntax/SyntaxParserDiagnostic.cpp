//
//  SyntaxParserDiagnostic.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#include "SyntaxParserDiagnostic.hpp"

#include "StdComparison.hpp"
#include "SyntaxRule.hpp"
#include "SyntaxSet.hpp"

#include <sstream>

using namespace std;

SyntaxParserDiagnostic::SyntaxParserDiagnostic(SyntaxRule const* rule, size_t queryPos, SyntaxParserDiagnostic::Format format,
	variant<monostate, char, SyntaxSet const*> expected)
	: rule(rule), queryPos(queryPos), format(format), expected(expected) {}

int compare(SyntaxParserDiagnostic const& first, SyntaxParserDiagnostic const& second)
{
	RETURN_IF_UNEQUAL_COMPARE(first.rule, second.rule);
	RETURN_IF_UNEQUAL_COMPARE(first.queryPos, second.queryPos);
	RETURN_IF_UNEQUAL_COMPARE((int)first.format, (int)second.format);
	RETURN_IF_UNEQUAL_COMPARE(first.expected, second.expected);
	return 0;
}

COMPARISON_OPERATOR_METHODS_IMPL(SyntaxParserDiagnostic)

SyntaxParserDiagnostic::operator string() const
{
	stringstream ss;
	ss << "{" << rule << ", " << queryPos << ", " << to_string(int(format)) << "}";
	return ss.str();
}

size_t SyntaxParserDiagnostic::toKey(SyntaxParserDiagnostic const& diag)
{
	return diag.queryPos;
}

string SyntaxParserDiagnostic::makeMessage(string const& query) const
{
	string msg;
	if (rule != nullptr)
	{
		msg = "'" + rule->getName() + "': ";
	}
	if (format == Format::TrailingChars)
	{
		msg += "Unexpected trailing characters at query[" + to_string(queryPos) + "].";
	}
	else if (format == Format::Alternative)
	{
		msg += "failed with alternatives:";
	}
	else if (format == Format::StringNoMatch || format == Format::SetNoMatch)
	{
		msg += "query[" + to_string(queryPos) + "] = '" + string(1, query[queryPos]) + "', ";
		msg += expectedMessage() + ".";
	}
	else if (format == Format::StringEnd || format == Format::SetEnd)
	{
		msg += "end of query string reached, ";
		msg += expectedMessage() + ".";
	}
	return msg;
}

string SyntaxParserDiagnostic::expectedMessage() const
{
	if (format == Format::StringNoMatch || format == Format::StringEnd)
	{
		return "expected '" + string(1, get<char>(expected)) + "'";
	}
	else if (format == Format::SetNoMatch || format == Format::SetEnd)
	{
		return "expected to match set " +
			string(*get<SyntaxSet const*>(expected));
	}
	return "";
}
