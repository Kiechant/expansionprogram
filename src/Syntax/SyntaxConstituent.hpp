//
//  SyntaxConstituent.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 2/7/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#ifndef SYNTAX_CONSTITUENT_HPP
#define SYNTAX_CONSTITUENT_HPP

#include "SyntaxConstituentDecl.hpp"

#include "ComparisonOperatorMethodsDecl.hpp"
#include "SyntaxSet.hpp"

#include <utility>
#include <variant>
#include <vector>

// Describes the restrictions on this particular part of the string.
enum class SyntaxConstituentFormat
{
	// Undesignated. This is the default initialisation.
	Null,
	
	// Terminal sequence of plain text characters.
	// Characters must match the sequence as written.
	String,
	
	// Enclosure with chevrons '<>'.
	// Specifies the name of a syntax rule, which characters must match.
	Rule,
	
	// Enclosure with parentheses '()'.
	// Groups a series of sub-constituents.
	// Operations are applied collectively onto the group.
	Group,
	
	// Enclosure with square brackets '[]'.
	// Specifies an unordered set of individual characters and character ranges.
	// A character must match one of these these individuals or ranges.
	Set,
	
	// Infix operator '|'. Behaves like an 'or' modifier.
	// A character sequence may conform to either
	// the left or right series of subconstituents.
	Alternative,
	
	// Postfix operator '?'. Use 0 or 1
	// of the previous character or enclosure.
	Optional,
	
	// Postfix operator '+'. Use 1 or more
	// of the previous character or enclosure.
	List,
	
	// Postfix operator '*'. Use 0 or more
	// of the previous character or enclosure.
	OptionalList
};

// Describes how strings can be arranged as a
// component of the full syntactical structure.
class SyntaxConstituent
{
public:
	// Constructors and destructors.
	SyntaxConstituent();
	template<SyntaxConstituentFormat F>
		static SyntaxConstituent* create(SyntaxConstituentValueTypeOfT<F> const& value);
	template<typename V>
		static SyntaxConstituent* create(SyntaxConstituentFormat format, AnySyntaxConstituentValueTypeT<V> const& value);
	~SyntaxConstituent();
	
	// Copy and move semantics.
	friend void swap(SyntaxConstituent&, SyntaxConstituent&);
	SyntaxConstituent(SyntaxConstituent const&);
	SyntaxConstituent(SyntaxConstituent&&);
	SyntaxConstituent& operator=(SyntaxConstituent);

	// Comparison.
	friend int compare(SyntaxConstituent const&, SyntaxConstituent const&);
	COMPARISON_OPERATOR_METHODS_DECL(SyntaxConstituent)

	// String conversion.
	operator std::string() const;
	static std::string nameOfFormat(SyntaxConstituentFormat format);
	
	// Basic getters and setters.
	SyntaxConstituentFormat getFormat() const;

	// TODO: Move into a more logical location.
	static bool isEnclosure(SyntaxConstituentFormat);
	static bool isPostfixOperator(SyntaxConstituentFormat);
	
	// SyntaxConstituentValueVariant and format querying.
	template<typename V, SyntaxConstituentFormat F>
		static bool isValueTypeOfFormat();
	template<typename V>
		static bool isValueTypeOfFormat(SyntaxConstituentFormat format);
	
	// SyntaxConstituentValueVariant access.
	template<SyntaxConstituentFormat F>
		SyntaxConstituentValueTypeOfT<F> const& getValue() const;
	template<typename V>
		AnySyntaxConstituentValueTypeT<V> const& getValue() const;
	template<SyntaxConstituentFormat F>
		std::add_pointer_t<SyntaxConstituentValueTypeOfT<F>> getMutableValue();
	template<typename V>
		std::add_pointer_t<AnySyntaxConstituentValueTypeT<V>> getMutableValue();
	
	// SyntaxConstituentValueVariant and format modification.
	template<SyntaxConstituentFormat F>
		void setValue(SyntaxConstituentValueTypeOfT<F> const& value);
	template<typename V>
		void setValue(AnySyntaxConstituentValueTypeT<V> const& value);
	template<SyntaxConstituentFormat F>
		void setValueAndFormat(SyntaxConstituentValueTypeOfT<F> const& value);
	template<typename V>
		void setValueAndFormat(SyntaxConstituentFormat format, AnySyntaxConstituentValueTypeT<V> const& value);
	
private:
	SyntaxConstituentFormat format;
	SyntaxConstituentValueVariant value;
	
	static bool isEnclosureType(SyntaxConstituentFormat);
};

#endif /* SYNTAX_CONSTITUENT_HPP */
