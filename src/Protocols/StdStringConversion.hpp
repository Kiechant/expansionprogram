//
//  StdStringConversion.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 9/2/20.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#ifndef STD_STRING_CONVERSION_HPP
#define STD_STRING_CONVERSION_HPP

#include "StringConversion.hpp"

#include <filesystem>
#include <sstream>

/* Provides custom string conversion functions for built-in and standard library types. */


/** Constraints **/

namespace StdStringConversion
{
	template<typename Ptr>
	constexpr bool IsIndirectStringConvertibleV = IsStringConvertibleV<std::remove_pointer_t<Ptr>>;

	template<typename Ptr>
	using StringIfPtrAndIndirectStringConvertibleT = std::enable_if_t<
		std::is_pointer_v<Ptr> && IsIndirectStringConvertibleV<Ptr>, std::string>;

	template<typename Ptr>
	using StringIfPtrAndNotIndirectStringConvertibleT = std::enable_if_t<
		std::is_pointer_v<Ptr> && !IsIndirectStringConvertibleV<Ptr>, std::string>;

	template<typename Char>
	using StringIfCharT = std::enable_if_t<TypeUtils::IsCharV<Char>, std::string>;

	template<typename Integer>
	using StringIfIntegerT = std::enable_if_t<TypeUtils::IsIntegerV<Integer>, std::string>;

	template<typename Floating>
	using StringIfFloatingT = std::enable_if_t<std::is_floating_point_v<Floating>, std::string>;
};


/** Declarations **/

/* A string naturally returns itself. */
std::string toString(std::string const&);
ADD_TO_STRING((std::string))

std::string toString(std::filesystem::path const& path);
ADD_TO_STRING((std::filesystem::path))

/* Stringifies the address of any pointer type to its hexadecimal representation. */
template<typename Ptr>
StdStringConversion::StringIfPtrAndIndirectStringConvertibleT<Ptr> toString(Ptr const&);
ADD_TO_STRING_TMPL((template<typename Ptr>),
	(Ptr), (std::enable_if_t<!IsInternalStringConvertibleV<Ptr> && std::is_pointer_v<Ptr> && StdStringConversion::IsIndirectStringConvertibleV<Ptr>>))

template<typename Ptr>
StdStringConversion::StringIfPtrAndNotIndirectStringConvertibleT<Ptr> toString(Ptr const&);
ADD_TO_STRING_TMPL((template<typename Ptr>),
	(Ptr), (std::enable_if_t<!IsInternalStringConvertibleV<Ptr> && std::is_pointer_v<Ptr> && !StdStringConversion::IsIndirectStringConvertibleV<Ptr>>))

template<typename Char>
StdStringConversion::StringIfCharT<Char> toString(Char const&);
ADD_TO_STRING_TMPL((template<typename Char>),
	(Char), (std::enable_if_t<!IsInternalStringConvertibleV<Char> && TypeUtils::IsCharV<Char>>))

template<typename Integer>
StdStringConversion::StringIfIntegerT<Integer> toString(Integer const&);
ADD_TO_STRING_TMPL((template<typename Integer>),
	(Integer), (std::enable_if_t<!IsInternalStringConvertibleV<Integer> && TypeUtils::IsIntegerV<Integer>>))

template<typename Floating>
StdStringConversion::StringIfFloatingT<Floating> toString(Floating const&);
ADD_TO_STRING_TMPL((template<typename Floating>),
	(Floating), (std::enable_if_t<!IsInternalStringConvertibleV<Floating> && std::is_floating_point_v<Floating>>))

template<typename FirstItem, typename SecondItem>
std::string toString(std::pair<FirstItem, SecondItem> const& pairObj);
template<typename FirstItem, typename SecondItem>
std::string toStringBy(std::pair<FirstItem, SecondItem> const& pairObj,
	ToStringFuncT<FirstItem> const& firstToStringFunc,
	ToStringFuncT<SecondItem> const& secondToStringFunc);
ADD_TO_STRING_TMPL((template<typename FirstItem, typename SecondItem>),
	(std::pair<FirstItem, SecondItem>),
	(std::enable_if_t<!IsInternalStringConvertibleV<std::pair<FirstItem, SecondItem>>>))

template<typename Item>
std::string toString(std::vector<Item> const& list);
template<typename Item>
std::string toStringBy(std::vector<Item> const& list,
	ToStringFuncT<Item> const& toStringFunc);
ADD_TO_STRING_TMPL((template<typename Item>),
	(std::vector<Item>),
	(std::enable_if_t<!IsInternalStringConvertibleV<std::vector<Item>>>))


/** Implementations **/

template<typename Ptr>
StdStringConversion::StringIfPtrAndIndirectStringConvertibleT<Ptr> toString(Ptr const& ptr)
{
	std::stringstream ss;
	ss << ptr << " -> " << ToString<std::remove_pointer_t<Ptr>>()(*ptr);
	return ss.str();
}

template<typename Ptr>
StdStringConversion::StringIfPtrAndNotIndirectStringConvertibleT<Ptr> toString(Ptr const& ptr)
{
	std::stringstream ss;
	ss << ptr;
	return ss.str();
}

template<typename Char>
StdStringConversion::StringIfCharT<Char> toString(Char const& ch)
{
	return std::string(1, ch);
}

template<typename Integer>
StdStringConversion::StringIfIntegerT<Integer> toString(Integer const& num)
{
	return std::to_string(num);
}

template<typename Floating>
StdStringConversion::StringIfFloatingT<Floating> toString(Floating const& num)
{
	std::string str = std::to_string(num);

	if (str.find('.') != std::string::npos)
	{
		// Removes trailing zeroes if a decimal value is present.
		str = str.substr(0, str.find_last_not_of('0') + 1);

		// Removes the decimal point if only zeroes were proceeding it.
		if (str.back() == '.')
		{
			str.pop_back();
		}
	}

	return str;
}

template<typename FirstItem, typename SecondItem>
std::string toString(std::pair<FirstItem, SecondItem> const& pairObj)
{
	return toStringBy(pairObj, ToString<FirstItem>(), ToString<SecondItem>());
}

template<typename FirstItem, typename SecondItem>
std::string toStringBy(std::pair<FirstItem, SecondItem> const& pairObj,
	ToStringFuncT<FirstItem> const& firstToStringFunc,
	ToStringFuncT<SecondItem> const& secondToStringFunc)
{
	std::stringstream ss;
	ss << "{" << firstToStringFunc(pairObj.first) << ", " <<
		secondToStringFunc(pairObj.second) << "}";
	return ss.str();
}

template<typename Item>
std::string toString(std::vector<Item> const& list)
{
	return toStringBy(list, ToString<Item>());
}

template<typename Item>
std::string toStringBy(std::vector<Item> const& list,
	ToStringFuncT<Item> const& toStringFunc)
{
	std::stringstream ss;
	ss << "{";
	auto const end = list.end();
	for (auto it = list.begin(); it != end; it++)
	{
		ss << toStringFunc(*it);
		if (it < end - 1)
			ss << ", ";
	}
	ss << "}";
	return ss.str();
}

#endif /* STD_STRING_CONVERSION_HPP */
