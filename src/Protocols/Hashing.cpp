//
//  Hashing.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 8/2/20.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#include "Hashing.hpp"

// Using hash constants and algorithm, see:
// https://stackoverflow.com/questions/17016175/c-unordered-map-using-a-custom-class-type-as-the-key
// https://stackoverflow.com/questions/1646807/quick-and-simple-hash-code-combinations/1646913#1646913

size_t initHash()
{
	return 17;
}

size_t hashMultiplier()
{
	return 31;
}

size_t combineHash(size_t first, size_t second)
{
	return first * hashMultiplier() + second;
}
