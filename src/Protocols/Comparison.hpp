//
//  Comparison.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 12/1/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#ifndef COMPARISON_HPP
#define COMPARISON_HPP

#include "ComparisonDecl.hpp"

#include "TypeUtils.hpp"

#include <functional>

/* Provides methods for implementing a custom compare method and comparison operators. */


/** Helper Types **/

/* True if the type is comparable and has implementations for all comparison operators. */
template<typename T>
constexpr bool IsInternalComparableV<T, TypeUtils::VoidT<std::enable_if_t<
	std::is_same_v<decltype(compare(std::declval<T const&>(), std::declval<T const&>())), int>>>> = true;

/* A functor which compares the type 'First' with the type 'Second'.
   Overloads for single type compared against itself. */
template<typename First, typename Second>
struct Compare<First, Second, std::enable_if_t<
IsInternalComparableV<First> && IsInternalComparableV<Second>>>
{
	int operator()(First const& first, Second const& second) const
	{ return compare(first, second); }
};


#define __ADD_COMPARE_REFLECTIVE_BODY(Type, EnableIf) \
	struct Compare<UNPACK_ARGUMENTS Type, UNPACK_ARGUMENTS Type, UNPACK_ARGUMENTS EnableIf> \
	{ \
		int operator()(UNPACK_ARGUMENTS Type const& first, UNPACK_ARGUMENTS Type const& second)  const\
		{ return compare(first, second); } \
	};

/* Adds an overload for the 'Compare' functor for a type compared against itself.
   for an overloaded 'compare()' function. */
#define ADD_COMPARE_REFLECTIVE(Type, EnableIf) \
	template<> __ADD_COMPARE_REFLECTIVE_BODY(Type, EnableIf)

/* Variant of 'ADD_COMPARE_REFLECTIVE' for a template.
   Expects 'Template' to be enclosed in one bracket. */
#define ADD_COMPARE_REFLECTIVE_TMPL(Template, Type, EnableIf) \
	UNPACK_ARGUMENTS Template __ADD_COMPARE_REFLECTIVE_BODY(Type, EnableIf)


/** Constraints **/

/* Creates helper templates for 'IsComparableV'. 'HasOperatorName<T>' evaluates to
   true if the type has an implementation of the comparison operator 'Operator'.
   Operator overload may be a member function or static function. */
#define __HAS_OPERATOR_QUERY_TMPL(HasOperatorName, Operator) \
	template<typename T, typename = TypeUtils::VoidT<>> \
	constexpr bool HasOperatorName = false; \
	template<typename T> \
	constexpr bool HasOperatorName<T, TypeUtils::VoidT<std::enable_if_t<std::is_same_v< \
		decltype(std::declval<T>() Operator std::declval<T const&>()), bool>>>> = true;

/* Creates helper templates of 'HasOperatorName' for each operator. */
__HAS_OPERATOR_QUERY_TMPL(HasOperatorEqualV, ==)
__HAS_OPERATOR_QUERY_TMPL(HasOperatorUnequalV, !=)
__HAS_OPERATOR_QUERY_TMPL(HasOperatorLessThanV, <)
__HAS_OPERATOR_QUERY_TMPL(HasOperatorGreaterThanV, >)
__HAS_OPERATOR_QUERY_TMPL(HasOperatorLessThanOrEqualV, <=)
__HAS_OPERATOR_QUERY_TMPL(HasOperatorGreaterThanOrEqualV, >=)

/* True if the type is comparable and has implementations for all comparison operators. */
template<typename T>
constexpr bool IsComparableV<T, TypeUtils::VoidT<std::enable_if_t<
	std::is_same_v<decltype(Compare<T>()(std::declval<T const&>(), std::declval<T const&>())), int> &&
	HasOperatorEqualV<T> &&
	HasOperatorUnequalV<T> &&
	HasOperatorLessThanV<T> &&
	HasOperatorGreaterThanV<T> &&
	HasOperatorLessThanOrEqualV<T> &&
	HasOperatorGreaterThanOrEqualV<T>>>> = true;

/* The function type of a compare function for types 'First' and 'Second'.
   Overloads for single type compared against itself. */
template<typename First, typename Second>
struct CompareFunc<First, Second, std::enable_if_t<
IsComparableV<First> && IsComparableV<Second>>>
{
	using type = std::function<int(First const&, Second const&)>;
};
template<typename First, typename Second = First>
using CompareFuncT = typename CompareFunc<First, Second>::type;


/** Helper Methods for Writing Compare Method **/

/* Performs a function and returns the result if unequal.
   Useful for custom comparison functions in a 'compare' method. */
#define RETURN_IF_UNEQUAL(First, Second, _Compare) \
	if (int comp = _Compare(First, Second); comp != 0) return comp; \
	FORCE_SEMICOLON()

/* Compares the first and second variables with the 'compare' function.
   Returns the comparison if they are not equal i.e. not 0. */
#define RETURN_IF_UNEQUAL_COMPARE(First, Second) \
	RETURN_IF_UNEQUAL(First, Second, compare)

/* Returns the outcome of a comparison from the result
   of an equal and less-than comparison. */
template<typename T>
int comparePiecewise(T const& first, T const& second,
	std::function<bool(T const&, T const&)> equalsComp = std::equal_to<T>(),
	std::function<bool(T const&, T const&)> lessThanComp = std::less<T>())
{
	if (equalsComp(first, second))
		return 0;
	else if (lessThanComp(first, second))
		return -1;
	return 1;
}

/* Compares the null status of two pointers.
   If one pointer is null and the other isn't, the null pointer is considered
   to come after the non-null pointer. */
template<typename T>
std::enable_if_t<std::is_pointer_v<T>, int>
compareNullStatus(T first, T second)
{
	if (first != nullptr && second != nullptr)
		return 0;
	else if (first != nullptr)
		return -1;
	else if (second != nullptr)
		return 1;
	return 0;
}

/* Returns the comparison between null checks for two pointers,
   followed by the values given a comparison function. */
template<typename T>
std::enable_if_t<std::is_pointer_v<T>, int>
compareNullable(T const& first, T const& second,
	std::function<int(TypeUtils::MutableIndirectOfT<T> const&, TypeUtils::MutableIndirectOfT<T> const&)> const& compareFunc)
{
	RETURN_IF_UNEQUAL(first, second, compareNullStatus);
	if (first != nullptr && second != nullptr)
		RETURN_IF_UNEQUAL(*first, *second, compareFunc);
	return 0;
}

/* Returns the comparison between null checks for two pointers,
   followed by the values given the object's default comparison function. */
template<typename T>
std::enable_if_t<std::is_pointer_v<T>, int>
compareNullable(T const& first, T const& second)
{
	using TValueConstRef = typename std::remove_const_t<std::remove_pointer_t<T>> const&;
	return compareNullable(first, second, Compare<TValueConstRef>());
}

template<typename Ptr, typename Key>
class CompareNullableWithKey
{
public:
	static_assert(std::is_pointer_v<Ptr>, "Cannot use CompareNullableWithKey with a non-pointer type, as it is not nullable.");

	using Value = typename std::remove_const_t<std::remove_pointer_t<Ptr>>;
	using KeyValue = typename std::remove_const_t<std::remove_reference_t<Key>>;

	std::function<Key(Value)> keyExtractor;

	CompareNullableWithKey(std::function<Key(Value)> const& keyExtractor)
		: keyExtractor(keyExtractor) {}

	int operator()(Ptr first, Ptr second) const
	{
		return compareNullable(first, second,
		[this](Value const& firstValue, Value const& secondValue)
		{ return Compare<KeyValue>()(this->keyExtractor(firstValue), this->keyExtractor(secondValue)); });
	}
};


/** Operator Methods Defined by Compare Method **/

/* Generator for operator implementation.
   Assumes 'Template' and 'Class' are enclosed in parentheses from wrapper functions. */
#define __COMPARISON_OPERATOR_METHOD_IMPL(Template, Class, Operator) \
	UNPACK_ARGUMENTS Template bool UNPACK_ARGUMENTS Class:: \
		operator Operator (UNPACK_ARGUMENTS Class const& other) const \
		{ int comp = compare(*this, other); return comp Operator 0; }

/* Comparison operator implementations.
   Assumes 'Template' and 'Class' are enclosed in parentheses from wrapper functions. */
#define OPERATOR_EQUAL_IMPL(Template, Class) \
	__COMPARISON_OPERATOR_METHOD_IMPL(Template, Class, ==)
#define OPERATOR_UNEQUAL_IMPL(Template, Class) \
	__COMPARISON_OPERATOR_METHOD_IMPL(Template, Class, !=)
#define OPERATOR_LESS_THAN_IMPL(Template, Class) \
	__COMPARISON_OPERATOR_METHOD_IMPL(Template, Class, <)
#define OPERATOR_GREATER_THAN_IMPL(Template, Class) \
	__COMPARISON_OPERATOR_METHOD_IMPL(Template, Class, >)
#define OPERATOR_LESS_THAN_OR_EQUAL_IMPL(Template, Class) \
	__COMPARISON_OPERATOR_METHOD_IMPL(Template, Class, <=)
#define OPERATOR_GREATER_THAN_OR_EQUAL_IMPL(Template, Class) \
	__COMPARISON_OPERATOR_METHOD_IMPL(Template, Class, >=)

/* Provides implementation for comparison operator instance methods
   based on a pre-existing 'compare' implementation.
   Assumes 'Class' is passed unenclosed in parentheses. */
#define COMPARISON_OPERATOR_METHODS_IMPL(Class) \
	OPERATOR_EQUAL_IMPL((), (Class)) \
	OPERATOR_UNEQUAL_IMPL((), (Class)) \
	OPERATOR_LESS_THAN_IMPL((), (Class)) \
	OPERATOR_GREATER_THAN_IMPL((), (Class)) \
	OPERATOR_LESS_THAN_OR_EQUAL_IMPL((), (Class)) \
	OPERATOR_GREATER_THAN_OR_EQUAL_IMPL((), (Class))

/* Variant of 'COMPARISON_OPERATOR_METHODS_IMPL' for templated typenamees.
   Assumes 'Template' and 'Class' arguments are passed unenclosed in parentheses. */
#define COMPARISON_OPERATOR_METHODS_TMPL_IMPL(Template, Class) \
	OPERATOR_EQUAL_IMPL((Template), (Class)) \
	OPERATOR_UNEQUAL_IMPL((Template), (Class)) \
	OPERATOR_LESS_THAN_IMPL((Template), (Class)) \
	OPERATOR_GREATER_THAN_IMPL((Template), (Class)) \
	OPERATOR_LESS_THAN_OR_EQUAL_IMPL((Template), (Class)) \
	OPERATOR_GREATER_THAN_OR_EQUAL_IMPL((Template), (Class))

#endif /* COMPARISON_HPP */
