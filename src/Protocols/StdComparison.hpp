//
//  StdComparison.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 12/1/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#ifndef STD_COMPARISON_HPP
#define STD_COMPARISON_HPP

#include "Comparison.hpp"

#include <optional>
#include <utility>
#include <variant>
#include <vector>

/* Provides custom comparison functions for built-in and standard library types.
   Container types have an additional compareBy function which accepts a function
   to compare its individual members. */


/** Constraints **/

namespace StdComparison
{
	template<typename Ptr>
	using IntIfPtrT = std::enable_if_t<std::is_pointer_v<Ptr>, int>;

	template<typename Arithmetic>
	using IntIfArithmeticT = std::enable_if_t<std::is_arithmetic_v<Arithmetic>, int>;
};


/** Declarations **/

/* Compares any pointer type by its address and not value. */
template<typename Ptr>
StdComparison::IntIfPtrT<Ptr> compare(Ptr first, Ptr second);
ADD_COMPARE_REFLECTIVE_TMPL((template<typename Ptr>),
	(Ptr), (std::enable_if_t<!IsInternalComparableV<Ptr> && std::is_pointer_v<Ptr>>))

/* Uses built-in equal and less-than operations for arithmetic types. */
template<typename Arithmetic>
StdComparison::IntIfArithmeticT<Arithmetic> compare(
	Arithmetic const& first, Arithmetic const& second);
ADD_COMPARE_REFLECTIVE_TMPL((template<typename Arithmetic>),
	(Arithmetic), (std::enable_if_t<!IsInternalComparableV<Arithmetic> && std::is_arithmetic_v<Arithmetic>>))

int compare(std::string const& first, std::string const& second);
ADD_COMPARE_REFLECTIVE((std::string), (std::enable_if_t<!IsInternalComparableV<std::string>>))

template<typename FirstItem, typename SecondItem>
int compare(std::pair<FirstItem, SecondItem> const& firstPair,
	std::pair<FirstItem, SecondItem> const& secondPair);
template<typename FirstItem, typename SecondItem>
int compareBy(std::pair<FirstItem, SecondItem> const& firstPair,
	std::pair<FirstItem, SecondItem> const& secondPair,
	CompareFuncT<FirstItem> const& compareFirst,
	CompareFuncT<SecondItem> const& compareSecond);
ADD_COMPARE_REFLECTIVE_TMPL((template<typename FirstItem, typename SecondItem>),
	(std::pair<FirstItem, SecondItem>),
	(std::enable_if_t<!IsInternalComparableV<std::pair<FirstItem, SecondItem>>>))

template<typename Item>
int compare(std::vector<Item> const& firstList,
	std::vector<Item> const& secondList);
template<typename Item>
int compareBy(std::vector<Item> const& firstList,
	std::vector<Item> const& secondList,
	CompareFuncT<Item> const& compareFunc);
ADD_COMPARE_REFLECTIVE_TMPL((template<typename Item>),
	(std::vector<Item>), (std::enable_if_t<!IsInternalComparableV<std::vector<Item>>>))

template<typename... VariantTypes>
int compare(std::variant<VariantTypes...> const& first,
	std::variant<VariantTypes...> const& second);
ADD_COMPARE_REFLECTIVE_TMPL((template<typename... VariantTypes>),
	(std::variant<VariantTypes...>), (std::enable_if_t<!IsInternalComparableV<std::variant<VariantTypes...>>>))

template<typename Item>
int compare(std::optional<Item> const& first,
	std::optional<Item> const& second);
ADD_COMPARE_REFLECTIVE_TMPL((template<typename Item>),
	(std::optional<Item>), (std::enable_if_t<!IsInternalComparableV<std::optional<Item>>>))


/** Implementations **/

template<typename Ptr>
StdComparison::IntIfPtrT<Ptr> compare(Ptr first, Ptr second)
{
	RETURN_IF_UNEQUAL_COMPARE(reinterpret_cast<uintptr_t>(first), reinterpret_cast<uintptr_t>(second));
	return 0;
}

template<typename Arithmetic>
StdComparison::IntIfArithmeticT<Arithmetic> compare(
	Arithmetic const& first, Arithmetic const& second)
{
	return comparePiecewise(first, second);
}

template<typename FirstItem, typename SecondItem>
int compare(std::pair<FirstItem, SecondItem> const& firstPair,
	std::pair<FirstItem, SecondItem> const& secondPair)
{
	return compareBy(firstPair, secondPair, Compare<FirstItem>(), Compare<SecondItem>());
}

template<typename FirstItem, typename SecondItem>
int compareBy(std::pair<FirstItem, SecondItem> const& firstPair,
	std::pair<FirstItem, SecondItem> const& secondPair,
	CompareFuncT<FirstItem> const& compareFirst,
	CompareFuncT<SecondItem> const& compareSecond)
{
	RETURN_IF_UNEQUAL(firstPair.first, secondPair.first, compareFirst);
	RETURN_IF_UNEQUAL(firstPair.second, secondPair.second, compareSecond);
	return 0;
}

template<typename Item>
int compare(std::vector<Item> const& firstList,
	std::vector<Item> const& secondList)
{
	return compareBy(firstList, secondList, Compare<Item, Item>());
}

template<typename Item>
int compareBy(std::vector<Item> const& firstList,
	std::vector<Item> const& secondList,
	CompareFuncT<Item> const& compareFunc)
{
	size_t minSize = (firstList.size() < secondList.size()) ?
		firstList.size() : secondList.size();
	for (size_t i = 0; i < minSize; i++)
	{
		RETURN_IF_UNEQUAL(firstList[i], secondList[i], compareFunc);
	}
	RETURN_IF_UNEQUAL(firstList.back(), secondList.back(), compareFunc);
	return 0;
}

template<typename... VariantTypes>
int compare(std::variant<VariantTypes...> const& first,
	std::variant<VariantTypes...> const& second)
{
	return comparePiecewise(first, second);   
}

template<typename Item>
int compare(std::optional<Item> const& first,
	std::optional<Item> const& second)
{
	RETURN_IF_UNEQUAL_COMPARE(first.has_value(), second.has_value());
	if (first.has_value()) // && second.has_value()
	{
		RETURN_IF_UNEQUAL_COMPARE(first.value(), second.value());
	}
	return 0;
}

#endif /* STD_COMPARISON_HPP */
