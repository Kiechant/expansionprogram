//
//  StringConversion.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 9/2/20.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#ifndef STRING_CONVERSION_HPP
#define STRING_CONVERSION_HPP

#include "StringConversionDecl.hpp"

#include "TypeUtils.hpp"

#include <functional>

/* Provides methods and structs for implementating a custom string conversion function. */


/** Constraints **/

/* True if the type has an internal string conversion implementation. */
template<typename T>
constexpr bool IsInternalStringConvertibleV<T, TypeUtils::VoidT<std::enable_if_t<std::is_same_v<
    decltype(toString(std::declval<T const&>())), std::string>>>> = true;

/* A functor wrapping the string conversion function for type 'T'. */
template<typename T>
struct ToString<T, std::enable_if_t<IsInternalStringConvertibleV<T>>>
{
    std::string operator()(T const& obj) const
    { return toString(obj); }
};

#define __ADD_TO_STRING_BODY(Type, EnableIf) \
    struct ToString<UNPACK_ARGUMENTS Type, UNPACK_ARGUMENTS EnableIf> \
    { \
        std::string operator()(UNPACK_ARGUMENTS Type const& obj) const \
        { return toString(obj); } \
    };

/* Adds an overload for 'ToString' for a the type with an overload for 'toString()'. */
#define ADD_TO_STRING(Type) \
    template<> __ADD_TO_STRING_BODY(Type, (void))

/* Variant of 'ADD_TO_STRING' for a templated type.
   Expects 'Template' to be enclosed in single brackets. */
#define ADD_TO_STRING_TMPL(Template, Type, EnableIf) \
    UNPACK_ARGUMENTS Template __ADD_TO_STRING_BODY(Type, EnableIf)

/* True if the type has a wrapped string conversion implementation. */
template<typename T>
constexpr bool IsStringConvertibleV<T, TypeUtils::VoidT<std::enable_if_t<std::is_same_v<
    decltype(ToString<T>()(std::declval<T const&>())), std::string>>>> = true;


/** Helper Types **/

/* The string conversion function type for type 'T'. */
template<typename T>
struct ToStringFunc<T, std::enable_if_t<IsStringConvertibleV<T>>>
{
    using type = std::function<std::string(T const&)>;
};
template<typename T>
using ToStringFuncT = typename ToStringFunc<T>::type;

#endif /* STRING_CONVERSION_HPP */
