//
//  ComparisonOperatorMethodsDecl.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef COMPARISON_OPERATOR_METHODS_DECL_HPP
#define COMPARISON_OPERATOR_METHODS_DECL_HPP

/* Provides declarations for comparison operator instance methods
   based on a pre-existing 'compare' implementation. */
#define COMPARISON_OPERATOR_METHODS_DECL(Class) \
	bool operator==(Class const&) const; \
	bool operator!=(Class const&) const; \
	bool operator< (Class const&) const; \
	bool operator> (Class const&) const; \
	bool operator<=(Class const&) const; \
	bool operator>=(Class const&) const;

#endif /* COMPARISON_OPERATOR_METHODS_DECL_HPP */
