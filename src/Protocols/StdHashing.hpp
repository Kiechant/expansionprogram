//
//  StdHashing.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 8/2/20.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#ifndef STD_HASHING_HPP
#define STD_HASHING_HPP

#include "Hashing.hpp"
#include "TupleUtils.hpp"

#include <optional>

/* Wraps standard-library hash implementations in the toHash() method. */

namespace StdHashing
{
	/* True if the type has a standard-library hash implementation. */
	template<typename T, typename = TypeUtils::VoidT<>>
	constexpr bool IsStdHashableV = false;
	template<typename T>
	constexpr bool IsStdHashableV<T, TypeUtils::VoidT<std::enable_if_t<
		std::is_same_v<decltype(std::hash<T>()(std::declval<T const&>())), size_t>>>> = true;
};

template<typename T>
std::enable_if_t<StdHashing::IsStdHashableV<T> || std::is_pointer_v<T>,
	size_t> toHash(T const& hashable)
{
	return std::hash<T>()(hashable);
}
ADD_TO_HASH_TMPL(
	(template<typename T>),
	(T),
	(std::enable_if_t<!IsInternalHashableV<T> && (StdHashing::IsStdHashableV<T> || std::is_pointer_v<T>)>))

template<typename T>
size_t toHash(std::optional<T> const& opt);
ADD_TO_HASH_TMPL(
	(template<typename T>),
	(std::optional<T>),
	(std::enable_if_t<!IsInternalHashableV<std::optional<T>>>))

template<typename T, typename U>
size_t toHash(std::pair<T, U> const& _pair);
ADD_TO_HASH_TMPL(
	(template<typename T, typename U>),
	(std::pair<T, U>),
	(std::enable_if_t<!IsInternalHashableV<std::pair<T, U>>>))

template<typename... TupleTypes>
size_t toHash(std::tuple<TupleTypes...> const& _tuple);
ADD_TO_HASH_TMPL(
	(template<typename... TupleTypes>),
	(std::tuple<TupleTypes...>),
	(std::enable_if_t<!IsInternalHashableV<std::tuple<TupleTypes...>>>))

template<typename T>
size_t toHash(std::optional<T> const& opt)
{
	size_t res = initHash();
	res = combineHash(res, toHash(opt.has_value()));
	if (opt.has_value())
	{
		res = combineHash(res, toHash(opt.value()));
	}
	return res;
}

template<typename T, typename U>
size_t toHash(std::pair<T, U> const& _pair)
{
	size_t res = initHash();
	res = combineHash(res, toHash(_pair.first));
	res = combineHash(res, toHash(_pair.second));
	return res;
}

template<typename... TupleTypes>
size_t toHash(std::tuple<TupleTypes...> const& _tuple)
{
	return TupleUtils::reduce(_tuple, initHash(),
		[](auto&& element, size_t res)
		{
			return combineHash(res, toHash(element));
		});
}

#endif /* HASHING_HPP */
