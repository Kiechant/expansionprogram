//
//  StringConversionDecl.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef STRING_CONVERSION_DECL_HPP
#define STRING_CONVERSION_DECL_HPP

#include "TypeUtils.hpp"

/* True if the type has an internal string conversion implementation. */
template<typename T, typename = TypeUtils::VoidT<>>
constexpr bool IsInternalStringConvertibleV = false;

/* A functor wrapping the string conversion function for type 'T'. */
template<typename T, typename = void>
struct ToString;

/* True if the type has a wrapped string conversion implementation. */
template<typename T, typename = TypeUtils::VoidT<>>
constexpr bool IsStringConvertibleV = false;

/* The string conversion function type for type 'T'. */
template<typename T, typename = void>
struct ToStringFunc;

#endif /* STRING_CONVERSION_DECL_HPP */
