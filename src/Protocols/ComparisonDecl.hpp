//
//  ComparisonDecl.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef COMPARISON_DECL_HPP
#define COMPARISON_DECL_HPP

#include "TypeUtils.hpp"

/* True if the type is comparable and has implementations for all comparison operators. */
template<typename T, typename = TypeUtils::VoidT<>>
constexpr bool IsInternalComparableV = false;

/* A functor which compares the type 'First' with the type 'Second'.
   Overloads for single type compared against itself. */
template<typename First, typename Second = First, typename = void>
struct Compare;

/* True if the type is comparable and has implementations for all comparison operators. */
template<typename T, typename = TypeUtils::VoidT<>>
constexpr bool IsComparableV = false;

/* The function type of a compare function for types 'First' and 'Second'.
   Overloads for single type compared against itself. */
template<typename First, typename Second = First, typename = void>
struct CompareFunc;

#endif /* COMPARISON_DECL_HPP */
