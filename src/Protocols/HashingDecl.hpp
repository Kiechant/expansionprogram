//
//  HashingDecl.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 20/2/2021.
//  Copyright © 2020 Kieren Chantrell. All rights reserved.
//

#ifndef HASHING_DECL_HPP
#define HASHING_DECL_HPP

#include "TypeUtils.hpp"

/* True if the type has a user-defined hash implementation. */
template<typename T, typename = TypeUtils::VoidT<>>
constexpr bool IsInternalHashableV = false;

/* A functor wrapping the user hash function for type 'T'. */
template<typename T, typename = void>
struct ToHash;

/* True if the type has a user-defined hash implementation or wrapper around the
   standard-library implementation defined in 'StdHashable.hpp'. */
template<typename T, typename = TypeUtils::VoidT<>>
constexpr bool IsHashableV = false;

/* The hash function type for type 'T'. */
template<typename T, typename = void>
struct ToHashFunc;

#endif /* HASHING_DECL_HPP */
