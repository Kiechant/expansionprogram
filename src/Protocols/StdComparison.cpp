//
//  StdComparison.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 12/1/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#include "StdComparison.hpp"

using namespace std;

int compare(string const& first, string const& second)
{
    return first.compare(second);
}
