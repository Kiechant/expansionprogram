//
//  StdStringConversion.cpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 9/2/20.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#include "StdStringConversion.hpp"

using namespace std;

string toString(string const& str)
{
    return str;
}

string toString(filesystem::path const& path)
{
	return path.string();
}
