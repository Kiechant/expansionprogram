//
//  Hashing.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 8/2/20.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#ifndef HASHING_HPP
#define HASHING_HPP

#include "HashingDecl.hpp"

#include "TypeUtils.hpp"

#include <functional>

/* Provides methods and structs for implementating a custom hash function. */


/** Helper Types **/

/* True if the type has a user-defined hash implementation. */
template<typename T>
constexpr bool IsInternalHashableV<T, TypeUtils::VoidT<std::enable_if_t<std::is_same_v<
	decltype(toHash(std::declval<T const&>())), size_t>>>> = true;

/* A functor wrapping the user hash function for type 'T'. */
template<typename T>
struct ToHash<T, std::enable_if_t<IsInternalHashableV<T>>>
{
	size_t operator()(T const& obj) const
	{ return toHash(obj); }
};

#define __ADD_TO_HASH_BODY(Type, EnableIf) \
	struct ToHash<UNPACK_ARGUMENTS Type, UNPACK_ARGUMENTS EnableIf> \
	{ \
		size_t operator()(UNPACK_ARGUMENTS Type const& obj) const \
		{ return toHash(obj); } \
	};

/* Adds an overload for 'ToHash' for a the type with an overload for 'toHash()'. */
#define ADD_TO_HASH(Type) \
	template<> __ADD_TO_HASH_BODY(Type, (void))

/* Variant of 'ADD_TO_HASH' for a templated type.
   Expects 'Template' to be enclosed in single brackets. */
#define ADD_TO_HASH_TMPL(Template, Type, EnableIf) \
	UNPACK_ARGUMENTS Template __ADD_TO_HASH_BODY(Type, EnableIf)


/** Constraints **/

/* True if the type has a user-defined hash implementation or wrapper around the
   standard-library implementation defined in 'StdHashable.hpp'. */
template<typename T>
constexpr bool IsHashableV<T, TypeUtils::VoidT<std::enable_if_t<std::is_same_v<
	decltype(ToHash<T>()(std::declval<T const&>())), size_t>>>> = true;

/* The hash function type for type 'T'. */
template<typename T>
struct ToHashFunc<T, std::enable_if_t<IsHashableV<T>>>
{
	using type = std::function<size_t(T const&)>;
};
template<typename T>
using ToHashFuncT = typename ToHashFunc<T>::type;


/** Hash Manipulation Methods **/

size_t initHash();
size_t hashMultiplier();
size_t combineHash(size_t first, size_t second);

#endif /* HASHING_HPP */
