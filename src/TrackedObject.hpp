//
//  TrackedObject.hpp
//  ExpansionProgram
//
//  Created by Kieren Chantrell on 3/7/19.
//  Copyright © 2019 Kieren Chantrell. All rights reserved.
//

#ifndef TRACKED_OBJECT_HPP
#define TRACKED_OBJECT_HPP

#include <iostream>

template<class T>
class TrackedObject
{
public:
	TrackedObject();
	~TrackedObject();
	
	static size_t getObjectCount();

private:
	static size_t objectCount;
};

template<class T>
size_t TrackedObject<T>::objectCount = 0;

template<class T>
TrackedObject<T>::TrackedObject()
{
	objectCount += 1;
}

template<class T>
TrackedObject<T>::~TrackedObject()
{
	objectCount -= 1;
}

template<class T>
size_t TrackedObject<T>::getObjectCount()
{
	return objectCount;
}

#endif /* TRACKED_OBJECT_HPP */
