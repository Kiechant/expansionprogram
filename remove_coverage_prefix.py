import argparse
import shutil
import os

from bs4 import BeautifulSoup

parser = argparse.ArgumentParser()
parser.add_argument("coverage_dir_name")
args = parser.parse_args()

project_dir = os.path.dirname(os.path.realpath(__file__))
base_coverage_dir = os.path.join(project_dir, "build", args.coverage_dir_name)
coverage_dir = os.path.join(base_coverage_dir, "coverage")
temp_coverage_dir = os.path.join(base_coverage_dir, "coverage-temp")
nested_coverage_dir = os.path.join(coverage_dir, os.path.relpath(project_dir, "/"))

assert not os.path.samefile(nested_coverage_dir, coverage_dir), "Nested coverage directory and existing coverage directory are the same."
assert not os.path.exists(temp_coverage_dir), "Temporary coverage directory already exists."
assert os.path.exists(nested_coverage_dir), "Nested coverage directory does not exist."

# Copies the html files from the nested coverage directory to the existing coverage
# directory via a temporary file.
os.mkdir(temp_coverage_dir)
for file in os.listdir(nested_coverage_dir):
	shutil.move(os.path.join(nested_coverage_dir, file), os.path.join(temp_coverage_dir, file))
os.removedirs(nested_coverage_dir)
assert not os.path.exists(coverage_dir), "Contents remaining in coverage directory."
os.rename(temp_coverage_dir, coverage_dir)

# Changes style of each html document to reflect absolute location.
# llvm-cov relies on the relative location, which this program changes.
for dir_path, dir_names, file_names in os.walk(coverage_dir):
	for file_name in file_names:
		file_path = os.path.join(dir_path, file_name)
		if os.path.splitext(file_path)[1] == ".html":
			soup = None
			with open(file_path, "r") as f:
				soup = BeautifulSoup(f, "html.parser")

				# Sets the style location if the stylesheet link exists in the expected component.
				assert soup.head.link["rel"][0] == "stylesheet", "The first link is not a stylesheet."
				assert soup.head.link["type"] == "text/css", "The stylesheet is not a css type."
				soup.head.link["href"] = os.path.join(base_coverage_dir, "style.css")
			if soup:
				with open(file_path, "w") as f:
					f.write(str(soup))
